import { Component } from "@angular/core";
import {
  NavController,
  ModalController,
  App,
  AlertController
} from "ionic-angular";

/* Servicios */
import { CreateAccountServiceProvider } from "../../../providers/create-account/create-account";
import { ConfigPluginsProvider } from "../../../providers/config-plugins/config-plugins";
import { ModalTermsConditionsPage } from "../modal-terms-conditions/modal-terms-conditions";
import { ServicesProvider } from "../../../providers/services/services";

import { WelcomePage } from "../../index.paginas";

/* Mensajes */
import { MESSAGES } from "../../../config/messages";

import { SERVICES } from "../../../config/url.servicios";

@Component({
  selector: "page-terms-conditions",
  templateUrl: "terms-conditions.html"
})
export class TermsConditionsPage {
  acceptTerms: boolean;
  errors: string[] = [];
  timeOut: any;
  timeOutControl: boolean = true;

  constructor(
    public navCtrl: NavController,
    public appCtrl: App,
    public createAccountService: CreateAccountServiceProvider,
    public ServicesProvider: ServicesProvider,
    public modalCtrl: ModalController,
    public configPluginsProvider: ConfigPluginsProvider,
    private alertCtrl: AlertController
  ) {}

  ionViewDidLoad() {
    this.configPluginsProvider
      .get("register4")
      .then(result => {
        if (result.email) {
          this.createAccountService.register4
            .get("email")
            .setValue(result.email);
        }

        if (result.user) {
          this.createAccountService.register4.get("user").setValue(result.user);
        }
      })
      .catch(err => {
        console.log("Error", err);
      });
  }


  clearTimeOut() {
    clearTimeout(this.timeOut);
  }

  validateStepBack() {
    this.navCtrl.parent.select(2);
  }

  termsConditions() {
    let modal = this.modalCtrl.create(ModalTermsConditionsPage);
    modal.present();

    modal.onDidDismiss(parametros => {
      if (parametros) {
        this.acceptTerms = true;
      } else {
        this.acceptTerms = false;
      }
    });
  }

  saveData() {
    //this.navCtrl.parent.select(0);
    //setTimeout(() => {
    let alert = this.alertCtrl.create({
      title: "Atención",
      message:
        "¿Está seguro que la <b>información</b> suministrada es <b>correcta</b>?",
      buttons: [
        {
          text: "Cancelar",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        },
        {
          text: "Aceptar",
          handler: () => {
            this.confirmSave();
          }
        }
      ]
    });

    alert.present();
    //}, 600);
  }

  confirmSave() {
    this.navCtrl.parent.select(0);
    let alert = this.alertCtrl.create({
      title: "Atención",
      message:
        "La siguiente información solo podrá ser " +
        "modificada durante las proximas <b>48 horas</b>, " +
        "luego deberá contactar al administrador: <br><br>" +
        "- Nombres<br>" +
        "- Apellidos<br>" +
        "- Sexo<br>" +
        "- Documento de ID<br>" +
        "- Lugar y fecha de nacimiento",
      buttons: [
        {
          text: "Cancelar",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        },
        {
          text: "Aceptar",
          handler: () => {
            this.serviceSaveData();
          }
        }
      ]
    });

    alert.present();
  }

  serviceSaveData() {
    let params = {
      nombres: this.createAccountService.register.value.name,
      apellido1: this.createAccountService.register.value.surname,
      apellido2:
        this.createAccountService.register.value.second_surname != null
          ? this.createAccountService.register.value.second_surname
          : "",
      genero: this.createAccountService.register.value.gender,
      token_pago: this.createAccountService.register.value.token

      /*,
      pais_nac: this.createAccountService.register.value.country,
      estado_nac: this.createAccountService.register.value.department,
      ciudad_nac: this.createAccountService.register.value.municipality.id_pk*/
    };


    //discapacidad
    console.log("discapacida", this.createAccountService.register.value.check_discapacidad, " ",(this.createAccountService.register.get("check_discapacidad")))

    if(this.createAccountService.register.value.check_discapacidad){
      params["tipo_discapacidad"]=this.createAccountService.register.value.discapacidad;
      params["descrip_discapacidad"]=this.createAccountService.register.value.discapacidad_desc;
    }


    console.log("reside", this.createAccountService.register.value.reside_actualmente, " ",(this.createAccountService.register.get("reside_actualmente")))
    if(this.createAccountService.register.value.reside_actualmente){
      params["pais_nac"]= this.createAccountService.register.value.country;
      params["estado_nac"]= this.createAccountService.register.value.department;
      params["ciudad_nac"]= this.createAccountService.register.value.municipality.id_pk;
    }
    else{
      params["pais_nac"]= null;
      params["estado_nac"]= null;
      params["ciudad_nac"]= null;
    }

    if (this.createAccountService.register.value.vereda) {
      params["vereda_nac"] = this.createAccountService.register.value.vereda;
    }
    params[
      "tipo_documento"
    ] = this.createAccountService.register.value.type_document;
    params["documento"] = this.createAccountService.register.value.document;
    let arrayDate = this.createAccountService.register.value.birthdate.split("-");
    let fechaNac = arrayDate[2] + "/" + arrayDate[1] +"/" + arrayDate[0];
    params["fecha_nacimiento"] = fechaNac;
    

//modificacion
    params["pais"] = this.createAccountService.register.value.country;
    params["estado"] = this.createAccountService.register.value.department;
    params[
      "ciudad"
    ] = this.createAccountService.register.value.municipality.id_pk;



    if (this.createAccountService.register3.value.vereda) {
      params["vereda"] = this.createAccountService.register3.value.vereda;
    }
    params["telefono"] =
      this.createAccountService.register3.value.phone != null
        ? this.createAccountService.register3.value.phone
        : "";
    params["celular"] = this.createAccountService.register3.value.cellphone;
    params["email"] = this.createAccountService.register4.value.email;
    params["usuario"] = this.createAccountService.register4.value.user;
    params["contrasena"] = this.createAccountService.register4.value.password;
    params[
      "confirmacion_contrasena"
    ] = this.createAccountService.register4.value.confirmPassword;
    if (this.createAccountService.idDispositivo) {
      console.log("idDispositivo... ", this.createAccountService.idDispositivo);
      params["id_dispositivo"] = this.createAccountService.idDispositivo;
    }
    let verifyAge = this.createAccountService.verifyAge();
    if (!verifyAge || this.createAccountService.register.controls['check_discapacidad'].value) {
      params[
        "parentesco"
      ] = this.createAccountService.register2.value.parentesco.id_pk;
      params[
        "doc_responsable"
      ] = this.createAccountService.register2.value.document;
      params[
        "nombres_responsable"
      ] = this.createAccountService.register2.value.name;
      params[
        "apellido1_responsable"
      ] = this.createAccountService.register2.value.surname;
      params["apellido2_responsable"] =
        this.createAccountService.register2.value.second_surname != null
          ? this.createAccountService.register2.value.second_surname
          : "";
      params[
        "direccion_responsable"
      ] = this.createAccountService.register2.value.address;
      params[
        "pais_res_responsable"
      ] = this.createAccountService.register2.value.country;
      params[
        "estado_res_responsable"
      ] = this.createAccountService.register2.value.department;
      params[
        "ciudad_res_responsable"
      ] = this.createAccountService.register2.value.municipality.id_pk;
      params["telefono_responsable"] =
        this.createAccountService.register2.value.phone != null
          ? this.createAccountService.register3.value.phone
          : "";
      params[
        "celular_responsable"
      ] = this.createAccountService.register2.value.cellphone;
      params[
        "email_responsable"
      ] = this.createAccountService.register2.value.email;

        params[
          "pac_fk_responsable"
        ] = this.createAccountService.register2.value.pack_fk_responsable; 

    }

    this.ServicesProvider.createLoader();

    console.log("PARAMETROS REGISTRO\n"+JSON.stringify(params));

    this.ServicesProvider.apiPost(params, SERVICES.REGISTRO_API_SERV)
      .then(status => {
        //his.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();

        this.clearTimeOut();
        this.errors = [];
        if (status["status"] == 404 || status["status"] == 500) {
          console.log("XXX:\n" + JSON.stringify(status));
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (status["status"] == 401) {
          this.ServicesProvider.toast(MESSAGES.services.unauthorized);
        } else {
          let respuesta = JSON.parse(status["_body"]);
          if (respuesta.success) {
            localStorage.setItem("token",respuesta.token);
            this.acceptTerms = false;
            this.appCtrl.getRootNav().setRoot(WelcomePage);
          } else {
            console.log("----------------errores de la respuesta del servicio:", respuesta.errors);
            this.ServicesProvider.toast(
               respuesta.errors[Object.keys(respuesta.errors)[0]]
            );
            /*for (var name in respuesta.errors) {
              let value = respuesta.errors[name];
              this.errors.push(value);
            }
            this.ServicesProvider.toast(this.errors.toString());
            this.errors = respuesta.errors;*/
          }
        }
      })
      .catch(err => {
          this.ServicesProvider.closeLoader();
          this.errors = [];
          this.clearTimeOut();
          this.ServicesProvider.toast(MESSAGES.services.error + err);
      });
  }
}
