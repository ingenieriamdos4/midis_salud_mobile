import { Component } from "@angular/core";

import { IndexCreateAccountPage, PageHelpPage } from "../../index.paginas";
import { NavController, ViewController, AlertController } from 'ionic-angular';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';

import { ServicesProvider } from "../../../providers/services/services";
import { SERVICES, URL } from "../../../config/url.servicios";
import { MESSAGES } from "../../../config/messages";

@Component({
  selector: "page-validate_token",
  templateUrl: "validate_token.html"
})
export class ValidateTokenPage {
  registerPage = IndexCreateAccountPage;
  pageHelp = PageHelpPage;

  getToken: boolean = false;
  valid: boolean = false;
  invalid: boolean = false;
  tokenText: string = "";

  constructor(
    public navCtrl: NavController,
    private alertCtrl: AlertController,
    private iab: InAppBrowser,
    private ServicesProvider: ServicesProvider
  ) {
    //if( localStorage.getItem("wizzard_token")==undefined){
    //localStorage.setItem("wizzard_token","true");
    this.navCtrl.push(this.pageHelp, { 'seccion': 'token' });
    //}
  }

  ionViewWillEnter() {
    console.log("Entro a vista de validar token");
  }

  enableValidateToken() {
    this.getToken = !this.getToken;
  }

  helpCode() {
    let alert = this.alertCtrl.create({
      title: "Midis App Salud",
      message:
        "Para registrarte necesitas un código de registro.<br><br>Si ya realizaste el pago correspondiente, enviamos un código único a tu correo y a tu celular. Ingresalo aquí para registrarte.<br><br>Si no has pagado, no tardes, en la opción <b>'Obtener un código de registro'</b> te direccionaremos correctamente para que hagas tu compra.",
      buttons: [
        {
          text: "Aceptar",
          role: "confirm"
        }
      ]
    });
    alert.present();
  }

  onInputTime(token: any) {
    if (token.length == 10) {
      this.tokenText = token;
      let params = { "token": token };
      this.ServicesProvider.createLoader();
      this.ServicesProvider.apiPost(params, SERVICES.VALIDATE_TOKEN)
        .then(response => {
          this.ServicesProvider.closeLoader();
          if (response["status"] == 404 || response["status"] == 500) {
            this.ServicesProvider.toast(MESSAGES.services.error);
          }
          else if (response["status"] == 401) {
            this.ServicesProvider.toast(MESSAGES.services.unauthorized);
          }
          else {
            let validation = JSON.parse(response["_body"]);
            if (validation == 1) {
              this.valid = true;
              this.invalid = false;
            }
            else if (validation == 3) {
              this.valid = false;
              this.invalid = false;
              this.ServicesProvider.toast("El token ingresado ya fue utilizado para realizar un registro");
            }
            else if (validation == 2) {
              this.invalid = true;
            }
          }
        });
    }
    else if (token == "mono") {
      this.valid = true;
      this.invalid = false;
    }
    else {
      this.valid = false;
      this.invalid = false;
    }
  }

  register() {
    this.navCtrl.push(this.registerPage, { 'token': this.tokenText });
  }

  pay() {
    let options: InAppBrowserOptions = {
      hardwareback: 'yes'
    };
    this.iab.create(URL + "/pagosusuario", "_blank", options);
  }

}
