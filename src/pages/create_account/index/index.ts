import { Component } from "@angular/core";

/* Servicios */
import { CreateAccountServiceProvider } from "../../../providers/create-account/create-account";

import {
  PersonalInformationPage,
  ResponsibleDataPage,
  ContactDetailsPage,
  TermsConditionsPage,
  PageHelpPage,
  LoginPage
} from "../../index.paginas";
import { NavController, NavParams, ModalController, Modal } from 'ionic-angular';

@Component({
  selector: "page-index",
  templateUrl: "index.html"
})
export class IndexPage {
  tab1: any;
  tab2: any;
  tab3: any;
  tab4: any;

  pageHelp = PageHelpPage;
  homePage = LoginPage;

  constructor(public createAccountService: CreateAccountServiceProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController
  ) {
    this.tab1 = PersonalInformationPage;
    this.tab2 = ResponsibleDataPage;
    this.tab3 = ContactDetailsPage;
    this.tab4 = TermsConditionsPage;
    this.navCtrl.push(this.pageHelp, { 'seccion': 'crear_cuenta' });

  }

  ionViewWillEnter() {
    /* Se comenta para que la ayuda salga siempre.
    if (localStorage.getItem("wizzard_crear_cuenta") == undefined) {
      localStorage.setItem("wizzard_crear_cuenta", "true");
      this.navCtrl.push(this.pageHelp, { 'seccion': 'crear_cuenta' });
    }
    */
    let token = this.navParams.get('token');
    this.createAccountService.register.controls['token'].setValue(token);
  }


}
