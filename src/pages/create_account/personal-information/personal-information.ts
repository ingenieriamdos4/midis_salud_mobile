import { Component, AfterViewInit } from "@angular/core";
import { NavController } from "ionic-angular";
import { FormControl } from "@angular/forms";
/* Servicios */
import { CreateAccountServiceProvider } from "../../../providers/create-account/create-account";
import { ServicesProvider } from "../../../providers/services/services";
import { ConfigPluginsProvider } from "../../../providers/config-plugins/config-plugins";
import { Person } from "../../../models/person";

/* Mensajes */
import { MESSAGES } from "../../../config/messages";
import "rxjs/add/operator/debounceTime";
import { SERVICES } from "../../../config/url.servicios";
import { Validators } from "@angular/forms";

@Component({
  selector: "page-personal-information",
  templateUrl: "personal-information.html"
})
export class PersonalInformationPage implements AfterViewInit {
  typesDocument: Array<string> = [];
  states: Array<string> = [];
  habilitar_tab_contacto:boolean=false;
  cities: Array<any> = [];
  searchTermCity: string = "";
  searchControl: FormControl;
  searching: any = false;
  citySelected: number;

  veredas: Array<string> = [];
  person: Person;
  timeOut: any;
  timeOutControl: boolean = true;
  maxDate: string;

  hidenList: boolean = false;

  constructor(
    public navCtrl: NavController,
    public createAccountService: CreateAccountServiceProvider,
    public ServicesProvider: ServicesProvider,
    public configPluginsProvider: ConfigPluginsProvider
  ) {
    this.searchControl = new FormControl();

    this.maxDate = new Date()
      .toJSON()
      .slice(0, 10)
      .replace(/-/g, "-");
    this.person = new Person();



    //this.testApi();
  }

  ionViewDidLoad(){
    this.createAccountService.register.controls['reside_actualmente'].setValue(false);
    this.createAccountService.register.controls['check_discapacidad'].setValue(false);
    this.createAccountService.register.controls['gender'].setValue('M');


  }
  /**
   * @version 1.0
   * @author John Freddy Arias Valencia
   * Inicializar los valores que vienen del localstorage/register
   * Que se guardo en el login.ts
   * Si no viene lleno la varible register, consultamos normal los paises
   * this.createAccountService.getCountrysService();
   * @param  {}
   * @return {}
  */
  ngAfterViewInit() {

    console.log("init")

    this.configPluginsProvider
      .get("register")
      .then(result => {
        if (result.name) {
          this.createAccountService.register.get("name").setValue(result.name);
        }
        if (result.surname) {
          this.createAccountService.register
            .get("surname")
            .setValue(result.surname);
        }
        if (result.second_surname) {
          this.createAccountService.register
            .get("second_surname")
            .setValue(result.second_surname);
        }
        if (result.gender) {
          this.createAccountService.register
            .get("gender")
            .setValue(result.gender);
        }
        if (result.country) {
          //this.person.country = result.country;
          this.createAccountService.register
            .get("country")
            .setValue(result.country);
        } else {
          if (!this.timeOutControl) {
            //this.createAccountService.register.reset();
            this.createAccountService.register.get("country").setValue(null);
            this.createAccountService.register.get("department").setValue(null);
            this.createAccountService.register
              .get("municipality")
              .setValue(null);
            this.createAccountService.register.get("vereda").setValue(null);
            this.createAccountService.register
              .get("type_document")
              .setValue(null);
          } else {

            this.createAccountService.getCountrysService();
            this.createAccountService.register.get("country").setValue(null);
          }
        }

        if (result.department) {
          //this.person.department = result.department;
          this.createAccountService.register
            .get("department")
            .setValue(result.department);
        }

        if (result.municipality) {
          this.searchTermCity = result.municipality.nombre;
          this.citySelected = result.municipality.id_pk;
          //this.createAccountService.register.get('municipality').setValue(result.municipality);
        }

        if (result.vereda) {
          this.createAccountService.register
            .get("vereda")
            .setValue(result.vereda);
        }
        this.loadPromisesIfHaveStorage(result);
        if (result.birthdate) {
          this.createAccountService.register
            .get("birthdate")
            .setValue(result.birthdate);
        }

        if (result.type_document) {
          this.createAccountService.register
            .get("type_document")
            .setValue(result.type_document);
        }
        if (result.document) {
          setTimeout(() => {
            this.createAccountService.register
              .get("document")
              .setValue(result.document);
          }, 200);
        }
      })
      .catch(err => {
        console.log("Error", err);
      });
      this.createAccountService.fn_getDiscapacidades();

  }

  // createLoader() {
  //   this.ServicesProvider.createLoader();
  //   this.timeOut = setTimeout(() => {
  //     this.timeOutControl = false;
  //     this.ServicesProvider.loading.dismiss();
  //     this.ServicesProvider.toast(MESSAGES.services.timeOut);
  //   }, MESSAGES.loading.time);
  // }


  fn_ruleDiscapacidad(check){
    console.log("foefs")
    //this.createAccountService.register.controls['discapacidad'].setValidators([Validators.required])
    setTimeout(() => {

      if(check){
       /* this.createAccountService.register.controls['discapacidad'].enable();
        this.createAccountService.register.controls['discapacidad_desc'].enable();*/
        this.createAccountService.register.controls['discapacidad'].setValidators([Validators.required]);
        this.createAccountService.register.controls['discapacidad'].updateValueAndValidity();
        this.createAccountService.register.controls['discapacidad_desc'].setValidators([Validators.required]);
        this.createAccountService.register.controls['discapacidad_desc'].updateValueAndValidity();
        console.log(this.createAccountService.register.controls['discapacidad_desc'])

        this.createAccountService.register3.controls['cellphone'].setValidators(Validators.compose([])); 
      }
      else{

        this.createAccountService.register.controls['discapacidad'].setValidators([]);
        this.createAccountService.register.controls['discapacidad'].updateValueAndValidity();
        this.createAccountService.register.controls['discapacidad_desc'].setValidators([]);
        this.createAccountService.register.controls['discapacidad_desc'].updateValueAndValidity(); 
      }
    })
  }



  clearTimeOut() {
    clearTimeout(this.timeOut);
  }

  updateAge(selectedValue: any) {
    let verifyAge = this.createAccountService.verifyAge();
    console.log(verifyAge)
    if (verifyAge) {
      this.createAccountService._verifyAge = true;
      return;
    }
    this.createAccountService._verifyAge = false;
  }

  validateStep() {
       console.log("reside", this.createAccountService.register.value.reside_actualmente, " ",(this.createAccountService.register.get("reside_actualmente")))

    console.log("discapacida", this.createAccountService.register.value.check_discapacidad, " ",(this.createAccountService.register.get("check_discapacidad")))

    let verifyAge = this.createAccountService.verifyAge();
      console.log(verifyAge, this.navCtrl.parent);

    if (this.createAccountService.register.controls['check_discapacidad'].value) {
      this.navCtrl.parent.select(1);
      return;
    }

    if (verifyAge ) {
      console.log("aqui")
      this.navCtrl.parent.select(2);
      return;
    }


    this.navCtrl.parent.select(1);
  }
  updateCountry(selectedValue: any) {
    this.createAccountService.register3.get("cellphone").setValue("");
    this.createAccountService.register3.get("phone").setValue("");


    if (this.createAccountService.countrys.length > 0) {
      let country: any = this.createAccountService.countrys.filter(function(
        item: any
      ) {
        return item.id_pk === Number(selectedValue);
      })[0];
      this.setRulesPhone(country.mask_celular,country.mask_fijo);

      this.createAccountService.isOlder = parseInt(country.mayor_edad);
      this.updateAge(this.createAccountService.register.controls['birthdate'].value);

      this.createAccountService.countrySelected=country;
      this.states = [];
      this.cities = [];
      this.veredas = [];
      this.typesDocument = [];
      this.searchTermCity = "";
      this.createAccountService.register.get("department").setValue(null);
      this.createAccountService.register.get("municipality").setValue(null);
      this.createAccountService.register.get("vereda").setValue(null);
      this.createAccountService.register.get("type_document").setValue(null);
      this.createAccountService.register.get("indicativo_pais").setValue(country.indicativo);
      this.createAccountService.isOlder = parseInt(country.mayor_edad);
      

      this.promiseTypeDocAndStateByCountry(
        this.ServicesProvider.apiGet(
          selectedValue,
          SERVICES.TIPODOCUMENTO_GETDEPAIS_SERV
        ),
        this.ServicesProvider.apiGet(
          selectedValue,
          SERVICES.UBICACION_LIST_ESTADODEPAIS_SERV
        )
      );
    }
  }

 setRulesPhone(mask_celular,mask_fijo){

        let contmaskCell=0;
        for(let i=0; i<=mask_celular.length; i++){
          if(mask_celular[i]=="-"){
            contmaskCell++;
          }
        }
        mask_celular=mask_celular.length-contmaskCell;
        let contmask_fijo=0;
        for(let i=0; i<=mask_fijo.length; i++){
          if(mask_fijo[i]=="-"){
            contmask_fijo++;
          }      
        }
        mask_fijo=mask_fijo.length-contmask_fijo;
                console.log(mask_celular,mask_fijo);



        this.createAccountService.person_validation_phone={"mask_celular": mask_celular, "mask_fijo":mask_fijo}
        this.createAccountService.register3.controls["cellphone"].setValidators([Validators.required, Validators.maxLength(mask_celular),Validators.minLength(mask_celular)]);
        this.createAccountService.register3.controls["cellphone"].updateValueAndValidity();
       
        this.createAccountService.register3.controls["phone"].setValidators([Validators.maxLength(mask_fijo),Validators.minLength(mask_fijo)]);
        this.createAccountService.register3.controls["phone"].updateValueAndValidity();
  }



  promiseTypeDocAndStateByCountry(promiseTypDoc, promiseStaByCount) {
    this.ServicesProvider.createLoader();
    Promise.all([promiseTypDoc, promiseStaByCount])
      .then(promises => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();

        if (promises[0]["status"] == 404 || promises[0]["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (promises[0]["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let rptTypesDocument = JSON.parse(promises[0]["_body"]);
          this.typesDocument = rptTypesDocument;
        }

        if (promises[1]["status"] == 404 || promises[1]["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (promises[1]["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let rptstates = JSON.parse(promises[1]["_body"]);
          this.states = rptstates;
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
          this.ServicesProvider.closeLoader();
          this.timeOutControl = false;
          this.clearTimeOut();
          this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  /**
   * @version 1.0
   * @author John Freddy Arias Valencia
   * Consultar las ciudades de acuerdo al estado/departamento
   * @param  {selectedValue}
   * @return  {Array Ciudades}
  */
  updateState(selectedValue: any) {
    console.log(selectedValue)
    this.createAccountService.register3.get("phone").setValue("");
    if (this.states.length > 0) {
      this.cities = [];
      this.veredas = [];
      this.searchTermCity = "";
      this.createAccountService.register.get("municipality").setValue(null);
      this.createAccountService.register.get("vereda").setValue(null);
      this.getCitiesByState(selectedValue);
    }
  }

  getCitiesByState(state: number) {
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiGet(state, SERVICES.UBICACION_LIST_CIUDADDEESTADO_SERV)
      .then(city => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();

        this.clearTimeOut();
        if (city["status"] == 404 || city["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (city["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(city["_body"]);
          this.createAccountService.citiesPersonalInformation = respuesta;
          this.setFilteredItems();
          this.searchControl.valueChanges
              .debounceTime(700)
             .subscribe(search => {

               console.log("aqui",search)
              this.searching = false;
              this.setFilteredItems();
              });
        }
      })
      .catch(err => {
        console.log("Error..", err);
        //this.ServicesProvider.loading.onDidDismiss(() => {
          this.ServicesProvider.closeLoader();

          this.timeOutControl = false;
          this.clearTimeOut();
          this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  updateCity(selectedValue: any) {
    if (this.cities.length > 0) {
      this.veredas = [];
      this.createAccountService.register.get("vereda").setValue(null);
      this.ServicesProvider.createLoader();
      this.ServicesProvider
        .apiGet(selectedValue, SERVICES.UBICACION_LIST_VEREDADECIUDAD_SERV)
        .then(city => {
          //this.ServicesProvider.loading.dismiss();
          this.ServicesProvider.closeLoader();

          this.clearTimeOut();
          if (city["status"] == 404 || city["status"] == 500) {
            this.timeOutControl = false;
            this.ServicesProvider.toast(MESSAGES.services.error);
          } else if (city["status"] == 401) {
            this.ServicesProvider.toast(
              MESSAGES.services.unauthorized
            );
            this.timeOutControl = false;
          } else {
            let respuesta = JSON.parse(city["_body"]);
            this.veredas = respuesta;
          }
        })
        .catch(err => {
          //this.ServicesProvider.loading.onDidDismiss(() => {
            this.ServicesProvider.closeLoader();
            this.timeOutControl = false;
            this.clearTimeOut();
            this.ServicesProvider.toast(MESSAGES.services.error);
          //});
        });
    }
  }

  loadPromisesIfHaveStorage(result: any) {
    let arrayPromises = [];
    let pro_doc_by_country: any;
    let pro_states_by_country: any;
    let pro_cities_by_state: any;
    let veredas_by_city: any;

    if (result.country) {
      pro_doc_by_country = this.ServicesProvider.apiGet(
        result.country,
        SERVICES.TIPODOCUMENTO_GETDEPAIS_SERV
      );
      pro_states_by_country = this.ServicesProvider.apiGet(
        result.country,
        SERVICES.UBICACION_LIST_ESTADODEPAIS_SERV
      );
      arrayPromises = [pro_doc_by_country, pro_states_by_country];
    }

    if (result.department) {
      pro_cities_by_state = this.ServicesProvider.apiGet(
        result.department,
        SERVICES.UBICACION_LIST_CIUDADDEESTADO_SERV
      );
      arrayPromises = [
        pro_doc_by_country,
        pro_states_by_country,
        pro_cities_by_state
      ];
    }

    if (result.municipality) {
      veredas_by_city = this.ServicesProvider.apiGet(
        result.municipality.id_pk,
        SERVICES.UBICACION_LIST_VEREDADECIUDAD_SERV
      );
      arrayPromises = [
        pro_doc_by_country,
        pro_states_by_country,
        pro_cities_by_state,
        veredas_by_city
      ];
    }

    if (arrayPromises.length > 0) {
      this.ServicesProvider.createLoader();
      Promise.all(arrayPromises)
        .then(promises => {
          //this.ServicesProvider.loading.dismiss();
          this.ServicesProvider.closeLoader();
          this.clearTimeOut();

          if (result.country) {
            if (promises[0]["status"] == 404 || promises[0]["status"] == 500) {
              this.timeOutControl = false;
              this.ServicesProvider.toast(MESSAGES.services.error);
            } else if (promises[0]["status"] == 401) {
              this.ServicesProvider.toast(
                MESSAGES.services.unauthorized
              );
              this.timeOutControl = false;
            } else {
              let rptTypesDocument = JSON.parse(promises[0]["_body"]);
              this.typesDocument = rptTypesDocument;
            }

            if (promises[1]["status"] == 404 || promises[1]["status"] == 500) {
              this.timeOutControl = false;
              this.ServicesProvider.toast(MESSAGES.services.error);
            } else if (promises[1]["status"] == 401) {
              this.ServicesProvider.toast(
                MESSAGES.services.unauthorized
              );
              this.timeOutControl = false;
            } else {
              let rptstates = JSON.parse(promises[1]["_body"]);
              this.states = rptstates;
            }
          }

          if (result.department) {
            if (promises[2]["status"] == 404 || promises[2]["status"] == 500) {
              this.timeOutControl = false;
              this.ServicesProvider.toast(MESSAGES.services.error);
            } else if (promises[2]["status"] == 401) {
              this.ServicesProvider.toast(
                MESSAGES.services.unauthorized
              );
              this.timeOutControl = false;
            } else {
              let rptsCities = JSON.parse(promises[2]["_body"]);
              //this.cities = rptsCities;
              this.createAccountService.citiesPersonalInformation = rptsCities;
              this.setFilteredItems();
              this.searchControl.valueChanges
                .debounceTime(700)
                .subscribe(search => {
                  this.searching = false;
                  this.setFilteredItems();
                });
            }
          }

          if (result.municipality) {
            if (promises[3]["status"] == 404 || promises[3]["status"] == 500) {
              this.timeOutControl = false;
              this.ServicesProvider.toast(MESSAGES.services.error);
            } else if (promises[3]["status"] == 401) {
              this.ServicesProvider.toast(
                MESSAGES.services.unauthorized
              );
              this.timeOutControl = false;
            } else {
              let rptveredas = JSON.parse(promises[3]["_body"]);
              this.veredas = rptveredas;
            }
          }
        })
        .catch(err => {
          console.log("Errror", err);
          //this.ServicesProvider.loading.onDidDismiss(() => {
            this.ServicesProvider.closeLoader();
            this.timeOutControl = false;
            this.clearTimeOut();
            this.ServicesProvider.toast(MESSAGES.services.error);
          //});
        });
    }
  }

  onSearchInput(event: any) {
    this.createAccountService.register.get("municipality").setValue(null);
    this.citySelected = 0;
    this.veredas = [];
    this.createAccountService.register.get("vereda").setValue(null);
    this.searching = true;
    this.hidenList = true;
  }

  setFilteredItems() {
    this.cities = this.createAccountService.filterItems(
      this.searchTermCity,
      this.createAccountService.citiesPersonalInformation
    );
  }

  filterCitySelected(city: any) {
    this.createAccountService.register.get("municipality").setValue(city);
    this.citySelected = city.id_pk;
    this.updateCity(city.id_pk);
    this.searchTermCity = city.nombre;
    this.createAccountService.register.get("indicativo_ciudad").setValue(city.indicativo);
    console.log("eu", this.createAccountService.register.value.indicativo_pais," ",this.createAccountService.register.value.indicativo_ciudad)
    this.hidenList = false;
    //console.log('Registro formControl', this.createAccountService.register.value);
  }

  newLoad() {
    this.timeOutControl = true;
    this.ngAfterViewInit();
  }
}
