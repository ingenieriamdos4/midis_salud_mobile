import { Component } from "@angular/core";
import { NavController } from "ionic-angular";
import { FormControl } from "@angular/forms";
/* Servicios */
import { CreateAccountServiceProvider } from "../../../providers/create-account/create-account";
import { ServicesProvider } from "../../../providers/services/services";
import { ConfigPluginsProvider } from "../../../providers/config-plugins/config-plugins";
import { Person } from "../../../models/person";

/* Mensajes */
import { MESSAGES } from "../../../config/messages";
import { SERVICES } from "../../../config/url.servicios";

@Component({
  selector: "page-contact-details",
  templateUrl: "contact-details.html"
})
export class ContactDetailsPage {
  states: Array<string> = [];
  cities: Array<string> = [];
  searchTermCity: string = "";
  searchControl: FormControl;
  searching: any = false;
  citySelected: number;
  veredas: Array<string> = [];
  indCountry: any;
  person: Person;
  timeOut: any;
  timeOutControl: boolean = true;
  hidenList: boolean = false;
  indicativoCellphone:any;
  indicativoPhone:any;
  habilitar_tab_contacto:boolean=false;
  constructor(
    public navCtrl: NavController,
    public createAccountService: CreateAccountServiceProvider,
    public ServicesProvider: ServicesProvider,
    public configPluginsProvider: ConfigPluginsProvider
  ) {
    this.searchControl = new FormControl();
    this.person = new Person();

  }

  ionViewDidEnter(){
    console.log(this.createAccountService.countrySelected)
    this.indicativoCellphone=this.createAccountService.register.value.indicativo_pais;
    this.indicativoPhone=this.createAccountService.register.value.indicativo_ciudad;
    console.log(this.createAccountService.register.value)
    console.log( this.indicativoCellphone," ",this.indicativoCellphone);

  }

  ionViewDidLoad() {

    console.log( this.indicativoCellphone," ",this.indicativoCellphone)
    this.configPluginsProvider
      .get("register3")
      .then(result => {
        /*if (result.country) {
          //this.person.country = result.country;
          this.createAccountService.register3
            .get("country")
            .setValue(result.country);
        } else {
          this.createAccountService.register3.get("country").setValue(null);
        }

        if (result.department) {
          //this.person.department = result.department;
          this.createAccountService.register3
            .get("department")
            .setValue(result.department);
        } else {
          this.createAccountService.register3.get("department").setValue(null);
        }

        if (result.municipality) {
          this.searchTermCity = result.municipality.nombre;
          this.citySelected = result.municipality.id_pk;
        } else {
          this.createAccountService.register3
            .get("municipality")
            .setValue(null);
        }
        if (result.vereda) {
          this.createAccountService.register3
            .get("vereda")
            .setValue(result.vereda);
        }*/
        this.loadPromisesIfHaveStorage(result);

        if (result.cellphone) {
          setTimeout(() => {
            this.createAccountService.register3
              .get("cellphone")
              .setValue(result.cellphone);
          }, 200);
        }
        if (result.phone) {
          this.createAccountService.register3
            .get("phone")
            .setValue(result.phone);
        }
      })
      .catch(err => {
        console.log("Error", err);
      });
  }

  // createLoader() {
  //   this.ServicesProvider.createLoader();
  //   this.timeOut = setTimeout(() => {
  //     this.timeOutControl = false;
  //     this.ServicesProvider.loading.dismiss();
  //     this.ServicesProvider.toast(MESSAGES.services.timeOut);
  //   }, MESSAGES.loading.time);
  // }

  clearTimeOut() {
    clearTimeout(this.timeOut);
  }

  validateStep() {
    this.habilitar_tab_contacto=true;
    this.navCtrl.parent.select(3);
  }

  validateStepBack() {
   
    if (this.createAccountService.register2.valid) {
      this.navCtrl.parent.select(1);
    }
    else{
      this.navCtrl.parent.select(0);
    }

  }

  /*updateCountry(selectedValue: any) {
    if (this.createAccountService.countrys.length > 0) {
      let country: any = this.createAccountService.countrys.filter(function(
        item: any
      ) {
        return item.id_pk === Number(selectedValue);
      })[0];

      this.states = [];
      this.cities = [];
      this.veredas = [];
      this.searchTermCity = "";
      this.createAccountService.register3.get("department").setValue(null);
      this.createAccountService.register3.get("municipality").setValue(null);
      this.createAccountService.register3.get("vereda").setValue(null);
      this.indicativoPhone = "";
      this.indicativoCellphone = country.indicativo;
      this.getStatesByCountry(selectedValue);
    }
  }*/


    updateCountry() {
      let selectedValue:any=this.createAccountService.register.value.country;
      if (this.createAccountService.countrys.length > 0) {
        let country: any = this.createAccountService.countrys.filter(function(
          item: any
        ) {
          return item.id_pk === Number(selectedValue);
        })[0];

        /*this.states = [];
        this.cities = [];
        this.veredas = [];
        this.searchTermCity = "";
        this.createAccountService.register3.get("department").setValue(null);
        this.createAccountService.register3.get("municipality").setValue(null);
        this.createAccountService.register3.get("vereda").setValue(null);
        this.indicativoPhone = "";*/
        this.indicativoCellphone = country.indicativo;
        this.getStatesByCountry(selectedValue);
      }
    }


  getStatesByCountry(country: number) {
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiGet(country, SERVICES.UBICACION_LIST_ESTADODEPAIS_SERV)
      .then(state => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();

        this.clearTimeOut();
        if (state["status"] == 404 || state["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (state["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(state["_body"]);
          this.states = respuesta;
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
          this.ServicesProvider.closeLoader();
          this.timeOutControl = false;
          this.clearTimeOut();
          this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  updateState(selectedValue: number) {
    if (this.states.length > 0) {
      this.cities = [];
      this.veredas = [];
      this.searchTermCity = "";
      this.createAccountService.register3.get("municipality").setValue(null);
      this.createAccountService.register3.get("vereda").setValue(null);
      this.getCitiesByState(selectedValue);
    }
  }

  getCitiesByState(state: number) {
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiGet(state, SERVICES.UBICACION_LIST_CIUDADDEESTADO_SERV)
      .then(city => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();

        this.clearTimeOut();
        if (city["status"] == 404 || city["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (city["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(city["_body"]);
          this.createAccountService.citiesContactoDetails = respuesta;
          this.setFilteredItems();
          this.searchControl.valueChanges
            .debounceTime(700)
            .subscribe(search => {
              this.searching = false;
              this.setFilteredItems();
            });
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
          this.ServicesProvider.closeLoader();
          this.timeOutControl = false;
          this.clearTimeOut();
          this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  updateCity(selectedValue: any) {
    if (this.cities.length > 0) {
      let city: any = this.cities.filter(function(item: any) {
        return item.id_pk === Number(selectedValue);
      })[0];

      this.veredas = [];
      this.createAccountService.register3.get("vereda").setValue(null);
      this.indicativoPhone = city.indicativo;
      this.ServicesProvider.createLoader();
      this.ServicesProvider
        .apiGet(selectedValue, SERVICES.UBICACION_LIST_VEREDADECIUDAD_SERV)
        .then(city => {
          //this.ServicesProvider.loading.dismiss();
          this.ServicesProvider.closeLoader();

          this.clearTimeOut();
          if (city["status"] == 404 || city["status"] == 500) {
            this.timeOutControl = false;
            this.ServicesProvider.toast(MESSAGES.services.error);
          } else if (city["status"] == 401) {
            this.ServicesProvider.toast(
              MESSAGES.services.unauthorized
            );
            this.timeOutControl = false;
          } else {
            let respuesta = JSON.parse(city["_body"]);
            this.veredas = respuesta;
          }
        })
        .catch(err => {
          //this.ServicesProvider.loading.onDidDismiss(() => {
            this.ServicesProvider.closeLoader();
            this.timeOutControl = false;
            this.clearTimeOut();
            this.ServicesProvider.toast(MESSAGES.services.error);
          //});
        });
    }
  }

  loadPromisesIfHaveStorage(result: any) {
    let arrayPromises = [];
    let pro_states_by_country: any;
    let pro_cities_by_state: any;
    let veredas_by_city: any;
    if (!this.timeOutControl) {
      this.timeOutControl = true;
    }

    if (result.country) {
      let country: any = this.createAccountService.countrys.filter(function(
        item: any
      ) {
        return item.id_pk === Number(result.country);
      })[0];
      this.indicativoCellphone = country.indicativo;
      pro_states_by_country = this.ServicesProvider.apiGet(
        result.country,
        SERVICES.UBICACION_LIST_ESTADODEPAIS_SERV
      );
      arrayPromises = [pro_states_by_country];
    }

    if (result.department) {
      pro_cities_by_state = this.ServicesProvider.apiGet(
        result.department,
        SERVICES.UBICACION_LIST_CIUDADDEESTADO_SERV
      );
      arrayPromises = [pro_states_by_country, pro_cities_by_state];
    }

    if (result.municipality) {
      veredas_by_city = this.ServicesProvider.apiGet(
        result.municipality.id_pk,
        SERVICES.UBICACION_LIST_VEREDADECIUDAD_SERV
      );
      arrayPromises = [
        pro_states_by_country,
        pro_cities_by_state,
        veredas_by_city
      ];
    }

    if (arrayPromises.length > 0) {
      this.ServicesProvider.createLoader();
      Promise.all(arrayPromises)
        .then(promises => {
          //this.ServicesProvider.loading.dismiss();
          this.ServicesProvider.closeLoader();

          this.clearTimeOut();
          if (result.country) {
            if (promises[0]["status"] == 404 || promises[0]["status"] == 500) {
              this.timeOutControl = false;
              this.ServicesProvider.toast(MESSAGES.services.error);
            } else if (promises[0]["status"] == 401) {
              this.ServicesProvider.toast(
                MESSAGES.services.unauthorized
              );
              this.timeOutControl = false;
            } else {
              let rptstates = JSON.parse(promises[0]["_body"]);
              this.states = rptstates;
            }
          }

          if (result.department) {
            if (promises[1]["status"] == 404 || promises[1]["status"] == 500) {
              this.timeOutControl = false;
              this.ServicesProvider.toast(MESSAGES.services.error);
            } else if (promises[1]["status"] == 401) {
              this.ServicesProvider.toast(
                MESSAGES.services.unauthorized
              );
              this.timeOutControl = false;
            } else {
              let rptsCities = JSON.parse(promises[1]["_body"]);

              this.createAccountService.citiesContactoDetails = rptsCities;
              this.setFilteredItems();
              this.searchControl.valueChanges
                .debounceTime(700)
                .subscribe(search => {
                  this.searching = false;
                  this.setFilteredItems();
                });
              if (result.municipality) {
                this.indicativoPhone = result.municipality.indicativo;
              }
            }
          }

          if (result.municipality) {
            if (promises[2]["status"] == 404 || promises[2]["status"] == 500) {
              this.timeOutControl = false;
              this.ServicesProvider.toast(MESSAGES.services.error);
            } else if (promises[2]["status"] == 401) {
              this.ServicesProvider.toast(
                MESSAGES.services.unauthorized
              );
              this.timeOutControl = false;
            } else {
              let rptveredas = JSON.parse(promises[2]["_body"]);
              this.veredas = rptveredas;
            }
          }
        })
        .catch(err => {
          console.log("Error", err);
          //this.ServicesProvider.loading.onDidDismiss(() => {
            this.ServicesProvider.closeLoader();

            this.timeOutControl = false;
            this.clearTimeOut();
            this.ServicesProvider.toast(MESSAGES.services.error);
          //});
        });
    }
  }

  onSearchInput(event: any) {
    this.createAccountService.register3.get("municipality").setValue(null);
    this.citySelected = 0;
    this.indicativoPhone = "";
    this.veredas = [];
    this.createAccountService.register3.get("vereda").setValue(null);
    this.searching = true;
    this.hidenList = true;
  }

  setFilteredItems() {
    this.cities = this.createAccountService.filterItems(
      this.searchTermCity,
      this.createAccountService.citiesContactoDetails
    );
  }

  filterCitySelected(city: any) {
    this.createAccountService.register3.get("municipality").setValue(city);
    this.citySelected = city.id_pk;
    this.updateCity(city.id_pk);
    this.searchTermCity = city.nombre;
    this.hidenList = false;
  }

  newLoad() {
    this.timeOutControl = true;
    this.ionViewDidLoad();
  }
}
