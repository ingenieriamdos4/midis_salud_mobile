import { Component } from "@angular/core";
import { ViewController, Platform } from "ionic-angular";
import { ServicesProvider } from "../../../providers/services/services";
import { SERVICES } from "../../../config/url.servicios";
import { MESSAGES } from "../../../config/messages";
import { InAppBrowser, InAppBrowserOptions } from "@ionic-native/in-app-browser";
import { FileTransfer, FileTransferObject } from "@ionic-native/file-transfer";
import { SocialSharing } from "@ionic-native/social-sharing";
import { ConfigPluginsProvider } from "../../../providers/config-plugins/config-plugins";
import { FileEntry, File } from "@ionic-native/file";

declare var cordova: any;

@Component({
  selector: "page-modal-terms-conditions",
  templateUrl: "modal-terms-conditions.html"
})
export class ModalTermsConditionsPage {

  storageDirectory: string = "";
  isIos: boolean = false;

  constructor(
    public viewCtrl: ViewController,
    public ServicesProvider: ServicesProvider,
    private iab: InAppBrowser,
    public platform: Platform,
    private socialSharing: SocialSharing,
    public configPluginsProvider: ConfigPluginsProvider,
    private transfer: FileTransfer,
    private file: File
  ) {
    this.getTermsPdf();

    this.platform.ready().then(() => {
      // make sure this is on a device, not an emulation (e.g. chrome tools device mode)
      if (!this.platform.is("cordova")) {
        return false;
      }
      if (this.platform.is("ios")) {
        this.storageDirectory = cordova.file.documentsDirectory;
        this.isIos = true;
      } else if (this.platform.is("android")) {
        this.storageDirectory = cordova.file.externalRootDirectory;
      } else {
        // exit otherwise, but you could add further types here e.g. Windows
        return false;
      }
    });
  }
  archivo_adjunto: any;
  timeOut: any;
  timeOutControl: boolean = true;
  showButtons: boolean = false;
  cancelModal() {
    this.viewCtrl.dismiss();
  }

  acceptModal() {
    this.viewCtrl.dismiss({ accept: 1 });
  }

  clearTimeOut() {
    clearTimeout(this.timeOut);
  }

  verifyTypeFile(file: string) {
    let ext: any = file
      .split(".")
      .pop()
      .toLowerCase();

    if (ext == "jpg" || ext == "jpeg" || ext == "png") {
      return true;
    }

    return false;
  }

  splitFileName(file: string) {
    return file.split("/").pop();
  }

  reviewFile(file: string) {
    let options: InAppBrowserOptions = {
      hardwareback: 'yes'
    };
    this.iab.create(file, "_blank", options);
  }

  pageRendered(e: CustomEvent) {
    this.showButtons = true;
  }

  download(_url: string) {

    if (this.isIos) {
      this.shareQr(_url);
      return;
    }

    let promise = this.configPluginsProvider.statusExternalStorageForApplication()

    this.configPluginsProvider.downloader(
      promise,
      this.transfer,
      _url,
      this.storageDirectory
    );
    
  }

  shareQr(url: string) {
    this.ServicesProvider.createLoader();
    this.socialSharing
      .share(MESSAGES.antecedent.share.message, "Midis App", url, "")
      .then(success => {
        this.ServicesProvider.closeLoader();
      })
      .catch(err => {
        this.ServicesProvider.closeLoader();
        /*this.ServicesProvider.toast(
          MESSAGES.antecedent.share.error
        );*/
      });
  }

  getTermsPdf() {

    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiGet("", SERVICES.TERMINOS_Y_CONDICIONES)
      .then(terminos => {
        this.ServicesProvider.closeLoader();
        //this.ServicesProvider.loading.dismiss();
        this.clearTimeOut();
        if (terminos["status"] == 404 || terminos["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (terminos["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {

          this.archivo_adjunto = terminos["_body"];
          console.log("URL"+this.archivo_adjunto)
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.timeOutControl = false;
        this.ServicesProvider.toast("Error: " + err);
        //});
      });
  }



}
