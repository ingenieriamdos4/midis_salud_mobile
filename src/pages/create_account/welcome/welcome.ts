import { Component, ViewChild } from "@angular/core";
import { App, Platform, Events, Slides } from "ionic-angular";
import { CreateAccountServiceProvider } from "../../../providers/create-account/create-account";
import { OneSignal } from "@ionic-native/onesignal";
import { LoginProviderService } from "../../../providers/login/login";
import { PatientProviderService } from "../../../providers/patient/patient";
import { ServicesProvider } from "../../../providers/services/services";
import { SERVICES } from "../../../config/url.servicios";
import { DomSanitizer } from '@angular/platform-browser';

/* Mensajes */
import { MESSAGES } from "../../../config/messages";
import { WIZZARD_SPA } from "../../../config/wizzard_spa";
import { HomePage } from "../../index.paginas";

@Component({
  selector: "page-welcome",
  templateUrl: "welcome.html"
})
export class WelcomePage {
  @ViewChild('mySlider') slider: Slides;

  slides: any;
  lastSlide: string = "assets/imgs/cargando.png";
  timeOut: any;
  timeOutControl: boolean = true;
  idDispositivo: string;

  constructor(
    public appCtrl: App,
    public createAccountService: CreateAccountServiceProvider,
    private oneSignal: OneSignal,
    public platform: Platform,
    public loginProviderService: LoginProviderService,
    public ServicesProvider: ServicesProvider,
    public patientProviderService: PatientProviderService,
    public events: Events,
    public sanitizer: DomSanitizer

  ) { }

  ionViewDidLoad() {
    /*setTimeout(() => 
    {
      this.getImages();
    }, 500);*/
    this.slides = WIZZARD_SPA.registro;
    console.log(this.slides)
  }

  ionSlideNextStart() {
    if (this.slider.isEnd()) {
      const length = this.slider.length() - 1;
      const index = this.slider.getActiveIndex();


      if (index > length) {
        console.log('pop view');
        this.iniciar();

        //this.navCtrl.pop()
      }
    }

  }

  slideToNext() {
    //this.slider.getSlider().slideNext(); // also not working
    if (this.slider.isEnd()) {
      //this.navCtrl.pop();
      this.iniciar();

    }
    else {
      this.slider.slideNext(); // not working
    }

  }

  close() {
    this.iniciar();
  }

  iniciar() {
    this.createAccountService.register.reset();
    this.createAccountService.register2.reset();
    this.createAccountService.register3.reset();
    this.getLogin(this.createAccountService.register4.value);
    this.createAccountService.register4.reset();
    //this.appCtrl.getRootNav().setRoot(LoginPage);
  }



  getDeviceId() {
    this.oneSignal
      .getIds()
      .then(success => {
        this.idDispositivo = success.userId;
      })
      .catch(error => {
        console.log("No se recibio ningun ID");
      });
  }

  getLogin(formValue: any) {
    let data = { "email": formValue.user, "password": formValue.password };
    if (this.idDispositivo) {
      data["id_dispositivo"] = this.idDispositivo;
    }
    else {
      data["id_dispositivo"] = "b7eee1e9-fbcf-45f0-8b58-5e3458f2cxxx";
    }
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiPost(data, "/login_api")
      .then(login => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();

        this.ServicesProvider.clearTimeOut();
        if (login["status"] == 404 || login["status"] == 500) {
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (login["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
        } else {
          let respuesta = JSON.parse(login["_body"]);
          if (respuesta.success != undefined) {
            this.getStatus(respuesta.success);
          } else {
            this.getUser(respuesta.id_paciente);
          }
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();
        this.ServicesProvider.clearTimeOut();
        this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  getUser(id_paciente: number) {
    let params = { "id_paciente": id_paciente };
    this.ServicesProvider.createLoader("Verificando datos...");
    this.ServicesProvider
      .apiPost(params, SERVICES.CONSULTAR_PACIENTE_API_SERV)
      .then(state => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();

        this.ServicesProvider.clearTimeOut();
        if (state["status"] == 404 || state["status"] == 500) {
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (state["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
        } else {
          let respuesta = JSON.parse(state["_body"]);
          //let respuesta = JSON.parse(state["_body"]);
          //localStorage.setItem("user", JSON.stringify(respuesta));
          localStorage.setItem("user", JSON.stringify(respuesta));
          localStorage.setItem("imagen_porcentaje", respuesta.imagen_porcentaje);
          this.events.publish('user:imagen_porcentaje', respuesta.imagen_porcentaje);
          let dataHealth: any = JSON.stringify({
            weight: respuesta.peso,
            height: respuesta.estatura,
            imc: respuesta.imc
          });
          localStorage.setItem("dataHealth", dataHealth);
          this.patientProviderService.avatar =
            respuesta.get_user[0].foto + "?1=" + new Date();
          this.patientProviderService.height = respuesta.estatura;
          this.patientProviderService.weight = respuesta.peso;
          this.patientProviderService.imc = respuesta.imc;

          let user = {
            name:
              respuesta.nombres +
              " " +
              respuesta.apellido1 +
              " " +
              (respuesta.apellido2 != null ? respuesta.apellido2 : ""),
            avatar: respuesta.get_user[0].foto + "?1=" + new Date(),
            status: 1,
            pac_fk: id_paciente,
            username: respuesta.get_user[0].login_pk,
            type_user: 1
          };

          this.appCtrl.getRootNav().setRoot(HomePage);

          if (this.platform.is("cordova")) {
            this.createUserSqlite(id_paciente, user);
          }
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();

        this.ServicesProvider.clearTimeOut();
        this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  createUserSqlite(id_paciente: number, user: any) {
    this.patientProviderService
      .getFindByUser(id_paciente)
      .then(userFind => {
        if (userFind == undefined) {
          this.createUser(user);
        } else {
          this.patientProviderService
            .updateUser(user)
            .then(response => {
              console.log(
                "Se actualizo correctamente" + JSON.stringify(response)
              );
            })
            .catch(error => {
              this.ServicesProvider.toast(
                "Error al actualzar" + JSON.stringify(user) + error
              );
            });
        }
      })
      .catch(err => {
        this.ServicesProvider.toast(
          "Error al encontrar el usuario" + JSON.stringify(user) + err
        );
      });
  }

  createUser(data: any) {
    this.patientProviderService
      .createUser(data)
      .then(response => {
        this.patientProviderService.users.unshift(data);
      })
      .catch(error => {
        this.ServicesProvider.toast(
          MESSAGES.sqlite.error2 + JSON.stringify(error)
        );
      });
  }

  getStatus(status: number) {
    switch (status) {
      case 1:
        this.ServicesProvider.toast(MESSAGES.login.status.state_1);
        break;
      case 2:
        this.ServicesProvider.toast(MESSAGES.login.status.state_2);
        break;
      case 3:
        this.ServicesProvider.toast(MESSAGES.login.status.state_3);
        break;
      case 4:
        this.ServicesProvider.toast(MESSAGES.login.status.state_4);
        break;
      default:
        this.ServicesProvider.toast(
          MESSAGES.login.status.state_5,
          "successToast"
        );
    }
  }
  /*
    getImages() {
      this.ServicesProvider.createLoader();
      this.ServicesProvider
        .apiGet("", SERVICES.IMAGENES_BIENVENIDA_SERV)
        .then(state => {
          //this.ServicesProvider.loading.dismiss();
          this.ServicesProvider.closeLoader();
  
          this.ServicesProvider.clearTimeOut();
          if (state["status"] == 404 || state["status"] == 500) {
            this.timeOutControl = false;
            this.ServicesProvider.toast(MESSAGES.services.error);
          } else if (state["status"] == 401) {
            this.ServicesProvider.toast(
              MESSAGES.services.unauthorized
            );
            this.timeOutControl = false;
          } else {
            let respuesta = JSON.parse(state["_body"]);
            console.log(respuesta)
            this.lastSlide = respuesta.splice(respuesta.length - 1, 1)[0].archivo;
            this.slides = respuesta;
          }
        })
        .catch(err => {
          //this.ServicesProvider.loading.onDidDismiss(() => {
            this.ServicesProvider.closeLoader();
  
            this.timeOutControl = false;
            this.ServicesProvider.clearTimeOut();
            this.ServicesProvider.toast(MESSAGES.services.error);
          //});
        });
    }*/


}
