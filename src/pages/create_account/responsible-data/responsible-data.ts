import { Component, ElementRef, ViewChild } from "@angular/core";
import { NavController } from "ionic-angular";
import { FormControl } from "@angular/forms";

/* Servicios */
import { CreateAccountServiceProvider } from "../../../providers/create-account/create-account";
import { Person } from "../../../models/person";
//import { PatientProviderService } from "../../../providers/patient/patient";
import { ServicesProvider } from "../../../providers/services/services";
import { ConfigPluginsProvider } from "../../../providers/config-plugins/config-plugins";

/* Mensajes */
import { MESSAGES } from "../../../config/messages";
import { SERVICES } from "../../../config/url.servicios";
import { Validators } from "@angular/forms";


@Component({
  selector: "page-responsible-data",
  templateUrl: "responsible-data.html"
})
export class ResponsibleDataPage {
  person: Person;
  states: Array<string> = [];
  cities: Array<string> = [];
  searchTermCity: string = "";
  searchControl: FormControl;
  searching: any = false;
  citySelected: number;
  countrys: Array<string> = [];
  typeDocuments: Array<string> = [];
  relationShips: Array<string> = [];
  timeOut: any;
  timeOutControl: boolean = true;
  indicativoCellphone: string;
  indicativoPhone: string;
  isContactFamily: boolean;
  idpk: number;
  id_dispositivo_contact: string;
  controlLoad_s_c: boolean = true;
  hideList: boolean = false;
  bHizoBusqueda:boolean=false;
  bFamiliarEncontrado:boolean;
  person_validation_phone:any;
  @ViewChild("country", { read: ElementRef })
  country: ElementRef;
  @ViewChild("state", { read: ElementRef })
  state: ElementRef;
  user:any;
  constructor(
    public navCtrl: NavController,
    public createAccountService: CreateAccountServiceProvider,
    public ServicesProvider: ServicesProvider,
    public configPluginsProvider: ConfigPluginsProvider
  ) {
    this.searchControl = new FormControl();
    this.promisesCountrysAndRelationShip(
      this.ServicesProvider.apiGet("", SERVICES.UBICACION_LIST_PAIS_SERV),
      this.ServicesProvider.apiGet("", SERVICES.PARENTESCO_LIST_SERV)
    );
    this.person = new Person();
  console.log("constructor");
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad2")
    this.createAccountService.register2.reset();
    this.createAccountService.citiesContact = [];
    this.indicativoCellphone = "";
    this.indicativoPhone = "";
    this.user=this.createAccountService.countrySelected;
    this.createAccountService.register2.get("country").setValue((this.user.id_pk)); 

  }

  ionViewWillEnter(){
    if(this.user){
          console.log(this.user, " ",this.user.indicativo, " ",this.createAccountService.countrySelected)

      if(this.user.indicativo!=this.createAccountService.countrySelected.indicativo){
        this.bHizoBusqueda=false;
        this.createAccountService.register2.get("cellphone").setValue(null);
        //fix para que el formulario se invalide
        this.createAccountService.register2.get("email").setValue(null);
 
      }
    }
    this.user=this.createAccountService.countrySelected;
    console.log(this.user)
    this.createAccountService.register2.get("country").setValue((this.user.id_pk));    

    //this.createAccountService.register2.get("country").setValue((this.user.id_pk));
  }


  promisesCountrysAndRelationShip(
    promiseCountry: any,
    promiseRelationShip: any
  ) {
    this.ServicesProvider.createLoader();
    Promise.all([promiseCountry, promiseRelationShip])
      .then(promises => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();        
        this.clearTimeOut();

        if (promises[0]["status"] == 404 || promises[0]["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (promises[0]["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let rptCountrys = JSON.parse(promises[0]["_body"]);
          this.countrys = rptCountrys;

      
          console.log("ëntra por aca",this.user.id_pk,this.countrys )
          console.log(this.createAccountService.register2.get("country").value)
          /*this.createAccountService.register2.controls["country"].setValue(this.user.get_pais_res.id_pk)
          setTimeout(function(){
             this.createAccountService.register2.get("country").setValue(this.user.get_pais_res.id_pk);
          },3000)*/
            
            this.indicativoCellphone =this.user.indicativo;
            this.setRulesPhone(this.user.mask_celular,this.user.mask_fijo);


        }

        if (promises[1]["status"] == 404 || promises[1]["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (promises[1]["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let rptRelationShips = JSON.parse(promises[1]["_body"]);
          this.relationShips = rptRelationShips;
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
          this.ServicesProvider.closeLoader();          
          this.timeOutControl = false;
          this.clearTimeOut();
          this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  updateCountry(selectedValue: any,algo:any) {
    console.log("eu3",selectedValue,algo)
      this.bHizoBusqueda=false;

    if (this.countrys.length > 0 && this.controlLoad_s_c) {
      let country: any = this.countrys.filter(function(item: any) {
        return item.id_pk === Number(selectedValue);
      })[0];
      this.states = [];
      this.cities = [];
      this.createAccountService.register2
        .get("municipality")
        .setValue(null);
      this.createAccountService.register2.get("department").setValue(null);
      this.indicativoPhone = "";
      this.searchTermCity = "";
      this.citySelected = 0;
      this.indicativoPhone = "";
      this.indicativoCellphone = country.indicativo;
      this.getStatesByCountry(selectedValue);
      this.setRulesPhone(country.mask_celular,country.mask_fijo);
    }
  }

  getStatesByCountry(country: number) {
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiGet(country, SERVICES.UBICACION_LIST_ESTADODEPAIS_SERV)
      .then(state => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();        
        this.clearTimeOut();

        if (state["status"] == 404 || state["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (state["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(state["_body"]);
          this.states = respuesta;
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
          this.ServicesProvider.closeLoader();          
          this.timeOutControl = false;
          this.clearTimeOut();
          this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }


  updateState(selectedValue: any) {
    console.log("upadte state");
    console.log(this.states.length, selectedValue)
    if (this.states.length > 0  && selectedValue.length!=0) {
    console.log("entraa")
      this.cities = [];
      if(selectedValue.length==0){
        this.searchTermCity = "";
        this.citySelected = 0;
        this.createAccountService.register2
        .get("municipality")
        .setValue(null);  
        this.indicativoPhone = "";
      }
      this.getCitiesByState(selectedValue);
    }
  }

   clearTimeOut() {
    clearTimeout(this.timeOut);
  }
  getCitiesByState(state: number) {
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiGet(state, SERVICES.UBICACION_LIST_CIUDADDEESTADO_SERV)
      .then(city => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();        
        this.clearTimeOut();
        if (city["status"] == 404 || city["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (city["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(city["_body"]);
          this.createAccountService.citiesContact = respuesta;
          this.setFilteredItems();
          this.searchControl.valueChanges
            .debounceTime(700)
            .subscribe(search => {
              this.searching = false;
              this.setFilteredItems();
            });
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
          this.ServicesProvider.closeLoader();          
          this.timeOutControl = false;
          this.clearTimeOut();
          this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  updateCity(city: any) {
    if (this.cities.length > 0) {
      this.indicativoPhone = city.indicativo;
    }
  }



  addMedical() {
    /*let formData = this.createAccountService.register2.value;
    /*$request->parentesco (int)
    $request->nombrefamiliar (varchar 50)
    $request->apellido1familiar (varchar 20)
    $request->apellido2familiar (varchar 20) opcional
    $request->telefonofijofamiliar (varchar 20) opcional
    $request->celular (varchar 20)
    $request->emailfamiliar (varchar 30) opcional
    $request->paisFamiliarcre (int)
    $request->estadoFamiliarcre (int)
    $request->ciudadFamiliarcre (int)
    $request->direccionfamiliar (varchar 20) opcional*/
     /*console.log(this.country)
    let data = {
      id_pk: "",
      parentesco: formData.parentesco.id_pk,
      parentescoNombre: formData.parentesco.nombre,
      nombrefamiliar: formData.name,
      apellido1familiar: formData.surname,
      apellido2familiar:
        formData.second_surname != null ? formData.second_surname : "",
      emailfamiliar: formData.email,
      direccionfamiliar: formData.address,
      paisFamiliarcre: formData.country,
      paisFamiliarcreNombre: this.country.nativeElement.textContent,
      estadoFamiliarcre: formData.department,
      estadoFamiliarcreNombre: this.state.nativeElement.textContent,
      ciudadFamiliarcre: formData.municipality.id_pk,
      ciudadFamiliarcreNombre: formData.municipality.nombre,
      celular: formData.cellphone == null ? "" : formData.cellphone,
      celularIndicativo: this.indicativoCellphone,
      telefonofijofamiliar: formData.phone == null ? "" : formData.phone,
      telefonofijofamiliarIndicativo: this.indicativoPhone,
      id_dispositivo: this.id_dispositivo_contact
        ? this.id_dispositivo_contact
        : ""
    };

    //this.viewCtrl.dismiss(data);
    data["pac_fk_familiar"]=this.createAccountService.register2
        .get("familiar_encontrado").value?this.createAccountService.register2
        .get("familiar_encontrado").value:'';
    this.createAccountService.dataFamily.unshift(data);

     //unshift
        //pack_fk_responsable
        /*params["pac_fk"] = this.updateProfileService.addMedicalData
        .get("medico_encontrado").value?this.updateProfileService.addMedicalData
        .get("medico_encontrado").value:'';
*/
    //this.saveData();*/


  }
  validateStep() {
    //this.createAccountService.habilitar_tab_contacto=true;
    this.navCtrl.parent.select(2);
  }
  validateStepBack() {
    this.navCtrl.parent.select(0);
  }


  familyExist() {
    let existFamily = this.createAccountService.register2.value;
    let params =
      "?pais=" + existFamily.country + "&celular=" + existFamily.cellphone+"&contacto=publico";
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiGet(params, SERVICES.DATOS_CONTACTO_API_SERV)
      .then(family => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (family["status"] == 404 || family["status"] == 500) {
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (family["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
        } else {
          let respuesta = JSON.parse(family["_body"]);
          if (respuesta.datos_contacto) {
            this.bHizoBusqueda=true;

            respuesta=respuesta.datos_contacto;
            console.log(respuesta)
            this.bFamiliarEncontrado=true;
           /* setTimeout(()=>{
              this.createAccountService.habilitar_tab_contacto=true;
            })*/

            this.controlLoad_s_c = false;
            this.idpk = respuesta.id_pk;
            this.id_dispositivo_contact = respuesta.id_dispositivo;

            this.createAccountService.register2
              .get("document")
              .setValue(respuesta.doc_iden_pk);
            this.createAccountService.register2
              .get("name")
              .setValue(respuesta.nombres);
            this.createAccountService.register2
              .get("surname")
              .setValue(respuesta.apellido1);
            this.createAccountService.register2
              .get("second_surname")
              .setValue(respuesta.apellido2 != null ? respuesta.apellido2 : "");
            this.createAccountService.register2
              .get("email")
              .setValue(respuesta.correo);
            this.createAccountService.register2
              .get("parentesco")
              .setValue(null);
            this.createAccountService.register2
              .get("address")
              .setValue(respuesta.direccion != null ? respuesta.direccion : "");

            this.createAccountService.register2
              .get("pack_fk_responsable")
              .setValue(respuesta.doc_iden_pk);   


            this.person.country = respuesta.get_pais_res.id_pk;
            //Cargar el estado y municipio de acuerdo a los datos que llegan del servicio
            //Paciente activo
            let promises=[
              this.ServicesProvider.apiGet(
                respuesta.get_pais_res.id_pk,
                SERVICES.UBICACION_LIST_ESTADODEPAIS_SERV
              ),
              this.ServicesProvider.apiGet(
                respuesta.get_estado_res.id_pk,
                SERVICES.UBICACION_LIST_CIUDADDEESTADO_SERV
              )
            ]
            this.promiseStateAndCity(promises);

            this.person.department = respuesta.get_estado_res.id_pk;
            //this.person.municipality = respuesta.get_ciudad_res.id_pk;
            this.createAccountService.register2
              .get("municipality")
              .setValue(respuesta.get_ciudad_res);
      console.log(this.createAccountService.register2
              .get("municipality"), respuesta.get_ciudad_res.nombre )
        
            this.searchTermCity = respuesta.get_ciudad_res.nombre;
            this.citySelected = respuesta.get_ciudad_res.id_pk;
            this.indicativoCellphone = respuesta.get_pais_res.indicativo;
            this.indicativoPhone = respuesta.get_ciudad_res.indicativo;
            this.createAccountService.register2
              .get("cellphone")
              .setValue(
                respuesta.tlf_contacto == null ||
                respuesta.tlf_contacto == "null"
                  ? ""
                  : respuesta.tlf_contacto
              );
        
            this.createAccountService.register2
              .get("phone")
              .setValue(
                respuesta.tlf_domicilio == null ||
                respuesta.tlf_domicilio == "null"
                  ? ""
                  : respuesta.tlf_domicilio
              );
        console.log(this.searchTermCity )
        
            //this.updateProfileService.toast('Se agrego correctamente el familiar '  + respuesta.nombres, 'successToast');
          } 
          else if(respuesta.datos_contacto_existe){
            this.bFamiliarEncontrado=true;
            /*setTimeout(()=>{
              this.createAccountService.habilitar_tab_contacto=true;
            })*/

            this.bHizoBusqueda=false;
            this.ServicesProvider.toast(
              MESSAGES.updateProfile.family_founded.duplicated
            );
          }

          else {
            /*this.ServicesProvider.toast(
              "El contacto familiar no se encontro "
            );*/
      
              this.bHizoBusqueda=true;

      console.log("no lo encuentra");
            let promises=[ this.ServicesProvider.apiGet(
                //this.user.get_pais_res.id_pk,
                this.createAccountService.register2.get("country").value,
                SERVICES.UBICACION_LIST_ESTADODEPAIS_SERV
              )];
            this.promiseStateAndCity(promises);

            this.bFamiliarEncontrado=false;
            /*setTimeout(()=>{
              this.createAccountService.habilitar_tab_contacto=false;
            })*/
            this.createAccountService.register2
              .get("familiar_encontrado")
              .setValue(false);     
            this.createAccountService.register2
              .get("parentesco")
              .setValue(null);
            this.createAccountService.register2
              .get("name")
              .setValue("");
            this.createAccountService.register2
              .get("surname")
              .setValue(null); 
            this.createAccountService.register2
              .get("second_surname")
              .setValue(null);              
             this.createAccountService.register2
              .get("email")
              .setValue("");
            this.createAccountService.register2
              .get("address")
              .setValue("");
            this.createAccountService.register2
              .get("phone")
              .setValue("");
            this.createAccountService.register2
              .get("pack_fk_responsable")
              .setValue(null);                                                   
            this.cities = [];
            this.createAccountService.register2.get("department").setValue(null);  
             this.createAccountService.register2
              .get("municipality")   
              .setValue(null);                             
            this.cities = [];
            this.searchTermCity = "";



          }
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
          this.ServicesProvider.closeLoader();
          this.clearTimeOut();
          this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  //promiseStateAndCity(promiseState: any, promiseCity: any) {
    promiseStateAndCity(promises) {
  if(!this.ServicesProvider.loading){
    this.ServicesProvider.createLoader();
  }
    Promise.all(promises)
      .then(promises => {
        if(this.ServicesProvider.loading){
        this.ServicesProvider.closeLoader();
      //this.ServicesProvider.loading.dismiss();
      this.clearTimeOut();      
    }


        if (promises[0]["status"] == 404 || promises[0]["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (promises[0]["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let rptState = JSON.parse(promises[0]["_body"]);
          this.states = rptState;
        }
        if(promises[1]){
          if (promises[1]["status"] == 404 || promises[1]["status"] == 500) {
            this.timeOutControl = false;
            this.ServicesProvider.toast(MESSAGES.services.error);
          } else if (promises[1]["status"] == 401) {
            this.ServicesProvider.toast(
              MESSAGES.services.unauthorized
            );
            this.timeOutControl = false;
          } else {
            let rptCity = JSON.parse(promises[1]["_body"]);
            this.createAccountService.citiesContact = rptCity;
      console.log(rptCity)
            this.setFilteredItems();
      console.log(this.searchTermCity)
            this.searchControl.valueChanges
              .debounceTime(700)
              .subscribe(search => {
                this.searching = false;
                this.setFilteredItems();
              });
          }          
        }


        this.controlLoad_s_c = true;
      })
      .catch(err => {
    if(this.ServicesProvider.loading){
      //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();        
        this.timeOutControl = false;
        this.clearTimeOut();
        this.ServicesProvider.toast(MESSAGES.services.error);
     // });    
    }
    else{
        this.timeOutControl = false;
        this.clearTimeOut();
        this.ServicesProvider.toast(MESSAGES.services.error);      
    }

      });
  }

  updateCountryByDocument(selected: any) {
    if (this.countrys.length > 0) {
      this.ServicesProvider.createLoader();
      this.ServicesProvider
        .apiGet(selected.id_pk, SERVICES.TIPODOCUMENTO_GETDEPAIS_SERV)
        .then(documents => {
          //this.updateProfileService.loading.dismiss();
          this.ServicesProvider.closeLoader();          
          this.clearTimeOut();
          if (documents["status"] == 404 || documents["status"] == 500) {
            this.timeOutControl = false;
            this.ServicesProvider.toast(MESSAGES.services.error);
          } else if (documents["status"] == 401) {
            this.ServicesProvider.toast(
              MESSAGES.services.unauthorized
            );
            this.timeOutControl = false;
          } else {
            let respuesta = JSON.parse(documents["_body"]);
            this.typeDocuments = respuesta;
          }
        })
        .catch(err => {
          //this.ServicesProvider.loading.onDidDismiss(() => {
            this.ServicesProvider.closeLoader();            
            this.timeOutControl = false;
            this.clearTimeOut();
            this.ServicesProvider.toast(MESSAGES.services.error);
          //});
        });
    }
  }

  onSearchInput(event: any) {
    console.log("eu0", this.searchTermCity)
    this.createAccountService.register2.get("municipality").setValue(null);
    this.indicativoPhone = "";
    this.citySelected = 0;
    this.searching = true;
    this.hideList = true;
  }

  setFilteredItems() {
    console.log( "eu",this.searchTermCity)
    this.cities = this.createAccountService.filterItems(
      this.searchTermCity,
      this.createAccountService.citiesContact
    );
  }

  filterCitySelected(city: any) {
   console.log("eu2", this.searchTermCity)
    this.createAccountService.register2.get("municipality").setValue(city);
    this.citySelected = city.id_pk;
    this.updateCity(city);
    this.searchTermCity = city.nombre;
    this.hideList = false;
  }

  newLoad() {
    this.timeOutControl = true;
    this.createAccountService.register2.reset();
    this.promisesCountrysAndRelationShip(
      this.ServicesProvider.apiGet("", SERVICES.UBICACION_LIST_PAIS_SERV),
      this.ServicesProvider.apiGet("", SERVICES.PARENTESCO_LIST_SERV)
    );
  }

  setRulesPhone(mask_celular,mask_fijo){

        let contmaskCell=0;
        for(let i=0; i<=mask_celular.length; i++){
          if(mask_celular[i]=="-"){
            contmaskCell++;
          }
        }
        mask_celular=mask_celular.length-contmaskCell;
        let contmask_fijo=0;
        for(let i=0; i<=mask_fijo.length; i++){
          if(mask_fijo[i]=="-"){
            contmask_fijo++;
          }      
        }
        mask_fijo=mask_fijo.length-contmask_fijo;
                console.log(mask_celular,mask_fijo);


        this.person_validation_phone={"mask_celular": mask_celular, "mask_fijo":mask_fijo}
        this.createAccountService.register2.controls["cellphone"].setValidators([Validators.required, Validators.maxLength(mask_celular),Validators.minLength(mask_celular)]);
        this.createAccountService.register2.controls["cellphone"].updateValueAndValidity();
       
        this.createAccountService.register2.controls["phone"].setValidators([Validators.maxLength(mask_fijo),Validators.minLength(mask_fijo)]);
        this.createAccountService.register2.controls["phone"].updateValueAndValidity();
  }


  saveData() {    
    /*let params = {};
    let personal_info = this.updateProfileService.updateDataPerson.value;
    let rptPatient: any = this.updateProfileService.infoPac;

    params["pac_fk"] = rptPatient.id_pk;
    params["email"] = personal_info.email;
    params["telefonodomicilio"] = personal_info.phone;
    params["celular"] = personal_info.cellphone;
    params["direccion"] = personal_info.address;
    params["pais"] = personal_info.country;
    params["estado"] = personal_info.department;
    params["ciudad"] = personal_info.municipality;

    this.updateProfileService.dataFamily.length > 0
      ? params["contactoFamiliar"] = JSON.stringify(this.updateProfileService.dataFamily) : "";

    this.ServicesProvider.createLoader("Procesando información");
    this.ServicesProvider
      .apiPost(params, SERVICES.EDITAR_PACIENTE_API_SERV)
      .then(update => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();        
        this.clearTimeOut();
        if (update["status"] == 404 || update["status"] == 500) {
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (update["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
        } else {
          let respuesta = JSON.parse(update["_body"]);

          if (respuesta.success) {
            /*this.ServicesProvider.toast(MESSAGES.updateProfile.update.success,
            "successToast");*/
           /* this.updateProfileService.getUser(rptPatient.id_pk);
            this.updateProfileService.loadFormFamily();
            this.viewCtrl.dismiss();
           /* this.patientProviderService.avatar = this.updateProfileService.avatar_profile;
            this.getUser(rptPatient.id_pk);*/
       /*   } else {
            console.log(Object.keys(respuesta.errors));
            this.updateProfileService.dataFamily.shift();
            this.ServicesProvider.toast(
               respuesta.errors[Object.keys(respuesta.errors)[0]]
            );
          }
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
          this.ServicesProvider.closeLoader();          
          this.clearTimeOut();
          this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });*/
  }

}