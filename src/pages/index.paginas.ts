export {  HomePage } from "./home/home";

export {  LoginPage } from "./login/login";

export {  PersonalInformationPage }
            from "./create_account/personal-information/personal-information";

export {  ResponsibleDataPage }
            from "./create_account/responsible-data/responsible-data";

export {  ContactDetailsPage }
            from "./create_account/contact-details/contact-details";

export {  TermsConditionsPage }
            from "./create_account/terms-conditions/terms-conditions";

export {  IndexPage as IndexCreateAccountPage }
            from "./create_account/index/index";

export { ValidateTokenPage }
            from "./create_account/validate_token/validate_token";

export { ModalTermsConditionsPage }
            from './create_account/modal-terms-conditions/modal-terms-conditions';

export { WelcomePage } from './create_account/welcome/welcome';

export { DatosSaludPage }
            from './user/health_data/datos-salud/datos-salud';
export { ModalSangre }
            from './user/health_data/modal_sangre/modal_sangre';

export { EmergenciasPage } from "./emergency/emergencias/emergencias";

export { GeolocationPage, EmergenciasRecibidasNotificacionPage } from "./emergency/index.paginas";

export { BasicPatientInformationPage } from './basic-patient-information/basic-patient-information';

export { CropImagePage } from './crop-image/crop-image';

export { ContactInformationScanPage } from './user/contact-information-scan/contact-information-scan';

export { DataHealthScanPage } from './user/data-health-scan/data-health-scan';

export { DocumentosPage }
            from './user/documents/documentos/documentos';

export { ReportsPage }
            from './user/reports/reports';

export { QrMidisPage }
          from "./user/qr-midis/qr-midis";

export { IndexUpdateProfilePage } from './user/update_profile/index-update-profile/index-update-profile';

export { UpdatePersonalInformationPage } from './user/update_profile/update-personal-information/update-personal-information';

export { PopoverMenuPage } from './popover-menu/popover-menu';

export { MisDocumentosPage } from './user/documents/mis-documentos/mis-documentos';

export { ViewDocumentsPage } from './user/documents/view-documents/view-documents';

export { FichaMedicaPage } from './user/ficha-medica/ficha-medica';

export { MidisSaludPage } from './user/midis-salud/midis-salud';

export { AppSaludPage } from './user/app-salud/app-salud';

export { AlmacenamientoPage } from './almacenamiento/almacenamiento';

export { SettingsPage } from './user/settings/settings';

export { NotificationsPage } from './user/notifications/notifications';

export { UpdateHealthcareDataPage } from './user/update_profile/update-healthcare-data/update-healthcare-data';

export { UpdateOtherDataPage } from './user/update_profile/update-other-data/update-other-data';

export { PasswordRecoveryPage } from './password-recovery/password-recovery';

export { PasswordRecoveryFormPage } from './password-recovery-form/password-recovery-form';

export { ClassificationImcPage } from './user/health_data/classification-imc/classification-imc';

export { ChangePasswordPage } from './user/change-password/change-password';

export { UpdateMedicalContactDataPage } from './user/update_profile/update-medical-contact-data/update-medical-contact-data';

export { UpdateFamilyContactDataPage } from './user/update_profile/update-family-contact-data/update-family-contact-data';

export { ModalAddMedicalPage } from './user/update_profile/modal-add-medical/modal-add-medical';

export { PopoverShareMedicalTabPage } from './user/popover-share-medical-tab/popover-share-medical-tab';

export { PopoverMidisPage } from './user/popover-midis/popover-midis';

export { ModalAddFamilyPage } from './user/update_profile/modal-add-family/modal-add-family';

export { ModalEditMedicalPage } from './user/update_profile/modal-edit-medical/modal-edit-medical';

export { ListFileHealthPage } from './user/documents/list-file-health/list-file-health';

export { HealthFilesDetailPage } from './user/documents/health-files-detail/health-files-detail';

export { UploadHealthFilesPage } from './user/documents/upload-health-files/upload-health-files';

export { SelectModulePage } from './user/documents/select-module/select-module';

export { ModalEditFamilyPage } from './user/update_profile/modal-edit-family/modal-edit-family';

export { AuthorizeAccessPage } from './authorize-access/authorize-access';

export { AuthorizeAccessSuccessfulPage }from './authorize-access-successful/authorize-access-successful';

export { UpdatePhotoProfilePage } from './user/update_profile/update-photo-profile/update-photo-profile';

export { EmergencyNotificationPage }from './emergency/emergency-notification/emergency-notification';

export { EmergenciasRecibidasPage }from './emergency/emergencias-recibidas/emergencias-recibidas';

export { PopoverHelpPage }from './popover-help/popover-help';

export { PageHelpPage }from './page-help/page-help';

