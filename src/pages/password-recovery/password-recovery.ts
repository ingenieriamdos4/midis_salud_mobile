import { Component } from "@angular/core";
import { NavController } from "ionic-angular";
import { Validators, FormBuilder, FormGroup } from "@angular/forms";
import { LoginPage, PasswordRecoveryFormPage } from "../index.paginas";
import { PatientProviderService } from "../../providers/patient/patient";
import { ServicesProvider } from "../../providers/services/services";

import { MESSAGES } from "../../config/messages";
import { SERVICES } from "../../config/url.servicios";


@Component({
  selector: "page-password-recovery",
  templateUrl: "password-recovery.html"
})
export class PasswordRecoveryPage {
  login = LoginPage;
  passwordRecoveryForm = PasswordRecoveryFormPage;
  timeOut: any;
  timeOutControl: boolean = true;
  recoveryForm: FormGroup;
  validateCodForm: FormGroup;
  isValidateCod: boolean = false;

  constructor(
    public patientProviderService: PatientProviderService,
    public ServicesProvider: ServicesProvider,
    public navCtrl: NavController,
    private formBuilder: FormBuilder
  ) {
    this.recoveryForm = this.formBuilder.group({
      email: [
        null,
        Validators.compose([
          Validators.required,
          Validators.maxLength(30),
          Validators.pattern(
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
          )
        ])
      ]
    });

    this.validateCodForm = this.formBuilder.group({
      email: [
        null,
        Validators.compose([
          Validators.required,
          Validators.maxLength(30),
          Validators.pattern(
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
          )
        ])
      ],
      cod: [null, Validators.compose([Validators.required])]
    });
  }


  clearTimeOut() {
    clearTimeout(this.timeOut);
  }

  submitForm(formValue: any) {
    this.ServicesProvider.createLoader();
    let formData = new FormData();
    formData.append("email", formValue.email);
    this.ServicesProvider
      .apiPost(formData, SERVICES.RESET_CLAVE_EMAIL_SERV)
      .then(success => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (success["status"] == 404 || success["status"] == 500) {
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else {
          let respuesta = JSON.parse(success["_body"]);
          if (respuesta.success) {
            this.ServicesProvider.toast(
              MESSAGES.passwordRecovery.message.success,
              "successToast"
            );
            //this.navCtrl.push(PasswordRecoveryFormPage);
            this.isValidateCod = true;
          } else {
            this.ServicesProvider.toast(
              MESSAGES.passwordRecovery.message.error
            );
          }
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
          this.ServicesProvider.closeLoader();
          this.clearTimeOut();
          this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  submitFormCod(formValue: any) {
    this.ServicesProvider.createLoader();
    let formData = new FormData();
    formData.append("email", formValue.email);
    formData.append("cod", formValue.cod);
    this.ServicesProvider
      .apiPost(formData, SERVICES.VERIFICAR_CODIGO_CLAVE_SERV)
      .then(success => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (success["status"] == 404 || success["status"] == 500) {
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else {
          let respuesta = JSON.parse(success["_body"]);
          if (respuesta.success) {
            this.ServicesProvider.toast(
              MESSAGES.passwordRecovery.verifyCod.success,
              "successToast"
            );
            this.recoveryForm.reset();
            this.validateCodForm.reset();
            this.navCtrl.push(PasswordRecoveryFormPage, {login_pk: respuesta.login_pk});
          } else {
            this.ServicesProvider.toast(respuesta.mensaje);
          }
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
          this.ServicesProvider.closeLoader();
          this.clearTimeOut();
          this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }
}
