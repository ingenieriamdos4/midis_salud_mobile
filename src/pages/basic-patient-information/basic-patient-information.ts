import { Component } from "@angular/core";
import { NavParams } from "ionic-angular";
import { ServicesProvider } from "../../providers/services/services";

import {
  ContactInformationScanPage,
  DataHealthScanPage
} from "../index.paginas";

@Component({
  selector: "page-basic-patient-information",
  templateUrl: "basic-patient-information.html"
})
export class BasicPatientInformationPage {
  tab1: any;
  tab2: any;

  infoPac: Array<string> = [];
  avatar: string;
  name: string;
  personal: boolean = false;
  loading: any;
  edad:any;
  constructor(public navParams: NavParams,public ServicesProvider: ServicesProvider) {
    this.tab1 = DataHealthScanPage;
    this.tab2 = ContactInformationScanPage;

    this.infoPac = this.navParams.get("paciente");
    this.personal = this.navParams.get("personal");

    let infoPaciente: any = this.infoPac;

    this.avatar = infoPaciente.get_user[0].foto+'?1='+ new Date();
    this.name =
      infoPaciente.nombres +
      " " +
      infoPaciente.apellido1 +
      " " +
      (infoPaciente.apellido2 != null ? infoPaciente.apellido2 : "");
     this.edad = ServicesProvider.getDateZero(this.infoPac["fecha_nac"]);

    // Cargar los parentescos
  }

  getAge(birth_day: string) {
    let birthday = +new Date(birth_day);
    let today = +new Date();
    let c_age = (today - birthday) / 31557600000;
    let age = Math.floor(c_age);
    return age;
  }
}
