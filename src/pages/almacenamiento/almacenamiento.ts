import { Component, ViewChild } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";
import { PatientProviderService } from "../../providers/patient/patient";
import { ServicesProvider } from "../../providers/services/services";
import { PackageProviderService } from "../../providers/package/package";
import { PackageRequestProviderService } from "../../providers/package-request/package-request";
import { PageHelpPage } from "../index.paginas";

import { MESSAGES } from "../../config/messages";
import { SERVICES, URL } from "../../config/url.servicios";
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';

@Component({
  selector: "page-almacenamiento",
  templateUrl: "almacenamiento.html",
})
export class AlmacenamientoPage {
  @ViewChild("doughnutCanvas") doughnutCanvas;
  @ViewChild("doughnutCanvasOther") doughnutCanvasOther;

  pageHelp = PageHelpPage;
  datos_val: Array<string> = [];
  datos_packs: Array<any> = [];
  buttonClicked: boolean = true;
  doughnutChart: any;
  doughnutChartOther: any;
  timeOut: any;
  timeOutControl: boolean = true;
  _almacenamiento: boolean = true;
  __almacenamiento: number;
  __almacenamiento_gb: number;
  __almacenamiento_gb_total: number;
  almacenamiento_acumulado: number;
  almacenamiento_particular: boolean = false;
  almc_adq: string;
  almc_ven: string;
  info_storage_inst: Array<any> = [];
  paymentString: any;
  options: InAppBrowserOptions = {
    hardwareback: 'yes',
    toolbar: 'yes',
    clearcache: 'yes'
  };
  id_paciente: any;
  infoStorage: any = {};
  infoEntities: any = [];
  infoInst: any = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public patientProviderService: PatientProviderService,
    public packageProviderService: PackageProviderService,
    public ServicesProvider: ServicesProvider,
    public packageRequestProviderService: PackageRequestProviderService,
    private iab: InAppBrowser
  ) {

    this.id_paciente = this.patientProviderService.getCurrentUser().id_pk;
    this.infoStorage["img_usuario"] = this.patientProviderService.getCurrentUser().get_user[0].foto;
    this.infoStorage["nombre_usuario"] =
      this.patientProviderService.getCurrentUser().nombres +
      " " +
      this.patientProviderService.getCurrentUser().apellido1 +
      " " +
      (this.patientProviderService.getCurrentUser().apellido2 != null ? this.patientProviderService.getCurrentUser().apellido2 : "");
    this.validator_info(this.id_paciente);
    this.getHistorial();
  }

  ionViewWillEnter() {
    /*if (localStorage.getItem("wizzard_almacenamiento") == undefined) {
      localStorage.setItem("wizzard_almacenamiento", "true");
      this.navCtrl.push(this.pageHelp, { 'seccion': 'almacenamiento' });
    }*/
  }

  getHistorial(){
    let params = {
      "pac_fk" : this.id_paciente
    }
    this.ServicesProvider.createLoader();
    this.ServicesProvider.apiPost(params, '/hist_lic_vinculacion_usuario_api')
      .then(historial => {
        this.clearTimeOut();
        this.ServicesProvider.closeLoader();
        if (historial["status"] == 404 || historial["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(
            MESSAGES.services.error
          );
        } else if (historial["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(historial["_body"]);
          this.infoEntities = respuesta.entidades_responsables;
          this.infoInst = respuesta.instituciones_salud;
        }
      })
      .catch(err => {
        this.clearTimeOut();
        this.ServicesProvider.closeLoader();
        this.ServicesProvider.toast(MESSAGES.services.error, err);
      });
  }

  renovarAlmacenamiento() {
    let options: InAppBrowserOptions = {
      hardwareback: 'yes'
    };
    this.iab.create(URL + '/pagosusuario?midis=' + this.id_paciente, '_blank', options);
  }

  ionViewDidLoad() { }

  clearTimeOut() {
    clearTimeout(this.timeOut);
  }

  public onButtonClick() {
    this.buttonClicked = !this.buttonClicked;
  }

  validator_info(id_paciente: number) {
    let params = { "id_paciente": id_paciente };
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiPost(params, SERVICES.CONSULTAR_PACIENTE_API_SERV)
      .then(validator => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (validator["status"] == 404 || validator["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else {
          let respuesta = JSON.parse(validator["_body"]);
          this.infoStorage["consumo"] = (JSON.stringify(respuesta.almacenamiento)).substr(0, 5);
          this.infoStorage["fecha_ini_usuario"] = respuesta.almacenamiento_fecha_adquirido;
          this.infoStorage["fecha_fin_usuario"] = respuesta.almacenamiento_fecha_vencimiento;
        }
      })
      .catch(err => {
        this.ServicesProvider.closeLoader()
        this.clearTimeOut();
        this.timeOutControl = false;
        this.ServicesProvider.toast(
          MESSAGES.services.error + err
        );
      });
  }

  formatDate(date: string) {
    let _date = date.split("-");
    return _date[2] + "-" + _date[1] + "-" + _date[0];
  }

  loadNew() {
    this.timeOutControl = true;
    let id_paciente = this.patientProviderService.getCurrentUser().id_pk;
    this.validator_info(id_paciente);
  }
}
