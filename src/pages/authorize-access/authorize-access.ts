import { Component } from "@angular/core";
import { NavController } from "ionic-angular";
/* Plugins */
import { BarcodeScanner, BarcodeScannerOptions } from "@ionic-native/barcode-scanner";
import { PatientProviderService } from "../../providers/patient/patient";
import { ServicesProvider } from "../../providers/services/services";
import { AuthorizeAccessSuccessfulPage, QrMidisPage, PageHelpPage } from "../index.paginas";
import { MESSAGES } from "../../config/messages";
import { SERVICES } from "../../config/url.servicios";
@Component({
  selector: "page-authorize-access",
  templateUrl: "authorize-access.html"
})
export class AuthorizeAccessPage {
  pageHelp = PageHelpPage;

  qrMidis = QrMidisPage;

  extMail: boolean = false;
  mail: string;

  constructor(
    public patientProviderService: PatientProviderService,
    private barcodeScanner: BarcodeScanner,
    public navCtrl: NavController,
    public ServicesProvider: ServicesProvider
  ) {

    /*if (localStorage.getItem("wizzard_autorizar_acceso") == undefined) {
      localStorage.setItem("wizzard_autorizar_acceso", "true");
      this.navCtrl.push(this.pageHelp, { 'seccion': 'autorizar_acceso' });
    }*/

  }
  onInputTime(mail: any) {
    this.mail = mail;
  }

  sendMailExt(type: any) {
    if (type == 0) {
      this.extMail = !this.extMail;
    }
    else if (type == 1) {
      if (this.mail != "" && this.mail != undefined) {
        var ER = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (ER.test(this.mail.toLowerCase())) {
          let params = {
            "pac_fk": this.patientProviderService.getCurrentUser().id_pk,
            "correo_prof_externo": this.mail.toLowerCase()
          }
          this.ServicesProvider.createLoader();
          this.ServicesProvider
            .apiPost(params, SERVICES.SEND_MAIL_PROF_EXT)
            .then(state => {
              this.ServicesProvider.closeLoader();
              if (state["status"] == 404 || state["status"] == 500) {
              } else if (state["status"] == 401) {
                this.ServicesProvider.toast(
                  MESSAGES.services.unauthorized
                );
              } else {
                let respuesta = JSON.parse(state["_body"]);
                if (respuesta.success == true) {
                  this.ServicesProvider.toast("Correo electrónico enviado correctamente.", "successToast");
                }
                else {
                  this.ServicesProvider.toast("Error al enviar el correo. " + respuesta.msg);
                }
              }
            })
            .catch(err => {
              this.ServicesProvider.closeLoader();
              this.ServicesProvider.toast("Error al enviar el correo.");
            });
        }
        else{
          this.ServicesProvider.toast("Ingrese un correco electrónico válido");
        }
      }
      else {
        this.ServicesProvider.toast("Ingrese un correo electrónico al cual desea enviar la información.");
      }
    }
  }

  scanQrMedic() {

    const options: BarcodeScannerOptions = {
      prompt: MESSAGES.codQr.prompt.auth
    }

    this.barcodeScanner
      .scan(options)
      .then(
        barcodeData => {
          if (barcodeData.cancelled == false && barcodeData.text != null) {
            this.autorizationPatient(
              this.patientProviderService.getCurrentUser().id_pk,
              JSON.stringify(barcodeData.text)
            );
          }
        },
        err => {
          this.ServicesProvider.toast(
            MESSAGES.codQr.message.close
          );
        }
      )
      .catch(err => {
        this.ServicesProvider.toast(MESSAGES.codQr.message.error);
      });
  }

  autorizationPatient(pac_fk: number, codQr: string) {
    let formData = new FormData();
    formData.append("codigoQr", codQr);
    formData.append("pac_fk", pac_fk.toString());

    this.ServicesProvider.createLoader();
    let timeOut = setTimeout(() => {
      //this.ServicesProvider.loading.dismiss();
      this.ServicesProvider.closeLoader();
      this.ServicesProvider.toast(MESSAGES.services.timeOut);
    }, MESSAGES.loading.time);

    this.ServicesProvider
      .apiPost(formData, SERVICES.AUTORIZACION_PACIENTE_SERV)
      .then(state => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        clearTimeout(timeOut);
        if (state["status"] == 404 || state["status"] == 500) {
          this.ServicesProvider.toast(MESSAGES.codQr.message.invalid);
        } else {
          let respuesta = JSON.parse(state["_body"]);
          if (respuesta.success) {
            this.ServicesProvider.toast(
              MESSAGES.autorizationPatien.message.success,
              "successToast"
            );
            this.navCtrl.push(AuthorizeAccessSuccessfulPage, { profesional: respuesta.profesional });
          } else {
            this.ServicesProvider.toast(
              MESSAGES.autorizationPatien.message.error
            );
          }
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();
        clearTimeout(timeOut);
        this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }


}
