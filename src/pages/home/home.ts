import { Component } from "@angular/core";
import { NavController, NavParams, Platform } from "ionic-angular";
import { PatientProviderService } from "../../providers/patient/patient";
import { ServicesProvider } from "../../providers/services/services";
import { SERVICES } from "../../config/url.servicios";

/* Plugins */
import {
  BarcodeScanner,
  BarcodeScannerOptions
} from "@ionic-native/barcode-scanner";

import {
  LoginPage,
  DatosSaludPage,
  AuthorizeAccessPage,
  NotificationsPage,
  QrMidisPage,
  EmergenciasPage,
  BasicPatientInformationPage,
  PageHelpPage
} from "../index.paginas";

import { IndexAntecedentPage } from "../user/antecedent/index.paginas";

/* Mensajes de la app */
import { MESSAGES } from "../../config/messages";

declare var cordova: any;

@Component({
  selector: "page-home",
  templateUrl: "home.html"
})
export class HomePage {
  rootPage: any;
  pageHelp = PageHelpPage;
  indexAntecedent = IndexAntecedentPage;
  authorizeAccess = AuthorizeAccessPage;
  notifications = NotificationsPage;
  datosSalud = DatosSaludPage;
  login = LoginPage;
  qrMidis = QrMidisPage;
  emergencias = EmergenciasPage;
  name: string;
  codQr: string;
  avatar: string;
  timeOut: any;
  timeOutControl: boolean = true;
  storageDirectory: string = "";
  menu_home: string;
  num_notify: any;

  imagesPublicity: Array<string> = [];

  constructor(
    public navCtrl: NavController,
    public patientProviderService: PatientProviderService,
    public ServicesProvider: ServicesProvider,
    private barcodeScanner: BarcodeScanner,
    public platform: Platform,
    public navParams: NavParams

  ) {

    /*if (localStorage.getItem("wizzard_home") == undefined) {
      this.menu_home = "menu_home";
      localStorage.setItem("wizzard_home", "true");
      this.navCtrl.push(this.pageHelp, { 'seccion': 'home' });
    }*/

    this.getNofify();
    this.getPublicity();

  }

  getNofify() {
    let params = { "pac_fk": this.patientProviderService.getCurrentUser().id_pk };
    let count = 0;
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiPost(params, SERVICES.NOTIFICATIONS_SERV)
      .then(data => {
        this.ServicesProvider.closeLoader();
        if (data["status"] == 404 || data["status"] == 500) {
          this.ServicesProvider.toast(MESSAGES.services.error);
        }
        else if (data["status"] == 401) {
          this.ServicesProvider.toast(MESSAGES.services.unauthorized);
        }
        else {
          let respuesta = JSON.parse(data["_body"]);
          count += respuesta.prescripciones_cargadas.length;
          count += respuesta.recomendaciones.length;
          count += respuesta.preparaciones.length;
          count += respuesta.cuidados.length;
          this.num_notify = count + 1;
        }
      })
      .catch(error => {
        this.ServicesProvider.closeLoader();
        this.ServicesProvider.toast(MESSAGES.services.error + error);
      });
  }

  ionViewDidLeave() {
    localStorage.setItem("wizzard_home", "true");
  }

  clearTimeOut() {
    clearTimeout(this.timeOut);
  }

  ionViewWillEnter() {
    if (this.navCtrl.getPrevious()) {
      if (this.navParams.get('accion') == "qr") {
        this.navCtrl.getPrevious().data = {};
        this.scan();
      }
    }
    console.log("HOME::::" + this.patientProviderService.getCurrentUser());

  }

  scan() {

    if (localStorage.getItem("wizzard_codigo_qr") == undefined) {
      localStorage.setItem("wizzard_codigo_qr", "true");
      this.navCtrl.push(this.pageHelp, { 'seccion': 'codigo_qr', 'accion': 'qr' });
    }
    else {

      const options: BarcodeScannerOptions = {
        prompt: MESSAGES.codQr.prompt.message
      };
      this.barcodeScanner
        .scan(options)
        .then(
          barcodeData => {
            if (barcodeData.cancelled == false && barcodeData.text != null) {
              let idEncrypt = barcodeData.text
                .split("/")
                .slice(-1)
                .pop();

              this.getIdPacienteWithQr(idEncrypt);
            }
          },
          err => {
            this.ServicesProvider.toast(
              MESSAGES.codQr.message.close
            );
          }
        )
        .catch(err => {
          this.ServicesProvider.toast(MESSAGES.codQr.message.error);
        });
    }
  }

  getIdPacienteWithQr(id_encrypt: string) {
    let params = { "qr_paciente": id_encrypt };
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiPost(params, "/desencriptar_id_paciente")
      .then(stateE => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();

        this.clearTimeOut();
        if (stateE["status"] == 404 || stateE["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (stateE["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(stateE["_body"]);

          if (respuesta.error != undefined) {
            this.ServicesProvider.toast(respuesta);
          } else if (respuesta.id_paciente == "Código QR Invalido") {
            this.ServicesProvider.toast(respuesta.id_paciente);
          } else {
            this.getPatientInformationByEncryption(respuesta.id_paciente);
            this.ServicesProvider.toast(MESSAGES.codQr.message.invalid);
          }
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();

        this.clearTimeOut();
        this.timeOutControl = false;
        this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  getPatientInformationByEncryption(id_paciente: number) {
    let params = { "id_paciente": id_paciente };
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiPost(params, SERVICES.INF_BASICA_OTROS_PACIENTES)
      .then(state => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (state["status"] == 404 || state["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (state["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(state["_body"]);
          this.navCtrl.push(BasicPatientInformationPage, {
            paciente: respuesta
          });
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();

        this.clearTimeOut();
        this.timeOutControl = false;
        this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  getPublicity() {
    this.ServicesProvider
      .apiGet("", SERVICES.PUBLICIDADES_MOBILE_SERV)
      .then(state => {
        if (state["status"] == 404 || state["status"] == 500) {
        } else if (state["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
        } else {
          let respuesta = JSON.parse(state["_body"]);
          this.imagesPublicity = respuesta;
          console.log(this.imagesPublicity);
          console.log("Repuesta", respuesta);
        }
      })
      .catch(err => { });
  }
}
