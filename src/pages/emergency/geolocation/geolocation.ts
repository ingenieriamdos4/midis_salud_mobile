import { Component } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";
import { Geolocation, GeolocationOptions } from "@ionic-native/geolocation";
import { PatientProviderService } from "../../../providers/patient/patient";
import { ServicesProvider } from "../../../providers/services/services";

/* Mensajes de la app */
import { MESSAGES } from "../../../config/messages";

@Component({
  selector: "page-geolocation",
  templateUrl: "geolocation.html"
})
export class GeolocationPage {
  lat: number;
  lng: number;
  timeOut: any;
  timeOutControl: boolean = true;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public geolocation: Geolocation,
    public patientProviderService: PatientProviderService,
    public ServicesProvider: ServicesProvider
  ) {

    this.lat = this.navParams.get('latitude');
    this.lng = this.navParams.get('longitude');
    //this.getPosition();
  }

  ionViewDidLoad() {}

  createLoader(time: number = MESSAGES.loading.time ) {
    this.ServicesProvider.createLoader();
    this.timeOut = setTimeout(() => {
      this.timeOutControl = false;
      //this.ServicesProvider.loading.dismiss();
      this.ServicesProvider.closeLoader();
      this.ServicesProvider.toast(MESSAGES.geolocation.position.error);
    }, time);
  }

  clearTimeOut() {
    clearTimeout(this.timeOut);
  }

  getPosition() {
    this.createLoader(60000);
    let options: GeolocationOptions = {
      enableHighAccuracy: false,
      timeout: 60000
    };

    this.geolocation
      .getCurrentPosition(options)
      .then(resp => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.lat = resp.coords.latitude;
        this.lng = resp.coords.longitude;
      })
      .catch(error => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
          this.ServicesProvider.closeLoader();
          this.clearTimeOut();
          this.timeOutControl = false;
          this.ServicesProvider.toast(
            MESSAGES.geolocation.position.error
          );
        //});
      });
  }

  load() {
    this.timeOutControl = true;
    this.getPosition();
  }
}
