export { GeolocationPage } from "./geolocation/geolocation";
export { EmergenciasPage } from "./emergencias/emergencias";
export { EmergencyNotificationPage } from "./emergency-notification/emergency-notification";
export { EmergenciasRecibidasPage } from "./emergencias-recibidas/emergencias-recibidas";
export { EmergenciasRecibidasNotificacionPage } from "./emergencias-recibidas-notificacion/emergencias-recibidas-notificacion";
