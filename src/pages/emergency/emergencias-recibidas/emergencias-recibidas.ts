import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { DatePipe } from '@angular/common';

/* Importing providers */
import { PatientProviderService } from "../../../providers/patient/patient";
import { ServicesProvider } from "../../../providers/services/services";
import { SERVICES } from "../../../config/url.servicios";


/* Importing pages */

import { EmergenciasRecibidasNotificacionPage } from "../../index.paginas";

/* App messages */
import { MESSAGES } from "../../../config/messages";

@Component({
  selector: 'page-emergencias-recibidas',
  templateUrl: 'emergencias-recibidas.html',
})
export class EmergenciasRecibidasPage {

  emergenciasRecibidas: Array<any> = [];
  id_paciente: number;
  user = JSON.parse(localStorage.getItem("user"));
  userImg: string;
  userName: string;
  userInfo: Array<any> = [];
  userInfoParse: Array<any> = [];
  emerNum: number;
  fecha: Date = new Date();
  fecha_inicio: string;
  fecha_fin: string;

  emergenciasRecibidasNotificacion = EmergenciasRecibidasNotificacionPage;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public patientProviderService: PatientProviderService,
    public ServicesProvider: ServicesProvider,
    public datepipe: DatePipe
  ) {
    this.id_paciente = this.patientProviderService.getCurrentUser().id_pk;
    this.fecha.setDate(this.fecha.getDate() - 7);
    this.fecha_fin = new Date().toJSON().slice(0, 10).replace(/-/g, "-");
    this.fecha_inicio = this.fecha.toJSON().slice(0, 10).replace(/-/g, "-");

    this.userInfo = this.navParams.get('respuesta');
    this.emerNum = this.navParams.get('longitud');

  }

  searchByFilter() {
    console.log(this.fecha_inicio);
    console.log(this.fecha_fin);
    this.getEmergenciasReportadas(this.id_paciente);
  }

  getEmergenciasReportadas(id_paciente: number) {
    this.ServicesProvider.createLoader();
    //let date = new Date();
    // let fecha_ini_ser = date.getDate(); /// 7 meses
    // let fecha_fin_ser = date.getDate();
    // let fecha_ini_ser = "20/01/2018";
    // let fecha_fin_ser = "25/05/2018";
    // if(this.fecha_inicio != null && this.fecha_fin != null){
    //   fecha_ini_ser = this.fecha_inicio;
    //   fecha_fin_ser = this.fecha_fin;
    // }
    //console.log(this.datepipe.transform(this.fecha_inicio, 'dd/MM/yyyy'));
    //console.log(this.datepipe.transform(this.fecha_fin, 'dd/MM/yyyy'));
    let params = { "pac_fk": this.user.id_pk, "fec_ini": this.datepipe.transform(this.fecha_inicio, 'dd/MM/yyyy'), "fec_fin": this.datepipe.transform(this.fecha_fin, 'dd/MM/yyyy') }
    //let params = {"pac_fk": this.user.id_pk , "fec_ini": this.fecha_inicio, "fec_fin":this.fecha_fin}
    this.ServicesProvider
      .apiPost(params, SERVICES.LISTAR_EMERG_RECIBIDAS_SERV)
      .then(state => {
        if (state["status"] == 404 || state["status"] == 500) {
        } else if (state["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
        } else {
          let respuesta = JSON.parse(state["_body"]);
          this.emergenciasRecibidas = respuesta;
          if (this.emergenciasRecibidas.length == 0) {
            this.ServicesProvider.toast("No se han encontrado emergencias en las fechas buscadas");
          }
          this.userInfo = respuesta;
          this.emerNum = respuesta.length;
          this.userInfo = respuesta;
          // for(let i=0;i<this.emergenciasRecibidas.length;i++){
          //   console.log("El simple: "+this.emergenciasRecibidas[i].pac_fk);
          //   this.getUserInfo(this.emergenciasRecibidas[i].pac_fk);
          // }
          // respuesta.foreach(emergencia =>
          // {
          //   this.getUserInfo(emergencia.pac_fk);
          // });

        }
      })
      .catch(err => {
        console.log(err);
      });
    //this.ServicesProvider.loading.dismiss();
    this.ServicesProvider.closeLoader();
  }

  getUserInfo(id_paciente: number) {
    let data = { "id_paciente": id_paciente };
    this.ServicesProvider
      .apiPost(data, SERVICES.CONSULTAR_PACIENTE_API_SERV)
      .then(state => {
        if (state["status"] == 404 || state["status"] == 500) {
          this.ServicesProvider.toast(MESSAGES.services.error);
          return 0;
        } else if (state["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          return 0;
        } else {
          let respuesta = JSON.parse(state["_body"]);
          this.userImg = respuesta.get_user[0].foto;
          this.userName = respuesta.nombres + ' ' + respuesta.apellido1;
          this.userInfo.push({
            'user_img': this.userImg,
            'user_name': this.userName,
            'user_lat': this.emergenciasRecibidas[0].latitud,
            'user_lon': this.emergenciasRecibidas[0].longitud,
            'user_id': this.emergenciasRecibidas[0].id_pk,
            'user_date': this.emergenciasRecibidas[0].created_at
          });

          console.log("El array: " + this.userInfo);

          return 1;
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

}
