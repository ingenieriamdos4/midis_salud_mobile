import { Component } from "@angular/core";
import {
  NavController,
  NavParams,
  Platform,
  AlertController
} from "ionic-angular";
import { OneSignal } from "@ionic-native/onesignal";
import { DatePipe } from '@angular/common';
import {
  Geolocation,
  GeolocationOptions,
  Coordinates
} from "@ionic-native/geolocation";
import { GeolocationPage, EmergenciasRecibidasPage } from "../index.paginas";
import { PatientProviderService } from "../../../providers/patient/patient";
import { ServicesProvider } from "../../../providers/services/services";
import { PushnotificationProvider } from "../../../providers/pushnotification/pushnotification";
import { SERVICES } from "../../../config/url.servicios";
import {
  BasicPatientInformationPage,
  PageHelpPage
} from "../../index.paginas";
import { Diagnostic } from "@ionic-native/diagnostic";
/* Mensajes de la app */
import { MESSAGES } from "../../../config/messages";

@Component({
  selector: "page-emergencias",
  templateUrl: "emergencias.html"
})
export class EmergenciasPage {
  idDeUsuario: any;
  contactDevicesId: Array<string> = [];
  cell_phones: Array<string> = [];
  error: any;
  latitude: any;
  longitude: any;
  timeOut: any;
  timeOutControl: boolean = true;

  isDisabledUrgencia: boolean = false;
  isDisabledShowInfo: boolean = false;
  location: Coordinates;

  moreOneUser: boolean = false;
  isShowListUser: boolean = false;
  SELECTUSERSQLITE: any;

  emergenciasRecibidas = EmergenciasRecibidasPage;
  pageHelp = PageHelpPage;

  enableButton: boolean = false;
  respRecibidas: any;
  longRecibidas: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public oneSignal: OneSignal,
    public geolocation: Geolocation,
    public patientProviderService: PatientProviderService,
    public ServicesProvider: ServicesProvider,
    public PushnotificationProvider: PushnotificationProvider,
    public platform: Platform,
    private diagnostic: Diagnostic,
    private alertCtrl: AlertController,
    public datepipe: DatePipe
  ) {
    
    this.navParams.get('count') ? this.moreOneUser = true : this.moreOneUser = false;
    if (this.platform.is("cordova")) {
      this.getAllUsers();
    }
    this.getEmergencias();
    this.showInformationPac(false);
    //this.getLocation();
  }

  getEmergencias() {
    let currentUser = JSON.parse(localStorage.getItem("user"));
    let fecha: Date = new Date();
    let fecha_inicio: string;
    let fecha_fin: string;
    fecha.setDate(fecha.getDate() - 7);
    fecha_fin = new Date().toJSON().slice(0, 10).replace(/-/g, "-");
    fecha_inicio = fecha.toJSON().slice(0, 10).replace(/-/g, "-");
    if (currentUser != null) {
      this.ServicesProvider.createLoader();
      let params = { "pac_fk": currentUser.id_pk, "fec_ini": this.datepipe.transform(fecha_inicio, 'dd/MM/yyyy'), "fec_fin": this.datepipe.transform(fecha_fin, 'dd/MM/yyyy') }
      this.ServicesProvider
        .apiPost(params, SERVICES.LISTAR_EMERG_RECIBIDAS_SERV)
        .then(state => {
          this.ServicesProvider.closeLoader();
          if (state["status"] == 404 || state["status"] == 500) {
          } else if (state["status"] == 401) {
            this.ServicesProvider.toast(
              MESSAGES.services.unauthorized
            );
          } else {
            let respuesta = JSON.parse(state["_body"]);
            this.respRecibidas = respuesta;
            this.longRecibidas = respuesta.length;
            if (respuesta.length > 0) {
              this.enableButton = true;
            }
          }
        })
        .catch(err => {
          this.ServicesProvider.closeLoader();
          console.log(err);
        });
    }
  }

  async getLocation() {
    await this.platform.ready();
    const { coords } = await this.geolocation.getCurrentPosition();
    this.location = coords;
    console.log("Cordenadas", this.location);
  }



  clearTimeOut() {
    clearTimeout(this.timeOut);
  }

  /* Mostrar mi información, Estará deshabilitada si no se cumple por lo menos una
    de las siguientes condiciones:
    Exista registrada información de por lo menos un
    contacto familiar.
    Exista registrada información de por lo menos un
    contacto médico.
    Exista registro de por lo menos un antecedente.
    Se haya seleccionado la seguridad social y la seguridad
    laboral (No cuenta como válido si ambas se
    encuentran en la opción “No tengo”).
    Se hayan registrado los campos de grupo sanguíneo,
    RH, peso, estatura en datos de salud. */
  ionViewDidLoad() {
    let currentUser = this.patientProviderService.getCurrentUser();
    if (currentUser != undefined && this.moreOneUser == false) {
      this.verifyUserInfo();
    }
    else if (currentUser != undefined && this.moreOneUser == true) {
      this.verifyExternalUserInfo();
    }
  }

  ionViewDidEnter() {
    /*if (localStorage.getItem("wizzard_emergencia") == undefined) {
      localStorage.setItem("wizzard_emergencia", "true");
      this.navCtrl.push(this.pageHelp, { 'seccion': 'emergencia' });
    }*/
  }

  alertPlayerId() {
    alert(localStorage.getItem("playerId"));
  }

  getAllUsers() {
    this.patientProviderService
      .getAllUser()
      .then(users => {
        this.patientProviderService.users = users;
      })
      .catch(error => {
        console.error(error);
      });
  }

  checkLocation() {
    this.cell_phones = [];
    this.contactDevicesId = [];

    this.diagnostic
      .isLocationEnabled()
      .then(isAvailable => {
        if (isAvailable) {
          this.alertConfirmeEmergency();
        } else {
          this.ServicesProvider.toast(
            MESSAGES.geolocation.message.error
          );
        }
      })
      .catch(e => {
        this.ServicesProvider.toast(
          MESSAGES.geolocation.message.error
        );
      });
    //this.platform.ready().then(readySource => {

    //});
  }

  alertConfirmeEmergency() {
    let alert = this.alertCtrl.create({
      title: "Emergencia",
      message: "¿Esta seguro que desea enviar la alerta?",
      buttons: [
        {
          text: "Cancelar",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        },
        {
          text: "Confirmar",
          handler: () => {
            this.sendNotificaction();
          }
        }
      ]
    });
    alert.present();
  }

  sendNotificaction() {
    // Método para traer todos los datos del usuario
    let currentUser = JSON.parse(localStorage.getItem("user"));
    let params = { "id_paciente": currentUser.id_pk };
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiPost(params, SERVICES.CONSULTAR_PACIENTE_API_SERV)
      .then(data => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (data["status"] == 404 || data["status"] == 500) {
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (data["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
        } else {
          let user = JSON.parse(data["_body"]);
          let familyContacts = user.get_datos_familiares;
          let medicalContacts = user.get_datos_asist_profesional;
          for (let familyContact of familyContacts) {
            if (familyContact.id_dispositivo != null && familyContact.id_dispositivo != "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx") {
              this.contactDevicesId.push(familyContact.id_dispositivo);
            }
            //if(this.cell_phones.indexOf(familyContact.celular) != -1){
            this.cell_phones.push(familyContact.celular);
            //}
          }
          for (let medicalContact of medicalContacts) {
            if (medicalContact.id_dispositivo != null && medicalContact.id_dispositivo != "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx") {
              this.contactDevicesId.push(medicalContact.id_dispositivo);
            }
            //if(this.cell_phones.indexOf(medicalContact.tlf_celular) != -1){
            this.cell_phones.push(medicalContact.tlf_celular);
            //}
          }

          let toastMsj = "";
          if (this.contactDevicesId.length == 0 || !this.contactDevicesId) {
            toastMsj = "Los contactos que has configurado no tienen una cuenta activa en Midis App Salud.";
            if (this.cell_phones.length > 0) {
              toastMsj = toastMsj + "\nEnviaremos mensaje de texto a los contactos sin Midis que has configurado.";
              //this.sendMessagesText();
            }
          }
          if (toastMsj != "") {
            this.ServicesProvider.toast(toastMsj, "warningToast");
          }

          this.getCoords(); //Se obtiene la unicación y a partir de ella se envían sms y push.

        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  getCoords() {
    this.ServicesProvider.createLoader();
    let timeOut = setTimeout(() => {
      //this.ServicesProvider.loading.dismiss();
      this.ServicesProvider.closeLoader();
    }, 60000);

    let options: GeolocationOptions = {
      enableHighAccuracy: true,
      timeout: 60000,
      maximumAge: 0
    };

    this.geolocation
      .getCurrentPosition(options)
      .then(resp => {
        this.ServicesProvider.closeLoader();
        clearTimeout(timeOut);
        this.latitude = resp.coords.latitude;
        this.longitude = resp.coords.longitude;
        this.sendPushNotification();
        this.sendMessagesText();
        //this.saveEmergency();
      })
      .catch(error => {
        console.log("Error", error);
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();
        this.ServicesProvider.toast(
          MESSAGES.geolocation.position.error
        );
        this.contactDevicesId = [];
        //});
      });
  }

  saveEmergency() {
    let currentUser = JSON.parse(localStorage.getItem("user"));
    // let params = "?pac_fk=" + currentUser.id_pk + "&latitud=" + this.latitude + "&longitud=" + this.longitude;
    let params = { "pac_fk": currentUser.id_pk, "latitud": this.latitude, "longitud": this.longitude, "id_dispositivo": this.contactDevicesId[0] };
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiPost(params, SERVICES.REGISTRAR_EMERGENCIA_SERV)
      .then(state => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (state["status"] == 404 || state["status"] == 500) {
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (state["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
        } else {
          let respuesta = JSON.parse(state["_body"]);
          console.log(respuesta);
          this.ServicesProvider.toast(
            "la emergencia se almacenó correctamente",
            "successToast"
          );
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  sendPushNotification() {
    let notificationObj: any = {
      small_icon: "icon-midis",
      large_icon: "icon-midis",
      headings: {
        en: JSON.parse(localStorage.getItem("user")).nombres
      },
      contents: {
        en: "Se encuentra en situación de emergencia, toca para ver su ubicación."
      },
      data: {
        longitud: this.longitude,
        latitud: this.latitude,
        user: JSON.parse(localStorage.getItem("user")).id_pk
      },
      content_available: true,
      //this.contactDevicesId
      include_player_ids: this.contactDevicesId
      // include_player_ids: [
      //   "2f2a4e2f-b821-44d4-9dd1-7e685a49b0e4",
      //   "cf519063-f203-431e-8350-92a87c3fb25a",
      //   "ec986ebf-24f3-4c4b-80d0-3b18b5644c80",
      //   "5185e0f0-8dfb-46c7-b634-4418049423c0",
      //   "9a76e8c1-7305-4c7d-ae17-28e812e7fdb2",
      //   "c84d0f9e-d5f5-42a3-aa89-3bc8d58b7f8b"
      // ]
    };

    this.oneSignal
      .postNotification(notificationObj)
      .then(success => {
        this.saveEmergency();
        this.ServicesProvider.toast(MESSAGES.notification.message.success, "successToast");
        this.contactDevicesId = [];
        this.navCtrl.push(GeolocationPage, {
          latitude: this.latitude,
          longitude: this.longitude
        });
      })
      .catch(error => {
        this.contactDevicesId = [];
        //this.ServicesProvider.toast(MESSAGES.notification.message.error + JSON.stringify(error));
        this.ServicesProvider.toast(MESSAGES.notification.message.error);
        this.navCtrl.push(GeolocationPage);
      });
  }

  sendMessagesText() {
    this.ServicesProvider.createLoader();
    let message =
      "MIDIS APP SALUD Te informa que: " +
      JSON.parse(localStorage.getItem("user")).nombres +
      " Se encuentra en situacion de emergencia. Su ubicacion es: "
      + "https://www.google.com/maps/@" + this.latitude + "," + this.longitude + ",20z";
    let params = { "nros_tlf": this.cell_phones.toString(), "mensaje": message };
    this.ServicesProvider
      .apiPost(params, SERVICES.ENVIO_MENSAJE_SMS_SERV)
      .then(state => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (state["status"] == 404 || state["status"] == 500) {
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (state["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
        } else {
          let respuesta = state["_body"];
          let flag = true;

          let list = respuesta.split("<br>");
          if (list.length == 1) {
            if (list[0] < 0) {
              if (list[0] == -1) {
                console.log("Credenciales del servicio de mensajería erróneas.");
              }
              else {
                console.log("Error del servicio de mensajería " + list[0]);
              }
            }
            return;
          }
          list.forEach(element => {
            let eachOne = element.split(",");
            if (eachOne.length > 1) {
              if (parseInt(eachOne[1]) < 0) {
                alert("No es posible enviar el mensaje a: " + eachOne[0] + "\n" + eachOne[1]);
                flag = false;
              }
            }
          });

          if (flag) {
            this.ServicesProvider.toast(
              "Se ha enviado un mensaje de texto a todos los contactos",
              "successToast"
            );
          } else {
            this.ServicesProvider.toast(
              "No se pudo enviar el mensaje de texto a todos sus contactos",
              "warningToast"
            );
          }
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();
        console.log("err text", err);
        this.clearTimeOut();
        this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }
  verifyExternalUserInfo() {
    this.ServicesProvider.createLoader();
    let currentUser = this.patientProviderService.getCurrentUser().id_pk;
    let params = { "id_paciente": currentUser };
    this.ServicesProvider
      .apiPostExternalUrl(params, SERVICES.INF_BASICA_OTROS_PACIENTES)
      .then(state => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (state["status"] == 404 || state["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (state["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(state["_body"]);
          localStorage.setItem("user", JSON.stringify(respuesta));
          let currenUser = respuesta;
          if (
            currenUser.get_datos_familiares.length == 0 &&
            currenUser.get_datos_asist_profesional.length == 0
          ) {
            this.isDisabledUrgencia = true;
          }

          if (currenUser.get_datos_familiares.length != 0) {
            this.isDisabledShowInfo = false;
          } else if (currenUser.get_datos_asist_profesional.length != 0) {
            this.isDisabledShowInfo = false;
          } else if (
            currenUser.get_eps.length != 0 &&
            currenUser.get_arl.length != 0
          ) {
            this.isDisabledShowInfo = false;
          } else if (
            currenUser.peso != null &&
            currenUser.estatura != null &&
            currenUser.tsa_fk != null
          ) {
            this.isDisabledShowInfo = false;
          } else {
            this.isDisabledShowInfo = true;
          }
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.timeOutControl = false;
        this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  verifyUserInfo() {
    this.ServicesProvider.createLoader();
    let currentUser = this.patientProviderService.getCurrentUser().id_pk;
    let params = { "id_paciente": currentUser };
    this.ServicesProvider
      .apiPost(params, SERVICES.CONSULTAR_PACIENTE_API_SERV)
      .then(state => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (state["status"] == 404 || state["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (state["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(state["_body"]);
          localStorage.setItem("user", JSON.stringify(respuesta));
          let currenUser = respuesta;
          if (
            currenUser.get_datos_familiares.length == 0 &&
            currenUser.get_datos_asist_profesional.length == 0
          ) {
            this.isDisabledUrgencia = true;
          }

          if (currenUser.get_datos_familiares.length != 0) {
            this.isDisabledShowInfo = false;
          } else if (currenUser.get_datos_asist_profesional.length != 0) {
            this.isDisabledShowInfo = false;
          } else if (
            currenUser.get_eps.length != 0 &&
            currenUser.get_arl.length != 0
          ) {
            this.isDisabledShowInfo = false;
          } else if (
            currenUser.peso != null &&
            currenUser.estatura != null &&
            currenUser.tsa_fk != null
          ) {
            this.isDisabledShowInfo = false;
          } else {
            this.isDisabledShowInfo = true;
          }
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.timeOutControl = false;
        this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  showInformationPac(showInfo: boolean) {

    let currentUser;
    if (this.moreOneUser) {
      if (this.navParams.get('count') == 1) {
        this.ServicesProvider.createLoader();
        currentUser = this.patientProviderService.users[0]['pac_fk'];
      } else if (this.navParams.get('count') > 1) {
        if (!this.SELECTUSERSQLITE) {
          this.isShowListUser = true;
          return;
        } else {
          this.ServicesProvider.createLoader();
          currentUser = this.SELECTUSERSQLITE.pac_fk;
        }
      }
    } else {
      this.ServicesProvider.createLoader();
      currentUser = this.patientProviderService.getCurrentUser().id_pk;
    }

    let params = { "id_paciente": currentUser };
    this.ServicesProvider
      .apiPost(params, SERVICES.INF_BASICA_OTROS_PACIENTES)
      .then(state => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (state["status"] == 404 || state["status"] == 500) {
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (state["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
        } else {
          let respuesta = JSON.parse(state["_body"]);

          if (showInfo) {
            this.navCtrl.push(BasicPatientInformationPage, {
              paciente: respuesta,
              personal: true
            });
          }
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  deleteUser(user: any, index) {
    this.patientProviderService
      .deleteUser(user)
      .then(response => {
        this.patientProviderService.users.splice(index, 1);
      })
      .catch(error => {
        this.ServicesProvider.toast(MESSAGES.sqlite.error);
      });
  }

  login(_user: any) {
    this.SELECTUSERSQLITE = _user;
    this.isShowListUser = true;
    this.showInformationPac(true);
  }



  loadNew() {
    this.timeOutControl = true;
    let currentUser = this.patientProviderService.getCurrentUser();
    if (currentUser != undefined) {
      this.verifyUserInfo();
    }
  }
}
