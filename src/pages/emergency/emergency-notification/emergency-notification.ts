import { Component } from "@angular/core";
import { PatientProviderService } from "../../../providers/patient/patient";
import { ServicesProvider } from "../../../providers/services/services";
import { SERVICES } from "../../../config/url.servicios";


import { CallNumber } from "@ionic-native/call-number";
/* Mensajes de la app */
import { MESSAGES } from "../../../config/messages";

@Component({
  selector: "page-emergency-notification",
  templateUrl: "emergency-notification.html"
})
export class EmergencyNotificationPage {
  name: string;
  user: Array<string> = [];
  additionalData: any;
  age: number;
  avatar: string;
  lat: number;
  lng: number;
  timeOut: any;
  timeOutControl: boolean = true;

  constructor(
    public patientProviderService: PatientProviderService,
    public ServicesProvider: ServicesProvider,
    private callNumber: CallNumber
  ) {}

  ionViewDidLoad() {
    let coords = JSON.parse(localStorage.getItem("coords"));
    console.log("-- coords: ", coords);
    this.additionalData = coords.notification.payload.additionalData;
    this.lat = Number(this.additionalData.latitud);
    this.lng = Number(this.additionalData.longitud);
    this.getUser(this.additionalData.user); //this.getUser(this.additionalData.user);
  }

  createLoader() {
    this.ServicesProvider.createLoader();
    this.timeOut = setTimeout(() => {
      this.timeOutControl = false;
      //this.ServicesProvider.loading.dismiss();
      this.ServicesProvider.closeLoader();
      this.ServicesProvider.toast(MESSAGES.services.timeOut);
    }, MESSAGES.loading.time);
  }

  clearTimeOut() {
    clearTimeout(this.timeOut);
  }

  getUser(id_pk: any) {
    let params = {"id_paciente":  id_pk};
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiPostExternalUrl(params, SERVICES.INF_BASICA_OTROS_PACIENTES)
      .then(data => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (data["status"] == 404 || data["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (data["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else
        {
          let respuesta = JSON.parse(data["_body"]);
          if (respuesta.error)
          {
            this.ServicesProvider.toast("Lo sentimos pero no hemos podido recuperar la información de su contacto");
          }
          else
          {
            this.user = respuesta;
            this.age = this.getAge(respuesta.fecha_nac);
            this.name = respuesta.nombres + " " + respuesta.apellido1 + " " + (respuesta.apellido2 != null ? respuesta.apellido2 : "");
            this.avatar = respuesta.get_user[0].foto;
          }
        }
      })
      .catch(error => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
          this.ServicesProvider.closeLoader();
          this.timeOutControl = false;
          this.clearTimeOut();
          this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  getAge(birth_day: string) {
    let birthday = +new Date(birth_day);
    let today = +new Date();
    let c_age = (today - birthday) / 31557600000;
    let age = Math.floor(c_age);
    return age;
  }

  callToAfriend(tlf: any) {
    this.callNumber
      .callNumber(tlf, true)
      .then(response => {
        this.ServicesProvider.toast(
          MESSAGES.callNumber.message.success,
          "successToast"
        );
      })
      .catch(error => {
        this.ServicesProvider.toast(
          MESSAGES.callNumber.message.error
        );
      });
  }

  loadNew() {
    this.timeOutControl = true;
    this.getUser(this.additionalData.user);
  }
}
