import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/* Importing providers */
import { PatientProviderService } from "../../../providers/patient/patient";
import { ServicesProvider } from "../../../providers/services/services";

import { SERVICES } from "../../../config/url.servicios";

import { CallNumber } from "@ionic-native/call-number";


/* App messages */
import { MESSAGES } from "../../../config/messages";

@Component({
  selector: 'page-emergencias-recibidas-notificacion',
  templateUrl: 'emergencias-recibidas-notificacion.html',
})
export class EmergenciasRecibidasNotificacionPage {

  name: string;
  names: string;
  apellido1: string;
  apellido2: string;
  user: Array<string> = [];
  additionalData: any;
  age: number;
  avatar: string;
  latitud: number;
  longitud: number;
  tel: number;
  timeOut: any;
  timeOutControl: boolean = true;
  id_user: number;
  created_at:any;
  fecha_nac:any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public patientProviderService: PatientProviderService,
    public ServicesProvider: ServicesProvider,
    private callNumber: CallNumber
  ) {

  }

  ionViewDidLoad() {
    this.id_user = this.navParams.get("id_pk");
    this.name = this.navParams.get("nombres");
    console.log("xxxxxxxxxxxx name", this.name)
    this.apellido1 = this.navParams.get("apellido1");
    this.apellido2 = this.navParams.get("apellido2");
    if(this.name == null)
    {
      this.name = " ";
      console.log("xxxxxxxxxxxx name", this.name)
    }
    if(this.apellido1 == null)
    {
      this.apellido1 = " ";
      console.log("xxxxxxxxxxxx apellido 1", this.apellido1)
    }
    if(this.apellido2 == null)
    {
      this.apellido2 = " ";
      console.log("xxxxxxxxxxxx apellido 2", this.apellido2)
    }
    this.names = this.name + " " +  this.apellido1 + " " + this.apellido2;
    this.latitud = JSON.parse(this.navParams.get("latitud"));
    this.longitud = JSON.parse(this.navParams.get("longitud"));
    console.log(this.navParams);
    this.created_at = this.navParams.get("created_at");
    this.fecha_nac= this.ServicesProvider.getDateZero(this.navParams.get("fecha_nac"));
    //this.latitud = this.navParams.get("latitud");
    //this.longitud = this.navParams.get("longitud");
    this.avatar = this.navParams.get("avatar");
    this.tel = this.navParams.get("tel");
    console.log(this.latitud + " : " + this.longitud + "  " + " " + this.names + "  " + this.tel);
    //this.getUser(this.id_user);
  }

  callToAfriend(tlf: any) {
    this.callNumber
      .callNumber(tlf, true)
      .then(response => {
        this.ServicesProvider.toast(
          MESSAGES.callNumber.message.success,
          "successToast"
        );
      })
      .catch(error => {
        this.ServicesProvider.toast(
          MESSAGES.callNumber.message.error
        );
      });
  }


  clearTimeOut() {
    clearTimeout(this.timeOut);
  }

  getUser(id_pk: number) {
    let params = {"id_paciente": id_pk};
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiPost(params, SERVICES.CONSULTAR_PACIENTE_API_SERV)
      .then(data => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (data["status"] == 404 || data["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (data["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(data["_body"]);
          if (respuesta.error) {
            this.ServicesProvider.toast(
              "Lo sentimos pero no hemos podido recuperar la información de su contacto"
            );
          } else {
            this.user = respuesta;
            this.age = this.getAge(respuesta.fecha_nac);
            this.name =
              respuesta.nombres +
              " " +
              respuesta.apellido1 +
              " " +
              (respuesta.apellido2 != null ? respuesta.apellido2 : "");
            this.avatar = respuesta.get_user[0].foto;
          }
        }
      })
      .catch(error => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
          this.ServicesProvider.closeLoader();
          this.timeOutControl = false;
          this.clearTimeOut();
          this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  getAge(birth_day: string) {
    let birthday = +new Date(birth_day);
    let today = +new Date();
    let c_age = (today - birthday) / 31557600000;
    let age = Math.floor(c_age);
    return age;
  }


}
