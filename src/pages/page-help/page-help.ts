import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Slides, Platform } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';
import { WIZZARD_SPA } from "../../config/wizzard_spa";

/* Importing providers */
import { ServicesProvider } from "../../providers/services/services";
/* App messages */
@Component({
  selector: 'page-page-help',
  templateUrl: 'page-help.html',
})
export class PageHelpPage {
  @ViewChild('mySlider') slider: Slides;

  seccion: any;
  imageTutorialHelp: string;
  aEstilosTexto: object;
  version: string;
  login:boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public ServicesProvider: ServicesProvider,
    public sanitizer: DomSanitizer,
    public plt: Platform
  ) {
    this.seccion = this.navParams.get('seccion');
    this.login = false;
    this.getTutorialHelp(this.seccion);
  }

  ionViewDidLoad() {

    if (this.plt.is('ios')) {
      this.version = 'ios_help'
    }
    else if (this.plt.is('android')) {
      this.version = 'android_help'
    }
    else {
      this.version = 'android_help'
    }

  }

  ionSlideNextStart() {
    if (this.slider.isEnd()) {
      const length = this.slider.length() - 1;
      const index = this.slider.getActiveIndex();

      //console.log('ionSlideNextStart', length, index);

      if (index > length) {
        //console.log('pop view');
        //this.navCtrl.pop()
        this.close();


      }
    }

  }

  close() {
    if (this.seccion == "codigo_qr") {
      this.navCtrl.getPrevious().data.accion = "qr";
    }
    this.navCtrl.pop();

  }
  slideToNext() {
    //this.slider.getSlider().slideNext(); // also not working
    if (this.slider.isEnd()) {
      //this.navCtrl.pop();
      this.close();
    }
    else {
      this.slider.slideNext(); // not working
    }

  }

  getTutorialHelp(seccion) {

    switch (seccion) {
      case "actualizar_perfil":
        this.aEstilosTexto = WIZZARD_SPA.actualizar_perfil;
        break;
      case "autorizar_acceso":
        this.aEstilosTexto = WIZZARD_SPA.autorizar_acceso;
        break;
      case "codigo_qr":
        this.aEstilosTexto = WIZZARD_SPA.codigo_qr;
        break;
      case "datos_salud":
        this.aEstilosTexto = WIZZARD_SPA.datos_salud;
        break;
      case "emergencia":
        this.aEstilosTexto = WIZZARD_SPA.emergencia;
        break;
      case "notificacion":
        this.aEstilosTexto = WIZZARD_SPA.notificacion;
        break;
      case "mis_documentos":
        this.aEstilosTexto = WIZZARD_SPA.mis_documentos;
        break;
      case "antecedente":
        this.aEstilosTexto = WIZZARD_SPA.antecedente;
        break;
      case "ficha_unica":
        this.aEstilosTexto = WIZZARD_SPA.ficha_unica;
        break;
      case "mis_archivos_salud":
        this.aEstilosTexto = WIZZARD_SPA.mis_archivos_salud;
        break;
      case "reportes":
        this.aEstilosTexto = WIZZARD_SPA.reportes;
        break;
      case "midis_salud":
        this.aEstilosTexto = WIZZARD_SPA.midis_salud;
        break;
      case "app_salud":
        this.aEstilosTexto = WIZZARD_SPA.app_salud;
        break;
      case "almacenamiento":
        this.aEstilosTexto = WIZZARD_SPA.almacenamiento;
        break;
      case "configuracion":
        this.aEstilosTexto = WIZZARD_SPA.configuracion;
        break;
      case "crear_cuenta":
        this.aEstilosTexto = WIZZARD_SPA.crear_cuenta;
        break;
      case "codigo_qr_midis":
        this.aEstilosTexto = WIZZARD_SPA.codigo_qr_midis;
        break;
      case "home":
        this.aEstilosTexto = WIZZARD_SPA.home;
        break;
      case "token":
        this.aEstilosTexto = WIZZARD_SPA.token;
        break;
      case "login":
        this.login = true;
        break;
    }


  }

}
