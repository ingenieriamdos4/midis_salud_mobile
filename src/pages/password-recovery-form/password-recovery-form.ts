import { Component } from "@angular/core";
import { NavController, NavParams, App } from "ionic-angular";
import { Validators, FormBuilder, FormGroup } from "@angular/forms";
import { PatientProviderService } from "../../providers/patient/patient";
import { ServicesProvider } from "../../providers/services/services";

import { MESSAGES } from "../../config/messages";
import { SERVICES } from "../../config/url.servicios";

import { LoginPage } from "../index.paginas";

@Component({
  selector: "page-password-recovery-form",
  templateUrl: "password-recovery-form.html"
})
export class PasswordRecoveryFormPage {
  login_pk: any;
  recoveryForm: FormGroup;
  timeOut: any;
  timeOutControl: boolean = true;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public patientProviderService: PatientProviderService,
    public ServicesProvider: ServicesProvider,
    private formBuilder: FormBuilder,
    public appCtrl: App
  ) {
    this.login_pk = this.navParams.get("login_pk");
    this.recoveryForm = this.formBuilder.group(
      {
        password: [
          null,
          Validators.compose([
            Validators.required,
            Validators.minLength(7),
            Validators.maxLength(15)
          ])
        ],
        confirmPassword: [
          null,
          Validators.compose([
            Validators.required,
            Validators.minLength(7),
            Validators.maxLength(15)
          ])
        ]
      },
      { validator: this.matchingPasswords("password", "confirmPassword") }
    );
  }

  matchingPasswords(passwordKey: string, confirmPasswordKey: string) {
    // TODO maybe use this https://github.com/yuyang041060120/ng2-validation#notequalto-1
    return (group: FormGroup): { [key: string]: any } => {
      let password = group.controls[passwordKey];
      let confirmPassword = group.controls[confirmPasswordKey];

      if (password.value !== confirmPassword.value) {
        return {
          mismatchedPasswords: true
        };
      }
    };
  }

  clearTimeOut() {
    clearTimeout(this.timeOut);
  }

  cancel() {
    this.recoveryForm.reset();
    this.appCtrl.getRootNav().setRoot(LoginPage);
  }

  submitForm(formValue: any) {
    this.ServicesProvider.createLoader();
    let formData = new FormData();
    formData.append("u", this.login_pk);
    formData.append("password", formValue.password);
    formData.append("password_confirmation", formValue.confirmPassword);
    this.ServicesProvider
      .apiPost(formData, SERVICES.RESETEAR_CLAVE_SERV)
      .then(success => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (success["status"] == 404 || success["status"] == 500) {
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else {
          let respuesta = JSON.parse(success["_body"]);
          if (respuesta.success)
          {
            this.ServicesProvider.toast(respuesta.mensaje,"successToast");
            this.recoveryForm.reset();
            this.appCtrl.getRootNav().setRoot(LoginPage);
          } 
          else
          {
            //this.ServicesProvider.toast(respuesta.mensaje);
            for (var name in respuesta.errors)
            {
              let value = respuesta.errors[name];
              this.ServicesProvider.toast(value.toString());                            
            }
          }
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
          this.ServicesProvider.closeLoader();
          this.clearTimeOut();
          this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }
}
