import { Component, ViewChild } from "@angular/core";
import {
  NavController,
  NavParams,
  ToastController,
  LoadingController
} from "ionic-angular";
import { PatientProviderService } from "../../../../providers/patient/patient";

import { Chart } from 'chart.js';

@Component({
  selector: "page-classification-imc",
  templateUrl: "classification-imc.html"
})

export class ClassificationImcPage {

  @ViewChild('canvasChart_1') canvasChart_1;
  @ViewChild('canvasChart_2') canvasChart_2;
  @ViewChild('canvasChart_3') canvasChart_3;
  @ViewChild('canvasChart_4') canvasChart_4;
  @ViewChild('canvasChart_5') canvasChart_5;
  @ViewChild('canvasChart_6') canvasChart_6;

  chart_1: any;
  chart_2: any;
  chart_3: any;
  chart_4: any;
  chart_5: any;
  chart_6: any;

  loading: any;
  classificationsImc: Array<string> = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public patientProviderService: PatientProviderService,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController
  ) {
  }

  ionViewDidLoad() {

    this.chart_1 = new Chart(this.canvasChart_1.nativeElement, {

                        type: 'pie',                                                
                        data: {
                            datasets: [{
                                data: [0, 18.4],                                
                                backgroundColor: [
                                    'rgba(30, 138, 194, 1)',
                                    'rgba(200, 200, 200, 1)'
                                ]
                            }]
                        },
                        options: {
                            tooltips: {
                                 enabled: false
                            }
                        }

                    });

    this.chart_2 = new Chart(this.canvasChart_2.nativeElement, {

                        type: 'pie',                        
                        data: {
                            datasets: [{
                                data: [18.5, 24.9],
                                label: 'IMC',
                                backgroundColor: [
                                  'rgba(30, 138, 194, 1)',
                                  'rgba(200, 200, 200, 1)'
                                ],
                                hoverBackgroundColor: [
                                    "#1E8AC2",
                                    "#dddddd"
                                ]
                            }]
                        },
                        options: {
                            tooltips: {
                                 enabled: false
                            }
                        }

                    });

    this.chart_3 = new Chart(this.canvasChart_3.nativeElement, {

                        type: 'pie',
                        data: {
                            datasets: [{
                                data: [25, 29.94],
                                backgroundColor: [
                                    'rgba(64, 190, 179, 1)',
                                    'rgba(200, 200, 200, 1)'
                                ]
                            }]
                        },
                        options: {
                            tooltips: {
                                 enabled: false
                            }
                        }

                    });

    this.chart_4 = new Chart(this.canvasChart_4.nativeElement, {

                        type: 'pie',
                        data: {
                            datasets: [{
                                data: [30, 34.9],
                                backgroundColor: [
                                  'rgba(248, 172, 89, 1)',
                                  'rgba(200, 200, 200, 1)'
                                ]
                            }]
                        },
                        options: {
                            tooltips: {
                                 enabled: false
                            }
                        }

                    });

    this.chart_5 = new Chart(this.canvasChart_5.nativeElement, {

                        type: 'pie',
                        data: {
                            datasets: [{
                                data: [35, 39.9],
                                backgroundColor: [
                                  'rgba(242, 109, 52, 1)',
                                  'rgba(200, 200, 200, 1)'
                                ]
                            }]
                        },
                        options: {
                            tooltips: {
                                 enabled: false
                            }
                        }

                    });

    this.chart_6 = new Chart(this.canvasChart_6.nativeElement, {

                        type: 'pie',
                        data: {
                            datasets: [{
                                data: [40, 0],
                                backgroundColor: [
                                  'rgba(228, 0, 58, 1)',
                                  'rgba(200, 200, 200, 1)'
                                ]
                            }]
                        },
                        options: {
                            tooltips: {
                                 enabled: false
                            }
                        }

                    });

  }

  loadingCreate() {
    this.loading = this.loadingCtrl.create({
      content: "<ion-spinner></ion-spinner>"
    });
  }

  mostrar_error(mensaje: string) {
    let toast = this.toastCtrl.create({
      message: mensaje,
      duration: 5000,
      position: "top"
    });

    toast.present();
  }
}
