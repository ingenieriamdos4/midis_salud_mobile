import { Component, ViewChild } from "@angular/core";
import {
  NavController,
  ModalController,
  NavParams,
  MenuController,
  Slides,
  Events,
  AlertController
} from "ionic-angular";
import { PatientProviderService } from "../../../../providers/patient/patient";
import { ServicesProvider } from "../../../../providers/services/services";
import { ClassificationImcPage } from "../classification-imc/classification-imc";
import { ModalSangre } from "../modal_sangre/modal_sangre";


/* Mensajes de la app */
import { MESSAGES } from "../../../../config/messages";
import { SERVICES } from "../../../../config/url.servicios";
import { PageHelpPage } from "../../../index.paginas";
import { DatePipe } from '@angular/common';
import { Chart } from 'chart.js';
import 'chartjs-plugin-zoom';

class Basic {
  height: any;
  weight: string;
  imc: number;
  blood_group: number;
  rh_letra: string;
  rh_signo: string;
  pa_s: string;
  pa_d: String;
  fc: string;
  fr: string;
  tc: string;
  glycemia: string;
}

class GinecoObstetricos {
  date_u_m: string;
  date_u_m_fin: string;
  embarazo: boolean;
  semanas_gestacion: number;
  date_posibility_parto: string;
  date_citologia_cervicouterina: string;
  result: string;
}

@Component({
  selector: "page-datos-salud",
  templateUrl: "datos-salud.html"
})
export class DatosSaludPage {
  // Campos editables Básicos: Estatura, Peso, PA/S, PA/D y Glucemia
  pageHelp = PageHelpPage;
  infoPac: Array<string> = [];
  basicModel: Basic;
  errors: Array<string> = [];
  errorsGineco: Array<string> = [];
  indiceMC: string;
  ginecoObstetricos: GinecoObstetricos;
  genero: string;
  blood_groups: Array<string> = [];
  classificationIMC = ClassificationImcPage;
  tipo_sangre: number;
  timeOut: any;
  timeOutControl: boolean = true;
  maxDate: string;
  lastMonth: any;
  ultimate_register_gineco: any;

  min_date_u_m: any;
  min_date_ultima_cito: any;

  fec_ini_basicos: any;
  fec_fin_basicos: any;
  fec_ini_glu: any;
  fec_fin_glu: any;
  fec_ini_presion: any;
  fec_fin_presion: any;
  fec_ini_frec: any;
  fec_fin_frec: any;
  fec_ini_temp: any;
  fec_fin_temp: any;

  fec_ini_fum: any;
  fec_fin_fum: any;
  fec_ini_emb: any;
  fec_fin_emb: any;
  fec_ini_ucit: any;
  fec_fin_ucit: any;
  archivo_adjunto: any;
  sangreLocalStorage: any;

  estado: string;
  ultimate_register: any;


  aChart: any = [];
  aTable: any = [];
  so_flag: any;
  //se almacenaran los chart en la variable de arriba, por secciones
  dato_basico: string;

  @ViewChild('dato_salud_chart') dato_salud_chart;
  @ViewChild('dato_glucemia_chart') dato_glucemia_chart;
  @ViewChild('dato_presion_chart') dato_presion_chart;
  @ViewChild('dato_frecuencia_chart') dato_frecuencia_chart;
  @ViewChild('dato_temp_chart') dato_temp_chart;


  dicData: Array<object>;

  keySave: object = {
    "OPC_TIPO_SANGRE": "5",
    "OPC_ESTATURA": "6",
    "OPC_PESO": "7",
    "OPC_IMC": "8",
    "OPC_PRESION_ARTERIAL": "9",
    "OPC_FREC_CARD": "12",
    "OPC_FREC_RESP": "13",
    "OPC_TEMP_CORP": "14",
    "OPC_GLUCEMIA": "15",
    "OPC_FUM": "30",
    "OPC_EMBARAZO": "31",
    "OPC_FEC_ULT_CITOLOGIA": "34",
  };


  @ViewChild("SwipedTabsSlider") SwipedTabsSlider: Slides;

  SwipedTabsIndicator: any = null;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private menuCtrl: MenuController,
    public patientProviderService: PatientProviderService,
    public ServicesProvider: ServicesProvider,
    public events: Events,
    public datepipe: DatePipe,
    public modalCtrl: ModalController,
    private alertCtrl: AlertController

  ) {
    this.lastMonth = new Date();
    this.lastMonth.setMonth(this.lastMonth.getMonth() - 1);
    this.lastMonth.toISOString().split('T');
    this.lastMonth = this.datepipe.transform(this.lastMonth, 'yyyy-MM-dd');
    this.so_flag = this.ServicesProvider.validate_so_v();
    this.basicModel = new Basic();
    this.ginecoObstetricos = new GinecoObstetricos();
    this.genero = this.getCurrentUser().genero;
    this.maxDate = new Date()
      .toJSON()
      .slice(0, 10)
      .replace(/-/g, "-");
    let params = { "pac_fk": this.getCurrentUser().id_pk };
    this.promiseInitialize(
      this.ServicesProvider.apiPost(
        params,
        SERVICES.DATOS_SALUD_PACIENTE_API_SERV
      ),
      this.ServicesProvider.apiGet("", SERVICES.LISTAR_TIPO_SANGRE_SERV),
      this.ServicesProvider.apiGet("?pac_fk=" + params.pac_fk, SERVICES.ULTIMOS_DATOS_SALUD)
    );
    this.archivo_adjunto = this.getCurrentUser().archivo_adjunto;
    this.fec_ini_basicos = this.datepipe.transform(this.lastMonth, 'yyyy-MM-dd');
    this.fec_fin_basicos = this.maxDate;
    this.fec_ini_glu = this.datepipe.transform(this.lastMonth, 'yyyy-MM-dd');
    this.fec_fin_glu = this.maxDate;
    this.fec_ini_presion = this.datepipe.transform(this.lastMonth, 'yyyy-MM-dd');
    this.fec_fin_presion = this.maxDate;
    this.fec_ini_frec = this.datepipe.transform(this.lastMonth, 'yyyy-MM-dd');
    this.fec_fin_frec = this.maxDate;
    this.fec_ini_temp = this.datepipe.transform(this.lastMonth, 'yyyy-MM-dd');
    this.fec_fin_temp = this.maxDate;
    this.estado = this.getCurrentUser().estado;

    this.fec_ini_fum = this.datepipe.transform(this.lastMonth, 'yyyy-MM-dd');
    this.fec_fin_fum = this.maxDate;
    this.fec_ini_emb = this.datepipe.transform(this.lastMonth, 'yyyy-MM-dd');
    this.fec_fin_emb = this.maxDate;
    this.fec_ini_ucit = this.datepipe.transform(this.lastMonth, 'yyyy-MM-dd');
    this.fec_fin_ucit = this.maxDate;



    // Set it to one month ago
    //this.getDataHealthPatient(this.getCurrentUser().id_pk);
  }

  clearTimeOut() {
    clearTimeout(this.timeOut);
  }

  fn_modalAdjunto() {
    let profileModal = this.modalCtrl.create(ModalSangre, { userId: 8675309 });
    profileModal.present();
    profileModal.onDidDismiss(data => {
      this.archivo_adjunto = this.getCurrentUser().archivo_adjunto;
    });

  }


  clickCard(id, item, index_chart, tipo) {
    if (this.aChart[index_chart] != undefined) {
      this.aChart[index_chart].destroy();
    }
    //se aplica la clase al padre cuando esta en el medio los iconos
    var card = document.getElementById(id);

    //var card = document.querySelector('.card');
    card.classList.toggle('is-flipped');

    this.dato_basico = item;
    if (card.classList.contains("is-flipped")) {
      if (tipo != "tabla") {
        this.getChart(index_chart);
      }
      else {
        this.getTable(index_chart);
      }
    }

  }



  ionViewDidEnter() {

    this.SwipedTabsIndicator = document.getElementById("indicator");
    /*if (localStorage.getItem("wizzard_datos_salud") == undefined) {
      localStorage.setItem("wizzard_datos_salud", "true");
      this.navCtrl.push(this.pageHelp, { 'seccion': 'datos_salud' });
    }*/
  }

  ionViewDidLoad() {
    this.getUser(this.getCurrentUser().id_pk);
  }

  selectTab(index) {
    this.SwipedTabsIndicator.style.webkitTransform =
      "translate3d(" + 100 * index + "%,0,0)";
    this.SwipedTabsSlider.slideTo(index, 500);
  }

  updateIndicatorPosition() {
    // this condition is to avoid passing to incorrect index
    if (
      this.SwipedTabsSlider.length() > this.SwipedTabsSlider.getActiveIndex()
    ) {
      this.SwipedTabsIndicator.style.webkitTransform =
        "translate3d(" +
        this.SwipedTabsSlider.getActiveIndex() * 100 +
        "%,0,0)";
    }
  }

  animateIndicator($event) {
    if (this.SwipedTabsIndicator)
      this.SwipedTabsIndicator.style.webkitTransform =
        "translate3d(" +
        $event.progress * (this.SwipedTabsSlider.length() - 1) * 100 +
        "%,0,0)";
  }

  getCurrentUser() {
    return JSON.parse(localStorage.getItem("user"));
  }

  showMenu() {
    this.menuCtrl.toggle();
  }

  saveItem(item, opc, tipo) {
    let params = {
      "estatura": this.basicModel.height,
      "peso": this.basicModel.weight,
      "tsa_fk": this.basicModel.blood_group,
      "pres_art_s": (this.basicModel.pa_s == null ? 0 : this.basicModel.pa_s),
      "pres_art_d": (this.basicModel.pa_d == null ? 0 : this.basicModel.pa_d),
      "glucemia": Math.round(Number(this.basicModel.glycemia)),
      "frec_card": (this.basicModel.fc == null ? 0 : this.basicModel.fc),
      "frec_resp": (this.basicModel.fr == null ? 0 : this.basicModel.fr),
      "pac_fk": this.getCurrentUser().id_pk
    };


    console.log(this.ginecoObstetricos.date_citologia_cervicouterina);
    let fec_ult_citologia;
    if (this.ginecoObstetricos.date_citologia_cervicouterina == 'undefined-undefined-' || this.ginecoObstetricos.date_citologia_cervicouterina == undefined || this.ginecoObstetricos.date_citologia_cervicouterina == null) {
      console.log("siiiiii editG");
      fec_ult_citologia = "";
    }
    else {
      fec_ult_citologia = this.patientProviderService.formatDate(this.ginecoObstetricos.date_citologia_cervicouterina);
    }
    if (this.ginecoObstetricos.embarazo == true) {

      params["pac_fk"] = this.getCurrentUser().id_pk;
      params["fum"] = this.patientProviderService.formatDate(this.ginecoObstetricos.date_u_m);
      params["fec_ult_citologia"] = fec_ult_citologia;
      params["result_citologia"] = this.ginecoObstetricos.result;
      params["embarazo"] = 1;
    } else {
      params["pac_fk"] = this.getCurrentUser().id_pk;
      params["fum"] = this.patientProviderService.formatDate(this.ginecoObstetricos.date_u_m);
      params["fec_ult_citologia"] = this.patientProviderService.formatDate(this.ginecoObstetricos.date_citologia_cervicouterina);
      params["result_citologia"] = this.ginecoObstetricos.result;
      params["embarazo"] = 0;
    }

    if (this.basicModel.tc != null) {
      params["temp_corp"] = this.basicModel.tc
    }
    var oSendSave = null;
    if (item == "pres_art_s" || item == "pres_art_d") {
      oSendSave = { "pac_fk": this.getCurrentUser().id_pk, "pres_art_s": params["pres_art_s"], "pres_art_d": params["pres_art_d"], "opc_dato_salud": this.keySave[opc] }
    }
    else if (item == "fum") {
      oSendSave = { "pac_fk": this.getCurrentUser().id_pk, "fum": this.patientProviderService.formatDate(this.ginecoObstetricos.date_u_m), "fum_fin": this.patientProviderService.formatDate(this.ginecoObstetricos.date_u_m_fin), "opc_dato_salud": this.keySave[opc] }
    }
    else if (item == "fec_ult_citologia") {
      oSendSave = { "pac_fk": this.getCurrentUser().id_pk, "fec_ult_citologia": this.patientProviderService.formatDate(this.ginecoObstetricos.date_citologia_cervicouterina), "result_citologia": this.ginecoObstetricos.result, "opc_dato_salud": this.keySave[opc] }
    }
    else if (item == "embarazo") {
      oSendSave = { "pac_fk": this.getCurrentUser().id_pk, "embarazo": params["embarazo"], "result_citologia": this.ginecoObstetricos.result, "opc_dato_salud": this.keySave[opc] }
    }
    else {
      oSendSave = { [item]: params[item], "opc_dato_salud": this.keySave[opc], "pac_fk": this.getCurrentUser().id_pk }
    }
    console.log(tipo)
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiPost(oSendSave, tipo ? SERVICES.EDIT_GINECO_DATOS_SALUD : SERVICES.EDITAR_DATOS_BASICOS_API_SERV)
      .then(state => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (state["status"] == 404 || state["status"] == 500) {
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (state["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
        } else {
          let respuesta = JSON.parse(state["_body"]);
          this.errors = [];
          if (respuesta.success) {
            this.getOnlyLastRegisters(item);

            /*this.ServicesProvider.toast(
               MESSAGES.dataHealth.basic.update.success,
               "successToast"
             );*/


            //this.patientProviderService.imc = this.basicModel.imc.toString();
            //localStorage.setItem("imc", this.basicModel.imc.toString());
            this.getUser(this.getCurrentUser().id_pk);
            console.log("eu")

            switch (item) {
              case "estatura":
                this.patientProviderService.height = this.basicModel.height;
                localStorage.setItem("height", this.basicModel.height);
                this.basicModel.height = "";
                break;
              case "peso":
                localStorage.setItem("weight", this.basicModel.weight);
                this.patientProviderService.weight = this.basicModel.weight;

                this.basicModel.weight = "";

                break;
              case "pres_art_s":
                this.basicModel.pa_s = "";
                this.basicModel.pa_d = "";
                break;
              case "pres_art_d":
                this.basicModel.pa_s = "";
                this.basicModel.pa_d = "";
                break;
              case "glucemia":
                this.basicModel.glycemia = "";
                break;
              case "frec_card":
                this.basicModel.fc = "";
                break;
              case "frec_resp":
                this.basicModel.fr = "";
                break;
              case "fum":
                this.ginecoObstetricos.date_u_m = null;
                this.ginecoObstetricos.date_u_m_fin = null;

                break;
              case "fec_ult_citologia":
                this.ginecoObstetricos.date_citologia_cervicouterina = null;
                this.ginecoObstetricos.result = "";
                break;

            }


            //this.getDataHealthPatient(this.getCurrentUser().id_pk);
          } else if (!respuesta.success == false) {
            this.ServicesProvider.toast(
              MESSAGES.dataHealth.basic.update.error
            );
          } else {
            /*for (var name in respuesta.errors) {
              let value = respuesta.errors[name];
              this.errors.push(value);
            }
            this.ServicesProvider.toast(this.errors.toString());*/
            if (respuesta.errors) {
              this.ServicesProvider.toast(
                respuesta.errors[Object.keys(respuesta.errors)[0]]
              );
            }
            else {
              if (respuesta.success != true) {
                this.ServicesProvider.toast(
                  "Existe un problema con el servicio, intentalo de nuevo"
                );
              }

            }
          }
        }
      })
      .catch(err => {
        // this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  getUser(id_paciente: number) {
    let params = { "id_paciente": id_paciente };
    this.ServicesProvider.createLoader("Actualizando.. por favor espere");
    this.ServicesProvider
      .apiPost(params, SERVICES.CONSULTAR_PACIENTE_API_SERV)
      .then(state => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (state["status"] == 404 || state["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (state["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(state["_body"]);
          localStorage.setItem("user", JSON.stringify(respuesta));
          this.sangreLocalStorage = respuesta.tsa_fk;
          this.events.publish('user:imagen_porcentaje', respuesta.imagen_porcentaje);
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.timeOutControl = false;
        //});
      });
  }

  //item define si se le pasa peso o estatura para evitar consumir el servicio de imc
  getOnlyLastRegisters(item) {
    let params = { "pac_fk": this.getCurrentUser().id_pk };
    this.ServicesProvider.createLoader("Actualizando.. por favor espere");
    this.ServicesProvider.apiGet("?pac_fk=" + params.pac_fk, SERVICES.ULTIMOS_DATOS_SALUD)
      .then(last => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (last["status"] == 404 || last["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (last["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let ultimate = JSON.parse(last["_body"]);
          console.log(ultimate)
          this.ultimate_register = this.getLastData(ultimate.ultimoDatoSaludBasico);
          if (this.genero == "F") {
            this.ultimate_register_gineco = this.getLastData(ultimate.ultimosDatosSaludGineco);
          }
          console.log(this.ultimate_register)
          if (item == "estatura" || item == "peso") {
            if (this.ultimate_register.imc) {
              this.getRangeImc(this.ultimate_register.imc.valor);
              this.patientProviderService.imc = this.ultimate_register.imc.valor;
              localStorage.setItem("imc", this.ultimate_register.imc.valor);
            }


          }
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.timeOutControl = false;
        //});
      });
  }


  getChart(chart_index) {
    this.ServicesProvider.createLoader();
    var item = this.dato_basico;
    var chart = null;
    var fecha_inicio = null;
    var fecha_fin = null;
    switch (chart_index) {
      case 0:
        chart = this.dato_salud_chart.nativeElement;
        fecha_inicio = this.fec_ini_basicos;
        fecha_fin = this.fec_fin_basicos;
        break;
      case 1:
        chart = this.dato_glucemia_chart.nativeElement;
        fecha_inicio = this.fec_ini_glu;
        fecha_fin = this.fec_fin_glu;
        break;
      case 2:
        chart = this.dato_presion_chart.nativeElement;
        fecha_inicio = this.fec_ini_presion;
        fecha_fin = this.fec_fin_presion;
        break;
      case 3:
        chart = this.dato_frecuencia_chart.nativeElement;
        fecha_inicio = this.fec_ini_frec;
        fecha_fin = this.fec_fin_frec;
        break;
      case 4:
        chart = this.dato_temp_chart.nativeElement;
        fecha_inicio = this.fec_ini_temp;
        fecha_fin = this.fec_fin_temp;
        break;

    }
    console.log("chart index", chart_index)
    console.log(this.fec_ini_glu, " ", this.fec_fin_basicos)

    this.ServicesProvider.apiGet("?pac_fk=" + this.getCurrentUser().id_pk + "&dato_salud=" + this.keySave[item] + "&fecIni=" + this.datepipe.transform(fecha_inicio, 'dd/MM/yyyy') + "&fecFin=" + this.datepipe.transform(fecha_fin, 'dd/MM/yyyy'), SERVICES.CHART_DATOS_SALUD)
      .then(last => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (last["status"] == 404 || last["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (last["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {

          let ultimate = JSON.parse(last["_body"]);

          if (ultimate.labels.length != 0) {
            this.aChart[chart_index] = new Chart(chart, {

              type: 'bar',
              data: {
                datasets: ultimate.datasets,
                //datasets: [{"label":"Estatura (cm)","backgroundColor":"rgba(0, 100, 150, 0.5)","pointBorderColor":"rgba(26,179,148,1)","data":["68.00","69.00","70.00","71.00","72.00","73.00","74.00","75.00","76.00","77.00","78.00","79.00","80.00","81.00","82.00","83.00","84.00","85.00","86.00","87.00","88.00","89.00","90.00","91.00","68.00","69.00","70.00","71.00","72.00","73.00","74.00","75.00","76.00","77.00","78.00","79.00","80.00","81.00","82.00","83.00","84.00","85.00","86.00","87.00","88.00","89.00","90.00","91.00"]}],
                labels: ultimate.labels
                //["08 Aug 2018 02:42 pm","09 Aug 2018 11:24 am","04 Sep 2018 01:23 pm","04 Sep 2018 01:26 pm","04 Sep 2018 01:31 pm","04 Sep 2018 01:33 pm","04 Sep 2018 01:35 pm","04 Sep 2018 01:36 pm","04 Sep 2018 01:38 pm","04 Sep 2018 01:38 pm","04 Sep 2018 01:40 pm","04 Sep 2018 01:45 pm","04 Sep 2018 01:46 pm","04 Sep 2018 01:49 pm","04 Sep 2018 01:49 pm","04 Sep 2018 01:49 pm","04 Sep 2018 01:53 pm","04 Sep 2018 02:07 pm","04 Sep 2018 02:19 pm","04 Sep 2018 02:19 pm","04 Sep 2018 02:20 pm","04 Sep 2018 02:30 pm","04 Sep 2018 02:31 pm","04 Sep 2018 02:33 pm","05 Sep 2018 09:30 am","05 Sep 2018 09:31 am","08 Aug 2018 02:42 pm","09 Aug 2018 11:24 am","04 Sep 2018 01:23 pm","04 Sep 2018 01:26 pm","04 Sep 2018 01:31 pm","04 Sep 2018 01:33 pm","04 Sep 2018 01:35 pm","04 Sep 2018 01:36 pm","04 Sep 2018 01:38 pm","04 Sep 2018 01:38 pm","04 Sep 2018 01:40 pm","04 Sep 2018 01:45 pm","04 Sep 2018 01:46 pm","04 Sep 2018 01:49 pm","04 Sep 2018 01:49 pm","04 Sep 2018 01:49 pm","04 Sep 2018 01:53 pm","04 Sep 2018 02:07 pm","04 Sep 2018 02:19 pm","04 Sep 2018 02:19 pm","04 Sep 2018 02:20 pm","04 Sep 2018 02:30 pm","04 Sep 2018 02:31 pm","04 Sep 2018 02:33 pm","05 Sep 2018 09:30 am","05 Sep 2018 09:31 am"],
              },
              options: {

                legend: {
                  position: 'top',
                },
                scales: {
                  xAxes: [{
                    ticks: {
                      autoSkip: true,
                      maxRotation: 0,
                      minRotation: 0
                    }
                  }],
                  yAxes: [{
                    ticks: {
                      beginAtZero: true
                    }
                  }]
                },
                pan: {
                  enabled: false,
                  mode: 'xy' // is panning about the y axis neccessary for bar charts?
                },
                zoom: {
                  //sensitivity:10, drag: false, enabled: true, mode: 'x'
                  sensitivity: 10, drag: false, enabled: true, mode: 'x'
                }
              }
            });
          }

        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.timeOutControl = false;
        //});
      });
  }


  getTable(chart_index) {
    var item = this.dato_basico;
    this.ServicesProvider.createLoader();
    var fecha_inicio = null;
    var fecha_fin = null;
    switch (chart_index) {
      case 0:
        fecha_inicio = this.fec_ini_fum;
        fecha_fin = this.fec_fin_fum;
        break;
      case 1:
        fecha_inicio = this.fec_ini_emb;
        fecha_fin = this.fec_fin_emb;
        break;
      case 2:
        fecha_inicio = this.fec_ini_ucit;
        fecha_fin = this.fec_fin_ucit;
        break;

    }


    this.ServicesProvider.apiGet("?pac_fk=" + this.getCurrentUser().id_pk + "&dato_salud=" + this.keySave[item] + "&fecIni=" + this.datepipe.transform(fecha_inicio, 'dd/MM/yyyy') + "&fecFin=" + this.datepipe.transform(fecha_fin, 'dd/MM/yyyy'), SERVICES.TABLA_GINECO)
      .then(last => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (last["status"] == 404 || last["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (last["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {

          let ultimate = JSON.parse(last["_body"]);
          //let ultimate={"opcionDatosSaludSeleccionada":["Fecha","Embarazo","Semanas de gestación","Fecha posible parto","Cargado por"],"historialDatosSalud":[{"fecha":"24 Jul 2018 09:40 am","embarazo":"1","semanas_gest":"16.00","fec_posible_parto":"07/01/2019","pro_fk_cambio":null,"get_profesional_cambio":null},{"fecha":"04 Sep 2018 02:35 pm","embarazo":"1","semanas_gest":"1.00","fec_posible_parto":"04/06/2019","pro_fk_cambio":null,"get_profesional_cambio":null}]}
          if (ultimate.historialDatosSalud[0]) {
            ultimate["keysbody"] = Object.keys(ultimate.historialDatosSalud[0]);
            this.aTable[chart_index] = ultimate;
          }


        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.timeOutControl = false;
        //});
      });
  }




  //////////////***************************************************************************FALTA TEXTO DE ESCALAS IMC///
  getRangeImc(valor) {
    this.ServicesProvider.createLoader("Actualizando.. por favor espere");
    this.ServicesProvider.apiGet("/" + valor, SERVICES.RANGOS_IMC)
      .then(last => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (last["status"] == 404 || last["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (last["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          console.log(JSON.parse(last["_body"]))
          this.indiceMC = JSON.parse(last["_body"]).nombre;
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.timeOutControl = false;
        //});
      });
  }



  promiseInitialize(promiseDateHealth: any, promisetypeRh: any, promiseUltimateRegister) {
    this.ServicesProvider.createLoader();
    Promise.all([promiseDateHealth, promisetypeRh, promiseUltimateRegister])
      .then(promises => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();

        if (promises[0]["status"] == 404 || promises[0]["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (promises[0]["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(promises[0]["_body"]);
          //localStorage.setItem("user", JSON.stringify(respuesta));
          this.infoPac = respuesta[0];
          if (this.infoPac["imc"]) {
            this.getRangeImc(this.infoPac["imc"]);
          }
          this.getBasic(this.infoPac);
        }

        if (promises[1]["status"] == 404 || promises[1]["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (promises[1]["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let rptTypeRh = JSON.parse(promises[1]["_body"]);
          this.blood_groups = rptTypeRh;
        }

        if (promises[2]["status"] == 404 || promises[2]["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (promises[2]["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let ultimate = JSON.parse(promises[2]["_body"]);
          if (ultimate.ultimoDatoSaludBasico.length != 0) {
            this.ultimate_register = this.getLastData(ultimate.ultimoDatoSaludBasico);
          }

          if (this.genero == "F") {
            if (ultimate.ultimosDatosSaludGineco.length != 0) {
              this.ultimate_register_gineco = this.getLastData(ultimate.ultimosDatosSaludGineco);
              if (this.ultimate_register_gineco.fum) {
                this.min_date_u_m = this._formatDate(
                  this.ultimate_register_gineco.fum.valor
                );
              }
              if (this.ultimate_register_gineco.fec_ult_citologia) {
                this.min_date_ultima_cito = this._formatDate(
                  this.ultimate_register_gineco.fec_ult_citologia.valor
                );
              }
            }
          }

          /*
          this.ginecoObstetricos.date_u_m = this._formatDate(
            this.ultimate_register_gineco.fum.valor
          );
          this.ginecoObstetricos.date_u_m_fin = this._formatDate(
            this.ultimate_register_gineco.fum_fin.valor
          );*/




        }

      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.timeOutControl = false;
        this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  //contiene los ultimos datos en un json
  getLastData(data) {
    let auxUltimate = {};
    for (var i in data) {
      auxUltimate[data[i].dato_salud] = data[i];
    }
    return auxUltimate
  }


  getBasic(infoPac: any) {
    /*this.basicModel.height = infoPac.estatura;
    this.basicModel.weight = infoPac.peso;
    this.basicModel.imc = infoPac.imc;*/
    this.basicModel.height = "";
    this.basicModel.weight = "";
    this.basicModel.imc = infoPac.imc;;

    //this.basicModel.pa_s = infoPac.pres_art_s;
    this.basicModel.fc = infoPac.frec_card;
    this.basicModel.fr = infoPac.frec_resp;
    //this.basicModel.pa_d = infoPac.pres_art_d;
    //this.basicModel.glycemia = infoPac.glucemia;
    this.basicModel.glycemia = "";

    this.basicModel.tc = infoPac.temp_corp;

    this.patientProviderService.weight = infoPac.peso;
    this.patientProviderService.height = infoPac.estatura;
    //this.patientProviderService.imc = infoPac.imc;
    //this.calculateImc();

    if (
      infoPac.get_tipo_sangre != undefined ||
      infoPac.get_tipo_sangre != null
    ) {
      this.tipo_sangre = Number(infoPac.get_tipo_sangre.id_pk);
      this.basicModel.blood_group = Number(infoPac.get_tipo_sangre.id_pk);
    }

    if (this.genero == "F") {
      this.loadDataGinecoObstetricos(infoPac);
    }
  }

  loadDataGinecoObstetricos(infoPac: any) {
    let ginicoObstetricos_data: any = infoPac.get_datos_gineco_obstetricos;

    if (ginicoObstetricos_data != undefined || ginicoObstetricos_data != null) {
      // Cuando el tipo de embarazo es no
      /*this.ginecoObstetricos.date_citologia_cervicouterina = this._formatDate(
        ginicoObstetricos_data.fec_ult_citologia
      );
  
      this.ginecoObstetricos.result = ginicoObstetricos_data.result_citologia;
        */
      // Cuando el tipo embarazo es si
      /*this.ginecoObstetricos.date_u_m = this._formatDate(
        ginicoObstetricos_data.fum
      );
      this.ginecoObstetricos.date_u_m_fin = this._formatDate(
        ginicoObstetricos_data.fum_fin
      );
  */

      //this.ginecoObstetricos.embarazo = '1';
      this.ginecoObstetricos.date_posibility_parto = this._formatDate(
        ginicoObstetricos_data.fec_posible_parto
      );
      // Verificamos si esta en embarazo por las semanas de gestación
      // se enlaza el atributo semanas_gest respuesta del servicio con la vista semanas_gestación
      //this.ginecoObstetricos.semanas_gestacion = ginicoObstetricos_data.semanas_gest;
      this.ginecoObstetricos.semanas_gestacion = ginicoObstetricos_data.semanas_gest;
      console.log(ginicoObstetricos_data)
      if (ginicoObstetricos_data.semanas_gest) {
        this.ginecoObstetricos.embarazo = true;
        console.log("embarazo " + this.ginecoObstetricos.embarazo);
      } else {
        this.ginecoObstetricos.embarazo = false;
      }

      this.ginecoObstetricos.semanas_gestacion =
        ginicoObstetricos_data.semanas_gest;
    }
  }

  formatDate(date: string) {
    // Transforma YYY-MM-DD
    if (date != null || date != undefined) {
      let dateTransform = date.split("-"); // asi se debe enviar YYY-MM-DD Ej: 2017-12-29
      let format_Date =
        dateTransform[0] +
        "-" +
        dateTransform[1] +
        "-" +
        dateTransform[2].split(" ")[0];
      return format_Date;
    }

    return null;
  }

  _formatDate(date: string) {
    // Transforma YYY-MM-DD
    if (date != null || date != undefined) {
      let dateTransform = date.split("/"); // asi se debe enviar YYY-MM-DD Ej: 2017-12-29
      let format_Date =
        dateTransform[2] + "-" + dateTransform[1] + "-" + dateTransform[0];
      return format_Date;
    }

    return null;
  }

  updateIsEmbarazo() {
    this.saveItem('embarazo', 'OPC_EMBARAZO', 'gineco');

    if (this.ginecoObstetricos.date_u_m != null) {
      if (this.ginecoObstetricos.embarazo == true) {
        let formatUm = this.ginecoObstetricos.date_u_m.split("-");
        let dateUtm = formatUm[2] + "-" + formatUm[1] + "-" + formatUm[0];
        let datePosPart = this.datePosibleParto(280, dateUtm);
        let format = datePosPart.split("-");
        this.ginecoObstetricos.date_posibility_parto =
          format[2] + "-" + format[1] + "-" + format[0];
        let fechaInicio = new Date(this.ginecoObstetricos.date_u_m).getTime();
        let fechaFin = +new Date(
          new Date()
            .toJSON()
            .slice(0, 10)
            .replace(/-/g, "-")
        ).getTime();
        let diff = fechaFin - fechaInicio;
        let days = diff / (1000 * 60 * 60 * 24) / 7;

        this.ginecoObstetricos.semanas_gestacion = Number(
          days.toString().split(".")[0]
        );
      }
    }
  }

  //dd/mm/aaaa y dd-mm-aaaa. Admite como separador ‘/’ y ‘-‘.
  datePosibleParto(d: any, _fecha: any) {
    let Fecha = new Date();
    let sFecha =
      _fecha ||
      Fecha.getDate() +
      "/" +
      (Fecha.getMonth() + 1) +
      "/" +
      Fecha.getFullYear();
    let sep = sFecha.indexOf("/") != -1 ? "/" : "-";
    let aFecha = sFecha.split(sep);
    let fecha: any = aFecha[2] + "/" + aFecha[1] + "/" + aFecha[0];
    fecha = new Date(fecha);
    fecha.setDate(fecha.getDate() + parseInt(d));
    let anno = fecha.getFullYear();
    let mes = fecha.getMonth() + 1;
    let dia = fecha.getDate();
    mes = mes < 10 ? "0" + mes : mes;
    dia = dia < 10 ? "0" + dia : dia;
    let fechaFinal = dia + sep + mes + sep + anno;
    return fechaFinal;
  }

  editGineObstetricos() {
    console.log(this.ginecoObstetricos.date_citologia_cervicouterina);
    let fec_ult_citologia;
    if (this.ginecoObstetricos.date_citologia_cervicouterina == 'undefined-undefined-' || this.ginecoObstetricos.date_citologia_cervicouterina == undefined || this.ginecoObstetricos.date_citologia_cervicouterina == null) {
      console.log("siiiiii editG");
      fec_ult_citologia = "";
    }
    else {
      fec_ult_citologia = this.patientProviderService.formatDate(this.ginecoObstetricos.date_citologia_cervicouterina);
    }
    let params = {};
    if (this.ginecoObstetricos.embarazo == true) {
      params = {
        "pac_fk": this.getCurrentUser().id_pk,
        "fum": this.patientProviderService.formatDate(this.ginecoObstetricos.date_u_m),
        "fum_fin": this.patientProviderService.formatDate(this.ginecoObstetricos.date_u_m_fin),
        "fec_ult_citologia": fec_ult_citologia,
        "result_citologia": this.ginecoObstetricos.result,
        "embarazo": 1
      }
    } else {
      params = {
        "pac_fk": this.getCurrentUser().id_pk,
        "fum": this.patientProviderService.formatDate(this.ginecoObstetricos.date_u_m),
        "fum_fin": this.patientProviderService.formatDate(this.ginecoObstetricos.date_u_m_fin),
        "fec_ult_citologia": this.patientProviderService.formatDate(this.ginecoObstetricos.date_citologia_cervicouterina),
        "result_citologia": this.ginecoObstetricos.result,
        "embarazo": 0
      }
    }

    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiPost(params, SERVICES.EDITAR_DATOS_GINECO_API_SERV)
      .then(state => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (state["status"] == 404) {
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (state["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
        } else {
          let respuesta = JSON.parse(state["_body"]);

          if (respuesta.success) {
            this.ServicesProvider.toast(
              MESSAGES.dataHealth.ginecoobstetricos.update.success,
              "successToast"
            );
            //this.getDataHealthPatient(this.getCurrentUser().id_pk);
            this.ginecoObstetricos.embarazo == true
              ? (this.ginecoObstetricos.embarazo = true)
              : (this.ginecoObstetricos.embarazo = false);
            this.errorsGineco = [];
          } else {
            this.errorsGineco = [];
            for (var name in respuesta.errors) {
              let value = respuesta.errors[name];
              this.errorsGineco.push(value);
            }
            this.ServicesProvider.toast(
              this.errorsGineco.toString()
            );
          }
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  setBlood_group(selected: number) {
    this.basicModel.blood_group = selected;
  }

  calculateImc() {
    if (this.basicModel.weight != "" && this.basicModel.height != "") {
      let meters_height = Number(this.basicModel.height) / 100;
      let imc =
        Number(this.basicModel.weight) / (meters_height * meters_height);
      this.basicModel.imc = Number(imc.toFixed(2));
      this.indiceMC = this.patientProviderService.clasificationImc(
        this.basicModel.imc
      );
    }
  }

  help(option: any) {
    let text = "";
    switch (option) {
      case '1':
        text = "En este campo podrás seleccionar tu tipo de sangre, dato de <b>vital</b> importancia ante cualquier atención que requieras.\nSi deseas adjunta una foto que ratifique la información proporcionada.";
        break;
      case '2':
        text = "En este campo podrás registrar el resultado y fecha de la última citología que te realizaste, con el fin de llevar un control y facilitar dicha información en caso de asistencia médica sobre el tema."
        break;
    }
    let alert = this.alertCtrl.create({
      title: "Midis App Salud",
      message:
        text,
      buttons: [
        {
          text: "Aceptar",
          role: "confirm"
        }
      ]
    });

    alert.present();
  }


  loadNew() {
    this.timeOutControl = true;
    let params = { "pac_fk": this.getCurrentUser().id_pk };
    this.promiseInitialize(
      this.ServicesProvider.apiPost(
        params,
        SERVICES.DATOS_SALUD_PACIENTE_API_SERV
      ),
      this.ServicesProvider.apiGet("", SERVICES.LISTAR_TIPO_SANGRE_SERV),
      this.ServicesProvider.apiGet("?pac_fk=" + params.pac_fk, SERVICES.ULTIMOS_DATOS_SALUD)
    );
  }
}
