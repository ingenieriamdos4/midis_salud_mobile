import { Component, ElementRef } from '@angular/core';
import { NavParams, Events, ViewController, ModalController, AlertController, Alert } from 'ionic-angular';
import { Camera, CameraOptions } from "@ionic-native/camera";
import { ImagePicker, ImagePickerOptions } from "@ionic-native/image-picker";
import { DocumentsProviderService } from "../../../../providers/documents/documents";
import { ServicesProvider } from "../../../../providers/services/services";
import { PatientProviderService } from "../../../../providers/patient/patient";
import { InAppBrowser, InAppBrowserOptions } from "@ionic-native/in-app-browser";
/* Mensajes de la app */
import { MESSAGES } from "../../../../config/messages";
import { SERVICES } from "../../../../config/url.servicios";

import { CropImagePage } from "../../../crop-image/crop-image";


@Component({
  templateUrl: 'modal_sangre.html'
})
export class ModalSangre {
  errors: Array<string> = [];
  estado: string;

  hasFile: boolean = false;
  //siempre es true, excepto que sea un android en una versión menor a la 4.4, en ese caso será false de acuerdo a la ejecución de la función validate_so_v
  so_flag: boolean = true;
  nombre: string = "";
  edad: number = 0;
  timeOut: any;
  carga_foto: boolean = false;
  carga_adjunto: boolean = false;
  archivo_adjunto: any;
  base64Image: any;
  timeOutControl: boolean = true;
  showButtons: boolean = false;
  constructor(
    public viewCtrl: ViewController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public ServicesProvider: ServicesProvider,
    private imagePicker: ImagePicker,
    private camera: Camera,
    private elem: ElementRef,
    public documentsProviderService: DocumentsProviderService,
    public patientProviderService: PatientProviderService,
    private iab: InAppBrowser,
    public events: Events
  ) {
    this.so_flag = this.ServicesProvider.validate_so_v();

    console.log(this.archivo_adjunto);
  }

  ionViewWillEnter() {
    this.archivo_adjunto = this.getCurrentUser().archivo_adjunto;
    this.estado = this.getCurrentUser().estado;
    console.log(this.archivo_adjunto, " ", this.estado);
  }

  getCurrentUser() {
    return JSON.parse(localStorage.getItem("user"));
  }

  verifyTypeFile(file: string) {
    let ext: any = file
      .split(".")
      .pop()
      .toLowerCase();

    if (ext == "jpg" || ext == "jpeg" || ext == "png") {
      return true;
    }

    return false;
  }
  splitFileName(file: string) {
    return file.split("/").pop();
  }
  reviewFile(file: string) {
    let options: InAppBrowserOptions = {
      hardwareback: 'yes'
    };
    this.iab.create(file, "_blank", options);
  }
  pageRendered(e: CustomEvent) {
    this.showButtons = true;
  }

  cancelModal() {
    this.documentsProviderService.imagesBase64 = [];
    this.base64Image = undefined;
    this.documentsProviderService.arrayFiles = [];
    this.viewCtrl.dismiss();
  }

  cerrar_sin_parametros() {
    this.viewCtrl.dismiss();
  }

  goCropEditor(image: any, aspect: any) {

    let cropAlert = this.alertCtrl.create({
      title: "Midis App Salud",
      message: "¿Desea cortar o editar esta foto?.\nPulse no para utilizar la original.",
      buttons: [
        {
          text: "Sí",
          handler: () => {
            let cropModal = this.modalCtrl.create(CropImagePage, { "image": image, "aspect": aspect });
            cropModal.onDidDismiss(data => {
              if (data.croppedImage != null) {
                this.base64Image = data.croppedImage;
              }
            });
            cropModal.present();
          }
        },
        {
          text: "No",
          role: "cancel",
          handler: () => {
            this.base64Image = image;
          }
        }
      ]
    });
    cropAlert.present();

    /*
    let response = this.configPluginsProvider.cropper(image, aspect);
    this.photo = response;
    */
  }

  tomarPhone() {
    this.documentsProviderService.imagesBase64 = [];
    this.base64Image = undefined;
    this.documentsProviderService.arrayFiles = [];
    const options: CameraOptions = {
      quality: 60,
      //allowEdit: true,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    };

    this.camera.getPicture(options).then(
      imageData => {
        let base64Image = "data:image/jpeg;base64," + imageData;
        //let croppedBase64Image = this.goCropEditor(base64Image, 0);
        //this.documentsProviderService.imagesBase64[0] = croppedBase64Image;
        this.goCropEditor(base64Image, 0);
        this.carga_foto = true;
        this.archivo_adjunto = false;
      },
      err => {
        this.ServicesProvider.toast(
          MESSAGES.camera.message.selected
        );
        this.carga_foto = false;

      }
    );
  }

  loadFile() {
    document.getElementById("fileUpload").click();
    this.hasFile = true;
  }


  getUser(id_paciente: number) {
    let params = { "id_paciente": id_paciente };
    this.ServicesProvider.createLoader("Actualizando.. por favor espere");
    this.ServicesProvider
      .apiPost(params, SERVICES.CONSULTAR_PACIENTE_API_SERV)
      .then(state => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (state["status"] == 404 || state["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (state["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(state["_body"]);
          localStorage.setItem("user", JSON.stringify(respuesta));
          this.events.publish('user:imagen_porcentaje', respuesta.imagen_porcentaje);
          this.cancelModal();
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();
        this.ServicesProvider.toast("ha ocurrido un problema, revisa tu conexión a internet"
        );
        this.clearTimeOut();
        this.timeOutControl = false;
        //});
      });
  }


  logForm() {
    let id_pk = this.patientProviderService.getCurrentUser().id_pk;
    let formData = new FormData();
    formData.append("pac_fk", id_pk);


    if (this.base64Image) {
      //formData.append("array_fotos_tipo_sangre[]", this.documentsProviderService.imagesBase64[0]);
      formData.append("array_fotos_tipo_sangre[]", this.base64Image);
    }

    if(this.documentsProviderService.arrayFiles.length > 0){
      for (let index = 0; index < this.documentsProviderService.arrayFiles.length; index++) {
        formData.append("adjunto_tipo_sangre", this.documentsProviderService.arrayFiles[index].file, this.documentsProviderService.arrayFiles[index].nameFile);
      }
    }
    
    this.ServicesProvider.createLoader();

    this.ServicesProvider
      .apiPost(formData, SERVICES.EDIT_ADJUNTO_SANGRE)
      .then(file => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (file["status"] == 404 || file["status"] == 500) {
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (file["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
        } else {
          let respuesta = JSON.parse(file["_body"]);
          this.errors = [];

          if (respuesta.success == true) {
            this.ServicesProvider.toast(MESSAGES.fileHealtData.create.success, "successToast");
            this.documentsProviderService.imagesBase64 = [];
            this.base64Image = undefined;
            this.documentsProviderService.arrayFiles = [];


            this.carga_adjunto = false;
            this.carga_foto = false;
            this.getUser(id_pk);

          }
          else if (respuesta.errors != undefined) {
            this.ServicesProvider.toast(
              respuesta.errors[Object.keys(respuesta.errors)[0]]
            );
          }
          else if (respuesta.success == false) {
            this.ServicesProvider.toast(
              MESSAGES.dataHealth.modal_sangre.cargar_sangre.error
            );
          }

          else if (respuesta.success == "sin_espacio") {
            this.ServicesProvider.toast(respuesta.mensaje);
          }
          else {
            for (var name in respuesta.errors) {
              let value = respuesta.errors[name];
              this.errors.push(value);
            }
            this.ServicesProvider.toast(this.errors.toString());
          }
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  clearTimeOut() {
    clearTimeout(this.timeOut);
  }

  loadImages() {
    this.documentsProviderService.imagesBase64 = [];
    this.documentsProviderService.arrayFiles = [];


    let options: ImagePickerOptions = {
      maximumImagesCount: 1,
      quality: 85,
      width: 510,
      height: 510,
      outputType: 1 // para que sea una imagen en base64, 0 fisico dispositivo
    };

    this.imagePicker
      .getPictures(options)
      .then(
        results => {
          //if (results.length == 1) {
          this.base64Image = "data:image/png;base64," + results[0];
          //}
          /*else {
            for (var i = 0; i < results.length; i++) {
              this.documentsProviderService.imagesBase64[0] = "data:image/png;base64," + results[i];
            }
          }*/
          //this.hasFile = true;
          this.carga_foto = true;
          this.archivo_adjunto = false;

        },
        err => {
          this.carga_foto = false;
          this.ServicesProvider.toast(
            MESSAGES.fileHealtData.upload.error
          );
        }
      )
      .catch(err => {
        this.ServicesProvider.toast(err);
      });
  }

  cancelLoadImages() {
    this.documentsProviderService.imagesBase64 = [];
    this.base64Image = undefined;
    //this.hasFile = false;
    this.carga_foto = false;
    this.archivo_adjunto = false;


  }

  onUploadChange(ev) {
    this.documentsProviderService.imagesBase64 = [];
    this.base64Image = undefined;
    this.documentsProviderService.arrayFiles = [];
    let extErrorMessage =
      "Solo se permiten archivos con extensión: .jpg, .jpeg, .gif, .png, .dcm, .doc, docx, o pdf";
    let allowedExtension = [
      "jpg",
      "jpeg",
      "gif",
      "png",
      "dicom",
      "pdf",
      "dcm",
      "doc",
      "docx"
    ];
    let extError = false;
    let myFiles = ev.target.files[0];

    if (myFiles != undefined) {
      let extName = myFiles["name"]
        .split(".")
        .pop()
        .toLowerCase();

      if (allowedExtension.indexOf(extName) == -1) {
        extError = true;
      }

      if (extError) {
        this.ServicesProvider.toast(extErrorMessage);
        this.carga_adjunto = false;

      } else {
        /*this.documentsProviderService.arrayFiles.push({
          nameFile: myFiles["name"],
          file: myFiles
        });*/
        this.documentsProviderService.arrayFiles[0] = {
          nameFile: myFiles["name"],
          file: myFiles
        };
        this.carga_adjunto = true;
        this.archivo_adjunto = false;

        this.elem.nativeElement.querySelector("#fileUpload").value = "";
      }
    }
  }

  deleteFile(index: number) {
    this.documentsProviderService.arrayFiles.splice(index, 1);
    this.hasFile = false;
    this.carga_adjunto = false;
    this.archivo_adjunto = false;
    console.log(this.hasFile);
  }











}
