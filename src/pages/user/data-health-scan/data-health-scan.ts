import { Component } from "@angular/core";
import { BasicPatientInformationPage } from "../../index.paginas";
import { PatientProviderService } from "../../../providers/patient/patient";
import { AntecedentProviderService } from "../../../providers/antecedent/antecedent";
import { ServicesProvider } from "../../../providers/services/services";
/* Mensajes de la app */
//import { MESSAGES } from "../../../config/messages";

@Component({
  selector: "page-data-health-scan",
  templateUrl: "data-health-scan.html"
})
export class DataHealthScanPage {
  rh: string;
  listAntecedentAlert: Array<string> = [];
  paciente: any;
  timeOut: any;
  timeOutControl: boolean = true;
  serv_amb:any;
  inst_asiste:any;
  segu_prepa:any;
  serv_trans:any;
  // en clase BasicPatientInformationPage están así:
  //this.infoPac = this.navParams.get("paciente");
  //this.personal = this.navParams.get("personal");
  constructor(
    public basicPatient: BasicPatientInformationPage,
    public patientProviderService: PatientProviderService,
    public ServicesProvider: ServicesProvider,
    public antecedentProviderService: AntecedentProviderService
  ) {
    this.paciente = this.basicPatient.infoPac;
    this.listAntecedentAlert = this.paciente.antecedentes.original;    
    console.log(this.paciente )
    this.rh = this.paciente.get_tipo_sangre?this.paciente.get_tipo_sangre.nombre:null;
    console.log("DataHealthScanPage. Paciente desde basicPatient = ", this.paciente);
    console.log("DataHealthScanPage. listAntecedentAlert = ", this.paciente.original);
    this.serv_amb=this.paciente.get_datos_institucion[0]?this.paciente.get_datos_institucion[0].serv_amb : false;
    this.inst_asiste=this.paciente.get_datos_institucion[0]?this.paciente.get_datos_institucion[0].inst_asiste : false;
    this.segu_prepa=this.paciente.get_datos_institucion[0]?this.paciente.get_datos_institucion[0].segu_prepa : false;
    this.serv_trans=this.paciente.get_datos_institucion[0]?this.paciente.get_datos_institucion[0].serv_trans : false;


  }

  ionViewDidLoad() {
    // this.listAntecedentAlert = this.paciente.antecedentes;
    // this.rh = this.paciente.get_tipo_sangre.nombre;
    // let params = {"pac_fk":this.paciente.id_pk, "solo_alerta" : 1, "mostrar_ant_completo": 0, "tan_fk": "" };

    // let params2 = {"pac_fk": this.paciente.id_pk};

    //this.listAntecedentAlert = this.paciente.antecedentes;
    //this.rh = respuesta[0].get_tipo_sangre != null ? respuesta[0].get_tipo_sangre.nombre: "";

    // this.listAntecedentAndDataHealthByPatient(
    //   this.ServicesProvider.apiPost(
    //     params,
    //     "/listar_antecedentes_paciente"
    //   ),
    //   this.ServicesProvider.apiPost(
    //     params2,
    //     "/datos_salud_paciente_api"
    //   )
    // );
  }

  clearTimeOut() {
    clearTimeout(this.timeOut);
  }

  // listAntecedentAndDataHealthByPatient(
  //   promiseAntecedent: any,
  //   promiseHealthData: any
  // ) {
  //   this.ServicesProvider.createLoader();
  //   Promise.all([promiseAntecedent, promiseHealthData])
  //     .then(promises => {
  //       this.ServicesProvider.loading.dismiss();
  //       this.clearTimeOut();
  //       if (promises[0]["status"] == 404 || promises[0]["status"] == 500) {
  //         this.timeOutControl = false;
  //         this.ServicesProvider.toast(MESSAGES.services.error);
  //       } else if (promises[0]["status"] == 401) {
  //         this.ServicesProvider.toast(
  //           MESSAGES.services.unauthorized
  //         );
  //         this.timeOutControl = false;
  //       } else {
  //         let respuesta = JSON.parse(promises[0]["_body"]);
  //         this.listAntecedentAlert = respuesta;
  //       }

  //       if (promises[1]["status"] == 404 || promises[1]["status"] == 404) {
  //         this.timeOutControl = false;
  //         this.ServicesProvider.toast(MESSAGES.services.error);
  //       } else if (promises[1]["status"] == 401) {
  //         this.ServicesProvider.toast(
  //           MESSAGES.services.unauthorized
  //         );
  //         this.timeOutControl = false;
  //       } else {
  //         let respuesta = JSON.parse(promises[1]["_body"]);
  //         this.rh =
  //           respuesta[0].get_tipo_sangre != null
  //             ? respuesta[0].get_tipo_sangre.nombre
  //             : "";
  //       }
  //     })
  //     .catch(err => {
  //       this.ServicesProvider.loading.onDidDismiss(() => {
  //         this.clearTimeOut();
  //         this.timeOutControl = false;
  //         this.ServicesProvider.toast(MESSAGES.services.error);
  //       });
  //     });
  // }

  getSexo(sexo: string) {
    return sexo == "F" ? "Femenino" : "Masculino";
  }

  loadNew() {
    this.timeOutControl = true;
    this.ionViewDidLoad();
  }
}
