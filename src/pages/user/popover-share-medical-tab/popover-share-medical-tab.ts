import { Component } from "@angular/core";
import { NavController, NavParams, Platform } from "ionic-angular";
import { FileOpener } from "@ionic-native/file-opener";
import { File } from "@ionic-native/file";
import { PatientProviderService } from "../../../providers/patient/patient";
import { ServicesProvider } from "../../../providers/services/services";
import { SocialSharing } from "@ionic-native/social-sharing";
import { PageHelpPage } from "../../page-help/page-help"
/* Mensajes de la app */
import { MESSAGES } from "../../../config/messages";
import { SERVICES } from "../../../config/url.servicios";
import { ViewController } from "ionic-angular";
declare var cordova: any;
@Component({
  selector: "page-popover-share-medical-tab",
  templateUrl: "popover-share-medical-tab.html"
})
export class PopoverShareMedicalTabPage {
  url: string;
  Message: any;
  timeOut: any;
  timeOutControl: boolean = true;
  storageDirectory: string = "";
  isFuncActive: boolean = true;
  isDownloaded: boolean = false;
  namePdf: string;
  imgHelp: string;
  pageHelp = PageHelpPage;
  isIos: boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public patientProviderService: PatientProviderService,
    public ServicesProvider: ServicesProvider,
    private socialSharing: SocialSharing,
    private fileOpener: FileOpener,
    private file: File,
    public viewCtrl: ViewController,
    public platform: Platform
  ) {
    this.platform.ready().then(() => {
      // make sure this is on a device, not an emulation (e.g. chrome tools device mode)
      if (!this.platform.is("cordova")) {
        return false;
      }
      if (this.platform.is("ios")) {
        this.storageDirectory = cordova.file.documentsDirectory;
        this.isIos = true;
      } else if (this.platform.is("android")) {
        this.storageDirectory = cordova.file.externalRootDirectory;
      } else {
        // exit otherwise, but you could add further types here e.g. Windows
        return false;
      }
    });

    this.imgHelp = this.navParams.get('typePage');
  }

  ionViewDidLoad() {
    let user = JSON.parse(localStorage.getItem("user"));
    this.namePdf = "fichaMedica" + user.nombres + ".pdf";
  }

  clearTimeOut() {
    clearTimeout(this.timeOut);
  }
  close() {
    this.viewCtrl.dismiss();    
  }

  download() {
    if(this.isIos){
      this.shareFichaMedical();
      return;
    }
    this.isFuncActive = true;
    this.getMedicalTab();
    this.close();
  }

  shareFichaMedical() {
    if (this.isDownloaded) {
      this.shareMedicalTab();
    } else {
      this.isFuncActive = false;
      this.getMedicalTab();      
    }
    this.close();
  }

  getMedicalTab() {
    //this.createLoader(50000);
    // Método para traer la url de la ficha médica única del usuario.
    this.patientProviderService.spinnerFichaMedica = true;
    let user = JSON.parse(localStorage.getItem("user"));
    let data = {"pac_fk": user.id_pk};
    this.ServicesProvider
      .apiPost(data, SERVICES.INFORME_FICHA_MEDICA_UNICA_API_SERV)
      .then(data => {
         if (data["status"] == 404 || data["status"] == 500) {
          this.ServicesProvider.toast(
            MESSAGES.services.error
          );
          this.patientProviderService.spinnerFichaMedica = false;
        } else {
          let respuesta = JSON.parse(data["_body"]);
          this.convert_base64_to_pdf(respuesta);
        }
      })
      .catch(err => {
        this.patientProviderService.spinnerFichaMedica = false;
        this.ServicesProvider.toast(
          MESSAGES.services.error
        );
      });
  }

  convertBaseb64ToBlob(b64Data, contentType): Blob 
  {
    contentType = contentType || '';
    const sliceSize = 512;
    b64Data = b64Data.replace(/^[^,]+,/, '');
    b64Data = b64Data.replace(/\s/g, '');
    const byteCharacters = window.atob(b64Data);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) 
    {
      const slice = byteCharacters.slice(offset, offset + sliceSize);
      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
          byteNumbers[i] = slice.charCodeAt(i);
      }
      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }
    return new Blob(byteArrays, {type: contentType});
  }

  convert_base64_to_pdf(downloadPDF: any)
  {
    console.log("test cb64 1");
    // fetch("data:application/pdf;base64," + downloadPDF, { method: "GET"})
    //   .then(res => res.blob())
    //   .then(blob => 
    //     {
          console.log("test cb64 2");
          this.file.writeFile( this.storageDirectory, this.namePdf, this.convertBaseb64ToBlob(downloadPDF, 'data:application/pdf;base64'),{ replace: true })
          .then(res => 
          {
            console.log("------------------------------ res = ", res);
            console.log("test cb64 3");            
            this.isDownloaded = true;
            this.patientProviderService.spinnerFichaMedica = false;
            console.log("test cb64 4");
            if (this.isFuncActive) 
            {
              console.log("test cb64 5");
              this.ServicesProvider.toast(MESSAGES.medical_record.downloader.success,"successToast");
              console.log("test cb64 6");              
              this.fileOpener.open(res.toInternalURL(), "application/pdf")
              .then(res => { console.log("se abrió el archivo");})
              .catch(err => 
              {
                console.log("problemas al abrir el archivo = " + err);
                this.patientProviderService.spinnerFichaMedica = false;
                this.ServicesProvider.toast(MESSAGES.fileOpener.message.error);
              });
              console.log("test cb64 7");
            } 
            else
            {
              console.log("test cb64 8");
              this.shareMedicalTab();
              console.log("test cb64 9");
            }
          })
          .catch(err => {
            console.log("test cb64 10");
            this.patientProviderService.spinnerFichaMedica = false;
            this.ServicesProvider.toast(
              MESSAGES.codQr.downloader.error
            );
          });
          console.log("test cb64 11");
      // })
      // .catch(err => {
      //   console.log("test cb64 12");
      //   this.patientProviderService.spinnerFichaMedica = false;
      //   console.log("test cb64 13");
      //   this.ServicesProvider.toast(
      //     MESSAGES.convertBase64ToPdf.message.error
      //   );
      //   console.log("test cb64 14");
      // });
  }

  shareMedicalTab() {
    this.Message = "comparto la ficha médica que tengo en Midis App";
    this.url = this.storageDirectory + "/"+this.namePdf;
    this.socialSharing
      .share(this.Message, "Midis App", this.url, "")
      .then((result) => {        
        this.ServicesProvider.toast("fin de la funcionalidad compartir ficha médica", 'successToast');
      })
      .catch(() => {
        this.ServicesProvider.closeLoader();
        this.ServicesProvider.toast("No se pudo compartir la ficha médica");
      });
  }
}
