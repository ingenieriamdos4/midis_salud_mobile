import { Component } from "@angular/core";
import {
  NavController,
  NavParams,
  AlertController,
  MenuController,
  App
} from "ionic-angular";

import {
  ChangePasswordPage,
  LoginPage,
  PageHelpPage
} from "../../index.paginas";
import { PatientProviderService } from "../../../providers/patient/patient";
import { ServicesProvider } from "../../../providers/services/services";

/* Mensajes de la app */
import { MESSAGES } from "../../../config/messages";
import { SERVICES } from "../../../config/url.servicios";

import { TranslateService } from '@ngx-translate/core';

class Configuration {
  pac_fk: number;
  residencia: boolean;
  datos_familiares: boolean;
  datos_medicos: boolean;
}

@Component({
  selector: "page-settings",
  templateUrl: "settings.html"
})
export class SettingsPage {
  pageHelp = PageHelpPage;
  changePassword = ChangePasswordPage;
  timeOut: any;
  timeOutControl: boolean = true;
  config: Configuration;
  getConfiguration: Array<string> = [];
  aux: boolean = false;
  isVisibleDataFamily: boolean = false;
  isVisibleDataMedical: boolean = false;
  isVisibleResidence: boolean = false;
  idioma: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public patientProviderService: PatientProviderService,
    public ServicesProvider: ServicesProvider,
    private alertCtrl: AlertController,
    private menuCtrl: MenuController,
    public appCtrl: App,
    private translateService: TranslateService
  ) {
    this.config = new Configuration();
    this.config.pac_fk = this.patientProviderService.getCurrentUser().id_pk;
    this.idioma = this.translateService.currentLang;
  }

  cambioIdioma(selectedValue: any) {
    this.translateService.use(selectedValue);
  }

  ionViewWillEnter() {
    /*if( localStorage.getItem("wizzard_configuracion")==undefined){
      localStorage.setItem("wizzard_configuracion","true");
      this.navCtrl.push( this.pageHelp ,{ 'seccion': 'configuracion' });
    }  */
  }

  ionViewDidLoad() {
    this.getUser(this.patientProviderService.getCurrentUser().id_pk);
  }

  clearTimeOut() {
    clearTimeout(this.timeOut);
  }

  getUser(id_paciente: number) {
    let params = { "id_paciente": id_paciente };
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiPost(params, SERVICES.CONSULTAR_PACIENTE_API_SERV)
      .then(state => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (state["status"] == 404 || state["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (state["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(state["_body"]);
          localStorage.setItem("user", JSON.stringify(respuesta));
          this.getConfiguration = respuesta["get_configuracion"];

          if (this.getConfiguration.length > 0) {
            this.getConfiguration[0]["residencia"] == "0"
              ? (this.isVisibleResidence = false)
              : (this.isVisibleResidence = true);
            this.getConfiguration[0]["datos_familiares"] == "0"
              ? (this.isVisibleDataFamily = false)
              : (this.isVisibleDataFamily = true);
            this.getConfiguration[0]["datos_medicos"] == "0"
              ? (this.isVisibleDataMedical = false)
              : (this.isVisibleDataMedical = true);
          }

          setTimeout(() => {
            this.aux = true;
          }, 100);
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.timeOutControl = false;
        this.ServicesProvider.toast(
          MESSAGES.services.error + err
        );
        //});
      });
  }

  configuration(configuration: Configuration) {
    let params = {
      "id_paciente": configuration.pac_fk,
      "residencia": (configuration.residencia != undefined ? configuration.residencia == false ? 0 : 1 : 0),
      "datos_familiares": (configuration.datos_familiares != undefined ? configuration.datos_familiares == false ? 0 : 1 : 0),
      "datos_medicos": (configuration.datos_medicos != undefined ? configuration.datos_medicos == false ? 0 : 1 : 0)
    };
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiPost(params, SERVICES.CONFIGURACION_PACIENTE_SERV)
      .then(state => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (state["status"] == 404 || state["status"] == 500) {
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (state["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
        } else {
          let respuesta = JSON.parse(state["_body"]);
          if (respuesta.success) {
            this.ServicesProvider.toast(
              MESSAGES.configuration.update.success,
              "successToast"
            );
          } else {
            this.ServicesProvider.toast(
              MESSAGES.configuration.update.error
            );
          }
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  updateConfig() {
    this.aux == true ? this.configuration(this.config) : "";
  }

  inactiveAccount() {
    let alert = this.alertCtrl.create({
      title: "Eliminar cuenta",
      message: "¿Esta seguro que desea eliminar la cuenta?",
      buttons: [
        {
          text: "Cancelar",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        },
        {
          text: "Confirmar",
          handler: () => {
            this.inactiveAccountService();
          }
        }
      ]
    });

    alert.present();
  }

  inactiveAccountService() {
    let params = {
      "login_pk": this.patientProviderService.getCurrentUser().get_user[0].login_pk,
      "pac_fk": this.patientProviderService.getCurrentUser().id_pk
    };
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiPost(params, SERVICES.DESACTIVAR_USUARIO_SERV)
      .then(state => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (state["status"] == 404 || state["status"] == 500) {
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (state["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
        } else {
          let respuesta = JSON.parse(state["_body"]);

          if (respuesta.success) {
            this.ServicesProvider.toast(
              MESSAGES.inactiveAccount.message.success,
              "successToast"
            );
            localStorage.clear();
            this.menuCtrl.enable(false);
            this.appCtrl.getRootNav().push(LoginPage);
          } else {
            this.ServicesProvider.toast(
              MESSAGES.inactiveAccount.message.error
            );
          }
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.ServicesProvider.toast(
          MESSAGES.services.error + err
        );
        //});
      });
  }

  loadNew() {
    this.timeOutControl = true;
    this.getUser(this.patientProviderService.getCurrentUser().id_pk);
  }
}
