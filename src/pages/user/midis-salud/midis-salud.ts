import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ModalController, PopoverController, Platform, Toast } from 'ionic-angular';
import { File } from "@ionic-native/file";
import { FileOpener } from "@ionic-native/file-opener";
import { SocialSharing } from "@ionic-native/social-sharing";
import { PatientProviderService } from "../../../providers/patient/patient";
import { ServicesProvider } from "../../../providers/services/services";
import { PageHelpPage, PopoverMidisPage } from "../../index.paginas";
import { SERVICES } from '../../../config/url.servicios';
import { MESSAGES } from '../../../config/messages';
import { MidisDetailPage } from './midis-detail/midis-detail';
import { MidisFilterPage } from './midis-filter/midis-filter';

declare var cordova: any;

@Component({
  selector: 'page-midis-salud',
  templateUrl: 'midis-salud.html',
})
export class MidisSaludPage {
  pageHelp = PageHelpPage;
  detailsPage = MidisDetailPage;
  filterPage = MidisFilterPage;
  imageMidisSaludPublicity: string = "assets/imgs/cargando.png";

  filter: Array<any> = [];

  pac_fk: any;
  filter_motivo_cons: any;
  filter_fecha_inicio_busq: any;
  filter_fecha_fin_busq: any;
  filter_diagnostico: any;
  filter_prof_cons: any;
  filter_inst_cons: any;

  midisDataResponse: any;

  enableSelect: boolean = false;
  consultsSelected: Array<boolean> = [];

  isIOS: boolean = false;

  consultsPDF: Array<any> = [];
  storageDirectory: string = "";
  namePdf: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public patientProviderService: PatientProviderService,
    public ServicesProvider: ServicesProvider,
    private alertCtrl: AlertController,
    private modalCtrl: ModalController,
    public popoverCtrl: PopoverController,
    public platform: Platform,
    private file: File,
    private fileOpener: FileOpener,
    private socialSharing: SocialSharing
  ) {
    this.platform.ready().then(() => {
      if (!this.platform.is("cordova")) {
        return false;
      }
      if (this.platform.is("ios")) {
        this.storageDirectory = cordova.file.documentsDirectory;
        this.isIOS = true;
      } else if (this.platform.is("android")) {
        this.storageDirectory = cordova.file.externalRootDirectory;
      } else {
        // exit otherwise, but you could add further types here e.g. Windows
        return false;
      }
    });
    this.pac_fk = this.patientProviderService.getCurrentUser().id_pk;
  }

  ionViewWillEnter() {
    /*if (localStorage.getItem("wizzard_midis_salud") == undefined) {
      localStorage.setItem("wizzard_midis_salud", "true");
      this.navCtrl.push(this.pageHelp, { 'seccion': 'midis_salud' });
    }*/
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MidisSaludPage');
    setTimeout(() => {
      this.getStorePublicity();
    }, 1000);
    this.getDataMidisSalud();
    this.namePdf = "Resultados consultas.pdf";
  }

  filterModal() {
    const modal = this.modalCtrl.create(this.filterPage);
    modal.onDidDismiss(data => {
      this.filter_fecha_inicio_busq = data[0].fecha_inicio;
      this.filter_fecha_fin_busq = data[0].fecha_fin;
      this.filter_motivo_cons = data[0].motivo;
      this.filter_diagnostico = data[0].diagnostico;
      this.filter_prof_cons = data[0].profesional;
      this.filter_inst_cons = data[0].institucion;
      this.getDataMidisSalud();
    });
    modal.present();
  }

  detailElement(element: any) {
    if (!this.enableSelect) {
      this.navCtrl.push(this.detailsPage, {
        elemento: element
      });
    }
  }

  getDataMidisSalud() {
    let params = {
      "pac_fk": this.pac_fk,
      "fecha_inicio_busq": this.filter_fecha_inicio_busq,
      "fecha_fin_busq": this.filter_fecha_fin_busq,
      "motivo_cons": this.filter_motivo_cons,
      "diagnostico": this.filter_diagnostico,
      "pro_fk": this.filter_prof_cons,
      "sdi_fk": this.filter_inst_cons
    };
    console.log(JSON.stringify(params));

    this.ServicesProvider.createLoader();
    this.ServicesProvider.apiPost(params, SERVICES.MIDIS_SALUD)
      .then(midisData => {
        this.ServicesProvider.closeLoader();
        if (midisData["status"] == 404 || midisData["status"] == 500) {
          this.ServicesProvider.toast(MESSAGES.services.error);
        }
        else if (midisData["status"] == 401) {
          this.ServicesProvider.toast(MESSAGES.services.unauthorized);
        }
        else {
          this.midisDataResponse = JSON.parse(midisData["_body"]);
          for (let i = 0; i < this.midisDataResponse.length; i++) {
            this.consultsSelected.push(false);
          }
        }
      });
  }

  popoverMidis(myEvent) {
    let popover = this.popoverCtrl.create(
      PopoverMidisPage,
      { typePage: "107" },
      { cssClass: "popover-ficha-medica" }
    );
    popover.present({
      ev: myEvent
    });
    popover.onDidDismiss(data => {
      if (data != undefined) {
        if (data === 'SELECT') {
          this.enableSelect = true;
        }
        else if (data === 'DOWNLOAD') {
          this.consultsPDF = [];
          for (let i = 0; i < this.midisDataResponse.length; i++) {
            this.consultsPDF.push(this.midisDataResponse[i].id_pk)
            this.consultsSelected[i] = true;
          }
          this.tryDownloadPDF();
        }
        else if (data == "SHARE") {
          let shareAlert = this.alertCtrl.create({
            title: 'Midis App Salud',
            message: 'Enviar un correo electrónico al profesional médico, que <b>no tiene Midis App</b>, para que le permitas el acceso a tu cuenta.',
            inputs: [
              {
                name: 'mail_prof_ext',
                placeholder: 'Correo electrónico del profesional'
              }
            ],
            buttons: [
              {
                text: 'Cancelar',
                role: 'cancel'
              },
              {
                text: 'Enviar',
                handler: data => {
                  this.sendMailExt(data.mail_prof_ext);
                }
              }
            ]
          });
          shareAlert.present();
        }
      }
    });
  }

  sendMailExt(mail: any) {
    if (mail != "" && mail != undefined) {
      var ER = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if (ER.test(mail.toLowerCase())) {
        let params = {
          "pac_fk": this.patientProviderService.getCurrentUser().id_pk,
          "correo_prof_externo": mail
        }
        this.ServicesProvider.createLoader();
        this.ServicesProvider
          .apiPost(params, SERVICES.SEND_MAIL_PROF_EXT)
          .then(state => {
            this.ServicesProvider.closeLoader();
            if (state["status"] == 404 || state["status"] == 500) {
            } else if (state["status"] == 401) {
              this.ServicesProvider.toast(
                MESSAGES.services.unauthorized
              );
            } else {
              let respuesta = JSON.parse(state["_body"]);
              if (respuesta.success == true) {
                this.ServicesProvider.toast("Correo electrónico enviado correctamente.", "successToast");
              }
              else {
                this.ServicesProvider.toast("Error al enviar el correo. " + respuesta.msg);
              }
            }
          })
          .catch(err => {
            this.ServicesProvider.closeLoader();
            this.ServicesProvider.toast("Error al enviar el correo.");
          });
      }
      else {
        this.ServicesProvider.toast("Ingrese un correco electrónico válido");
      }
    }
    else {
      this.ServicesProvider.toast("Ingrese un correo electrónico al cual desea enviar la información.");
    }
  }

  back() {
    this.consultsPDF = [];
    for (let i = 0; i < this.consultsSelected.length; i++) {
      if (this.consultsSelected[i]) {
        this.consultsSelected[i] = false;
      }
    }
    this.enableSelect = !this.enableSelect;
  }
  tryDownloadPDF() {
    let alert = this.alertCtrl.create({
      title: "Informe Midis",
      message: "¿Esta seguro que desea descargar el informe de " + this.consultsPDF.length + " consulta(s)?",
      buttons: [
        {
          text: "Cancelar",
          role: "cancel",
          handler: () => {
            this.consultsPDF = [];
            for (let i = 0; i < this.consultsSelected.length; i++) {
              if (this.consultsSelected[i]) {
                this.consultsSelected[i] = false;
              }
            }
            this.enableSelect = false;
            this.consultsPDF = [];
            for (let i = 0; i < this.consultsSelected.length; i++) {
              this.consultsSelected[i] = false;
            }
          }
        },
        {
          text: "Confirmar",
          handler: () => {
            this.downloadPDF();
          }
        }
      ]
    });
    alert.present();
  }
  downloadPDF() {
    let params = {
      "con_fk": this.concatConsults()
    }
    this.ServicesProvider.createLoader();
    this.ServicesProvider.apiGet(this.concatConsults(), SERVICES.INFORME_CONSULTA + '?con_fk=')
      .then(midisData => {
        this.ServicesProvider.closeLoader();
        if (midisData["status"] == 404 || midisData["status"] == 500) {
          this.ServicesProvider.toast(MESSAGES.services.error);
        }
        else if (midisData["status"] == 401) {
          this.ServicesProvider.toast(MESSAGES.services.unauthorized);
        }
        else {
          let docResponse = JSON.parse(midisData["_body"]);
          this.convert_base64_to_pdf(docResponse);
          this.enableSelect = false;
          this.consultsPDF = [];
          for (let i = 0; i < this.consultsSelected.length; i++) {
            this.consultsSelected[i] = false;
          }
        }
      })
      .catch(err => {
        this.ServicesProvider.toast(
          MESSAGES.services.error
        );
      });
  }

  concatConsults() {
    let response = "";
    for (let i = 0; i < this.consultsPDF.length; i++) {
      response += this.consultsPDF[i];
      if (i < this.consultsPDF.length - 1) {
        response += ",";
      }
    }
    return response;
  }

  getStorePublicity() {
    let params = {
      'mod_padre': 1
    };
    this.ServicesProvider
      .apiPost(params, "/publicidades_modulos")
      .then(state => {
        if (state["status"] == 404 || state["status"] == 500) {
        } else {
          let respuesta = state["_body"];
          this.imageMidisSaludPublicity = respuesta;
        }
      })
      .catch(err => { console.log(err); });
  }

  checkConsult(element: any, i: any) {
    if (this.consultsPDF.indexOf(element) != -1) {
      this.consultsPDF.splice(this.consultsPDF.indexOf(element), 1);
      this.consultsSelected[i] = false;
    }
    else {
      this.consultsPDF.push(element);
      this.consultsSelected[i] = true;
    }
  }

  convertBaseb64ToBlob(b64Data, contentType): Blob {
    contentType = contentType || '';
    const sliceSize = 512;
    b64Data = b64Data.replace(/^[^,]+,/, '');
    b64Data = b64Data.replace(/\s/g, '');
    const byteCharacters = window.atob(b64Data);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);
      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }
      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }
    return new Blob(byteArrays, { type: contentType });
  }

  convert_base64_to_pdf(downloadPDF: any) {
    this.file.writeFile(this.storageDirectory, this.namePdf, this.convertBaseb64ToBlob(downloadPDF, 'data:application/pdf;base64'), { replace: true })
      .then(res => {
        this.patientProviderService.spinnerFichaMedica = false;
        if (!this.isIOS) {
          this.ServicesProvider.toast(MESSAGES.medical_record.downloader.success, "successToast");
          this.fileOpener.open(res.toInternalURL(), "application/pdf")
            .then(res => { console.log("se abrió el archivo"); })
            .catch(err => {
              console.log("problemas al abrir el archivo = " + err);
              this.patientProviderService.spinnerFichaMedica = false;
              this.ServicesProvider.toast(MESSAGES.fileOpener.message.error);
            });
          this.enableSelect = false;
          this.consultsPDF = [];
          for (let i = 0; i < this.consultsSelected.length; i++) {
            this.consultsSelected[i] = false;
          }
        }
        else {
          this.shareDoc();
          this.enableSelect = false;
          this.consultsPDF = [];
          for (let i = 0; i < this.consultsSelected.length; i++) {
            this.consultsSelected[i] = false;
          }
        }
      })
      .catch(err => {
        this.patientProviderService.spinnerFichaMedica = false;
        this.ServicesProvider.toast(
          MESSAGES.codQr.downloader.error
        );
        this.enableSelect = false;
        this.consultsPDF = [];
        for (let i = 0; i < this.consultsSelected.length; i++) {
          this.consultsSelected[i] = false;
        }
      });
  }

  shareDoc() {
    let message = "Informe de consultas seleccionadas en MidisApp Salud.";
    let url = this.storageDirectory + "/" + this.namePdf;
    this.socialSharing
      .share(message, "Midis App", url, "")
      .then((result) => {
        this.ServicesProvider.toast("Informe realizado correctamente", 'successToast');
      })
      .catch(() => {
        this.ServicesProvider.closeLoader();
        this.ServicesProvider.toast("No se pudo realizar el informe correctamente");
      });
  }

}
