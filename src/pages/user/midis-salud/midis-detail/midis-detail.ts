import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServicesProvider } from "../../../../providers/services/services";
import { SERVICES } from '../../../../config/url.servicios';
import { MESSAGES } from '../../../../config/messages';
import { PrescDetailPage } from '../../presc-detail/presc-detail';

@Component({
  selector: 'page-midis-detail',
  templateUrl: 'midis-detail.html',
})
export class MidisDetailPage {
  prescPage = PrescDetailPage;

  element: any;
  date: any;
  hour: any;
  profesional: any;
  diagnostico_presuntivo: any;
  profResultados: any;
  cargaResultados: any;

  dataDetailsResponse: any;
  dataPrescriptions: any;


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public ServicesProvider: ServicesProvider
  ) {
    this.element = this.navParams.get("elemento");
    this.date = this.element.fecha.split(" ")[0];
    this.hour = this.element.fecha.split(" ")[1];
    this.profesional = this.getProfesion();
    this.diagnostico_presuntivo = this.getDiagnosticoPresuntivo();
  }

  ionViewDidLoad() {
    this.getDataDetails();
    console.log('ionViewDidLoad MidisDetailPage');
  }

  getProfesion() {
    if (this.element) {
      let profesional = this.element.get_profesional;
      let response =
        profesional.nombre;
      if (profesional.apellido1) {
        response += " " + profesional.apellido1;
      }
      if (profesional.apellido2) {
        response += " " + profesional.apellido2;
      }
      response += " / ";

      if (profesional.get_especialidad.length > 0) {
        for (let i = 0; i < profesional.get_especialidad.length; i++) {
          response += profesional.get_especialidad[i].nombre;
          if (i + 1 < profesional.get_especialidad.length) {
            response += ", ";
          }
        }
      }
      else {
        for (let i = 0; i < profesional.get_subtipo_profesionales.length; i++) {
          response += profesional.get_subtipo_profesionales[i].nombre;
          if (i + 1 < profesional.get_subtipo_profesionales.length) {
            response += ", ";
          }
        }
      }
      return response;
    }
    return "";
  }

  getDiagnosticoPresuntivo() {
    let response = "";
    if (this.element) {
      let diagnostico = this.element.get_diagnostico;
      for (let i = 0; i < diagnostico.length; i++) {
        response += diagnostico[i].nombre;
        if (i + 1 < diagnostico.length) {
          response += "\n";
        }
      }
    }
    return response;
  }

  detailPrescripcion(element: any) {
    let logo = "";
    let nombre = "";
    if (element.get_sede_institucion) {
      logo = element.get_sede_institucion.get_institucion.logo;
      nombre = element.get_sede_institucion.nombre;
    }
    this.navCtrl.push(this.prescPage, {
      elemento: element,
      profesional: this.profesional,
      diagnostico: this.diagnostico_presuntivo,
      tipoEvento: this.element.get_tipo_consulta.nombre,
      motivo: this.element.motivo,
      profResultados: this.getProfResultados(element),
      cargaResultados: this.getCargaResultados(element),
      logoInst: logo,
      nombreInst: nombre
    });
  }

  getDataDetails() {
    let con_fk = this.element.id_pk;
    let params = { "con_fk": con_fk };

    this.ServicesProvider.createLoader();
    this.ServicesProvider.apiPost(params, SERVICES.MIDIS_DETAILS)
      .then(midisDataDetails => {
        this.ServicesProvider.closeLoader();
        if (midisDataDetails["status"] == 404 || midisDataDetails["status"] == 500) {
          this.ServicesProvider.toast(MESSAGES.services.error);
        }
        else if (midisDataDetails["status"] == 401) {
          this.ServicesProvider.toast(MESSAGES.services.unauthorized);
        }
        else {
          this.dataDetailsResponse = JSON.parse(midisDataDetails["_body"]);
          console.log("MIDIS DETAILS\n" + JSON.stringify(this.dataDetailsResponse));
          this.dataPrescriptions = this.dataDetailsResponse.prescripciones;
          //ESTE FOR RECORRE AQUELLAS DESCRIPCIONES QUE DEBEN SER REFACTORIZADAS Y LAS ADECÚA
          for (let i = 0; i < this.dataPrescriptions.length; i++) {
            if (this.dataPrescriptions[i].tpc_fk == 2) { //SE CAPTURAN DOCUMENTOS DE SALUD
              let split = "";
              if (this.dataPrescriptions[i].descripcion.includes('Dias: ')) {
                split = this.dataPrescriptions[i].descripcion.split('Dias: ')[1].split(' Fecha de Inicio: ');
                this.dataPrescriptions[i]["dias"] = split[0];
                let split2 = split[1].split('Fecha de Inicio: ')[0].split(' Fecha de Finalización: ');
                this.dataPrescriptions[i]["fecha_de_inicio"] = split2[0];
                this.dataPrescriptions[i]["fecha_de_finalizacion"] = split2[1];
                this.dataPrescriptions[i]["descripcion"] = "MidisAppSalud_HaveDetails_Inc";
              }
              else if (this.dataPrescriptions[i].descripcion.includes('Fecha: ')) {
                split = this.dataPrescriptions[i].descripcion.split('Fecha: ')[1].split(' Hora Inicio: ');
                this.dataPrescriptions[i]["fecha_cons"] = split[0];
                let split2 = split[1].split('Hora Inicio: ')[0].split(' Hora Fin: ');
                this.dataPrescriptions[i]["hora_de_inicio"] = split2[0];
                this.dataPrescriptions[i]["hora_de_finalizacion"] = split2[1];
                this.dataPrescriptions[i]["descripcion"] = "MidisAppSalud_HaveDetails_Cons";
              }
            }
          }
          //
        }
      });
  }

  getProfResultados(element: any) {
    if (element.get_resultados.length > 0) {
      if (element.get_resultados[0].pro_ext && element.get_resultados[0].pro_ext != null) {
        return element.get_resultados[0].pro_ext;
      }

      if (element.get_resultados[0].get_profesional && element.get_resultados[0].get_profesional != null) {
        let response =
          element.get_resultados[0].get_profesional.nombre;
        if (element.get_resultados[0].get_profesional.apellido1) {
          response += " " + element.get_resultados[0].get_profesional.apellido1;
        }
        if (element.get_resultados[0].get_profesional.apellido2) {
          response += " " + element.get_resultados[0].get_profesional.apellido2;
        }
        return response;
      }
      if (element.get_resultados[0].ins_ext && element.get_resultados[0].ins_ext != null) {
        return "Nombre no encontrado";
      }
    }
    return "Error obteniendo la información";
  }

  getCargaResultados(element: any) {
    if (element.get_resultados.length > 0) {
      if (element.get_resultados[0].carga_pac_fk != null) {
        return "Paciente";
      }
      if (element.get_resultados[0].carga_pro_fk != null) {
        return "Profesional";
      }
      return "Usuario no encontrado";
    }
    return "Error obteniendo la información";
  }

}
