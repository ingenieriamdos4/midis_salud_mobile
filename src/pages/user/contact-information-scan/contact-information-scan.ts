import { Component } from "@angular/core";
import { BasicPatientInformationPage } from "../../index.paginas";

@Component({
  selector: "page-contact-information-scan",
  templateUrl: "contact-information-scan.html"
})
export class ContactInformationScanPage {
  paciente: any;
  personal: boolean = false;
  isVisibleDataFamily: boolean = false;
  isVisibleDataMedical: boolean = false;
  isVisibleResidence: boolean = false;

  constructor(public basicPatient: BasicPatientInformationPage) {
    this.personal = this.basicPatient.personal;
    this.paciente = this.basicPatient.infoPac;
    console.log(this.paciente);
    console.log(this.basicPatient);
    if (this.basicPatient.infoPac["get_configuracion"] != []) {
      this.basicPatient.infoPac["get_configuracion"][0].datos_familiares == "0"
        ? (this.isVisibleDataFamily = false)
        : (this.isVisibleDataFamily = true);
      this.basicPatient.infoPac["get_configuracion"][0].datos_medicos == "0"
        ? (this.isVisibleDataMedical = false)
        : (this.isVisibleDataMedical = true);
      this.basicPatient.infoPac["get_configuracion"][0].residencia == "0"
        ? (this.isVisibleResidence = false)
        : (this.isVisibleResidence = true);
    }
    //console.log("IsPersonal", this.personal, "Paciente ", this.paciente.get_configuracion);
  }
}
