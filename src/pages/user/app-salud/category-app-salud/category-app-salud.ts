import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { PrescDetailPage } from '../../presc-detail/presc-detail';
import { AppFilterPage } from '../app-filter/app-filter';

@Component({
  selector: 'page-category-app-salud',
  templateUrl: 'category-app-salud.html',
})
export class CategoryAppSaludPage {

  prescPage = PrescDetailPage;
  filterPage = AppFilterPage;

  data: any;
  title: any;
  color: any;
  icon: any;

  profesional: any;
  diagnostico_presuntivo: any;

  image_noData: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private modalCtrl: ModalController
  ) {
    this.image_noData = navParams.get("image");
    this.data = navParams.get("data");
    this.title = navParams.get("title");
    this.color = navParams.get("color");
    if (this.data.length > 0) {
      this.icon = this.data[0].get_tipo_prescripcion.get_modulo.icono;
    }
    //ESTE FOR RECORRE AQUELLAS DESCRIPCIONES QUE DEBEN SER REFACTORIZADAS Y LAS ADECÚA
    for (let i = 0; i < this.data.length; i++) {
      if (this.data[i].tpc_fk == 2) { //SE CAPTURAN DOCUMENTOS DE SALUD
        let split = "";
        if (this.data[i].descripcion.includes('Dias: ')) {
          split = this.data[i].descripcion.split('Dias: ')[1].split(' Fecha de Inicio: ');
          this.data[i]["dias"] = split[0];
          let split2 = split[1].split('Fecha de Inicio: ')[0].split(' Fecha de Finalización: ');
          this.data[i]["fecha_de_inicio"] = split2[0];                  
          this.data[i]["fecha_de_finalizacion"] = split2[1];        
          this.data[i]["descripcion"] = "MidisAppSalud_HaveDetails_Inc";          
        }
        else if (this.data[i].descripcion.includes('Fecha: ')) {
          split = this.data[i].descripcion.split('Fecha: ')[1].split(' Hora Inicio: ');
          this.data[i]["fecha_cons"] = split[0];
          let split2 = split[1].split('Hora Inicio: ')[0].split(' Hora Fin: ');
          this.data[i]["hora_de_inicio"] = split2[0];                  
          this.data[i]["hora_de_finalizacion"] = split2[1];        
          this.data[i]["descripcion"] = "MidisAppSalud_HaveDetails_Cons"; 
        }
      }
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CategoryAppSaludPage');
  }

  /*
  filterModal() {
    const modal = this.modalCtrl.create(this.filterPage);
    modal.onDidDismiss(data => {
      this.filter_fecha_inicio_busq = data[0].fecha_inicio;
      this.filter_fecha_fin_busq = data[0].fecha_fin;
      this.filter_motivo_cons = data[0].motivo;
      this.filter_diagnostico = data[0].diagnostico;
      this.filter_prof_cons = data[0].profesional;
      this.filter_inst_cons = data[0].institucion;
      this.getDataMidisSalud();
    });
    modal.present();
  }*/

  getProfesion(i: any) {
    if (this.data[i]) {
      let profesional = this.data[i].get_consultas.get_profesional;
      let response =
        profesional.nombre;
      if (profesional.apellido1) {
        response += " " + profesional.apellido1;
      }
      if (profesional.apellido2) {
        response += " " + profesional.apellido2;
      }
      response += " / ";

      if (profesional.get_especialidad.length > 0) {
        for (let i = 0; i < profesional.get_especialidad.length; i++) {
          response += profesional.get_especialidad[i].nombre;
          if (i + 1 < profesional.get_especialidad.length) {
            response += ", ";
          }
        }
      }
      else {
        for (let i = 0; i < profesional.get_subtipo_profesionales.length; i++) {
          response += profesional.get_subtipo_profesionales[i].nombre;
          if (i + 1 < profesional.get_subtipo_profesionales.length) {
            response += ", ";
          }
        }
      }
      return response;
    }
    return "";
  }

  getDiagnosticoPresuntivo(i: any) {
    let response = "";
    if (this.data[i]) {
      let diagnostico = this.data[i].get_consultas.get_diagnostico;
      for (let i = 0; i < diagnostico.length; i++) {
        response += diagnostico[i].nombre;
        if (i + 1 < diagnostico.length) {
          response += "\n";
        }
      }
    }
    return response;
  }

  getProfResultados(element: any) {
    if (element.get_resultados.length > 0) {
      if (element.get_resultados[0].pro_ext && element.get_resultados[0].pro_ext != null) {
        return element.get_resultados[0].pro_ext;
      }

      if (element.get_resultados[0].get_profesional && element.get_resultados[0].get_profesional != null) {
        let response =
          element.get_resultados[0].get_profesional.nombre;
        if (element.get_resultados[0].get_profesional.apellido1) {
          response += " " + element.get_resultados[0].get_profesional.apellido1;
        }
        if (element.get_resultados[0].get_profesional.apellido2) {
          response += " " + element.get_resultados[0].get_profesional.apellido2;
        }
        return response;
      }
      if (element.get_resultados[0].ins_ext && element.get_resultados[0].ins_ext != null) {
        return "Nombre no encontrado";
      }
    }
    return "Error obteniendo la información";
  }

  getCargaResultados(element: any) {
    if (element.get_resultados.length > 0) {
      if (element.get_resultados[0].carga_pac_fk != null) {
        return "Paciente";
      }
      if (element.get_resultados[0].carga_pro_fk != null) {
        return "Profesional";
      }
      return "Usuario no encontrado";
    }
    return "Error obteniendo la información";
  }

  viewPresc(element: any, i: any) {
    let logo = "";
    let nombre = "";
    if (element.get_consultas.get_sede_institucion) {
      logo = element.get_consultas.get_sede_institucion.get_institucion.logo;
      nombre = element.get_consultas.get_sede_institucion.nombre;
    }
    this.navCtrl.push(this.prescPage, {
      elemento: element,
      profesional: this.getProfesion(i),
      diagnostico: this.getDiagnosticoPresuntivo(i),
      tipoEvento: element.get_consultas.get_tipo_consulta.nombre,
      motivo: element.get_consultas.motivo,
      profResultados: this.getProfResultados(element),
      cargaResultados: this.getCargaResultados(element),
      logoInst: logo,
      nombreInst: nombre
    });
  }

}
