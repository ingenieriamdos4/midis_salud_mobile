import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { PatientProviderService } from "../../../providers/patient/patient";
import { ServicesProvider } from "../../../providers/services/services";
import { PageHelpPage } from "../../index.paginas"
import { CategoryAppSaludPage } from "./category-app-salud/category-app-salud";

import { MESSAGES } from "../../../config/messages";
import { SERVICES } from "../../../config/url.servicios";

@Component({
  selector: 'page-app-salud',
  templateUrl: 'app-salud.html',
})
export class AppSaludPage {
  pageHelp = PageHelpPage;
  categoryPage = CategoryAppSaludPage;
  imageAppSaludPublicity: string = "assets/imgs/cargando.png";
  treehealthFile: Array<string> = [];
  timeOut: any;
  timeOutControl: boolean = true;
  image_noData: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public patientProviderService: PatientProviderService,
    public ServicesProvider: ServicesProvider,
    private alertCtrl: AlertController
  ) { }

  ionViewWillEnter() {
    /*if (localStorage.getItem("wizzard_app_salud") == undefined) {
      localStorage.setItem("wizzard_app_salud", "true");
      this.navCtrl.push(this.pageHelp, { 'seccion': 'app_salud' });
    }*/
  }

  ionViewDidLoad() {
    let params = {
      "id_paciente": this.patientProviderService.getCurrentUser().id_pk
    };
    this.promisesHealthData(
      this.ServicesProvider.apiPost(params, SERVICES.CONSULTAR_PACIENTE_API_SERV),
      this.ServicesProvider.apiPost({"pac_fk":this.patientProviderService.getCurrentUser().id_pk}, SERVICES.ARBOL_APP_SALUD2_API_SERV)
      //this.ServicesProvider.apiGet("", SERVICES.ARBOL_APP_SALUD_API_SERV)
    );
  }

  promisesHealthData(promiseUser: any, promiseTreeHealthFiles: any) {
    this.ServicesProvider.createLoader();
    Promise.all([promiseUser, promiseTreeHealthFiles])
      .then(promise => {
        this.ServicesProvider.closeLoader();

        this.clearTimeOut();

        if (promise[0]["status"] == 404 || promise[0]["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (promise[0]["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(promise[0]["_body"]);
          localStorage.setItem("user", JSON.stringify(respuesta));
        }

        if (promise[1]["status"] == 404 || promise[1]["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (promise[1]["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(promise[1]["_body"]);
          this.treehealthFile = respuesta;
          console.log("ARBOL APP: "+JSON.stringify(this.treehealthFile));
        }
      })
      .catch(err => {
        this.ServicesProvider.closeLoader();

        this.clearTimeOut();
        this.timeOutControl = false;
        this.ServicesProvider.toast(MESSAGES.services.error);
      });
  }

  loadNew() {
    this.timeOutControl = true;
    this.ionViewDidLoad();
  }

  clearTimeOut() {
    clearTimeout(this.timeOut);
  }

  getColor(type: any) {
    switch (type) {
      case 1:
        this.image_noData = "assets/imgs/eventos_de_salud_no.png";
        return 'verde';
      case 2:
        this.image_noData = "assets/imgs/documentos_de_salud_no.png";
        return 'violeta';
      case 3:
        this.image_noData = "assets/imgs/estudios_diagnosticos_no.png";
        return 'amarillo';
      case 4:
        this.image_noData = "assets/imgs/tratamientos_de_salud_no.png";
        return 'morado';
      case 5:
        this.image_noData = "assets/imgs/intervenciones_no.png";
        return 'azul-light';
      case 6:
        this.image_noData = "assets/imgs/internaciones_no.png";
        return 'azul-dark';
      case 7:
        this.image_noData = "assets/imgs/consentimientos_no.png";
        return 'gris';
      case 8:
        this.image_noData = "assets/imgs/bancos_de_salud_no.png";
        return 'rojo';
    }
  }

  clickCardAppSalud(app: any) {
    let params = {
      "pac_fk": this.patientProviderService.getCurrentUser().id_pk,
      "tipo_pres": app.tipo_pres
    }
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiPost(params, SERVICES.PRESC_APP_SALUD)
      .then(data => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (data["status"] == 404 || data["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (data["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let response = JSON.parse(data["_body"]);
          console.log(JSON.stringify(response));
          /*
          if (response.length == 0) {
            let alert = this.alertCtrl.create({
              title: "Midis App Salud",
              message: "Aún no tiene resultados de " + app.text 
                + " cargadas en esta sección, solicite al profesional de salud que su Consulta/Atención sea cargada en Midis app Salud",
              buttons: [
                {
                  text: "Aceptar",
                  role: "acept"
                }
              ]
            });
            alert.present();
          }
          */
          //else {
          this.navCtrl.push(
            this.categoryPage,
            {
              'data': response,
              'title': app.text,
              'color': this.getColor(app.tipo_pres),
              'image': this.image_noData
            }
          );
          //}
        }
      })
      .catch(err => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.timeOutControl = false;
        this.ServicesProvider.toast("Error: " + err);
      });
  }

}
