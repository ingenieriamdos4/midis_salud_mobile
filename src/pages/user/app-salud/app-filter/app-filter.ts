import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ViewController, ToastController } from 'ionic-angular';
import { SelectSearchableComponent } from 'ionic-select-searchable';
import { ServicesProvider } from "../../../../providers/services/services";
import { PatientProviderService } from "../../../../providers/patient/patient";
import { MESSAGES } from '../../../../config/messages';

@Component({
  selector: 'page-app-filter',
  templateUrl: 'app-filter.html',
})
export class AppFilterPage {
  textOfDiagnostic: string = "";
  today: string;

  filter_motivo_cons: any;
  filter_fecha_inicio_busq: any;
  filter_fecha_fin_busq: any;
  filter_diagnostico: any;
  filter_prof_cons: any;
  filter_inst_cons: any;

  pac_fk: any;

  profesionales: any;
  instituciones: any;
  diagnosticos: any;
  showDiagnostics: boolean = false;

  dataProfInst: any;


  @ViewChild('myselect') selectComponent: SelectSearchableComponent;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private view: ViewController,
    private toastCtrl: ToastController,
    private ServicesProvider: ServicesProvider,
    public patientProviderService: PatientProviderService
  ) {
    this.today = this.formatDateToday(new Date().toLocaleDateString());
    console.log("TODAY IS: " + this.today);
    this.pac_fk = this.patientProviderService.getCurrentUser().id_pk;
    this.getDataProfInst();
  }

  onInputTime(filtro: any) {
    if (filtro.length > 3) {
      this.showDiagnostics = true;
      this.ServicesProvider.createLoader();
      this.ServicesProvider.apiGet(filtro, '/buscar_diagnosticos_api' + '?filtro=')
        .then(diagnosticos => {
          this.ServicesProvider.closeLoader();
          if (diagnosticos["status"] == 404 || diagnosticos["status"] == 500) {
            this.ServicesProvider.toast(MESSAGES.services.error);
          }
          else if (diagnosticos["status"] == 401) {
            this.ServicesProvider.toast(MESSAGES.services.unauthorized);
          }
          else {
            this.diagnosticos = JSON.parse(diagnosticos["_body"]);
          }
        });
    }
    else {
      this.showDiagnostics = false;
    }
  }

  onClose() {
    let toast = this.toastCtrl.create({
      message: "Usuario seleccionado",
      duration: 2000
    });
    toast.present();
  }

  openFromCode() {
    this.selectComponent.open();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MidisFilterPage');
  }

  aplyFilter() {
    let data = [
      {
        "fecha_inicio": this.formatDate(this.filter_fecha_inicio_busq),
        "fecha_fin": this.formatDate(this.filter_fecha_fin_busq),
        "motivo": this.filter_motivo_cons,
        "diagnostico": this.filter_diagnostico,
        "profesional": this.filter_prof_cons,
        "institucion": this.filter_inst_cons
      }
    ];
    this.validateDates(this.filter_fecha_inicio_busq, this.filter_fecha_fin_busq);
    this.view.dismiss(data);
  }

  cancelFilter() {
    let data = [
      {
        "fecha_inicio": "",
        "fecha_fin": "",
        "motivo": "",
        "diagnostico": "",
        "profesional": "",
        "institucion": ""
      }
    ];
    this.view.dismiss(data);
  }

  formatDate(date: any) {
    if (date) {
      var splitDate = date.split("-");
      var year = splitDate[0];
      var month = splitDate[1];
      var day = splitDate[2];
      var dateFormated = day + "/" + month + "/" + year;
      return dateFormated;
    }
    return "";
  }

  formatDateToday(date: any){
    if (date) {
      var splitDate = date.split("/");
      var year = splitDate[0];
      var month = splitDate[1];
      var day = splitDate[2];
      var dateFormated = day + "-" + month + "-" + year;
      return dateFormated;
    }
    return "";
  }

  validateDates(begin: any, end: any) {
    if (!begin && end) {
      this.ServicesProvider.toast(MESSAGES.filters.dateMidis.error);
    }
    else if (begin && !end) {
      this.ServicesProvider.toast(MESSAGES.filters.dateMidis.error);
    }
  }

  getDataProfInst() {
    let params = {
      "pac_fk": this.pac_fk
    };

    this.ServicesProvider.createLoader();
    this.ServicesProvider.apiPost(params, '/prof_inst_paciente')
      .then(midisData => {
        this.ServicesProvider.closeLoader();
        if (midisData["status"] == 404 || midisData["status"] == 500) {
          this.ServicesProvider.toast(MESSAGES.services.error);
        }
        else if (midisData["status"] == 401) {
          this.ServicesProvider.toast(MESSAGES.services.unauthorized);
        }
        else {
          this.dataProfInst = JSON.parse(midisData["_body"]);
          this.profesionales = this.dataProfInst.profesionales;
          this.instituciones = this.dataProfInst.instituciones;
        }
      });
  }

  diagnosticSelected(element: any) {
    this.filter_diagnostico = element.id;
    this.textOfDiagnostic = element.text;
    this.showDiagnostics = false;
  }

}
