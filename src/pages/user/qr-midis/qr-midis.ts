import { Component } from "@angular/core";
import {
  NavController,
  NavParams,
  Platform,
  AlertController
} from "ionic-angular";
import { FileOpener } from "@ionic-native/file-opener";
import { File } from "@ionic-native/file";
import { FileTransfer } from "@ionic-native/file-transfer";
import { PatientProviderService } from "../../../providers/patient/patient";
import { ServicesProvider } from "../../../providers/services/services";
/* Mensajes de la app */
import { MESSAGES } from "../../../config/messages";
import { SERVICES, URL } from "../../../config/url.servicios";

/* Plugins */
import {
  BarcodeScanner,
  BarcodeScannerOptions
} from "@ionic-native/barcode-scanner";
//import { FileTransfer, FileTransferObject } from "@ionic-native/file-transfer";
//import { FileEntry } from "@ionic-native/file";
import { SocialSharing } from "@ionic-native/social-sharing";

/* Servicio */
import { BasicPatientInformationPage, PageHelpPage } from "../../index.paginas";
import { ConfigPluginsProvider } from "../../../providers/config-plugins/config-plugins";
declare var cordova: any;

import { InAppBrowser, InAppBrowserOptions } from "@ionic-native/in-app-browser";

@Component({
  selector: "page-qr-midis",
  templateUrl: "qr-midis.html"
})
export class QrMidisPage {
  pac: number;
  name: string;
  photo: any;
  codQr: string;
  carnetQr: string;
  avatar: string;
  timeOut: any;
  timeOutControl: boolean = true;
  storageDirectory: string = "";
  isViewAutorized: boolean = true;
  isIos: boolean = false;
  testRadioOpen: boolean = true;
  testRadioResult: Array<any> = [];
  //bandera: number = 1;

  //para descargar y compartir base64 como pdf
  url: string;
  Message: any;
  //timeOut: any;
  //timeOutControl: boolean = true;
  //storageDirectory: string = "";
  isFuncActive: boolean = true;
  isDownloaded: boolean = false;
  namePdf: string;
  nameQR: string;
  nameCarne: string
  imgHelp: string;
  pageHelp = PageHelpPage;
  //isIos: boolean = false;

  carnePDF: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public patientProviderService: PatientProviderService,
    public ServicesProvider: ServicesProvider,
    private barcodeScanner: BarcodeScanner,
    //private transfer: FileTransfer,
    private socialSharing: SocialSharing,
    public platform: Platform,
    public configPluginsProvider: ConfigPluginsProvider,
    public alertCtrl: AlertController,
    private fileOpener: FileOpener,
    private file: File,
    private transfer: FileTransfer,
    private iab: InAppBrowser
  ) {
    this.navParams.get("data")
      ? (this.isViewAutorized = false)
      : (this.isViewAutorized = true);
    this.platform.ready().then(() => {
      // make sure this is on a device, not an emulation (e.g. chrome tools device mode)
      if (!this.platform.is("cordova")) {
        return false;
      }

      if (this.platform.is("ios")) {
        this.storageDirectory = cordova.file.documentsDirectory;
        this.isIos = true;
      } else if (this.platform.is("android")) {
        this.storageDirectory = cordova.file.externalRootDirectory;
      } else {
        // exit otherwise, but you could add further types here e.g. Windows
        return false;
      }
    });

  }

  ionViewDidEnter() {
    /*if (localStorage.getItem("wizzard_codigo_qr") == undefined) {
      localStorage.setItem("wizzard_codigo_qr", "true");
      this.navCtrl.push(this.pageHelp, { 'seccion': 'codigo_qr_midis' });
    }*/
  }


  ionViewDidLoad() {
    this.getUser(this.patientProviderService.getCurrentUser().id_pk);
    let user = JSON.parse(localStorage.getItem("user"));
    this.nameQR = "Qr-Midis" + user.nombres + ".pdf";
    this.nameCarne = "Carne-Midis" + user.nombres + ".pdf";
  }

  clearTimeOut() {
    clearTimeout(this.timeOut);
  }

  getUser(id_paciente: number) {
    let params = { "id_paciente": id_paciente };
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiPost(params, SERVICES.CONSULTAR_PACIENTE_API_SERV)
      .then(state => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (state["status"] == 404 || state["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (state["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(state["_body"]);
          localStorage.setItem("user", JSON.stringify(respuesta));

          this.pac = respuesta.id_pk;
          this.codQr = respuesta.codigo_qr;
          this.photo = respuesta.get_user[0].foto;
          this.name = respuesta.nombres;
          this.avatar = respuesta.get_user[0].foto + "?1=" + new Date();
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.timeOutControl = false;
        //});
        this.ServicesProvider.toast(MESSAGES.services.error);
      });
  }

  scan() {
    const options: BarcodeScannerOptions = {
      prompt: MESSAGES.codQr.prompt.message
    };

    this.barcodeScanner
      .scan(options)
      .then(
        barcodeData => {
          if (barcodeData.cancelled == false && barcodeData.text != null) {
            let idEncrypt = barcodeData.text
              .split("/")
              .slice(-1)
              .pop();

            this.getIdPacienteWithQr(idEncrypt);
          }
        },
        err => {
          this.ServicesProvider.toast(
            MESSAGES.codQr.message.close
          );
        }
      )
      .catch(err => {
        this.ServicesProvider.toast(MESSAGES.codQr.message.error);
      });
  }

  getIdPacienteWithQr(id_encrypt: string) {
    let params = { "qr_paciente": id_encrypt };
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiPost(params, SERVICES.DESENCRIPTAR_ID_PACIENTE_SERV)
      .then(stateE => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (stateE["status"] == 404 || stateE["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (stateE["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(stateE["_body"]);

          if (respuesta.error != undefined) {
            this.ServicesProvider.toast(respuesta);
          } else if (respuesta.id_paciente == "Código QR Invalido") {
            this.ServicesProvider.toast(respuesta.id_paciente);
          } else {
            this.getPatientInformationByEncryption(respuesta.id_paciente);
            this.ServicesProvider.toast(MESSAGES.codQr.message.invalid);
          }
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.timeOutControl = false;
        this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  getPatientInformationByEncryption(id_paciente: number) {
    let params = { "id_paciente": id_paciente };
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiPost(params, SERVICES.INF_BASICA_OTROS_PACIENTES)
      .then(state => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (state["status"] == 404 || state["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (state["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(state["_body"]);
          this.navCtrl.push(BasicPatientInformationPage, {
            paciente: respuesta
          });
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.timeOutControl = false;
        this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  functionCarne(option: any) {
    let params = { "pac_fk": this.pac, "carnetQr": 1 };

    this.ServicesProvider.createLoader();
    this.ServicesProvider.apiPost(params, SERVICES.CODIGO_QR_PDF_API_SERV)
      .then(state => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (state["status"] == 404 || state["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        }
        else if (state["status"] == 401) {
          this.ServicesProvider.toast(MESSAGES.services.unauthorized);
          this.timeOutControl = false;
        }
        else {
          let respuesta = JSON.parse(state["_body"]);
          console.log("RESPUESTA::" + JSON.stringify(respuesta));
          let ruta = URL + '/' + respuesta;
          if (option == 0) { //COMPARTIR CARNÉ
            this.ServicesProvider.createLoader();
            this.socialSharing
              .share(MESSAGES.documents.share.message, "Midis App", ruta, "")
              .then(() => {
                this.ServicesProvider.closeLoader();
              })
              .catch(err => {
                this.ServicesProvider.closeLoader();
                console.log("Error ruta: " + err);
              });
          }
          else if (option == 1) { //DESCARGAR CARNÉ
            let promise = this.configPluginsProvider.statusExternalStorageForApplication()

            this.configPluginsProvider.downloader(
              promise,
              this.transfer,
              ruta,
              this.storageDirectory
            );
          }
        }
      })
      .catch(err => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.timeOutControl = false;
        this.ServicesProvider.toast(MESSAGES.services.error);
        console.log("catch. Error: " + err);
      });
  }

  getQrOrCarne(userSelection: number) {
    this.carnetQr = undefined;
    let params: any;
    if (userSelection == 1) {
      params = { "pac_fk": this.pac, "carnetQr": 1 };
    }
    else {
      params = { "pac_fk": this.pac };
    }

    this.ServicesProvider.createLoader();
    this.ServicesProvider.apiPost(params, SERVICES.CODIGO_QR_PDF_API_SERV)
      .then(state => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (state["status"] == 404 || state["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        }
        else if (state["status"] == 401) {
          this.ServicesProvider.toast(MESSAGES.services.unauthorized);
          this.timeOutControl = false;
        }
        else {

          let respuesta = JSON.parse(state["_body"]);
          this.carnetQr = respuesta;
          if (userSelection == 1) {
            try {
              let promise = this.configPluginsProvider.statusExternalStorageForApplication()

              this.configPluginsProvider.downloader(
                promise,
                this.transfer,
                this.carnetQr,
                this.storageDirectory
              );
            } catch (error) {
              this.ServicesProvider.toast("Error en la descarga: " + error);
            }
          }
          console.log("-- getQrOrCarne-------------------------------");
          console.log("-- userSelection :", userSelection);
          console.log(respuesta);
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {          
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.timeOutControl = false;
        this.ServicesProvider.toast(MESSAGES.services.error);
        console.log("catch. Error: " + err);
        //});        
      });
    // console.log("--bandera: ", this.bandera);
    // if(this.bandera == 1)
    // {
    //   this.getQrOrCarne(userSelection);
    // }
    // this.bandera = this.bandera + 1;

  }

  showRadioAlert(opt: string) {
    let alert = this.alertCtrl.create();
    alert.setTitle('Código QR');
    alert.setMessage('Seleccione cómo quiere descargar o compartir su Código QR.');

    alert.addInput({
      type: 'radio',
      label: 'Carné: incluye foto, nombre, documento de identidad y código QR.',
      value: 'carne',
      checked: false
    });
    alert.addInput({
      type: 'radio',
      label: 'Código QR y nombre.',
      value: 'codigo-qr',
      checked: true
    });

    alert.addButton('Cancel');
    alert.addButton({
      text: 'OK',
      handler: data => {
        this.testRadioOpen = false;
        this.testRadioResult = data;

        if (this.isIos) {
          opt = 'share';
        }

        if (opt == 'share') {
          this.isFuncActive = false;
          if (data == 'codigo-qr') {
            this.getQrOrCarne(0);
            //primero se debe descargar y convertir el código qr a pdf para poder compartirlo
            //se espera la descarga del base64 antes de convertirlo
            setTimeout(() => {
              this.convert_base64_to_pdf(this.carnetQr, 0);
            }, 5000);
            this.shareMedicalTab("Te comparto mi código qr desde Midis App Salud", 0);
            this.ServicesProvider.toast("Código QR compartido con éxito", "successToast");
          }
          else if (data == 'carne') {
            this.functionCarne(0);
          }
        }
        else if (opt == 'download') {
          this.isFuncActive = true;
          if (data == 'codigo-qr') {
            this.getQrOrCarne(0);
            //this.download();            
            setTimeout(() => {
              this.convert_base64_to_pdf(this.carnetQr, 0);
            }, 5000);
            console.log("descarga de qr");
          }
          else if (data == 'carne') {
            this.functionCarne(1);
          }
        }
        this.carnetQr = "";
      }
    });
    alert.present();
  }

  convertBaseb64ToBlob(b64Data, contentType): Blob {
    contentType = contentType || '';
    const sliceSize = 512;
    b64Data = b64Data.replace(/^[^,]+,/, '');
    b64Data = b64Data.replace(/\s/g, '');
    const byteCharacters = window.atob(b64Data);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);
      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }
      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }
    return new Blob(byteArrays, { type: contentType });
  }

  convert_base64_to_pdf(downloadPDF: any, type: number) {
    if (type == 0) {
      //qr: type == 0
      this.namePdf = this.nameQR;
    }
    else if (type == 1) {
      //carne: type == 1
      this.namePdf = this.nameCarne;
    }
    this.file.writeFile(this.storageDirectory, this.namePdf, this.convertBaseb64ToBlob(downloadPDF, 'data:application/pdf;base64'), { replace: true })
      .then(res => {
        this.isDownloaded = true;
        this.patientProviderService.spinnerFichaMedica = false;
        if (this.isFuncActive) {
          this.ServicesProvider.toast(MESSAGES.medical_record.downloader.success, "successToast");
          this.fileOpener
            .open(res.toInternalURL(), "application/pdf")
            .then(res => { })
            .catch(err => {
              this.patientProviderService.spinnerFichaMedica = false;
              this.ServicesProvider.toast(MESSAGES.fileOpener.message.error);
            });
        }
      })
      .catch(err => {
        console.log(err);
        this.patientProviderService.spinnerFichaMedica = false;
        this.ServicesProvider.toast(MESSAGES.codQr.downloader.error);
      });
  }


  shareMedicalTab(text: string, type: number) {
    if (type == 0) {
      //qr: type == 0
      this.namePdf = this.nameQR;
    }
    else if (type == 1) {
      //carne: type == 1
      this.namePdf = this.nameCarne;
    }
    this.Message = text;
    this.url = this.storageDirectory + "/" + this.namePdf;
    this.ServicesProvider.createLoader();
    this.socialSharing
      .share(this.Message, "Midis App", this.url, "")
      .then(() => {
        this.ServicesProvider.closeLoader();
        // this.ServicesProvider.toast(
        //   "Ficha médica compartida con éxito", ''
        // );
      })
      .catch(() => {
        this.ServicesProvider.closeLoader();
        this.ServicesProvider.toast(
          "No se pudo compartir el archivo"
        );
      });
  }



  loadNew() {
    this.timeOutControl = true;
    this.getUser(this.patientProviderService.getCurrentUser().id_pk);
  }
}
