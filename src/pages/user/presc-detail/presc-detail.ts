import { Component } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';

import { ConfigPluginsProvider } from "../../../providers/config-plugins/config-plugins";
import { ServicesProvider } from "../../../providers/services/services";
import { SocialSharing } from "@ionic-native/social-sharing";
import { FileTransfer } from "@ionic-native/file-transfer";
import { InAppBrowser, InAppBrowserOptions } from "@ionic-native/in-app-browser";
import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer';

import { UploadResultsPage } from "./upload-results/upload-results";

import { MESSAGES } from "../../../config/messages";

declare var cordova: any;
declare var functionDicoms: any;

@Component({
  selector: 'page-presc-detail',
  templateUrl: 'presc-detail.html',
})
export class PrescDetailPage {
  uploadPage = UploadResultsPage;

  data: any;
  datePresc: any;

  storageDirectory: string = "";
  timeOut: any;

  profesional: any;
  diagnostico: any;
  tipoEvento: any;
  motivo: any;
  profResultados: any;
  cargaResultados: any;
  logoInst: any;
  nombreInst: any;

  dicoms: Array<string> = [];

  showButtons: boolean = false;
  isIos: boolean = false;

  navbar_color: any;

  params: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public platform: Platform,
    public configPluginsProvider: ConfigPluginsProvider,
    public ServicesProvider: ServicesProvider,
    private transfer: FileTransfer,
    private iab: InAppBrowser,
    private socialSharing: SocialSharing,
    private document: DocumentViewer
  ) {
    this.platform.ready().then(() => {
      if (!this.platform.is("cordova")) {
        return false;
      }
      if (this.platform.is("ios")) {
        this.storageDirectory = cordova.file.documentsDirectory;
        this.isIos = true;
      } else if (this.platform.is("android")) {
        this.storageDirectory = cordova.file.externalRootDirectory;
      } else {
        return false;
      }
    });

    this.loadPresc();
    //console.log(JSON.stringify(navParams)); 
  }

  loadPresc() {
    this.data = this.navParams.get("elemento")
    this.datePresc = this.data.fecha.split(" ")[0];
    this.profesional = this.navParams.get("profesional");
    this.diagnostico = this.navParams.get("diagnostico");
    this.tipoEvento = this.navParams.get("tipoEvento");
    this.motivo = this.navParams.get("motivo");
    this.profResultados = this.navParams.get("profResultados");
    this.cargaResultados = this.navParams.get("cargaResultados");
    this.logoInst = this.navParams.get("logoInst");
    if (this.logoInst == "") {
      this.logoInst = "./assets/imgs/prof_ext.jpg";
    }
    this.nombreInst = this.navParams.get("nombreInst");
    if (this.nombreInst == "") {
      this.nombreInst = "Profesional externo";
    }
    this.dicoms = this.getFilesDicom();

    if (this.dicoms.length > 0) {
      functionDicoms(this.dicoms);
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PrescDetailPage');
    this.loadPresc();
    this.navbarColor(this.data.tipo_presc.id_pk);
  }

  navbarColor(type: any) {
    switch (type) {
      case 1:
        this.navbar_color = 'verde';
        return;
      case 2:
        this.navbar_color = 'violeta';
        return;
      case 3:
        this.navbar_color = 'amarillo';
        return;
      case 4:
        this.navbar_color = 'morado';
        return;
      case 5:
        this.navbar_color = 'azul-light';
        return;
      case 6:
        this.navbar_color = 'azul-dark';
        return;
      case 7:
        this.navbar_color = 'gris';
        return;
      case 8:
        this.navbar_color = 'rojo';
        return;
    }
  }

  verifyTypeFile(file: string) {
    let ext: any = file
      .split(".")
      .pop()
      .toLowerCase();

    if (ext == "jpg" || ext == "jpeg" || ext == "png") {
      return true;
    }

    return false;
  }

  reviewFile(file: string) {
    /*
    let options: InAppBrowserOptions = {
      hardwareback: 'yes'
    };
    this.iab.create(file, "_blank", options);
    */
    const options: DocumentViewerOptions = {
      title: 'My PDF'
    }
    this.document.viewDocument(file, 'application/pdf', options)
  }

  pageRendered(e: CustomEvent) {
    this.showButtons = true;
  }

  download(_url: string) {

    if (this.isIos) {
      this.share(_url);
      return;
    }
    let promise = this.configPluginsProvider.statusExternalStorageForApplication()

    this.configPluginsProvider.downloader(
      promise,
      this.transfer,
      _url,
      this.storageDirectory
    );
  }

  share(file: string) {
    this.ServicesProvider.createLoader();
    this.socialSharing
      .share(MESSAGES.documents.share.message, "Midis App", file, "")
      .then(() => {
        this.ServicesProvider.closeLoader();
      })
      .catch(err => {
        this.ServicesProvider.closeLoader();
        /*this.ServicesProvider.toast(
          MESSAGES.documents.share.error
        );*/
      });
  }

  clearTimeOut() {
    clearTimeout(this.timeOut);
  }

  getFilesDicom() {
    var dicoms: Array<string> = [];
    let data = this.data.get_resultados;
    if (data.length > 0) {
      if (data[0].dicom == 1) {
        data.forEach(element => {
          dicoms.push(element.ruta_archivo);
        });
      }
    }
    return dicoms;
  }

  uploadResults(element: any) {
    this.navCtrl.push(this.uploadPage, {
      data: element,
      color: this.navbar_color
    });
  }

}
