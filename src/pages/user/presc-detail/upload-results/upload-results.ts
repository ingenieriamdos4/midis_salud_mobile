import { Component, ElementRef } from "@angular/core";
import { NavController, NavParams, ModalController, AlertController } from "ionic-angular";
import { DocumentsProviderService } from "../../../../providers/documents/documents";
import { Validators, FormBuilder, FormGroup } from "@angular/forms";
import { PatientProviderService } from "../../../../providers/patient/patient";
import { ServicesProvider } from "../../../../providers/services/services";
/*Plugins*/
import { Camera, CameraOptions } from "@ionic-native/camera";
import { ImagePicker, ImagePickerOptions } from "@ionic-native/image-picker";
/* Mensajes de la app */
import { MESSAGES } from "../../../../config/messages";
import { SERVICES } from "../../../../config/url.servicios";

import { CropImagePage } from "../../../crop-image/crop-image";

@Component({
  selector: 'page-upload-results',
  templateUrl: 'upload-results.html',
})
export class UploadResultsPage {

  navbar_color: string;
  data: any;

  categories: Array<string> = [];
  treehealthFile: Array<string> = [];
  formUploadResults: FormGroup;
  errors: Array<string> = [];
  file: string;
  timeOut: any;
  timeOutControl: boolean = true;
  hasFile: boolean = false;
  base64Image: any;

  so_flag: boolean = true;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public documentsProviderService: DocumentsProviderService,
    private formBuilder: FormBuilder,
    public patientProviderService: PatientProviderService,
    public ServicesProvider: ServicesProvider,
    private imagePicker: ImagePicker,
    private camera: Camera,
    private elem: ElementRef
  ) {
    this.so_flag = this.ServicesProvider.validate_so_v();
    this.navbar_color = this.navParams.get('color');
    this.data = this.navParams.get("data");

    this.formUploadResults = this.formBuilder.group({
      date: [null, Validators.compose([Validators.required])],
      institution: [null, Validators.compose([Validators.required])],
      profesional: [null, Validators.compose([])]
    });
  }

  clearTimeOut() {
    clearTimeout(this.timeOut);
  }

  saveData() {
    let id_prescripcion = this.data.id_pk;
    let id_pk = this.patientProviderService.getCurrentUser().id_pk;
    let formData = new FormData();
    formData.append("id_prescripcion", id_prescripcion);
    formData.append("pac_fk", id_pk);

    if (this.data.tpc_fk != 4) {
      formData.append("fecha_carga", this.documentsProviderService.formatDate(
        this.formUploadResults.value.date
      ));
      if (this.formUploadResults.value.institution == "") {
        this.ServicesProvider.toast("El campo institución debe ser diligenciado");
        return;
      }
      else {
        formData.append("inst_ext", this.formUploadResults.value.institution);
      }
    }
    else {
      formData.append("fecha_inicio_toma", this.documentsProviderService.formatDate(
        this.formUploadResults.value.date
      ));
    }

    if (this.formUploadResults.value.profesional != "") {
      formData.append(
        "pro_ext",
        this.formUploadResults.value.profesional
      );
    }


    if (this.base64Image) {
      formData.append("array_resultados[]", this.base64Image);
    }
    else {
      if (this.documentsProviderService.imagesBase64.length > 0) {
        this.documentsProviderService.imagesBase64.forEach(base64 => {
          formData.append("array_resultados[]", base64);
        });
      }
    }

    for (let index = 0; index < this.documentsProviderService.arrayFiles.length; index++) {
      formData.append("adj_inf[]", this.documentsProviderService.arrayFiles[index].file, this.documentsProviderService.arrayFiles[index].nameFile);
    }

    if (this.hasFile == false) {
      this.ServicesProvider.toast("Debe cargar un archivo");
      return;
    }

    console.log("PARAMS::\n" + JSON.stringify(formData));

    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiPost(formData, SERVICES.CARGAR_RESULTADOS)
      .then(file => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (file["status"] == 404 || file["status"] == 500) {
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (file["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
        } else {
          let respuesta = JSON.parse(file["_body"]);
          this.errors = [];

          if (respuesta.success == true) {
            console.log("RESPUESTA: TRUE");
            this.ServicesProvider.toast(MESSAGES.documents.create.success, "successToast");
            this.documentsProviderService.imagesBase64 = [];
            this.documentsProviderService.arrayFiles = [];
            this.base64Image = undefined;
            this.navCtrl.pop();
          }
          else if (respuesta.success == false) {
            console.log("RESPUESTA: FASLE\n" + JSON.stringify(respuesta));
            this.ServicesProvider.toast(
              MESSAGES.documents.create.error
            );
          }
          else if (respuesta.errors != undefined) {
            let i = 0;
            for (i; i < Object.keys(respuesta.errors).length; i++) {
              let campo = Object.keys(respuesta.errors)[i];
              let error = (<any>Object).values(respuesta.errors)[i];
              this.ServicesProvider.toast(campo + ": " + error);
            }
          }
          else if (respuesta.success == "sin_espacio") {
            this.ServicesProvider.toast(respuesta.mensaje);
          }
          else {
            for (var name in respuesta.errors) {
              let value = respuesta.errors[name];
              this.errors.push(value);
            }
            this.ServicesProvider.toast(this.errors.toString());
          }
        }
      })
      .catch(err => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.ServicesProvider.toast(MESSAGES.services.error);
      });

    this.navCtrl.popTo(this.navCtrl.getByIndex(this.navCtrl.length() - 3));
  }

  loadImages() {
    let options: ImagePickerOptions = {
      maximumImagesCount: 5,
      quality: 85,
      width: 510,
      height: 510,
      outputType: 1 // para que sea una imagen en base64, 0 fisico dispositivo
    };

    this.imagePicker
      .getPictures(options)
      .then(
        results => {
          if (results.length == 1) {
            this.base64Image = this.goCropEditor("data:image/png;base64," + results[0], 0);
          }
          else {
            for (var i = 0; i < results.length; i++) {
              this.documentsProviderService.imagesBase64.push(
                "data:image/png;base64," + results[i]
              );
            }
          }
          this.hasFile = true;
        },
        err => {
          this.ServicesProvider.toast(
            MESSAGES.fileHealtData.upload.error
          );
        }
      )
      .catch(err => {
        this.ServicesProvider.toast(err);
      });
  }

  cancelLoadImages() {
    this.documentsProviderService.imagesBase64 = [];
    this.base64Image = undefined;
    this.hasFile = false;
    console.log("Cancel load images: " + this.hasFile);
  }

  onUploadChange(ev) {
    let extErrorMessage =
      "Solo se permiten archivos con extensión: .jpg, .jpeg, .gif, .png, .dcm, .doc, docx, o pdf";
    let allowedExtension = [
      "jpg",
      "jpeg",
      "gif",
      "png",
      "dicom",
      "pdf",
      "dcm",
      "doc",
      "docx"
    ];
    let extError = false;
    let myFiles = ev.target.files[0];

    if (myFiles != undefined) {
      let extName = myFiles["name"]
        .split(".")
        .pop()
        .toLowerCase();

      if (allowedExtension.indexOf(extName) == -1) {
        extError = true;
      }

      if (extError) {
        this.ServicesProvider.toast(extErrorMessage);
      } else {
        this.documentsProviderService.arrayFiles.push({
          nameFile: myFiles["name"],
          file: myFiles
        });
        this.elem.nativeElement.querySelector("#fileUpload").value = "";
      }
    }
  }

  deleteFile(index: number) {
    this.documentsProviderService.arrayFiles.splice(index, 1);
    this.hasFile = false;
  }

  goCropEditor(image: any, aspect: any) {

    let cropAlert = this.alertCtrl.create({
      title: "Midis App Salud",
      message: "¿Desea cortar o editar esta foto?.\nPulse no para utilizar la original.",
      buttons: [
        {
          text: "Sí",
          handler: () => {
            let cropModal = this.modalCtrl.create(CropImagePage, { "image": image, "aspect": aspect });
            cropModal.onDidDismiss(data => {
              if (data.croppedImage != null) {
                this.base64Image = data.croppedImage;
              }
            });
            cropModal.present();
            return;
          }
        },
        {
          text: "No",
          role: "cancel",
          handler: () => {
            this.base64Image = image;
          }
        }
      ]
    });
    cropAlert.present();

    /*
    let response = this.configPluginsProvider.cropper(image, aspect);
    this.photo = response;
    */
  }

  tomarPhone() {
    const options: CameraOptions = {
      quality: 85,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    };

    this.camera.getPicture(options).then(
      imageData => {
        let base64Image = "data:image/jpeg;base64," + imageData;
        //let croppedBase64Image = this.goCropEditor(base64Image, 0);
        //this.documentsProviderService.imagesBase64.push(croppedBase64Image);
        this.goCropEditor(base64Image, 0);
        this.hasFile = true;
      },
      err => {
        this.ServicesProvider.toast(
          MESSAGES.camera.message.selected
        );
      }
    );
  }

  loadFile() {
    document.getElementById("fileUpload").click();
    this.hasFile = true;
  }

  loadNew() {
    this.timeOutControl = true;
    console.log("Solicitud de regarga, upload-results");
  }

}
