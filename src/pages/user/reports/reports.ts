import { Component, ViewChild } from "@angular/core";
import { ReportsProviderService } from "../../../providers/index.service";
import { PatientProviderService } from "../../../providers/patient/patient";
import { ServicesProvider } from "../../../providers/services/services";
import { PageHelpPage } from "../../index.paginas";
/* Mensajes de la app */
import { MESSAGES } from "../../../config/messages";
import { SERVICES } from "../../../config/url.servicios";
import { NavController, NavParams } from 'ionic-angular';

import { Chart } from 'chart.js';
import 'chartjs-plugin-zoom';

@Component({
  selector: "page-reports",
  templateUrl: "reports.html"
})
export class ReportsPage {
  todo = {};
  eventos = {};
  presc = {};
  reportes: string;
  consult_params: Array<string> = [];
  typesData: Array<string> = [];
  timeOut: any;
  timeOutControl: boolean = true;
  maxDate: string;
  imageReportsPublicity: string;
  pageHelp = PageHelpPage;

  chartDatos: any;
  @ViewChild('datos_salud_chart') datos_salud_chart;

  chartEventos: any;
  @ViewChild('eventos_salud_chart') eventos_salud_chart;

  chartPrescripciones: any;
  @ViewChild('prescripciones_chart') prescripciones_chart;

  constructor(
    public reportsProviderService: ReportsProviderService,
    public ServicesProvider: ServicesProvider,
    public patientProviderService: PatientProviderService,
    public navCtrl: NavController,
    public navParams: NavParams

  ) {
    this.filterDefault();
    this.maxDate = new Date()
      .toJSON()
      .slice(0, 10)
      .replace(/-/g, "-");

    let date = new Date();
    date.setMonth(date.getMonth() - 5);
    console.log("getMonth: ", date.getMonth());
    if (date.getMonth() == 0) {
      this.todo["fecha_ini"] = date.getFullYear() - 1 + "-" + "12" + "-" + (date.getDate() < 10 ? "0" + date.getDate() : date.getDate());
      this.eventos["fecha_ini"] = date.getFullYear() - 1 + "-" + "12" + "-" + (date.getDate() < 10 ? "0" + date.getDate() : date.getDate());
      this.presc["fecha_ini"] = date.getFullYear() - 1 + "-" + "12" + "-" + (date.getDate() < 10 ? "0" + date.getDate() : date.getDate());
    }
    else {
      this.todo["fecha_ini"] = date.getFullYear() + "-" + (date.getMonth() < 10 ? "0" + date.getMonth() : date.getMonth()) +
        "-" + (date.getDate() < 10 ? "0" + date.getDate() : date.getDate());
      this.eventos["fecha_ini"] = date.getFullYear() + "-" + (date.getMonth() < 10 ? "0" + date.getMonth() : date.getMonth()) +
        "-" + (date.getDate() < 10 ? "0" + date.getDate() : date.getDate());
      this.presc["fecha_ini"] = date.getFullYear() + "-" + (date.getMonth() < 10 ? "0" + date.getMonth() : date.getMonth()) +
        "-" + (date.getDate() < 10 ? "0" + date.getDate() : date.getDate());
    }
    this.todo["fecha_fin"] = this.maxDate;
    this.eventos["fecha_fin"] = this.maxDate;
    this.presc["fecha_fin"] = this.maxDate;

    this.reportes = "datosSalud";
    this.getPublicity();
  }

  getCantMonths(ini: any, fin: any) {
    var cant: any;

    let f_ini = ini.split('-');
    let f_fin = fin.split('-');

    let f = ((f_fin[0] * 12) + parseInt(f_fin[1]));
    let i = ((f_ini[0] * 12) + parseInt(f_ini[1]));

    cant = (f - i);

    return cant;
  }

  getDatosChart() {
    var canvas = this.datos_salud_chart.nativeElement;
    var fecha_ini = this.todo["fecha_ini"];
    var fecha_fin = this.todo["fecha_fin"];
    if (this.chartDatos) {
      this.chartDatos.destroy();
    }
    if (this.getCantMonths(fecha_ini, fecha_fin) <= 12) {
      this.ServicesProvider.createLoader();
      this.ServicesProvider.apiGet("?pac_fk=" + this.patientProviderService.getCurrentUser().id_pk + "&dato_salud=" + this.todo['dato'] + "&fecIni=" + this.formatDate(fecha_ini) + "&fecFin=" + this.formatDate(fecha_fin), SERVICES.CHART_DATOS_SALUD)
        .then(chart => {
          this.ServicesProvider.closeLoader();
          this.clearTimeOut();
          if (chart["status"] == 404 || chart["status"] == 500) {
            this.timeOutControl = false;
            this.ServicesProvider.toast(MESSAGES.services.error);
          } else if (chart["status"] == 401) {
            this.ServicesProvider.toast(
              MESSAGES.services.unauthorized
            );
            this.timeOutControl = false;
          } else {
            let dataChart = JSON.parse(chart["_body"]);

            if (dataChart.labels.length != 0) {
              this.chartDatos = new Chart(canvas, {
                type: 'bar',
                data: {
                  datasets: dataChart.datasets,
                  labels: dataChart.labels
                },
                options: {
                  legend: {
                    position: 'top',
                  },
                  scales: {
                    xAxes: [{
                      ticks: {
                        autoSkip: true,
                        maxRotation: 0,
                        minRotation: 0
                      }
                    }],
                    yAxes: [{
                      ticks: {
                        beginAtZero: true
                      }
                    }]
                  },
                  pan: {
                    enabled: false,
                    mode: 'xy' // is panning about the y axis neccessary for bar charts?
                  },
                  zoom: {
                    //sensitivity:10, drag: false, enabled: true, mode: 'x'
                    sensitivity: 10, drag: false, enabled: true, mode: 'x'
                  }
                }
              });
            }
            else {
              this.chartDatos = new Chart(canvas, {});
              this.ServicesProvider.toast("No hay información disponible en el rango de fechas proporcionado.", "warningToast");
            }

          }
        })
        .catch(error => {
          this.ServicesProvider.closeLoader();
          this.clearTimeOut();
          this.timeOutControl = false;
          this.ServicesProvider.toast("Ha ocurrido un error al momento de obtener la información");
        });
    }
    else {
      this.chartDatos = new Chart(canvas, {});
      this.ServicesProvider.toast("El rango de fechas ingresado no puede ser mayor a 12 meses");
    }
  }

  getEventosChart() {
    var canvas = this.eventos_salud_chart.nativeElement;
    var fecha_ini = this.eventos["fecha_ini"];
    var fecha_fin = this.eventos["fecha_fin"];
    if (this.chartEventos) {
      this.chartEventos.destroy();
    }
    if (this.getCantMonths(fecha_ini, fecha_fin) <= 12) {
      let params = {
        "pac_fk": this.patientProviderService.getCurrentUser().id_pk,
        "fec_ini": this.formatDate(fecha_ini),
        "fec_fin": this.formatDate(fecha_fin)
      }

      this.ServicesProvider.createLoader();
      this.ServicesProvider.apiPost(params, SERVICES.CHART_EVENTOS_SALUD_API)
        .then(chart => {
          this.ServicesProvider.closeLoader();
          this.clearTimeOut();
          if (chart["status"] == 404 || chart["status"] == 500) {
            this.timeOutControl = false;
            this.ServicesProvider.toast(MESSAGES.services.error);
          } else if (chart["status"] == 401) {
            this.ServicesProvider.toast(
              MESSAGES.services.unauthorized
            );
            this.timeOutControl = false;
          } else {
            let dataChart = JSON.parse(chart["_body"]);

            if (dataChart.labels.length != 0) {
              this.chartEventos = new Chart(canvas, {
                type: 'bar',
                data: {
                  datasets: dataChart.datasets,
                  labels: dataChart.labels
                },
                options: {
                  legend: {
                    position: 'top',
                  },
                  scales: {
                    xAxes: [{
                      ticks: {
                        autoSkip: true,
                        maxRotation: 0,
                        minRotation: 0
                      }
                    }],
                    yAxes: [{
                      ticks: {
                        beginAtZero: true
                      }
                    }]
                  },
                  pan: {
                    enabled: false,
                    mode: 'xy' // is panning about the y axis neccessary for bar charts?
                  },
                  zoom: {
                    //sensitivity:10, drag: false, enabled: true, mode: 'x'
                    sensitivity: 10, drag: false, enabled: true, mode: 'x'
                  }
                }
              });
            }
            else {
              this.chartEventos = new Chart(canvas, {});
              this.ServicesProvider.toast("No hay información disponible en el rango de fechas proporcionado.", "warningToast");
            }

          }
        })
        .catch(error => {
          this.ServicesProvider.closeLoader();
          this.clearTimeOut();
          this.timeOutControl = false;
          this.ServicesProvider.toast("Ha ocurrido un error al momento de obtener la información");
        });
    }
    else {
      this.chartEventos = new Chart(canvas, {});
      this.ServicesProvider.toast("El rango de fechas ingresado no puede ser mayor a 12 meses");
    }
  }

  getPrescripcionesChart() {
    var canvas = this.prescripciones_chart.nativeElement;
    var fecha_ini = this.presc["fecha_ini"];
    var fecha_fin = this.presc["fecha_fin"];
    if (this.chartPrescripciones) {
      this.chartPrescripciones.destroy();
    }
    if (this.getCantMonths(fecha_ini, fecha_fin) <= 12) {
      let params = {
        "pac_fk": this.patientProviderService.getCurrentUser().id_pk,
        "fec_ini": this.formatDate(fecha_ini),
        "fec_fin": this.formatDate(fecha_fin)
      }

      this.ServicesProvider.createLoader();
      this.ServicesProvider.apiPost(params, SERVICES.CHART_PRESCRIPCIONES_API)
        .then(chart => {
          this.ServicesProvider.closeLoader();
          this.clearTimeOut();
          if (chart["status"] == 404 || chart["status"] == 500) {
            this.timeOutControl = false;
            this.ServicesProvider.toast(MESSAGES.services.error);
          } else if (chart["status"] == 401) {
            this.ServicesProvider.toast(
              MESSAGES.services.unauthorized
            );
            this.timeOutControl = false;
          } else {
            let dataChart = JSON.parse(chart["_body"]);
            console.log("PRESCRIPCIONES\n"+JSON.stringify(dataChart));

            if (dataChart.labels.length != 0) {
              this.chartPrescripciones = new Chart(canvas, {
                type: 'bar',
                data: {
                  datasets: dataChart.datasets,
                  labels: dataChart.labels
                },
                options: {
                  legend: {
                    position: 'top',
                  },
                  scales: {
                    xAxes: [{
                      ticks: {
                        autoSkip: true,
                        maxRotation: 0,
                        minRotation: 0
                      }
                    }],
                    yAxes: [{
                      ticks: {
                        beginAtZero: true
                      }
                    }]
                  },
                  pan: {
                    enabled: false,
                    mode: 'xy' // is panning about the y axis neccessary for bar charts?
                  },
                  zoom: {
                    //sensitivity:10, drag: false, enabled: true, mode: 'x'
                    sensitivity: 10, drag: false, enabled: true, mode: 'x'
                  }
                }
              });
            }
            else {
              this.chartPrescripciones = new Chart(canvas, {});
              this.ServicesProvider.toast("No hay información disponible en el rango de fechas proporcionado.", "warningToast");
            }

          }
        })
        .catch(error => {
          this.ServicesProvider.closeLoader();
          this.clearTimeOut();
          this.timeOutControl = false;
          this.ServicesProvider.toast("Ha ocurrido un error al momento de obtener la información");
        });
    }
    else {
      this.chartPrescripciones = new Chart(canvas, {});
      this.ServicesProvider.toast("El rango de fechas ingresado no puede ser mayor a 12 meses");
    }
  }

  ionViewWillEnter() {
    /*if (localStorage.getItem("wizzard_reportes") == undefined) {
      localStorage.setItem("wizzard_reportes", "true");
      this.navCtrl.push(this.pageHelp, { 'seccion': 'reportes' });
    }*/
  }

  clearTimeOut() {
    clearTimeout(this.timeOut);
  }

  filterDefault() {
    //this.todo["dato"] = 9;
    let id_pac = this.patientProviderService.getCurrentUser().id_pk;
    //let paramsFilter = "?id_pac=" + id_pac + "&dato=" + 9;
    //let paramsFilter = "?id_pac=" + id_pac;
    let params = { "id_pac": id_pac };
    this.promisesByConsults(
      this.ServicesProvider.apiPost(
        params,
        SERVICES.DATA_TABLE_DATSAL_BASICOS_SERV
      )
    );
  }

  logForm() {
    let id_pac = this.patientProviderService.getCurrentUser().id_pk;
    let dataFilter: any = this.todo;
    let fecha_ini: string = "";
    let fecha_final: string = "";

    if (
      dataFilter.fecha_ini != null ||
      (dataFilter.fecha_ini != undefined && dataFilter.fecha_fin != null) ||
      dataFilter.fecha_fin != undefined
    ) {
      fecha_ini = this.formatDate(dataFilter.fecha_ini);
      fecha_final = this.formatDate(dataFilter.fecha_fin);
    }

    let params = { "id_pac": id_pac, "dato": dataFilter.dato, "fecha_inicial": fecha_ini, "fecha_final": fecha_final };
    console.log(JSON.stringify(params));
    this.reports_consult(params);
    this.getDatosChart();
  }

  formatDate(date: string) {
    let splitDate = date.split("-");
    return splitDate[2] + "/" + splitDate[1] + "/" + splitDate[0];
  }

  promisesByConsults(promisesAllSelect: any) {
    this.ServicesProvider.createLoader();
    Promise.all([promisesAllSelect])
      .then(promises => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();

        if (promises[0]["status"] == 404 || promises[0]["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (promises[0]["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let rptSelectAll = JSON.parse(promises[0]["_body"]);

          for (let item of rptSelectAll.success.tipos_datos) {
            if (item.id_pk != undefined) {
              this.typesData.push(item);
            }
          }
        }


      })
      .catch(err => {

        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.timeOutControl = false;
        this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  reports_consult(params: any) {
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiPost(params, SERVICES.DATA_TABLE_DATSAL_BASICOS_SERV)
      .then(consult => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (consult["status"] == 404 || consult["status"] == 500) {
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (consult["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
        } else {
          let respuesta = JSON.parse(consult["_body"]);
          this.consult_params = respuesta.success;
          console.log(this.consult_params['datos']);
          if (this.consult_params['datos'].length == 0) {
            this.ServicesProvider.toast("No hay información disponible en el rango de fechas proporcionado.", "warningToast");
          }
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  getPublicity() {
    let params = {
      'mod_padre': 102
    };
    this.ServicesProvider
      .apiPost(params, SERVICES.PUBLICIDADES_MODULOS_SERV)
      .then(state => {
        if (state["status"] == 404 || state["status"] == 500) {
        } else if (state["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
        } else {
          let respuesta = state["_body"];
          this.imageReportsPublicity = respuesta;
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  loadNew() {
    this.timeOutControl = true;
    this.filterDefault();
  }
}
