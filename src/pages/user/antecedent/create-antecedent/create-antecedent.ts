import { Component, group } from "@angular/core";
import { NavController, NavParams, Events, ModalController, AlertController } from "ionic-angular";
import { AntecedentProviderService } from "../../../../providers/antecedent/antecedent";
import { PatientProviderService } from "../../../../providers/patient/patient";
import { ServicesProvider } from "../../../../providers/services/services";

import {
  Validators,
  FormBuilder,
  FormControl,
  FormGroup
} from "@angular/forms";

/* Mensajes de la app */
import { MESSAGES } from "../../../../config/messages";

/*Plugins*/
import { Camera, CameraOptions } from "@ionic-native/camera";
import { ImagePicker, ImagePickerOptions } from "@ionic-native/image-picker";
import { SERVICES } from "../../../../config/url.servicios";

import { CropImagePage } from "../../../crop-image/crop-image";

class FilterAjax {
  public id: number;
  public text: string;
}

@Component({
  selector: "page-create-antecedent",
  templateUrl: "create-antecedent.html"
})
export class CreateAntecedentPage {
  antecedent: any;
  groupForm: Array<any> = [];
  formAntecedent: FormGroup;
  valuesRadio: Array<any> = [];
  selectDimanic: Array<string> = [];
  resultsServiceAjax: Array<string> = [];
  resultServiceAjax: FilterAjax;
  arrayModel: Array<string> = [];
  arrayModelHidden: Array<string> = [];
  filterSearchChanges: Array<string> = [];
  arrayPhonesAntecedent: string;
  fileUpload: string = "";
  timeOut: any;
  timeOutControl: boolean = true;
  nameFile: string;
  isSearchFilter: boolean = false;
  maxDate: string;

  verifyCancelsearchbar: any;

  nameGroupRadio: Array<any> = [];
  contentGroupsRadios: Array<Array<any>> = [];

  visibilityGroup: Array<any> = [];
  visibilityRadio: Array<any> = [];

  elements: Array<any> = [];
  visibilityElement: Array<any> = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public antecedentProviderService: AntecedentProviderService,
    private formBuilder: FormBuilder,
    private imagePicker: ImagePicker,
    private camera: Camera,
    public patientProviderService: PatientProviderService,
    public ServicesProvider: ServicesProvider,
    public events: Events,
    public modalCtrl: ModalController
  ) {
    this.formAntecedent = new FormGroup({});

    this.formAntecedent = this.formBuilder.group({
      description: ["", Validators.compose([Validators.required])],
      privateAnt: ["", Validators.compose([])]
    });

    this.maxDate = new Date()
      .toJSON()
      .slice(0, 10)
      .replace(/-/g, "-");

    this.formAntecedent.get('privateAnt').setValue('0');

    this.getFormAntecedent(
      this.navParams.get("antecedent").id,
      this.navParams.get("subType") != undefined
        ? this.navParams.get("subType").id
        : 0
    );
  }

  clearTimeOut() {
    clearTimeout(this.timeOut);
  }

  processResponse(body: any) {
    for (let i = 0; i < body.length; i++) {
      if (body[i].grupo.tipo_campo === "radio") {
        this.contentGroupsRadios.forEach(groups => {
          if (groups.indexOf(body[i]) != -1) {
            body[i]["options"] = JSON.parse(JSON.stringify(groups));
            body[i]["title"] = body[i].grupo.grupo_radio;
            this.elements.push(body[i]);
            if (body[i].grupo.ocultar_onload == 1) {
              this.visibilityElement.push(true);
            }
            else {
              this.visibilityElement.push(false);
            }
            i += groups.length;
          }
        });
      }
      if (body[i] != null) {
        this.elements.push(body[i]);
        if (body[i].grupo.ocultar_onload == 1) {
          this.visibilityElement.push(true);
        }
        else {
          this.visibilityElement.push(false);
        }
      }
    }
  }

  getFormAntecedent(antecedent: number, subType: number) {
    let params: any;
    if (subType != 0) {
      params = { "tan_fk": antecedent, "san_fk": subType };
    }
    else {
      params = { "tan_fk": antecedent };
    }

    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiPost(params, SERVICES.FORMULARIO_ANTECEDENTES_SERV)
      .then(parentesco => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (parentesco["status"] == 404 || parentesco["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else {
          let respuesta = JSON.parse(parentesco["_body"]);

          for (let i = 0; i < respuesta.length; i++) {
            if (respuesta[i].grupo.tipo_campo === "radio") {
              this.valuesRadio.push(respuesta[i]);
            } else {
              this.groupForm.push(respuesta[i]);
              if (respuesta[i].grupo.ocultar_onload == 1) {
                this.visibilityGroup.push(true);
              }
              else {
                this.visibilityGroup.push(false);
              }
              this.formAntecedent.setControl(
                respuesta[i].grupo.name_campo,
                new FormControl("", Validators.compose([]))
              );
            }
          }
          this.groupRadios(this.valuesRadio);
          this.processResponse(respuesta);

          this.formAntecedent.setControl(
            "searchText",
            new FormControl("", Validators.compose([]))
          );

          this.nameGroupRadio.forEach(radio => {
            this.formAntecedent.setControl(
              radio,
              new FormControl("", Validators.compose([]))
            )
          });

        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.timeOutControl = false;
        this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  groupRadios(valuesRadio: any) {
    valuesRadio.forEach(radio => {
      if (this.nameGroupRadio.indexOf(radio.grupo.name_campo) == -1) {
        this.nameGroupRadio.push(radio.grupo.name_campo);
      }
    });
    this.nameGroupRadio.forEach(groups => {
      this.contentGroupsRadios.push([]);
      this.visibilityRadio.push(false);
    });
    valuesRadio.forEach(radio => {
      let index = this.nameGroupRadio.indexOf(radio.grupo.name_campo);
      this.contentGroupsRadios[index].push(radio);
      if (radio.grupo.ocultar_onload == 1) {
        this.visibilityRadio[index] = true;
      }
    });
  }

  // Un archivo adjunto y una foto por cada antecedente creado
  logForm() {

    let formData = new FormData();
    let obj = this.formAntecedent.value;

    let arrayCamposAntecedentes: any[] = [];
    for (var name in obj) {
      let value = obj[name];

      if (name != "description" && name != "searchText" && name != 'privateAnt') {
        if (!value) {
          value = "";
        }
        let newValue = value.split("-");
        if (newValue.length == 3) {
          value = newValue[2] + "/" + newValue[1] + "/" + newValue[0]; // DD/MM/YYY
        }
        arrayCamposAntecedentes.push({ name_campo: name, valor: value });
      }
    }

    let reference = this.formAntecedent.value.description;
    let privado = this.formAntecedent.value.privateAnt;
    let pac_fk = this.patientProviderService.getCurrentUser().id_pk;

    let antecedent = this.navParams.get("antecedent").id;

    let subType =
      this.navParams.get("subType") != undefined
        ? this.navParams.get("subType").id
        : null;

    let arrayAntecedentes = [
      {
        tan_fk: antecedent,
        san_fk: subType,
        referencia: reference,
        privado: privado,
        name_adjunto: "archivo_adjunto",
        campos: arrayCamposAntecedentes
      }
    ];

    formData.append("pac_fk", pac_fk);
    if (this.arrayPhonesAntecedent) {
      formData.append("array_fotos_antecedentes[]", this.arrayPhonesAntecedent);
    }

    if (this.fileUpload) {
      formData.append(
        "archivo_adjunto",
        this.fileUpload,
        this.fileUpload["name"]
      );
    }

    formData.append("arrayAntecedentes", JSON.stringify(arrayAntecedentes));

    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiPost(formData, SERVICES.EDITAR_ANTECEDENTES_SERV)
      .then(antec => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (antec["status"] == 404 || antec["status"] == 500) {
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else {
          let respuesta = JSON.parse(antec["_body"]);
          this.nameFile = "";
          if (respuesta.success) {
            this.antecedentProviderService.antecedentsSuccess = true;
            this.navCtrl.pop();
            this.ServicesProvider.toast(MESSAGES.antecedent.create.success, "successToast");
            this.getUser(pac_fk);
          }
          else if (respuesta.errors != undefined) {
            let i = 0;
            for (i; i < Object.keys(respuesta.errors).length; i++) {
              let error = (<any>Object).values(respuesta.errors)[i];
              this.ServicesProvider.toast(error);
            }
          }
          else {
            this.ServicesProvider.toast(MESSAGES.antecedent.create.error);
          }
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  getUser(id_paciente: number) {
    let params = { "id_paciente": id_paciente };
    this.ServicesProvider.createLoader("Actualizando.. por favor espere");
    this.ServicesProvider
      .apiPost(params, SERVICES.CONSULTAR_PACIENTE_API_SERV)
      .then(state => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (state["status"] == 404 || state["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (state["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(state["_body"]);
          localStorage.setItem("user", JSON.stringify(respuesta));
          this.events.publish('user:imagen_porcentaje', respuesta.imagen_porcentaje);
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.timeOutControl = false;
        //});
      });
  }

  onSelectedItem(selected: number, name_campo: string, name_table: string) {
    // let params =
    // SERVICES.SELECT_DINAMICO_SERV + selected + "/" + name_campo + "/" + name_table;
    let params = { "id": selected, "filtro": name_campo, "nombTabla": name_table }
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiPost(params, SERVICES.SELECT_DINAMICO_SERV)
      .then(dinamic => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (dinamic["status"] == 404 || dinamic["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else {
          let respuesta = JSON.parse(dinamic["_body"]);
          this.selectDimanic = respuesta;
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();
        this.timeOutControl = false;
        this.clearTimeOut();
        this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  searchGroupRadio(element: any) {
    let index;
    this.contentGroupsRadios.forEach(group => {
      if (group.indexOf(element) != -1) {
        index = this.contentGroupsRadios.indexOf(group);
      }
    });
    return index;
  }

  checkRadio(element: any) {
    if (element.onclick_campo != null) {
      let obj = this.searchElement(element.onclick_campo);
      obj.forEach(e => {
        let index = this.elements.indexOf(e);
        if (element.accion_onclick == 'M') {
          if (index != -1) {
            this.visibilityElement[index] = false;
          }
        }
        else if (element.accion_onclick == 'O') {
          if (index != -1) {
            this.visibilityElement[index] = true;
          }
        }
      });
    }
  }

  checkRadioOld(element: any) {
    if (element.onclick_campo != null) {
      let obj = this.searchElement(element.onclick_campo);
      obj.forEach(e => {
        let index = this.groupForm.indexOf(e);
        if (element.accion_onclick == 'M') {
          if (e.grupo.tipo_campo == "radio") {
            index = this.searchGroupRadio(e);
            if (index != -1) {
              this.visibilityRadio[index] = false;
            }
          }
          else {
            this.visibilityGroup[index] = false;
          }
        }
        else if (element.accion_onclick == 'O') {
          if (e.grupo.tipo_campo == "radio") {
            index = this.searchGroupRadio(e);
            if (index != -1) {
              this.visibilityRadio[index] = true;
            }
          }
          else {
            this.visibilityGroup[index] = true;
          }
        }
      });
    }
  }

  searchElement(id: any) {
    let find: Array<any> = [];
    this.groupForm.forEach(element => {
      if (element.grupo.id_pk == id) {
        find.push(element);
      }
    });
    this.valuesRadio.forEach(element => {
      let arrayId = id.split(',');
      let index = JSON.stringify(element.grupo.id_pk);
      if (arrayId.indexOf(index) != -1) {
        find.push(element);
      }
    });
    return find;
  }

  resultsFilterAjax(url: string, ev: any) {
    if (ev.cancelable != undefined && ev.cancelable == true) {
      this.arrayModel[this.verifyCancelsearchbar] = '';
      this.arrayModelHidden[this.verifyCancelsearchbar] = '';
      this.filterSearchChanges = [];

      this.resultsServiceAjax = [];
      return;
    }

    let val = ev.target.value;
    if (val != undefined) {
      if (val.length > 4) {

        let country = this.patientProviderService.getCurrentUser().get_tipo_documento.pais_fk;
        let params = "";
        if (url == "buscar_vademecum") {
          params = "?pais_usuario=" + country + "&filtro=" + val;
        } else {
          params = "?filtro=" + val;
        }

        let parameters = "/" + url + "_api" + params

        this.ServicesProvider
          .apiGet(parameters, '')
          .then(vademecum => {
            if (vademecum["status"] == 404 || vademecum["status"] == 404) {
              this.ServicesProvider.toast(
                MESSAGES.services.error
              );
            } else {
              let respuesta = JSON.parse(vademecum["_body"]);
              this.resultsServiceAjax = respuesta;
            }
          })
          .catch(err => {
            this.ServicesProvider.toast(
              MESSAGES.services.error
            );
          });
      } else {
        this.resultsServiceAjax = [];
      }
    }
  }

  itemSelectedFilter(
    filterAjax: any,
    itemField: string,
    objectElement: any
  ) {
    this.isSearchFilter = true;
    this.resultsServiceAjax = [];
    this.verifyCancelsearchbar = itemField;
    this.arrayModel[itemField] = filterAjax.text;
    this.arrayModelHidden[itemField] = filterAjax.id;

    if (
      objectElement.objChange != null ||
      objectElement.objChange != undefined
    ) {
      this.onSelectedItemFilterSearch(
        filterAjax.id,
        objectElement.grupo.name_campo,
        objectElement.objChange.nomb_tabla
      );
    }
  }

  onSelectedItemFilterSearch(
    selected: number,
    name_campo: string,
    name_table: string
  ) {
    //let params = SERVICES.SELECT_DINAMICO_SERV + selected + "/" + name_campo + "/" + name_table;    
    let params = { "id": selected, "filtro": name_campo, "nombTabla": name_table }
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiPost(params, SERVICES.SELECT_DINAMICO_SERV)
      .then(dinamic => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (dinamic["status"] == 404 || dinamic["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else {
          let respuesta = JSON.parse(dinamic["_body"]);
          this.filterSearchChanges = respuesta;
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.timeOutControl = false;
        this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  onUploadChange(ev) {
    let extErrorMessage =
      "Solo se permiten archivos con extensión: .jpg, .jpeg, .png o pdf";
    let allowedExtension = ["jpg", "jpeg", "png", "pdf"];
    let extError = false;
    let myFiles = ev.target.files[0];

    if (myFiles != undefined) {

      let extName = myFiles["name"].split(".").pop().toLowerCase();

      if (allowedExtension.indexOf(extName) == -1) {
        extError = true;
      }

      if (extError) {
        this.ServicesProvider.toast(extErrorMessage);
      } else {
        this.fileUpload = myFiles;
        this.nameFile = this.fileUpload["name"];
      }
    } else {

      this.fileUpload = '';
      this.nameFile = "";
    }
  }

  goCropEditor(image: any, aspect: any) {
    let cropAlert = this.alertCtrl.create({
      title: "Midis App Salud",
      message: "¿Desea cortar o editar esta foto?.\nPulse no para utilizar la original.",
      buttons: [
        {
          text: "Sí",
          handler: () => {
            let cropModal = this.modalCtrl.create(CropImagePage, { "image": image, "aspect": aspect });
            cropModal.onDidDismiss(data => {
              if (data.croppedImage != null) {
                this.arrayPhonesAntecedent = data.croppedImage;
              }
            });
            cropModal.present();
            return;
          }
        },
        {
          text: "No",
          role: "cancel",
          handler: () => {
            this.arrayPhonesAntecedent = image;
          }
        }
      ]
    });
    cropAlert.present();

    /*
    let response = this.configPluginsProvider.cropper(image, aspect);
    this.photo = response;
    */
  }

  loadImage() {

    let options: ImagePickerOptions = {
      maximumImagesCount: 1,
      quality: 85,
      width: 510,
      height: 510,
      outputType: 1 // para que sea una imagen en base64, 0 fisico dispositivo
    };

    this.imagePicker.getPictures(options).then(
      results => {

        let base64Image = "data:image/jpeg;base64," + results[0];
        this.goCropEditor(base64Image, 0);

      },
      err => {
        this.ServicesProvider.toast(
          "Error al momento de cargar la imagen!"
        );
      }
    )
      .catch(err => {
        this.ServicesProvider.toast(err);

      });
  }

  tomarPhone() {
    const options: CameraOptions = {
      quality: 85,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      //allowEdit: true,
      correctOrientation: true
    };

    this.camera.getPicture(options).then(
      imageData => {
        let base64Image = "data:image/jpeg;base64," + imageData;
        this.goCropEditor(base64Image, 0);
      },
      err => {
        this.ServicesProvider.toast(
          MESSAGES.camera.message.selected
        );
      }
    );
  }

  loadFile() {
    document.getElementById("fileUpload").click();
  }

  deleteFileImg() {
    this.arrayPhonesAntecedent = '';
  }

  deleteFile() {
    this.fileUpload = '';
    this.nameFile = "";
  }

  loadNew() {
    this.timeOutControl = true;
    this.getFormAntecedent(
      this.navParams.get("antecedent").id,
      this.navParams.get("subType") != undefined
        ? this.navParams.get("subType").id
        : 0
    );
  }
}
