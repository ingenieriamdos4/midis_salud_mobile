import { Component } from "@angular/core";
import {
  NavController,
  NavParams,
  Refresher,
  AlertController
} from "ionic-angular";
import { AntecedentProviderService } from "../../../../providers/antecedent/antecedent";
import { ListAntecedentPage } from "../list-antecedent/list-antecedent";
import { ShowAntecedentPage } from "../show-antecedent/show-antecedent";
import { PatientProviderService } from "../../../../providers/patient/patient";
import { ServicesProvider } from "../../../../providers/services/services";
import { PageHelpPage } from "../../../index.paginas";

/* Mensajes de la app */
import { MESSAGES } from "../../../../config/messages";
import { SERVICES } from "../../../../config/url.servicios";

@Component({
  selector: "page-index-antecedent",
  templateUrl: "index-antecedent.html"
})
export class IndexAntecedentPage {
  listAntecedent: any = ListAntecedentPage;
  pageHelp = PageHelpPage;
  filterAntecedents: Array<any> = [];
  timeOut: any;
  timeOutControl: boolean = true;
  imageAntecedentsPublicity: string;
  oDiccionaryEstado: object =
    {
      "pendiente": {
        "color": "naranja-2"
      },
      "validado": {
        "color": "azul-2"
      },
      "no validado": {
        "color": "danger"
      }
    }
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public antecedentProviderService: AntecedentProviderService,
    public ServicesProvider: ServicesProvider,
    private alertCtrl: AlertController,
    public patientProviderService: PatientProviderService
  ) {
    this.getStorePublicity();
  }


  ionViewWillEnter() {
    /*if (localStorage.getItem("wizzard_antecedentes") == undefined) {
      localStorage.setItem("wizzard_antecedentes", "true");
      this.navCtrl.push(this.pageHelp, { 'seccion': 'antecedente' });
    }*/

    if (this.antecedentProviderService.antecedentsSuccess) {
      this.getListAntecedentAlertByUser("");
    }
  }

  ionViewDidLoad() {
    if (this.navParams.get("filter") == "med") {
      this.getListAntecedentAlertByUser("4");
    }
    else {
      if (!this.antecedentProviderService.antecedentsSuccess) {
        this.getListAntecedentAlertByUser("");
      }
    }
  }

  clearTimeOut() {
    clearTimeout(this.timeOut);
  }

  getListAntecedentAlertByUser(filter: string) {
    console.log("--------------filtro: ", filter);
    let pac_fk = this.patientProviderService.getCurrentUser().id_pk;
    let params = { "pac_fk": pac_fk, "solo_alerta": 0, "mostrar_ant_completo": 0, "tan_fk": filter };

    this.ServicesProvider.createLoader();
    this.ServicesProvider.apiPost(params, "/listar_antecedentes_paciente")
      .then(antec => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (antec["status"] == 404 || antec["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        }
        else if (antec["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(antec["_body"]);
          this.antecedentProviderService.antecedentsSuccess = false;
          this.antecedentProviderService.antecedent_alert = [];
          this.antecedentProviderService.othersAntecedent = [];
          this.antecedentProviderService.healthyHabits = [];
          console.log(this.antecedentProviderService)
          respuesta.forEach(antecedent => {
            if (antecedent["antPac"]["get_tipo_antecedente"]["alerta"] == "1") {
              this.antecedentProviderService.antecedent_alert.push(antecedent);
            }
            if (antecedent["antPac"]["get_tipo_antecedente"]["alerta"] == "0" &&
              antecedent["antPac"]["get_tipo_antecedente"]["habito_saludable"] == null) {
              this.antecedentProviderService.othersAntecedent.push(antecedent);
            }
            if (antecedent["antPac"]["get_tipo_antecedente"]["alerta"] == "0" &&
              antecedent["antPac"]["get_tipo_antecedente"]["habito_saludable"] == "1") {
              this.antecedentProviderService.healthyHabits.push(antecedent);
            }
          });
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader()

        this.clearTimeOut();
        this.timeOutControl = false;
        this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  reloadListAntecedent(refresher: Refresher) {
    setTimeout(() => {
      this.getListAntecedentAlertByUser("");
      refresher.complete();
    }, 1500);
  }

  detailAntecedent(antecedent: any, index: number) {
    this.navCtrl.push(ShowAntecedentPage, {
      antecedent: antecedent,
      index: index
    });
  }

  filterAntecedent() {
    if (this.filterAntecedents.length == 0) {
      this.ServicesProvider.createLoader();
      this.ServicesProvider
        .apiGet("?pac_fk=" + JSON.parse(localStorage.getItem('user')).id_pk, SERVICES.ARBOL_ANTECEDENTES_SERV)
        .then(parentesco => {
          this.ServicesProvider.closeLoader();
          this.clearTimeOut();
          if (parentesco["status"] == 404 || parentesco["status"] == 500) {
            this.timeOutControl = false;
            this.ServicesProvider.toast(
              MESSAGES.services.error
            );
          } else if (parentesco["status"] == 401) {
            this.ServicesProvider.toast(
              MESSAGES.services.unauthorized
            );
            this.timeOutControl = false;
          } else {
            let respuesta = JSON.parse(parentesco["_body"]);

            respuesta.forEach(element => {
              this.filterAntecedents.push({
                type: "radio",
                label: element.nombre,
                value: element.id
              });
            });

            this.presentAlert();
          }
        })
        .catch(err => {
          //this.ServicesProvider.loading.onDidDismiss(() => {
          this.ServicesProvider.closeLoader()
          this.clearTimeOut();
          this.timeOutControl = false;
          this.ServicesProvider.toast("Error: " + err);
          //});
        });
    } else {
      this.presentAlert();
    }
  }

  presentAlert() {
    let prompt = this.alertCtrl.create({
      title: "Filtrar",
      message: "Filtrar antecedentes por categoría ",
      inputs: this.filterAntecedents,
      buttons: [
        {
          text: "Cancelar",
          handler: data => { }
        },
        {
          text: "Aceptar",
          handler: data => {
            this.getListAntecedentAlertByUser(data);
          }
        }
      ]
    });

    prompt.present();
  }

  getStorePublicity() {
    let params = {
      'mod_padre': 104
    };
    this.ServicesProvider
      .apiPost(params, "/publicidades_modulos")
      .then(state => {
        if (state["status"] == 404 || state["status"] == 500) {
        } else if (state["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
        } else {
          let respuesta = state["_body"];
          this.imageAntecedentsPublicity = respuesta;
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  loadNew() {
    this.timeOutControl = true;
    if (this.navParams.get("filter") == "med") {
      this.getListAntecedentAlertByUser("4");
      return;
    }
    this.getListAntecedentAlertByUser("");
  }
}
