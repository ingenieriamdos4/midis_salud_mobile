import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AntecedentProviderService } from "../../../../providers/antecedent/antecedent";
import { CreateAntecedentPage } from "../create-antecedent/create-antecedent";

@Component({
  selector: 'page-subcategory-antecedent',
  templateUrl: 'subcategory-antecedent.html',
})
export class SubcategoryAntecedentPage {
  subcategoryAntecedents: Array<any> = [];
  timeOut: any;
  timeOutControl: boolean = true;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public antecedentProviderService: AntecedentProviderService) {

      this.cargarSubcategorias(
        this.navParams.get("antecedent")
      );
  }

  clearTimeOut() {
    clearTimeout(this.timeOut);
  }

  cargarSubcategorias(subcategories: any) {
    this.subcategoryAntecedents = subcategories;
  }

  subTypeAntecedent(subtype: any, antecedent: any) {
    console.log("--- san_fk: ", subtype);
    this.navCtrl.push(CreateAntecedentPage, {
      antecedent: antecedent,
      subType: subtype
    });
  }

}
