
export { ListAntecedentPage  } from './list-antecedent/list-antecedent';
export { ShowAntecedentPage  } from './show-antecedent/show-antecedent';
export { IndexAntecedentPage  } from './index-antecedent/index-antecedent';
export { CreateAntecedentPage  } from './create-antecedent/create-antecedent';
export { SubcategoryAntecedentPage  } from './subcategory-antecedent/subcategory-antecedent';
