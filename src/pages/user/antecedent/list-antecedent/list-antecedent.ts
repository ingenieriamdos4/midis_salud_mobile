import { Component } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";
import { AntecedentProviderService } from "../../../../providers/antecedent/antecedent";
import { ServicesProvider } from "../../../../providers/services/services";

import { CreateAntecedentPage } from "../create-antecedent/create-antecedent";
import { SubcategoryAntecedentPage } from "../subcategory-antecedent/subcategory-antecedent";

/* Mensajes de la app */
import { MESSAGES } from "../../../../config/messages";
import { SERVICES } from "../../../../config/url.servicios";

@Component({
  selector: "page-list-antecedent",
  templateUrl: "list-antecedent.html"
})
export class ListAntecedentPage {
  antecedents: Array<any> = [];
  timeOut: any;
  timeOutControl: boolean = true;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public ServicesProvider: ServicesProvider,
    public antecedentProviderService: AntecedentProviderService
  ) {}

  ionViewDidLoad() {
    this.getListAntecedent();
  }

  clearTimeOut() {
    clearTimeout(this.timeOut);
  }

  getListAntecedent() {
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiGet("?pac_fk="+JSON.parse(localStorage.getItem('user')).id_pk, SERVICES.ARBOL_ANTECEDENTES_SERV)
      .then(parentesco => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (parentesco["status"] == 404 || parentesco["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (parentesco["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(parentesco["_body"]);
          this.antecedents = respuesta;
          console.log("ANTECEDENT\n" + JSON.stringify(this.antecedents))
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
          this.ServicesProvider.closeLoader();
          this.clearTimeOut();
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  toggleSection(i) {
    this.formAntecedent(this.antecedents[i]);
    this.antecedents[i].open = !this.antecedents[i].open;
  }

  toggleItem(i, j) {
    this.antecedents[i].nodes[j].open = !this.antecedents[i].nodes[j].open;
  }

  subTypeAntecedent(subtype: any, antecedent: any) {
    this.navCtrl.push(CreateAntecedentPage, {
      antecedent: antecedent,
      subType: subtype
    });
  }

  formAntecedent(antecedent: any) {
    if (antecedent.nodes == null) {
      this.navCtrl.push(CreateAntecedentPage, { antecedent: antecedent });
    } else {
      this.navCtrl.push(SubcategoryAntecedentPage, { antecedent: antecedent });
    }
  }

  loadNew() {
    this.timeOutControl = true;
    this.getListAntecedent();
  }
}
