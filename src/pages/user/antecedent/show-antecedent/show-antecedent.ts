import { Component } from "@angular/core";
import {
  NavController,
  NavParams,
  AlertController,
  Platform
} from "ionic-angular";

import { AntecedentProviderService } from "../../../../providers/antecedent/antecedent";
import { ConfigPluginsProvider } from "../../../../providers/config-plugins/config-plugins";
import { ServicesProvider } from "../../../../providers/services/services";


import {
  Validators,
  FormBuilder,
  FormControl,
  FormGroup
} from "@angular/forms";
/* Mensajes de la app */
import { MESSAGES } from "../../../../config/messages";
import { SERVICES } from "../../../../config/url.servicios";


import { InAppBrowser, InAppBrowserOptions } from "@ionic-native/in-app-browser";
import { File, FileEntry } from "@ionic-native/file";
import { FileTransfer, FileTransferObject } from "@ionic-native/file-transfer";
import { SocialSharing } from "@ionic-native/social-sharing";

declare var cordova: any;
@Component({
  selector: "page-show-antecedent",
  templateUrl: "show-antecedent.html"
})
export class ShowAntecedentPage {
  antecedent: any;
  groupForm: Array<string> = [];
  formAntecedent: FormGroup;
  titleAntecedent: string;
  titleSubtypeAntecedent: string;
  valuesRadio: Array<string> = [];
  vademecum: Array<string> = [];
  reference: string;
  radioChecked: Array<string> = [];
  arrayModel: Array<string> = [];
  tan_fk: number;
  san_fk: number;
  archivo_adjunto: string;
  archivo_foto: string;
  timeOut: any;
  timeOutControl: boolean = true;
  isDeleteAntecedent: boolean = false;
  iconClass: string;
  storageDirectory: string = "";
  isPrivate: boolean = false;
  isIos: boolean = false;
  user = JSON.parse(localStorage.getItem("user"));
  observaciones: Array<object> = [];
  estado: string;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public antecedentProviderService: AntecedentProviderService,
    public ServicesProvider: ServicesProvider,
    private formBuilder: FormBuilder,
    private alertCtrl: AlertController,
    private iab: InAppBrowser,
    public platform: Platform,
    private socialSharing: SocialSharing,
    public configPluginsProvider: ConfigPluginsProvider,
    private file: File,
    private transfer: FileTransfer
  ) {
    this.platform.ready().then(() => {
      // make sure this is on a device, not an emulation (e.g. chrome tools device mode)
      if (!this.platform.is("cordova")) {
        return false;
      }

      if (this.platform.is("ios")) {
        this.storageDirectory = this.file.documentsDirectory;
        this.isIos = true;
      } else if (this.platform.is("android")) {
        this.storageDirectory = this.file.externalRootDirectory;
      } else {
        // exit otherwise, but you could add further types here e.g. Windows
        return false;
      }
    });

    this.formAntecedent = new FormGroup({});

    this.formAntecedent = this.formBuilder.group({
      description: ["", Validators.compose([Validators.required])]
    });

    this.loadInfo();

    //this.groupForm = this.navParams.get("antecedent");
  }

  loadInfo() {
    let antPaciente: any = this.navParams.get("antecedent");
    antPaciente.antPac.privado == '1' ? this.isPrivate = true : this.isPrivate = false;
    this.estado = this.navParams.get("antecedent").antPac.validado;
    console.log(this.estado);
    console.log("-- the data navParams", this.navParams.get("antecedent"));
    console.log("-- antPaciente.antPac.privado", antPaciente.antPac.privado)
    console.log("-- this.isPrivate = ", this.isPrivate);
    antPaciente.antPac.pro_fk_cambio != null
      ? (this.isDeleteAntecedent = true)
      : "";
    this.iconClass = antPaciente.antPac.get_tipo_antecedente.icono.trim();
    this.titleAntecedent = antPaciente.antPac.get_tipo_antecedente.nombre;
    this.titleSubtypeAntecedent =
      antPaciente.antPac.get_subtipo_antecedente != null
        ? antPaciente.antPac.get_subtipo_antecedente.nombre
        : "";


    this.getDetailAntecedentByPac(antPaciente.antPac.id_pk, this.user.id_pk);

    this.tan_fk = antPaciente.antPac.tan_fk;
    this.san_fk = antPaciente.antPac.san_fk;
    
    this.archivo_adjunto = antPaciente.antPac.archivo_adjunto;
    this.archivo_foto = antPaciente.antPac.archivo_foto;
    this.reference = antPaciente.antPac.referencia;
  }

  clearTimeOut() {
    clearTimeout(this.timeOut);
  }

  getDetailAntecedentByPac(id_antecedent: number, id_pk: number) {
    let params = {
      "id": id_antecedent,
      "pac_fk": id_pk
    };
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiPost(params, SERVICES.DETALLE_ANTECEDENTES_PAC_SERV)
      .then(parentesco => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (parentesco["status"] == 404 || parentesco["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (parentesco["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(parentesco["_body"]);
          this.observaciones = respuesta[0].observaciones;

          let campos = respuesta[0].campos;
          console.log("CAMPOS\n" + JSON.stringify(campos));
          for (let i = 0; i < campos.length; i++) {
            if (campos[i].tipo_campo === "text" && campos[i].plugin == "D") {
              let valueDate = campos[i].valor.split("/");

              campos[i].valor =
                valueDate[2] + "-" + valueDate[1] + "-" + valueDate[0];
            }

            this.arrayModel[i] = campos[i].valor;

            this.groupForm.push(campos[i]);
            this.formAntecedent.setControl(
              campos[i].name_campo,
              new FormControl("", Validators.compose([]))
            );
          }
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.timeOutControl = false;
        this.ServicesProvider.toast("Error: " + err);
        //});
      });
  }

  deleteAntecedent() {
    let alert = this.alertCtrl.create({
      title: "Eliminar antecedente",
      message: "¿Esta seguro que desea eliminar este antecedente?",
      buttons: [
        {
          text: "Cancelar",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        },
        {
          text: "Confirmar",
          handler: () => {
            this.serviceDeleteAntecedent();
          }
        }
      ]
    });

    alert.present();
  }

  serviceDeleteAntecedent() {
    this.ServicesProvider.createLoader();
    let antecedentShow = this.navParams.get("antecedent").antPac;
    let params = {
      "id": antecedentShow.id_pk,
      "pac_fk": this.user.id_pk
    };
    //let params = "?pac_fk=" + antecedentShow.id_pk;
    this.ServicesProvider
      .apiPost(params, SERVICES.ELIMINAR_ANTECEDENTE_API_SERV)
      .then(antec => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (antec["status"] == 404 || antec["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (antec["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(antec["_body"]);
          if (respuesta.success) {
            let index = this.navParams.get("index");
            this.antecedentProviderService.antecedentsSuccess = true;
            if (antecedentShow["get_tipo_antecedente"]["alerta"] == "1") {
              this.antecedentProviderService.antecedent_alert.splice(index, 1);
            }

            if (
              antecedentShow["get_tipo_antecedente"]["alerta"] == "0" &&
              antecedentShow["get_tipo_antecedente"]["habito_saludable"] == null
            ) {
              this.antecedentProviderService.othersAntecedent.splice(index, 1);
            }

            if (
              antecedentShow["get_tipo_antecedente"]["alerta"] == "0" &&
              antecedentShow["get_tipo_antecedente"]["habito_saludable"] == "1"
            ) {
              this.antecedentProviderService.healthyHabits.splice(index, 1);
            }
            this.navCtrl.pop();
            this.ServicesProvider.toast(
              MESSAGES.antecedent.delete.success,
              "successToast"
            );
          } else {
            this.ServicesProvider.toast(
              MESSAGES.antecedent.delete.error
            );
          }
          //console.log('respuesta ', respuesta);
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.timeOutControl = false;
        this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  verifyTypeFile(file: string) {
    let ext: any = file
      .split(".")
      .pop()
      .toLowerCase();

    if (ext == "jpg" || ext == "jpeg" || ext == "png") {
      return true;
    }
    return false;
  }

  reviewFile(file: string) {
    let options: InAppBrowserOptions = {
      hardwareback: 'yes'
    };
    this.iab.create(file, "_blank", options);
  }

  splitFileName(file: string) {
    return file.split("/").pop();
  }

  download(_url: string) {

    if (this.isIos) {
      this.shareQr(_url);
      return;
    }

    let promise = this.configPluginsProvider.statusExternalStorageForApplication()

    this.configPluginsProvider.downloader(
      promise,
      this.transfer,
      _url,
      this.storageDirectory
    );
  }

  shareQr(url: string) {
    this.ServicesProvider.createLoader();
    this.socialSharing
      .share(MESSAGES.antecedent.share.message, "Midis App", url, "")
      .then(success => {
        this.ServicesProvider.closeLoader();
      })
      .catch(err => {
        this.ServicesProvider.closeLoader();
        /*this.ServicesProvider.toast(
          MESSAGES.antecedent.share.error
        );*/
      });
  }

  servicePrivacityAntecedent() {
    console.log("this.isPrivate ? '0' : '1' = ", this.isPrivate ? '0' : '1')
    let formData = new FormData();
    let antPaciente: any = this.navParams.get("antecedent").antPac;
    formData.append('id', antPaciente.id_pk);
    formData.append('pac_fk', this.user.id_pk);
    formData.append('privado_ant', this.isPrivate ? '0' : '1');
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiPost(formData, SERVICES.EDITAR_PRIVACIDAD_ANTECEDENTES_API_SERV)
      .then(antec => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (antec["status"] == 404 || antec["status"] == 500) {
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (antec["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
        } else {
          let respuesta = JSON.parse(antec["_body"]);
          if (respuesta.success) {
            this.isPrivate ? this.isPrivate = false : this.isPrivate = true;
            this.antecedentProviderService.antecedentsSuccess = true;
            this.ServicesProvider.toast(
              MESSAGES.antecedent.privacity.success,
              "successToast"
            );
          } else {
            this.ServicesProvider.toast(
              MESSAGES.antecedent.privacity.error
            );
          }
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  loadNew() {
    this.timeOutControl = true;
    this.loadInfo();
  }
}
