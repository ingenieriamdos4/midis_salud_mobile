import { Component, ElementRef, ViewChild } from "@angular/core";
import {
  NavController,
  NavParams,
  ViewController
} from "ionic-angular";
import { FormControl } from "@angular/forms";
import { Person } from "../../../../models/person";
import { UpdateProfileProviderService } from "../../../../providers/update-profile/update-profile";
import { PatientProviderService } from "../../../../providers/patient/patient";
import { ServicesProvider } from "../../../../providers/services/services";
/* Mensajes de la app */
import { MESSAGES } from "../../../../config/messages";
import "rxjs/add/operator/debounceTime";
import { SERVICES } from "../../../../config/url.servicios";
import { Validators } from "@angular/forms";

@Component({
  selector: "page-modal-edit-family",
  templateUrl: "modal-edit-family.html"
})
export class ModalEditFamilyPage {
  states: Array<string> = [];
  cities: Array<string> = [];
  searchTermCity: string = "";
  searchControl: FormControl;
  searching: any = false;
  citySelected: number;
  countrys: Array<string> = [];
  indCountry: any;
  person: Person;
  relationShips: Array<string> = [];
  timeOut: any;
  timeOutControl: boolean = true;
  indicativoCellphone: string;
  indicativoPhone: string;
  hidenList: boolean = false;
  person_validation_phone:any={};
  user:any;
  pac_fk_midis:any;
  @ViewChild("parentesco", { read: ElementRef })
  parentesco: ElementRef;
  @ViewChild("country", { read: ElementRef })
  country: ElementRef;
  @ViewChild("state", { read: ElementRef })
  state: ElementRef;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public updateProfileService: UpdateProfileProviderService,
    public patientProviderService: PatientProviderService,
    public viewCtrl: ViewController,
    public ServicesProvider: ServicesProvider
  ) {
    this.searchControl = new FormControl();
    this.person = new Person();
    this.user= JSON.parse(localStorage.getItem("user"));
    this.pac_fk_midis=this.navParams.data.pac_fk_midis;

    this.promisesCountrysAndRelationShip(
      this.ServicesProvider.apiGet("", SERVICES.UBICACION_LIST_PAIS_SERV),
      this.ServicesProvider.apiGet("", SERVICES.PARENTESCO_LIST_SERV)
    );

    console.log("Paramentros que llegan", this.navParams.data);

    this.person.country = this.navParams.data.paisFamiliarcre;
    this.person.department = this.navParams.data.estadoFamiliarcre;
    this.searchTermCity = this.navParams.data.ciudadFamiliarcreNombre;
    this.citySelected = this.navParams.data.ciudadFamiliarcre;
    this.updateProfileService.addFamilyData
      .get("municipality")
      .setValue(this.navParams.data.ciudadFamiliarcre);
    this.person.relationShip = this.navParams.data.parentesco;

    this.updateProfileService.addFamilyData
      .get("name")
      .setValue(this.navParams.data.nombrefamiliar);
    this.updateProfileService.addFamilyData
      .get("surname")
      .setValue(this.navParams.data.apellido1familiar);
    this.updateProfileService.addFamilyData
      .get("second_surname")
      .setValue(this.navParams.data.apellido2familiar);
    this.updateProfileService.addFamilyData
      .get("email")
      .setValue(this.navParams.data.emailfamiliar);
    this.updateProfileService.addFamilyData
      .get("address")
      .setValue(this.navParams.data.direccionfamiliar);
    this.updateProfileService.addFamilyData
      .get("cellphone")
      .setValue(this.navParams.data.celular);
    this.updateProfileService.addFamilyData
      .get("phone")
      .setValue(this.navParams.data.telefonofijofamiliar);
    this.updateProfileService.addMedicalData
      .get("id_dispositivo")
      .setValue(this.navParams.data.id_dispositivo);

    this.indicativoCellphone = this.navParams.data.celularIndicativo;
    this.indCountry = this.navParams.data.celularprofesionalIndicativo;
    this.indicativoPhone = this.navParams.data.telefonofijofamiliarIndicativo;
  }

  clearTimeOut() {
    clearTimeout(this.timeOut);
  }
  cancelModal() {
    this.viewCtrl.dismiss();
  }
  promisesCountrysAndRelationShip(
    promiseCountry: any,
    promiseRelationShip: any
  ) {
    this.ServicesProvider.createLoader();
    Promise.all([promiseCountry, promiseRelationShip])
      .then(promises => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();        
        this.clearTimeOut();

        if (promises[0]["status"] == 404 || promises[0]["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (promises[0]["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let rptCountrys = JSON.parse(promises[0]["_body"]);
          this.countrys = rptCountrys;
          this.setRulesPhone(this.navParams.data.mask_celular,this.navParams.data.mask_fijo);
        }

        if (promises[1]["status"] == 404 || promises[1]["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (promises[1]["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let rptRelationShips = JSON.parse(promises[1]["_body"]);
          this.relationShips = rptRelationShips;
        }

        this.promiseStateAndCity(
          this.ServicesProvider.apiGet(
            this.person.country,
            SERVICES.UBICACION_LIST_ESTADODEPAIS_SERV
          ),
          this.ServicesProvider.apiGet(
            this.person.department,
            SERVICES.UBICACION_LIST_CIUDADDEESTADO_SERV
          )
        );
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
          this.ServicesProvider.closeLoader();          
          this.timeOutControl = false;
          this.clearTimeOut();
          this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  promiseStateAndCity(promiseState: any, promiseCity: any) {
    this.ServicesProvider.createLoader();
    Promise.all([promiseState, promiseCity])
      .then(promises => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();        
        this.clearTimeOut();

        if (promises[0]["status"] == 404 || promises[0]["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (promises[0]["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let rptState = JSON.parse(promises[0]["_body"]);
          this.states = rptState;
        }

        if (promises[1]["status"] == 404 || promises[1]["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (promises[1]["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let rptCity = JSON.parse(promises[1]["_body"]);
          this.updateProfileService.citiesContact = rptCity;
          this.setFilteredItems();
          this.searchControl.valueChanges
            .debounceTime(700)
            .subscribe(search => {
              this.searching = false;
              this.setFilteredItems();
            });
        }
      })
      .catch(err => {
        //this.updateProfileService.loading.onDidDismiss(() => {
          this.ServicesProvider.closeLoader();          
          this.timeOutControl = false;
          this.clearTimeOut();
          this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  updateCountry(selectedValue: any) {
    let country: any = this.countrys.filter(function(item: any) {
      return item.id_pk === Number(selectedValue);
    })[0];

    this.states = [];
    this.cities = [];
    this.updateProfileService.addFamilyData.get("municipality").setValue(null);
    this.updateProfileService.addFamilyData.get("department").setValue(null);
    this.indicativoPhone = "";
    this.searchTermCity = "";
    this.citySelected = 0;

    this.indCountry = country.indicativo;
    this.indicativoCellphone = this.indCountry;
    this.getStatesByCountry(selectedValue);
    this.setRulesPhone(country.mask_celular,country.mask_fijo);

  }

  getStatesByCountry(country: number) {
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiGet(country, SERVICES.UBICACION_LIST_ESTADODEPAIS_SERV)
      .then(state => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();        
        this.clearTimeOut();
        if (state["status"] == 404 || state["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (state["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(state["_body"]);
          this.states = respuesta;
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
          this.ServicesProvider.closeLoader();          
          this.timeOutControl = false;
          this.clearTimeOut();
          this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  updateState(selectedValue: number) {
    if (this.states.length > 0) {
      this.cities = [];
      this.searchTermCity = "";
      this.citySelected = 0;
      this.indicativoPhone = "";
      this.updateProfileService.addFamilyData
        .get("municipality")
        .setValue(null);
      this.getCitiesByState(selectedValue);
    }
  }

  getCitiesByState(state: number) {
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiGet(state, SERVICES.UBICACION_LIST_CIUDADDEESTADO_SERV)
      .then(city => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();        
        this.clearTimeOut();
        if (city["status"] == 404 || city["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (city["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(city["_body"]);
          this.updateProfileService.citiesContact = respuesta;
          this.setFilteredItems();
          this.searchControl.valueChanges
            .debounceTime(700)
            .subscribe(search => {
              this.searching = false;
              this.setFilteredItems();
            });
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
          this.ServicesProvider.closeLoader();          
          this.timeOutControl = false;
          this.clearTimeOut();
          this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  updateCity(city: any) {
    if (this.cities.length > 0) {
      this.indicativoPhone = city.indicativo;
    }
  }

  cancelAddMedical() {
    this.viewCtrl.dismiss();
  }

  addMedical() {
    let formData = this.updateProfileService.addFamilyData.value;

    let data = {
      id_pk: this.navParams.data.id_pk,
      parentesco: formData.parentesco,
      parentescoNombre: this.parentesco.nativeElement.textContent,
      nombrefamiliar: formData.name,
      apellido1familiar: formData.surname,
      apellido2familiar:
        formData.second_surname != null ? formData.second_surname : "",
      emailfamiliar: formData.email,
      direccionfamiliar: formData.address,
      paisFamiliarcre: formData.country,
      paisFamiliarcreNombre: this.country.nativeElement.textContent,
      estadoFamiliarcre: formData.department,
      estadoFamiliarcreNombre: this.state.nativeElement.textContent,
      ciudadFamiliarcre: formData.municipality,
      ciudadFamiliarcreNombre: this.searchTermCity,
      celular: formData.cellphone == null ? "" : formData.cellphone,
      celularIndicativo: this.indicativoCellphone,
      telefonofijofamiliar: formData.phone == null ? "" : formData.phone,
      telefonofijofamiliarIndicativo: this.indicativoPhone
    };

     this.updateProfileService.dataFamily[this.navParams.data.index] =data;
     this.saveData();

    //this.viewCtrl.dismiss(data);
  }



  onSearchInput(event: any) {
    this.updateProfileService.addFamilyData.get("municipality").setValue(null);
    this.indicativoPhone = "";
    this.citySelected = 0;
    this.searching = true;
    this.hidenList = true;
  }

  setFilteredItems() {
    this.cities = this.updateProfileService.filterItems(
      this.searchTermCity,
      this.updateProfileService.citiesContact
    );
  }

  filterCitySelected(city: any) {
    this.updateProfileService.addFamilyData
      .get("municipality")
      .setValue(city.id_pk);
    this.citySelected = city.id_pk;
    this.updateCity(city);
    this.searchTermCity = city.nombre;
    this.hidenList = false;
  }

  newLoad() {
    this.timeOutControl = true;
    this.promisesCountrysAndRelationShip(
      this.ServicesProvider.apiGet("", SERVICES.UBICACION_LIST_PAIS_SERV),
      this.ServicesProvider.apiGet("", SERVICES.PARENTESCO_LIST_SERV)
    );
  }




  saveData() {    
    let params = {};
    let personal_info = this.updateProfileService.updateDataPerson.value;
    let rptPatient: any = this.updateProfileService.infoPac;

    params["pac_fk"] = rptPatient.id_pk;
    params["email"] = personal_info.email;
    params["telefonodomicilio"] = personal_info.phone;
    params["celular"] = personal_info.cellphone;
    params["direccion"] = personal_info.address;
    params["pais"] = personal_info.country;
    params["estado"] = personal_info.department;
    params["ciudad"] = personal_info.municipality;

    this.updateProfileService.dataFamily.length > 0
      ? params["contactoFamiliar"] = JSON.stringify(this.updateProfileService.dataFamily) : "";

    this.ServicesProvider.createLoader("Procesando información");
    this.ServicesProvider
      .apiPost(params, SERVICES.EDITAR_PACIENTE_API_SERV)
      .then(update => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();       
        this.clearTimeOut();
        if (update["status"] == 404 || update["status"] == 500) {
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (update["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
        } else {
          let respuesta = JSON.parse(update["_body"]);

          if (respuesta.success) {
            this.ServicesProvider.toast(MESSAGES.updateProfile.update.success,
            "successToast");
            this.updateProfileService.getUser(rptPatient.id_pk);
            this.updateProfileService.loadFormFamily();
            this.viewCtrl.dismiss();
           /* this.patientProviderService.avatar = this.updateProfileService.avatar_profile;
            this.getUser(rptPatient.id_pk);*/
          } else {
            this.ServicesProvider.toast(
               respuesta.errors[Object.keys(respuesta.errors)[0]]
            );
          }
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
          this.ServicesProvider.closeLoader();          
          this.clearTimeOut();
          this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  setRulesPhone(mask_celular,mask_fijo){

        let contmaskCell=0;
        for(let i=0; i<=mask_celular.length; i++){
          if(mask_celular[i]=="-"){
            contmaskCell++;
          }
        }
        mask_celular=mask_celular.length-contmaskCell;
        let contmask_fijo=0;
        for(let i=0; i<=mask_fijo.length; i++){
          if(mask_fijo[i]=="-"){
            contmask_fijo++;
          }      
        }
        mask_fijo=mask_fijo.length-contmask_fijo;
                console.log(mask_celular,mask_fijo);


        this.person_validation_phone={"mask_celular": mask_celular, "mask_fijo":mask_fijo}
        this.updateProfileService.addFamilyData.controls["cellphone"].setValidators([Validators.required, Validators.maxLength(mask_celular),Validators.minLength(mask_celular)]);
        this.updateProfileService.addFamilyData.controls["cellphone"].updateValueAndValidity();
       
        this.updateProfileService.addFamilyData.controls["phone"].setValidators([Validators.maxLength(mask_fijo),Validators.minLength(mask_fijo)]);
        this.updateProfileService.addFamilyData.controls["phone"].updateValueAndValidity();
  }

}
