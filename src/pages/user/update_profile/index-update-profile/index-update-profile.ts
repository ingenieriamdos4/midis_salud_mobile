import { Component } from "@angular/core";
import { NavController, NavParams, ModalController, PopoverController } from "ionic-angular";

import {
  UpdatePersonalInformationPage,
  UpdateHealthcareDataPage,
  UpdateOtherDataPage,
  UpdateMedicalContactDataPage,
  UpdateFamilyContactDataPage,
  PageHelpPage
} from "../../../index.paginas";


/* Importing services*/
import { UpdateProfileProviderService } from "../../../../providers/update-profile/update-profile";
import { UpdatePhotoProfilePage } from "../update-photo-profile/update-photo-profile";
import { PatientProviderService } from "../../../../providers/patient/patient";
import { ServicesProvider } from "../../../../providers/services/services";

/* App messages */
import { MESSAGES } from "../../../../config/messages";
import { SERVICES } from "../../../../config/url.servicios";

@Component({
  selector: "page-index-update-profile",
  templateUrl: "index-update-profile.html"
})
export class IndexUpdateProfilePage {
  tabUno: any;
  tabDos: any;
  tabTres: any;
  tabCuatro: any;
  tabCinco: any;
  name: string;
  currentUser: any;
  avatar:string;
  color_nivel:string;
  pageHelp = PageHelpPage;
  edad:any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public popoverCtrl: PopoverController,
    public updateProfileService: UpdateProfileProviderService,
    public modalCtrl: ModalController,
    public patientProviderService: PatientProviderService,
    public ServicesProvider: ServicesProvider
  ) {
    this.tabUno = UpdatePersonalInformationPage;
    this.tabDos = UpdateHealthcareDataPage;
    this.tabTres = UpdateMedicalContactDataPage;
    this.tabCuatro = UpdateFamilyContactDataPage;
    this.tabCinco = UpdateOtherDataPage;

    let currentUser = JSON.parse(localStorage.getItem("user"));

    this.currentUser = currentUser;
    this.updateProfileService.avatar_profile =
      currentUser.get_user[0].foto;
    this.name =
      currentUser.nombres +
      " " +
      currentUser.apellido1 +
      " " +
      (currentUser.apellido2 != null ? currentUser.apellido2 : "");
      this.edad = ServicesProvider.getDateZero(this.currentUser.fecha_nac);

      this.getColorLevel(currentUser.id_pk);
      /*if( localStorage.getItem("wizzard_actualizar_perfil")==undefined){
        localStorage.setItem("wizzard_actualizar_perfil","true");
        this.navCtrl.push( this.pageHelp ,{ 'seccion': 'actualizar_perfil' });
      }*/

  }

  getAge(birth_day: string) {
    let birthday = +new Date(birth_day);
    let today = +new Date();
    let c_age = (today - birthday) / 31557600000;
    let age = Math.floor(c_age);
    return age;
  }

  updatePhotoProfile(avatar:string){

    let profileModal = this.modalCtrl.create(UpdatePhotoProfilePage, {photo: avatar});
    profileModal.onDidDismiss(data => {

      if(data != undefined) {

        if (data.avatar != undefined) {

          this.updateProfileService.avatar_profile = data.avatar;

        }

      }

    });
    profileModal.present();

  }

  getColorLevel(id_paciente:any) {
    let data = {"id_paciente": id_paciente };
    this.ServicesProvider
      .apiPost(data, SERVICES.CONSULTAR_PACIENTE_API_SERV)
      .then(state => {
        if (state["status"] == 404 || state["status"] == 500) {
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (state["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
        } else {
          let respuesta = JSON.parse(state["_body"]);
          this.color_nivel = respuesta.color_nivel;
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  /*popoverHelp(myEvent) {
    let popover = this.popoverCtrl.create(PopoverHelpPage, {typePage: '101'}, {cssClass: 'popover-help'});
    popover.present({
      ev: myEvent
    });
  }
*/

   createWizzard(secccion){

   }

}
