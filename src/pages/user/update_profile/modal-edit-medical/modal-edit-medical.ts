import { Component, ViewChild, ElementRef } from "@angular/core";
import {
  NavController,
  NavParams,
  ViewController,
  AlertController
} from "ionic-angular";
import { FormControl } from "@angular/forms";
import { UpdateProfileProviderService } from "../../../../providers/update-profile/update-profile";
import { PatientProviderService } from "../../../../providers/patient/patient";
import { ServicesProvider } from "../../../../providers/services/services";
import { Person } from "../../../../models/person";
/* Mensajes de la app */
import { MESSAGES } from "../../../../config/messages";
import "rxjs/add/operator/debounceTime";
import { SERVICES } from "../../../../config/url.servicios";
import { Validators } from "@angular/forms";

@Component({
  selector: "page-modal-edit-medical",
  templateUrl: "modal-edit-medical.html"
})
export class ModalEditMedicalPage {
  states: Array<string> = [];
  cities: Array<string> = [];
  searchTermCity: string = "";
  searchControl: FormControl;
  searching: any = false;
  citySelected: number;
  countrys: Array<string> = [];
  indCountry: any;
  person: Person;
  person_validation_phone:any={};
  tipoProfesion: number;
  profesion: number;
  especialidad: number;

  profesiones: Array<string> = [];
  typeprofesion: Array<string> = [];
  especialidades: Array<string> = [];
  timeOut: any;
  timeOutControl: boolean = true;
  indicativoCellphone: string;
  indicativoPhone: string;
  pac_fk_midis:any;
  hideList: boolean = false;

  @ViewChild("tipoProfessional", { read: ElementRef })
  tipoProfessional: ElementRef;
  @ViewChild("profession", { read: ElementRef })
  profession: ElementRef;
  @ViewChild("speciality", { read: ElementRef })
  speciality: ElementRef;

  @ViewChild("country", { read: ElementRef })
  country: ElementRef;
  @ViewChild("state", { read: ElementRef })
  state: ElementRef;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public updateProfileService: UpdateProfileProviderService,
    public patientProviderService: PatientProviderService,
    public ServicesProvider: ServicesProvider,
    public viewCtrl: ViewController,
    private alertCtrl: AlertController
  ) {
    this.searchControl = new FormControl();
    this.person = new Person();
    this.promisesCountrysAndRelationShip(
      this.ServicesProvider.apiGet("", SERVICES.UBICACION_LIST_PAIS_SERV),
      this.ServicesProvider.apiGet("", SERVICES.TPF_LIST_TIPOPROFESIONAL_SERV)
    );

    console.log("Paramentros que llegan", this.navParams.data);

    /*

      $request->nombreprofesional (varchar 50)
      $request->apellidosprofesional (varchar 50)
      $request->tipo_prof (int) opcional
      $request->profesion (int) opcional
      $request->especialidad (int) opcional
      $request->direccionprofesional (varchar 20) opcional
      $request->paisProfesional (int) opcional
      $request->estadoProfesional (int) opcional
      $request->ciudadProfesional (int) opcional
      $request->telefonofijoprofesional (varchar 20) opcional
      $request->celularprofesional (varchar 20)
      $request->emailprofesional (varchar 30) opcional

      */

    this.person.country = this.navParams.data.paisProfesional;
    this.person.department = this.navParams.data.estadoProfesional;
    this.searchTermCity = this.navParams.data.ciudadProfesionalNombre;
    this.citySelected = this.navParams.data.ciudadProfesional;
    this.updateProfileService.addMedicalData
      .get("municipality")
      .setValue(this.navParams.data.ciudadProfesional);
    //this.person.municipality = this.navParams.data.ciudadProfesional;

    this.tipoProfesion = this.navParams.data.tipo_prof;
    this.profesion = this.navParams.data.profesion;
    this.especialidad = this.navParams.data.especialidad;

    this.updateProfileService.addMedicalData
      .get("name")
      .setValue(this.navParams.data.nombreprofesional);
    this.updateProfileService.addMedicalData
      .get("second_surname")
      .setValue(this.navParams.data.apellidosprofesional);
    this.updateProfileService.addMedicalData
      .get("email")
      .setValue(this.navParams.data.emailprofesional);
    this.updateProfileService.addMedicalData
      .get("address")
      .setValue(this.navParams.data.direccionprofesional);
    this.updateProfileService.addMedicalData
      .get("cellphone")
      .setValue(this.navParams.data.celularprofesional);
    this.updateProfileService.addMedicalData
      .get("phone")
      .setValue(this.navParams.data.telefonofijoprofesional);
    this.updateProfileService.addMedicalData
      .get("id_dispositivo")
      .setValue(this.navParams.data.id_dispositivo);

    this.indicativoCellphone = this.navParams.data.celularprofesionalIndicativo;
    this.indCountry = this.navParams.data.celularprofesionalIndicativo;
    this.indicativoPhone = this.navParams.data.telefonofijoprofesionalIndicativo;
  console.log(   this.updateProfileService.addMedicalData
      .get("country"))
  }

  clearTimeOut() {
    clearTimeout(this.timeOut);
  }
  cancelModal() {
    this.viewCtrl.dismiss();
  }
  promisesCountrysAndRelationShip(
    promiseCountry: any,
    promiseTypeProfessional: any
  ) {
    this.ServicesProvider.createLoader();
    Promise.all([promiseCountry, promiseTypeProfessional])
      .then(promises => {
        console.log("entraa")
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();        
        this.clearTimeOut();

        if (promises[0]["status"] == 404 || promises[0]["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (promises[0]["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let rptCountrys = JSON.parse(promises[0]["_body"]);
          this.countrys = rptCountrys;
          /*let user=JSON.parse(localStorage.getItem("user"));
          */
          this.navParams.data.mask_celular,
          this.setRulesPhone(this.navParams.data.mask_celular,this.navParams.data.mask_fijo);

        }

        if (promises[1]["status"] == 404 || promises[1]["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (promises[1]["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let rptTypeProfessional = JSON.parse(promises[1]["_body"]);
          this.typeprofesion = rptTypeProfessional;
        }

        if (this.navParams.data.tipo_prof != "") {
          this.promiseStateAndCityAndOthers(
            this.ServicesProvider.apiGet(
              this.person.country,
              SERVICES.UBICACION_LIST_ESTADODEPAIS_SERV
            ),
            this.ServicesProvider.apiGet(
              this.person.department,
              SERVICES.UBICACION_LIST_CIUDADDEESTADO_SERV
            ),
            this.ServicesProvider.apiGet(
              this.navParams.data.tipo_prof,
              SERVICES.TPF_LIST_SBP_SERV
            ),
            this.navParams.data.profesion != ""
              ? this.ServicesProvider.apiGet(
                  this.navParams.data.profesion,
                  SERVICES.SUBTIPOPROF_LIST_ESPECIALIDADESDESUBTIPO_SERV
                )
              : ""
          );
        } else {
          this.promiseStateAndCity(
            this.ServicesProvider.apiGet(
              this.person.country,
              SERVICES.UBICACION_LIST_ESTADODEPAIS_SERV
            ),
            this.ServicesProvider.apiGet(
              this.person.department,
              SERVICES.UBICACION_LIST_CIUDADDEESTADO_SERV
            )
          );
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
          this.ServicesProvider.closeLoader();          
          this.timeOutControl = false;
          this.clearTimeOut();
          this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  promiseStateAndCity(promiseState: any, promiseCity: any) {
    this.ServicesProvider.createLoader();
    Promise.all([promiseState, promiseCity])
      .then(promises => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();

        if (promises[0]["status"] == 404 || promises[0]["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (promises[0]["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let rptState = JSON.parse(promises[0]["_body"]);
          this.states = rptState;
        }

        if (promises[1]["status"] == 404 || promises[1]["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (promises[1]["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let rptCity = JSON.parse(promises[1]["_body"]);
          //this.cities = rptCity;
          this.updateProfileService.citiesContact = rptCity;
          this.setFilteredItems();
          this.searchControl.valueChanges
            .debounceTime(700)
            .subscribe(search => {
              this.searching = false;
              this.setFilteredItems();
            });
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
          this.ServicesProvider.closeLoader();
          this.timeOutControl = false;
          this.clearTimeOut();
          this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  promiseStateAndCityAndOthers(
    promiseState: any,
    promiseCity: any,
    promiseProfessional: any,
    promiseSpecialities: any
  ) {
    this.ServicesProvider.createLoader();
    Promise.all([
      promiseState,
      promiseCity,
      promiseProfessional,
      promiseSpecialities
    ])
      .then(promises => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();        
        this.clearTimeOut();

        if (promises[0]["status"] == 404 || promises[0]["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (promises[0]["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let rptState = JSON.parse(promises[0]["_body"]);
          this.states = rptState;
        }

        if (promises[1]["status"] == 404 || promises[1]["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (promises[1]["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let rptCity = JSON.parse(promises[1]["_body"]);
          this.updateProfileService.citiesContact = rptCity;
          this.setFilteredItems();
          this.searchControl.valueChanges
            .debounceTime(700)
            .subscribe(search => {
              this.searching = false;
              this.setFilteredItems();
            });
        }

        if (promises[2]["status"] == 404 || promises[2]["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (promises[2]["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let rptProfessional = JSON.parse(promises[2]["_body"]);
          this.profesiones = rptProfessional;
        }

        if (promiseSpecialities != "") {
          if (promises[3]["status"] == 404 || promises[3]["status"] == 500) {
            this.timeOutControl = false;
            this.ServicesProvider.toast(MESSAGES.services.error);
          } else if (promises[3]["status"] == 401) {
            this.ServicesProvider.toast(
              MESSAGES.services.unauthorized
            );
            this.timeOutControl = false;
          } else {
            let rptSpecialities = JSON.parse(promises[3]["_body"]);
            this.especialidades = rptSpecialities;
          }
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
          this.ServicesProvider.closeLoader();          
          this.timeOutControl = false;
          this.clearTimeOut();
          this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  updateCountry(selectedValue: any) {
    let country: any = this.countrys.filter(function(item: any) {
      return item.id_pk === Number(selectedValue);
    })[0];

    this.states = [];
    this.cities = [];
    this.updateProfileService.addMedicalData.get("municipality").setValue(null);
    this.updateProfileService.addMedicalData.get("department").setValue(null);
    this.indicativoPhone = "";
    this.searchTermCity = "";
    this.citySelected = 0;
    this.indCountry = country.indicativo;
    this.indicativoCellphone = this.indCountry;
    this.getStatesByCountry(selectedValue);
    this.setRulesPhone(country.mask_celular,country.mask_fijo);

  }

  getStatesByCountry(country: number) {
    this.ServicesProvider.createLoader();

    this.ServicesProvider
      .apiGet(country, SERVICES.UBICACION_LIST_ESTADODEPAIS_SERV)
      .then(state => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();        
        this.clearTimeOut();
        if (state["status"] == 404 || state["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (state["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(state["_body"]);
          this.states = respuesta;
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
          this.ServicesProvider.closeLoader();          
          this.timeOutControl = false;
          this.clearTimeOut();
          this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  updateState(selectedValue: number) {
    if (this.states.length > 0) {
      this.cities = [];
      this.searchTermCity = "";
      this.citySelected = 0;
      this.indicativoPhone = "";
      this.updateProfileService.addMedicalData
        .get("municipality")
        .setValue(null);
      this.getCitiesByState(selectedValue);
    }
  }

  getCitiesByState(state: number) {
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiGet(state, SERVICES.UBICACION_LIST_CIUDADDEESTADO_SERV)
      .then(city => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();        
        this.clearTimeOut();
        if (city["status"] == 404 || city["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (city["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(city["_body"]);
          this.updateProfileService.citiesContact = respuesta;
          this.setFilteredItems();
          this.searchControl.valueChanges
            .debounceTime(700)
            .subscribe(search => {
              this.searching = false;
              this.setFilteredItems();
            });
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
          this.ServicesProvider.closeLoader();          
          this.timeOutControl = false;
          this.clearTimeOut();
          this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  updateCity(city: any) {
    if (this.cities.length > 0) {
      this.indicativoPhone = city.indicativo;
    }
  }

  cancelAddMedical() {
    this.viewCtrl.dismiss();
  }

  addMedical() {
    let formData = this.updateProfileService.addMedicalData.value;

    let params = {
      id_pk: this.navParams.data.id_pk,
      nombreprofesional: formData.name,
      apellidosprofesional: formData.second_surname,
      emailprofesional: formData.email,
      direccionprofesional: formData.address,
      paisProfesional: formData.country,
      paisProfesionalNombre: this.country.nativeElement.textContent,
      estadoProfesional: formData.department,
      estadoProfesionalNombre: this.state.nativeElement.textContent,
      ciudadProfesional: formData.municipality,
      ciudadProfesionalNombre: this.searchTermCity,
      celularprofesional: formData.cellphone,
      celularprofesionalIndicativo: this.indicativoCellphone,
      telefonofijoprofesional: formData.phone != null ? formData.phone : "",
      telefonofijoprofesionalIndicativo: this.indicativoPhone,
      profesion_nombre:
        this.profesiones.length > 0
          ? this.profession.nativeElement.textContent
          : "",
      profesion: this.profesiones.length > 0 ? formData.profesion : "",
      tipo_prof_nombre: this.tipoProfessional.nativeElement.textContent,
      tipo_prof: formData.tipo_prof,
      especialidad_nombre:
        this.especialidades.length > 0
          ? this.speciality.nativeElement.textContent
          : "",
      especialidad: this.especialidades.length > 0 ? formData.especialidad : ""
    };

    console.log("Parametros para editar", params);
     this.updateProfileService.dataProfesional[this.navParams.data.index] =params;
     this.saveData();
    //this.viewCtrl.dismiss(params);

   /* this.updateProfileService.dataProfesional[index] = data;
    this.saveData();*/

  }



  setRulesPhone(mask_celular,mask_fijo){

        let contmaskCell=0;
        for(let i=0; i<=mask_celular.length; i++){
          if(mask_celular[i]=="-"){
            contmaskCell++;
          }
        }
        mask_celular=mask_celular.length-contmaskCell;
        let contmask_fijo=0;
        for(let i=0; i<=mask_fijo.length; i++){
          if(mask_fijo[i]=="-"){
            contmask_fijo++;
          }      
        }
        mask_fijo=mask_fijo.length-contmask_fijo;
                console.log(mask_celular,mask_fijo);


        this.person_validation_phone={"mask_celular": mask_celular, "mask_fijo":mask_fijo}
        this.updateProfileService.addMedicalData.controls["cellphone"].setValidators([Validators.required, Validators.maxLength(mask_celular),Validators.minLength(mask_celular)]);
        this.updateProfileService.addMedicalData.controls["cellphone"].updateValueAndValidity();
       
        this.updateProfileService.addMedicalData.controls["phone"].setValidators([Validators.maxLength(mask_fijo),Validators.minLength(mask_fijo)]);
        this.updateProfileService.addMedicalData.controls["phone"].updateValueAndValidity();
  }




  selectedTypeProfesion(selected: number) {
    this.profesiones = [];
    this.especialidades = [];
    this.updateProfileService.addMedicalData.get("profesion").setValue(null);
    this.updateProfileService.addMedicalData.get("especialidad").setValue(null);

    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiGet(selected, SERVICES.TPF_LIST_SBP_SERV)
      .then(professional => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();        
        this.clearTimeOut();
        if (professional["status"] == 404 || professional["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (professional["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(professional["_body"]);
          this.profesiones = respuesta;
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
          this.ServicesProvider.closeLoader();          
          this.timeOutControl = false;
          this.clearTimeOut();
          this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  selectedProfesion(selected: any) {
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiGet(selected, SERVICES.SUBTIPOPROF_LIST_ESPECIALIDADESDESUBTIPO_SERV)
      .then(professional => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();        
        this.clearTimeOut();
        if (professional["status"] == 404 || professional["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (professional["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(professional["_body"]);
          this.especialidades = respuesta;
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
          this.ServicesProvider.closeLoader();          
          this.timeOutControl = false;
          this.clearTimeOut();
          this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }




  deleteMedical() {
    let alert = this.alertCtrl.create({
      title: "Eliminar contacto médico",
      message: "¿Esta seguro que desea eliminar el contacto médico?",
      buttons: [
        {
          text: "Cancelar",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        },
        {
          text: "Confirmar",
          handler: () => {
            if (this.navParams.data.id_pk != "") {
              this.serviceDeleteMedical(this.navParams.data.id_pk);
              console.log("Id_pkkk", this.navParams.data.id_pk);
            } else {
              this.viewCtrl.dismiss({ delete: true });
            }
          }
        }
      ]
    });

    alert.present();
  }

  serviceDeleteMedical(id_pk: number) {
    let formData = new FormData();
    formData.append("id", id_pk.toString());
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiGet(formData, SERVICES.ELIMINAR_MEDICO_API_SERV)
      .then(_delete => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();        
        this.clearTimeOut();
        if (_delete["status"] == 404 || _delete["status"] == 500) {
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (_delete["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
        } else {
          let respuesta = JSON.parse(_delete["_body"]);
          if (respuesta.success) {
            this.ServicesProvider.toast(
              MESSAGES.updateProfile.medical.delete.success,
              "successToast"
            );
            this.viewCtrl.dismiss({ delete: true });
          } else {
            this.ServicesProvider.toast(
              MESSAGES.updateProfile.medical.delete.error
            );
          }
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
          this.ServicesProvider.closeLoader();          
          this.clearTimeOut();
          this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  onSearchInput(event: any) {
    this.updateProfileService.addMedicalData.get("municipality").setValue(null);
    this.indicativoPhone = "";
    this.citySelected = 0;
    this.searching = true;
    this.hideList = true;
  }

  setFilteredItems() {
    this.cities = this.updateProfileService.filterItems(
      this.searchTermCity,
      this.updateProfileService.citiesContact
    );
  }

  filterCitySelected(city: any) {
    this.updateProfileService.addMedicalData
      .get("municipality")
      .setValue(city.id_pk);
    this.citySelected = city.id_pk;
    this.updateCity(city);
    this.searchTermCity = city.nombre;
    this.hideList = false;
  }

  newLoad() {
    this.timeOutControl = true;
    this.promisesCountrysAndRelationShip(
      this.ServicesProvider.apiGet("", SERVICES.UBICACION_LIST_PAIS_SERV),
      this.ServicesProvider.apiGet("", SERVICES.TPF_LIST_TIPOPROFESIONAL_SERV)
    );
  }


  saveData() {    
    let params = {};
    let personal_info = this.updateProfileService.updateDataPerson.value;
    let rptPatient: any = this.updateProfileService.infoPac;

    params["pac_fk"] = rptPatient.id_pk;
    params["email"] = personal_info.email;
    params["telefonodomicilio"] = personal_info.phone;
    params["celular"] = personal_info.cellphone;
    params["direccion"] = personal_info.address;
    params["pais"] = personal_info.country;
    params["estado"] = personal_info.department;
    params["ciudad"] = personal_info.municipality;

    console.log( this.updateProfileService.dataProfesional);
    this.updateProfileService.dataProfesional.length > 0
     ? params["contactoMedico"] =  JSON.stringify(this.updateProfileService.dataProfesional) : "";


         //pac_fk_midis recibir

    /*params["pac_fk"] = this.updateProfileService.addMedicalData
              .get("medico_encontrado").value?this.updateProfileService.addMedicalData
              .get("medico_encontrado").value:'';
*/
    this.ServicesProvider.createLoader("Procesando información");
    this.ServicesProvider
      .apiPost(params, SERVICES.EDITAR_PACIENTE_API_SERV)
      .then(update => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();        
        this.clearTimeOut();
        if (update["status"] == 404 || update["status"] == 500) {
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (update["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
        } else {
          let respuesta = JSON.parse(update["_body"]);

          if (respuesta.success) {
            /*this.ServicesProvider.toast(MESSAGES.updateProfile.update.success,
            "successToast");*/
            this.updateProfileService.getUser(rptPatient.id_pk);
            this.updateProfileService.loadFormProfesional();
            this.viewCtrl.dismiss();
           /* this.patientProviderService.avatar = this.updateProfileService.avatar_profile;
            this.getUser(rptPatient.id_pk);*/
          } else {
            console.log(Object.keys(respuesta.errors));
            this.ServicesProvider.toast(
               respuesta.errors[Object.keys(respuesta.errors)[0]]
            );
          }
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
          this.ServicesProvider.closeLoader();
          this.clearTimeOut();
          this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }




  
}
