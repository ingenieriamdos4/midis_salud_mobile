import { Component } from "@angular/core";
import { NavController, NavParams, ModalController, AlertController } from "ionic-angular";
import { UpdateProfileProviderService } from "../../../../providers/update-profile/update-profile";
import { ModalAddMedicalPage } from "../modal-add-medical/modal-add-medical";
import { ModalEditMedicalPage } from "../modal-edit-medical/modal-edit-medical";
import { ServicesProvider } from "../../../../providers/services/services";
import { MESSAGES } from "../../../../config/messages";
import "rxjs/add/operator/debounceTime";
import { SERVICES } from "../../../../config/url.servicios";
@Component({
  selector: "page-update-medical-contact-data",
  templateUrl: "update-medical-contact-data.html"
})
export class UpdateMedicalContactDataPage {
  timeOut: any;
  timeOutControl: boolean = true;
  profesionalUpdate: Array<any> = [];
  constructor(
    public navCtrl: NavController,
    public updateProfileService: UpdateProfileProviderService,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public navParams: NavParams,
    public ServicesProvider: ServicesProvider

  ) {
    this.updateProfileService.loadFormProfesional();
  }

  backStep() {
    this.navCtrl.parent.select(1);
  }

  validateStep() {
    this.navCtrl.parent.select(3);
  }

  presentModalMedical() {
    let profileModal = this.modalCtrl.create(ModalAddMedicalPage);
    profileModal.present();
  }

  editMedical(item: any, index: number) {
    console.log(item)
    item["index"] = index;
    let profileModal = this.modalCtrl.create(ModalEditMedicalPage, item);
    profileModal.present();
  }

  deleteMedical(id_contacto) {
    console.log(id_contacto)
    let alert = this.alertCtrl.create({
      title: "Eliminar contacto médico",
      message: "¿Esta seguro que desea eliminar el contacto médico?",
      buttons: [
        {
          text: "Cancelar",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        },
        {
          text: "Confirmar",
          handler: () => {
            if (id_contacto != "") {
              this.serviceDeleteMedical(id_contacto);
              console.log("Id_pkkk", id_contacto);
            }
          }
        }
      ]
    });

    alert.present();
  }
  clearTimeOut() {
    clearTimeout(this.timeOut);
  }

  serviceDeleteMedical(id_pk: number) {
    let rptPatient: any = this.updateProfileService.infoPac;
    let formData = new FormData();
    formData.append("id", id_pk.toString());
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiPost(formData, SERVICES.ELIMINAR_MEDICO_API_SERV)
      .then(_delete => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (_delete["status"] == 404 || _delete["status"] == 500) {
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (_delete["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
        } else {
          let respuesta = JSON.parse(_delete["_body"]);
          if (respuesta.success) {
            this.ServicesProvider.toast(
              MESSAGES.updateProfile.medical.delete.success,
              "successToast"
            );
            console.log("asdasd")
            this.updateProfileService.getUser(rptPatient.id_pk);
            this.updateProfileService.loadFormProfesional();
          } else {

            this.ServicesProvider.toast(
              MESSAGES.updateProfile.medical.delete.error
            );
          }
        }
      })
      .catch(err => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.ServicesProvider.toast(MESSAGES.services.error);
      });
  }



}
