import { Component } from "@angular/core";
import { NavController } from "ionic-angular";
import { UpdateProfileProviderService } from "../../../../providers/update-profile/update-profile";
import { PatientProviderService } from "../../../../providers/patient/patient";
import { ServicesProvider } from "../../../../providers/services/services";

/* Mensajes de la app */
import { MESSAGES } from "../../../../config/messages";
import { SERVICES } from "../../../../config/url.servicios";


@Component({
  selector: "page-update-healthcare-data",
  templateUrl: "update-healthcare-data.html"
})
export class UpdateHealthcareDataPage {
  eps: string[];
  arl: string[];
  timeOut: any;
  timeOutControl: boolean = true;

  constructor(
    public updateProfileService: UpdateProfileProviderService,
    public patientProviderService: PatientProviderService,
    public ServicesProvider: ServicesProvider,
    public navCtrl: NavController
  ) {
    this.nestedPromises(
      this.updateProfileService.updateDataPerson.value.country
    );
  }

  ionViewWillEnter() {
    /*this.nestedPromises(
      this.updateProfileService.updateDataPerson.value.country
    );*/
  }

  clearTimeOut() {
    clearTimeout(this.timeOut);
  }

  loadDatapatient() {
    let rptPatient: any = this.updateProfileService.infoPac;

    let eps: Array<string> = [];
    let arl: Array<string> = [];

    rptPatient.get_eps.forEach(_eps => {
      eps.push(_eps["id_pk"]);
    });

    rptPatient.get_arl.forEach(_arl => {
      arl.push(_arl["id_pk"]);
    });

    this.updateProfileService.updateHalthcareData
      .get("eps")
      .setValue(eps.length > 0 ? eps : "");
    this.updateProfileService.updateHalthcareData
      .get("arl")
      .setValue(arl.length > 0 ? arl : "");

    if (rptPatient.get_datos_institucion.length > 0) {
      this.updateProfileService.updateHalthcareData
        .get("prepaid_security")
        .setValue(
          rptPatient.get_datos_institucion[0].segu_prepa != "null"
            ? rptPatient.get_datos_institucion[0].segu_prepa
            : ""
        );
      this.updateProfileService.updateHalthcareData
        .get("institution")
        .setValue(
          rptPatient.get_datos_institucion[0].inst_asiste != "null"
            ? rptPatient.get_datos_institucion[0].inst_asiste
            : ""
        );
      this.updateProfileService.updateHalthcareData
        .get("service_ambulance")
        .setValue(
          rptPatient.get_datos_institucion[0].serv_amb != "null"
            ? rptPatient.get_datos_institucion[0].serv_amb
            : ""
        );
      this.updateProfileService.updateHalthcareData
        .get("service_special_transfer")
        .setValue(
          rptPatient.get_datos_institucion[0].serv_trans != "null"
            ? rptPatient.get_datos_institucion[0].serv_trans
            : ""
        );
    }

    this.updateProfileService.isValidateUpdateHalthcareData = true;
  }

  nestedPromises(country: number) {
    this.ServicesProvider.createLoader();
    Promise.all([
      this.ServicesProvider.apiGet(country, SERVICES.EPS_GETDEPPAIS_SERV),
      this.ServicesProvider.apiGet(country, SERVICES.ARL_GETDEPPAIS_SERV)
    ])
      .then(promises => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (promises[0]["status"] == 404) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (promises[0]["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(promises[0]["_body"]);
          this.eps = respuesta;
        }

        if (promises[1]["status"] == 404) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (promises[1]["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(promises[1]["_body"]);
          this.arl = respuesta;
        }
        this.loadDatapatient();
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.timeOutControl = false;
        this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  backStep() {
    this.navCtrl.parent.select(0);
  }

  validateStep() {
    this.navCtrl.parent.select(2);
  }

  loadNew() {
    this.timeOutControl = true;
    this.nestedPromises(
      this.updateProfileService.updateDataPerson.value.country
    );
  }


  saveData() {
    let params = {};
    let rptPatient: any = this.updateProfileService.infoPac;
    let halthcareData = this.updateProfileService.updateHalthcareData.value;

    params["pac_fk"] = rptPatient.id_pk;

    if (halthcareData != undefined) {

      params["eps"] = this.updateProfileService.updateHalthcareData.controls['eps'].value == ""
        ? "MidisAppSaludDeleteRow" : this.updateProfileService.updateHalthcareData.controls['eps'].value;

      params["arl"] = this.updateProfileService.updateHalthcareData.controls['arl'].value == ""
        ? "MidisAppSaludDeleteRow" : this.updateProfileService.updateHalthcareData.controls['arl'].value;

      halthcareData.institution != null
        ? params["institucion"] = halthcareData.institution == "" ? "MidisAppSaludDeleteRow" : halthcareData.institution
        : "";

      halthcareData.service_ambulance != null
        ? params["ambulancia"] = halthcareData.service_ambulance == "" ? "MidisAppSaludDeleteRow" : halthcareData.service_ambulance
        : "";

      halthcareData.service_special_transfer != null
        ? params["transporte"] = halthcareData.service_special_transfer == "" ? "MidisAppSaludDeleteRow" : halthcareData.service_special_transfer
        : "";

      halthcareData.prepaid_security != null
        ? params["seguridadprepar"] = halthcareData.prepaid_security == "" ? "MidisAppSaludDeleteRow" : halthcareData.prepaid_security
        : "";

    }

    console.log("HEALTHCARE PARAMS:\n" + JSON.stringify(params));

    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiPost(params, '/editar_datos_de_asistencia_api')
      .then(update => {
        console.log("THEN");
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (update["status"] == 404 || update["status"] == 500) {
          this.ServicesProvider.toast(MESSAGES.services.error);
          console.log("404,500");
        } else if (update["status"] == 401) {
          console.log("401");
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
        } else {
          console.log("ANSWER");
          let respuesta = JSON.parse(update["_body"]);
          console.log(JSON.stringify(respuesta));

          if (respuesta.success) {
            console.log("SUCCESS");
            this.ServicesProvider.toast(MESSAGES.updateProfile.update.success,
              "successToast");
          } else if (respuesta.errors != undefined) {
            console.log("NO SUCCESS");
            console.log(Object.keys(respuesta.errors));
            this.ServicesProvider.toast(MESSAGES.updateProfile.update.error);
          }
          else{
            this.ServicesProvider.toast(MESSAGES.services.error);
          }
        }
      })
      .catch(err => {
        console.log("CATCH");
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.ServicesProvider.toast(MESSAGES.services.error, err);
      });
      console.log("OUT");

  }


}
