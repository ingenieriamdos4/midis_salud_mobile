import { Component } from "@angular/core";
import {
  NavController,
  NavParams,
  ViewController,
  ToastController,
  Platform,
  ModalController,
  AlertController
  //Events
} from "ionic-angular";
import { UpdateProfileProviderService } from "../../../../providers/update-profile/update-profile";

import { PatientProviderService } from "../../../../providers/patient/patient";
/*Plugins*/
import { Camera, CameraOptions } from "@ionic-native/camera";
import { ImagePicker, ImagePickerOptions } from "@ionic-native/image-picker";
import { Base64 } from '@ionic-native/base64';
import {
  UpdateOtherDataPage,
} from "../../../index.paginas";
/* SERVICIOS */
import { ServicesProvider } from "../../../../providers/services/services";
import { ConfigPluginsProvider } from "../../../../providers/config-plugins/config-plugins";
/* Mensajes de la app */
import { MESSAGES } from "../../../../config/messages";
import { SERVICES } from "../../../../config/url.servicios";
import { CropImagePage } from "../../../crop-image/crop-image";

@Component({
  selector: "page-update-photo-profile",
  templateUrl: "update-photo-profile.html"
})
export class UpdatePhotoProfilePage {
  photo: any;
  fileUpload: string;
  previewBase64: any;
  typeFile: string;
  updateOtherDataPage: UpdateOtherDataPage

  isIos: boolean = false;

  cropBase64Image: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public viewCtrl: ViewController,
    private camera: Camera,
    public toastCtrl: ToastController,
    private imagePicker: ImagePicker,
    public ServicesProvider: ServicesProvider,
    public patientProviderService: PatientProviderService,
    public updateProfileService: UpdateProfileProviderService,
    public base64: Base64,
    public platform: Platform,
    public configPluginsProvider: ConfigPluginsProvider
  ) {
    this.platform.ready().then(() => {
      if (this.platform.is("ios")) {
        ;
        this.isIos = true;
      }
    });

    this.photo = this.navParams.get("photo");
    if (localStorage.getItem("updated_avatar") != null && localStorage.getItem("updated_avatar") != "undefined") {
      this.previewBase64 = localStorage.getItem("updated_avatar");
    }
  }

  cancel() {
    this.photo = this.navParams.get("photo");
    this.viewCtrl.dismiss();
  }

  accept() {
    if (this.previewBase64 != null) {
      localStorage.setItem("updated_avatar", this.previewBase64);
      this.patientProviderService.avatar = this.previewBase64;
      this.updatePhoto(this.previewBase64);
    }
    this.viewCtrl.dismiss({ avatar: this.previewBase64, });
  }

  updatePhoto(img: any) {
    let user = JSON.parse(localStorage.getItem("user"));
    let formData = new FormData();
    formData.append("login_pk", user.get_user[0].login_pk);
    formData.append("array_foto_perfil[]", img);

    console.log("----- user.get_user[0].login_pk = " + user.get_user[0].login_pk);
    console.log("img cargada en formato base64 = ", img);

    this.ServicesProvider
      .apiPost(formData, SERVICES.EDITAR_FOTO_PACIENTE)
      .then(state => {
        if (state["status"] == 404 || state["status"] == 500) {
        } else if (state["status"] == 401) {
          this.ServicesProvider.toast(MESSAGES.services.unauthorized);
        } else {
          let respuesta = state["_body"];
          console.log(respuesta);
          this.ServicesProvider.toast("su foto de perfil ha sido actualizada", "successToast");
        }
      })
      .catch(err => {
        console.log(err);
        this.ServicesProvider.toast("Error al momento de actualizar su foto de perfil");
      });
  }

  //Image: Base64 de la imagen que se desea editar, Aspect: 1 si se desea un corte cuadrado, 0 si se desea libre.
  goCropEditor(image: any, aspect: any) {

    let cropAlert = this.alertCtrl.create({
      title: "Midis App Salud",
      message: "¿Desea cortar o editar esta foto?.\nPulse no para utilizar la original.",
      buttons: [
        {
          text: "Sí",
          handler: () => {
            let cropModal = this.modalCtrl.create(CropImagePage, { "image": image, "aspect": aspect });
            cropModal.onDidDismiss(data => {
              if (data.croppedImage != null) {
                this.photo = data.croppedImage;
              } else {
                this.photo = this.navParams.get("photo");
              }
              this.previewBase64 = data.croppedImage;
            });
            cropModal.present();
            return;
          }
        },
        {
          text: "No",
          role: "cancel",
          handler: () => {
            this.photo = image;
            this.previewBase64 = image;
          }
        }
      ]
    });
    cropAlert.present();

    /*
    let response = this.configPluginsProvider.cropper(image, aspect);
    this.photo = response;
    */
  }

  loadImage() {

    let options: ImagePickerOptions = {
      maximumImagesCount: 1,
      quality: 85,
      width: 510,
      height: 510,
      outputType: 1 // para que sea una imagen en base64, 0 fisico dispositivo
    };

    this.imagePicker.getPictures(options).then(
      results => {

        let base64Image = "data:image/jpeg;base64," + results[0];

        this.goCropEditor(base64Image, 1);

      },
      err => {
        this.ServicesProvider.toast(
          "Error al momento de cargar la imagen!"
        );
      }
    )
      .catch(err => {
        this.ServicesProvider.toast(err);

      });
  }

  tomarPhone() {
    const options: CameraOptions = {
      quality: 85,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      //allowEdit: true
    };

    this.camera.getPicture(options).then(
      imageData => {
        let base64Image = "data:image/jpeg;base64," + imageData;

        this.goCropEditor(base64Image, 1);

      },
      err => {
        this.ServicesProvider.toast(MESSAGES.camera.message.selected);
      }
    );
  }
}
