import { Component } from "@angular/core";
import {
  NavController,
  ModalController,
  AlertController  
} from "ionic-angular";
import { UpdateProfileProviderService } from "../../../../providers/update-profile/update-profile";
import { ModalAddFamilyPage } from "../modal-add-family/modal-add-family";
import { ModalEditFamilyPage } from "../modal-edit-family/modal-edit-family";
import { ServicesProvider } from "../../../../providers/services/services";
import { SERVICES } from "../../../../config/url.servicios";
import { MESSAGES } from "../../../../config/messages";

@Component({
  selector: "page-update-family-contact-data",
  templateUrl: "update-family-contact-data.html"
})
export class UpdateFamilyContactDataPage {
  timeOut: any;

  constructor(
    public navCtrl: NavController,
    public updateProfileService: UpdateProfileProviderService,
    public modalCtrl: ModalController,
    private alertCtrl: AlertController,
    public ServicesProvider: ServicesProvider


  ) {
    this.updateProfileService.loadFormFamily();
  }

  ionViewDidLoad() {}

  presentModalFamily() {
    let profileModal = this.modalCtrl.create(ModalAddFamilyPage);
    profileModal.onDidDismiss(data => {
      if (data != undefined) {
        if (data.nombrefamiliar != "") {
          this.updateProfileService.dataFamily.unshift(data); //unshift
        }
      }
    });
    profileModal.present();
  }

  backStep() {
    this.navCtrl.parent.select(2);
  }

  validateStep() {
    //console.log('Datos familiares', this.updateProfileService.dataFamily);
    this.navCtrl.parent.select(4);
  }

  editFamily(item: any, index: number) {
    item["index"]=index;
    console.log(item)
    let profileModal = this.modalCtrl.create(ModalEditFamilyPage, item);
    /*profileModal.onDidDismiss(data => {
      console.log("Datos que se editaron", data);

      if (data != undefined) {
        if (data.nombrefamiliar != undefined) {
          this.updateProfileService.dataFamily[index] = data; //unshift
        }else if (data.delete == true) {
          this.updateProfileService.dataFamily.splice(index, 1);
        }
      }
    });*/
    profileModal.present();
  }

  clearTimeOut() {
    clearTimeout(this.timeOut);
  }
  deleteFamily(id_familiar) {
    let alert = this.alertCtrl.create({
      title: "Eliminar contacto familiar",
      message: "¿Esta seguro que desea eliminar el contacto familiar?",
      buttons: [
        {
          text: "Cancelar",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        },
        {
          text: "Confirmar",
          handler: () => {
            //if (this.navParams.data.id_pk != "") {
              this.serviceDeleteFamily(id_familiar);
            //} 
            /*else {
              //this.viewCtrl.dismiss({ delete: true });
            }*/
          }
        }
      ]
    });

    alert.present();
  }

  serviceDeleteFamily(id_pk: number) {
    let rptPatient: any = this.updateProfileService.infoPac;
    let formData = new FormData();
    formData.append("id", id_pk.toString());
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiPost(formData, SERVICES.ELIMINAR_FAMILIAR_API_SERV)
      .then(_delete => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();        
        this.clearTimeOut();
        if (_delete["status"] == 404 || _delete["status"] == 500) {
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (_delete["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
        } else {
          let respuesta = JSON.parse(_delete["_body"]);
          if (respuesta.success) {
            this.ServicesProvider.toast(
              MESSAGES.updateProfile.medical.delete.success,
              "successToast"
            );
            this.updateProfileService.getUser(rptPatient.id_pk);
            this.updateProfileService.loadFormFamily();
            //this.viewCtrl.dismiss({ delete: true });
          } else {
            //if(respuesta.no_eliminar_fam){
              this.ServicesProvider.toast(
                 MESSAGES.updateProfile.family.delete.error
              );              
            //}
            /*else{           
              this.ServicesProvider.toast(
                MESSAGES.updateProfile.medical.delete.error
              );
            }*/
          }
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
          this.ServicesProvider.closeLoader();          
          this.clearTimeOut();
          this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

}
