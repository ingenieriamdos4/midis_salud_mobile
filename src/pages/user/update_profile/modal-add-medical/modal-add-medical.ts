import { Component, ElementRef, ViewChild } from "@angular/core";
import { NavController, NavParams, ViewController } from "ionic-angular";
import { FormControl } from "@angular/forms";
import { UpdateProfileProviderService } from "../../../../providers/update-profile/update-profile";
import { PatientProviderService } from "../../../../providers/patient/patient";
import { ServicesProvider } from "../../../../providers/services/services";

import { Person } from "../../../../models/person";
/* Mensajes de la app */
import { MESSAGES } from "../../../../config/messages";
import "rxjs/add/operator/debounceTime";
import { SERVICES } from "../../../../config/url.servicios";
import { Validators } from "@angular/forms";

@Component({
  selector: "page-modal-add-medical",
  templateUrl: "modal-add-medical.html"
})
export class ModalAddMedicalPage {
  states: Array<string> = [];
  cities: Array<string> = [];
  searchTermCity: string = "";
  searchControl: FormControl;
  searching: any = false;
  citySelected: number;
  countrys: Array<string> = [];
  typeDocuments: Array<string> = [];
  indCountry: any;
  person: Person;
  profesiones: Array<string> = [];
  typeprofesion: Array<string> = [];
  especialidades: Array<string> = [];
  timeOut: any;
  timeOutControl: boolean = true;
  indicativoCellphone: string;
  indicativoPhone: string;
  isContactFamily: boolean;
  idpk: number;
  id_dispositivo_contact: string;
  controlLoad_s_c: boolean = true;
  hidenList: boolean = false;
  bMedicoEncontrado: boolean;
  bHizoBusqueda: boolean = false;
  user: any

  person_validation_phone: any;
  @ViewChild("country", { read: ElementRef })
  country: ElementRef;
  @ViewChild("state", { read: ElementRef })
  state: ElementRef;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public updateProfileService: UpdateProfileProviderService,
    public patientProviderService: PatientProviderService,
    public ServicesProvider: ServicesProvider,
    public viewCtrl: ViewController
  ) {
    this.searchControl = new FormControl();
    this.promisesCountrysAndRelationShip(
      this.ServicesProvider.apiGet("", SERVICES.UBICACION_LIST_PAIS_SERV),
      this.ServicesProvider.apiGet("", SERVICES.TPF_LIST_TIPOPROFESIONAL_SERV)
    );
    this.person = new Person();
    this.user = JSON.parse(localStorage.getItem("user"));
  }

  ionViewDidLoad() {
    this.updateProfileService.existFamily.reset();
    this.updateProfileService.addMedicalData.reset();
    this.updateProfileService.citiesContact = [];
    this.indicativoCellphone = "";
    this.indicativoPhone = "";
  }

  clearTimeOut() {
    clearTimeout(this.timeOut);
  }
  cancelModal() {
    this.viewCtrl.dismiss();
  }

  promisesCountrysAndRelationShip(
    promiseCountry: any,
    promiseTypeProfessional: any
  ) {
    console.log("las promesas")
    this.ServicesProvider.createLoader();
    Promise.all([promiseCountry, promiseTypeProfessional])
      .then(promises => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();

        this.clearTimeOut();

        if (promises[0]["status"] == 404 || promises[0]["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (promises[0]["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let rptCountrys = JSON.parse(promises[0]["_body"]);
          this.countrys = rptCountrys;
          console.log("consume paises")
          this.updateProfileService.addMedicalData
            .get("country")
            .setValue(this.user.get_pais_res.id_pk);
          this.indicativoCellphone = this.user.get_pais_res.indicativo;
          this.setRulesPhone(this.user.get_pais_res.mask_celular, this.user.get_pais_res.mask_fijo);

        }

        if (promises[1]["status"] == 404 || promises[1]["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (promises[1]["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let rptTypeProfessional = JSON.parse(promises[1]["_body"]);
          this.typeprofesion = rptTypeProfessional;
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();
        this.timeOutControl = false;
        this.clearTimeOut();
        this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  updateCountry(selectedValue: any) {
    //if (this.countrys.length > 0 && this.controlLoad_s_c) {
    // if (this.countrys.length > 0 ) {
    let country: any = this.countrys.filter(function (item: any) {
      return item.id_pk === Number(selectedValue);
    })[0];
    this.states = [];
    this.cities = [];
    this.updateProfileService.addMedicalData
      .get("municipality")
      .setValue(null);
    this.updateProfileService.addMedicalData.get("department").setValue(null);
    this.indicativoPhone = "";
    this.searchTermCity = "";
    this.citySelected = 0;

    this.indCountry = country.indicativo;
    this.indicativoCellphone = this.indCountry;
    this.getStatesByCountry(selectedValue);
    this.setRulesPhone(country.mask_celular, country.mask_fijo);
    //}
  }
  disableSearchCity(condition) {
    if (condition) {
      const inputs: any = document.getElementById("SearchCity").getElementsByTagName("INPUT");
      inputs[0].disabled = true;
    }
    else {
      const inputs: any = document.getElementById("SearchCity").getElementsByTagName("INPUT");
      inputs[0].disabled = false;
    }

  }

  setRulesPhone(mask_celular, mask_fijo) {

    let contmaskCell = 0;
    for (let i = 0; i <= mask_celular.length; i++) {
      if (mask_celular[i] == "-") {
        contmaskCell++;
      }
    }
    mask_celular = mask_celular.length - contmaskCell;
    let contmask_fijo = 0;
    for (let i = 0; i <= mask_fijo.length; i++) {
      if (mask_fijo[i] == "-") {
        contmask_fijo++;
      }
    }
    mask_fijo = mask_fijo.length - contmask_fijo;


    this.person_validation_phone = { "mask_celular": mask_celular, "mask_fijo": mask_fijo }
    this.updateProfileService.addMedicalData.controls["cellphone"].setValidators([Validators.required, Validators.maxLength(mask_celular), Validators.minLength(mask_celular)]);
    this.updateProfileService.addMedicalData.controls["cellphone"].updateValueAndValidity();

    this.updateProfileService.addMedicalData.controls["phone"].setValidators([Validators.maxLength(mask_fijo), Validators.minLength(mask_fijo)]);
    this.updateProfileService.addMedicalData.controls["phone"].updateValueAndValidity();
  }


  getStatesByCountry(country: number) {
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiGet(country, SERVICES.UBICACION_LIST_ESTADODEPAIS_SERV)
      .then(state => {
        this.ServicesProvider.closeLoader();
        //this.ServicesProvider.loading.dismiss();
        this.clearTimeOut();
        if (state["status"] == 404 || state["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (state["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(state["_body"]);
          this.states = respuesta;
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();

        this.timeOutControl = false;
        this.clearTimeOut();
        this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  updateState(selectedValue: any) {
    if (this.states.length > 0 && this.controlLoad_s_c && selectedValue.length != 0) {
      this.cities = [];
      if (selectedValue.length == 0) {
        this.searchTermCity = "";
        this.citySelected = 0;
        this.updateProfileService.addFamilyData
          .get("municipality")
          .setValue(null);
        this.indicativoPhone = "";
      }

      this.getCitiesByState(selectedValue);
    }
  }

  getCitiesByState(state: number) {
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiGet(state, SERVICES.UBICACION_LIST_CIUDADDEESTADO_SERV)
      .then(city => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();

        this.clearTimeOut();
        if (city["status"] == 404 || city["status"] == 400) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (city["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(city["_body"]);
          this.updateProfileService.citiesContact = respuesta;
          this.setFilteredItems();
          this.searchControl.valueChanges
            .debounceTime(700)
            .subscribe(search => {
              this.searching = false;
              this.setFilteredItems();
            });
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();

        this.timeOutControl = false;
        this.clearTimeOut();
        this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  updateCity(city: any) {
    if (this.cities.length > 0) {
      this.indicativoPhone = city.indicativo;
    }
  }

  cancelAddMedical() {
    this.viewCtrl.dismiss();
  }


  addMedical() {
    let formData = this.updateProfileService.addMedicalData.value;
    let params = {
      id_pk: "",
      nombreprofesional: formData.name,
      apellidosprofesional: formData.second_surname,
      emailprofesional: formData.email,
      direccionprofesional: formData.address,
      paisProfesional: formData.country,
      paisProfesionalNombre: this.country.nativeElement.textContent,
      estadoProfesional: formData.department,
      estadoProfesionalNombre: this.state.nativeElement.textContent,
      ciudadProfesional: formData.municipality.id_pk,
      ciudadProfesionalNombre: formData.municipality.nombre,
      celularprofesional: formData.cellphone,
      celularprofesionalIndicativo: this.indicativoCellphone,
      telefonofijoprofesional: formData.phone != null ? formData.phone : "",
      telefonofijoprofesionalIndicativo: this.indicativoPhone != null ? this.indicativoPhone : "",
      profesion: formData.profesion != null ? formData.profesion : "",
      tipo_prof: formData.tipo_prof != null ? formData.tipo_prof : "",
      especialidad: formData.especialidad != null ? formData.especialidad : "",
      id_dispositivo: this.id_dispositivo_contact
        ? this.id_dispositivo_contact
        : ""
    };

    console.log("ADD MEDICAL CONTACT: \n" + params)

    params["pac_fk_medico"] = this.updateProfileService.addMedicalData
      .get("medico_encontrado").value ? this.updateProfileService.addMedicalData
        .get("medico_encontrado").value : '';
    this.updateProfileService.dataProfesional.unshift(params);

    this.saveData();

  }

  selectedTypeProfesion(item: any) {
    this.person.speciality = null;
    this.person.professional = null;
    this.profesiones = [];
    this.especialidades = [];
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiGet(item.id_pk == undefined ? item : item.id_pk, SERVICES.TPF_LIST_SBP_SERV)
      //.apiGet(item.id_pk, SERVICES.TPF_LIST_SBP_SERV)
      .then(professional => {
        console.log(item, professional)

        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (professional["status"] == 404 || professional["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (professional["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(professional["_body"]);
          console.log(respuesta)

          this.profesiones = respuesta;
          if (item.subtipo_profesional_id) {
            console.log(item)
            this.updateProfileService.addMedicalData
              .get("profesion")
              .setValue(item.subtipo_profesional_id);
            if (item.especialidades_id) {
              this.selectedProfesion({ "id_pk": item.subtipo_profesional_id, "especialidades_id": item.especialidades_id })
            }
          }

        }
      })
      .catch(err => {
        console.log(err)
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();
        this.timeOutControl = false;
        this.clearTimeOut();
        this.ServicesProvider.toast("Error: " + err);
        //});*/
      });
  }

  selectedProfesion(item: any) {
    if (item.length != 0) {
      this.ServicesProvider.createLoader();
      this.ServicesProvider
        .apiGet(item.id_pk == undefined ? item : item.id_pk, SERVICES.SUBTIPOPROF_LIST_ESPECIALIDADESDESUBTIPO_SERV)
        .then(professional => {
          this.ServicesProvider.closeLoader();
          console.log(item, professional)

          //this.ServicesProvider.loading.dismiss();
          this.clearTimeOut();
          if (professional["status"] == 404 || professional["status"] == 404) {
            this.timeOutControl = false;
            this.ServicesProvider.toast(MESSAGES.services.error);
          } else if (professional["status"] == 401) {
            this.ServicesProvider.toast(
              MESSAGES.services.unauthorized
            );
            this.timeOutControl = false;
          } else {
            let respuesta = JSON.parse(professional["_body"]);
            this.especialidades = respuesta;
            if (item.especialidades_id) {
              this.updateProfileService.addMedicalData
                .get("especialidad")
                .setValue(item.especialidades_id);
            }


          }


        })
        .catch(err => {
          //this.ServicesProvider.loading.onDidDismiss(() => {
          this.ServicesProvider.closeLoader();

          this.timeOutControl = false;
          this.clearTimeOut();
          this.ServicesProvider.toast("Error: " + err);
          // });
        });
    }
  }

  familyExist() {

    //let existFamily = this.updateProfileService.existFamily.value;
    //let existFamily = this.updateProfileService.existFamily.value;
    console.log(this.updateProfileService.addMedicalData.controls.country.value)

    //let existFamily =this.updateProfileService.addMedicalData.controls.country.value;
    let params =
      "?pais=" + this.updateProfileService.addMedicalData.controls.country.value + "&celular=" + this.updateProfileService.addMedicalData.controls.cellphone.value + "&contacto=medico";
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiGet(params, SERVICES.DATOS_CONTACTO_API_SERV)
      .then(family => {
        this.ServicesProvider.closeLoader();
        console.log(JSON.parse(family["_body"]))

        this.clearTimeOut();
        if (family["status"] == 404 || family["status"] == 500) {
          this.ServicesProvider.toast(MESSAGES.services.error + "404, 500");
        } else if (family["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
        } else {
          let respuesta = JSON.parse(family["_body"]);
          if (respuesta.datos_contacto) {
            respuesta = respuesta.datos_contacto;
            this.bHizoBusqueda = true;
            this.bMedicoEncontrado = true;
            /* if(respuesta.get_user){
               this.updateProfileService.addMedicalData
                 .get("medico_encontrado")
                 .setValue(respuesta.get_user[0].pac_fk);  
             }
             else{*/
            this.updateProfileService.addMedicalData
              .get("medico_encontrado")
              .setValue(respuesta.id_pk);
            //}

            this.cities = [];
            //this.disableSearchCity(true);
            this.controlLoad_s_c = false;
            this.idpk = respuesta.id_pk;
            this.id_dispositivo_contact = respuesta.id_dispositivo;
            this.updateProfileService.addMedicalData
              .get("name")
              .setValue(respuesta.nombres);
            this.updateProfileService.addMedicalData
              .get("second_surname")
              .setValue(
                respuesta.apellido1 +
                " " +
                (respuesta.apellido2 != null ? respuesta.apellido2 : "")
              );
            console.log("breakpoint")
            this.updateProfileService.addMedicalData
              .get("email")
              .setValue(respuesta.correo);
            this.updateProfileService.addMedicalData
              .get("address")
              .setValue(respuesta.direccion != null ? respuesta.direccion : "");
            this.person.country = respuesta.get_pais_res.id_pk;

            if (respuesta.tipo_profesional_id) {
              this.updateProfileService.addMedicalData
                .get("tipo_prof")
                .setValue(respuesta.tipo_profesional_id);

              console.log("lo que encuentra", respuesta)
              if (respuesta.subtipo_profesional_id) {
                this.selectedTypeProfesion({ "id_pk": respuesta.tipo_profesional_id, "subtipo_profesional_id": respuesta.subtipo_profesional_id, "especialidades_id": respuesta.especialidades_id });
              }

            }
            //Cargar el estado y municipio de acuerdo a los datos que llegan del servicio
            //Paciente activo
            let promises = [
              this.ServicesProvider.apiGet(
                respuesta.get_pais_res.id_pk,
                SERVICES.UBICACION_LIST_ESTADODEPAIS_SERV
              ),
              this.ServicesProvider.apiGet(
                respuesta.get_estado_res.id_pk,
                SERVICES.UBICACION_LIST_CIUDADDEESTADO_SERV
              )];
            this.promiseStateAndCity(promises);
            this.person.department = respuesta.get_estado_res.id_pk;
            this.updateProfileService.addMedicalData
              .get("municipality")
              .setValue(respuesta.get_ciudad_res);
            this.searchTermCity = respuesta.get_ciudad_res.nombre;
            this.citySelected = respuesta.get_ciudad_res.id_pk;
            this.indicativoCellphone = respuesta.get_pais_res.indicativo;
            this.indicativoPhone = respuesta.get_ciudad_res.indicativo;
            this.updateProfileService.addMedicalData
              .get("cellphone")
              .setValue(
                respuesta.tlf_contacto == null ||
                  respuesta.tlf_contacto == "null"
                  ? ""
                  : respuesta.tlf_contacto
              );
            this.updateProfileService.addMedicalData
              .get("phone")
              .setValue(
                respuesta.tlf_domicilio == null ||
                  respuesta.tlf_domicilio == "null"
                  ? ""
                  : respuesta.tlf_domicilio
              );
            console.log(respuesta)
            //this.updateProfileService.toast('Se agrego correctamente el familiar '  + respuesta.nombres, 'successToast');
          }
          else if (respuesta.datos_contacto_existe) {
            this.bMedicoEncontrado = true;
            this.bHizoBusqueda = false;
            this.ServicesProvider.toast(
              MESSAGES.updateProfile.medical_founded.duplicated
            );
          }

          else {
            /*this.ServicesProvider.toast(
              "El contacto médico no se encontro "
            );*/
            this.bHizoBusqueda = true;
            let promise = [
              this.ServicesProvider.apiGet(
                this.updateProfileService.addMedicalData.get("country").value,
                SERVICES.UBICACION_LIST_ESTADODEPAIS_SERV
              )
            ]
            this.promiseStateAndCity(promise);

            this.bMedicoEncontrado = false;
            //this.states = [];
            this.updateProfileService.addMedicalData
              .get("medico_encontrado")
              .setValue(false);
            this.cities = [];
            this.updateProfileService.addMedicalData
              .get("phone")
              .setValue("");
            this.updateProfileService.addMedicalData
              .get("name")
              .setValue("");
            this.updateProfileService.addMedicalData
              .get("second_surname")
              .setValue("");
            this.updateProfileService.addMedicalData
              .get("email")
              .setValue("");
            this.updateProfileService.addMedicalData
              .get("address")
              .setValue("");
            this.updateProfileService.addMedicalData
              .get("profesion")
              .setValue(null);
            this.updateProfileService.addMedicalData
              .get("tipo_prof")
              .setValue(null);
            this.updateProfileService.addMedicalData
              .get("especialidad")
              .setValue(null);

            this.updateProfileService.addMedicalData.get("department").setValue(null);
            this.updateProfileService.addMedicalData
              .get("municipality")
              .setValue(null);
            this.indicativoPhone = "";
            this.searchTermCity = "";
            this.citySelected = 0;
            //this.disableSearchCity(false);
          }
        }
      })
      .catch(err => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.ServicesProvider.toast(MESSAGES.services.error, err);
      });
  }









  promiseStateAndCity(promises) {
    this.ServicesProvider.createLoader();
    Promise.all(promises)
      .then(promises => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();

        if (promises[0]["status"] == 404 || promises[0]["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (promises[0]["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let rptState = JSON.parse(promises[0]["_body"]);
          this.states = rptState;
        }

        if (promises[1]) {
          if (promises[1]["status"] == 404 || promises[1]["status"] == 500) {
            this.timeOutControl = false;
            this.ServicesProvider.toast(MESSAGES.services.error);
          } else if (promises[1]["status"] == 401) {
            this.ServicesProvider.toast(
              MESSAGES.services.unauthorized
            );
            this.timeOutControl = false;
          } else {
            let rptCity = JSON.parse(promises[1]["_body"]);
            //this.cities = rptCity;
            this.updateProfileService.citiesContact = rptCity;
            this.setFilteredItems();
            this.searchControl.valueChanges
              .debounceTime(700)
              .subscribe(search => {
                this.searching = false;
                this.setFilteredItems();
              });
          }
        }


        this.controlLoad_s_c = true;
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();
        this.timeOutControl = false;
        this.clearTimeOut();
        this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  updateCountryByDocument(selected: any) {
    if (this.countrys.length > 0) {
      this.ServicesProvider.createLoader();
      this.ServicesProvider
        .apiGet(selected.id_pk, SERVICES.TIPODOCUMENTO_GETDEPAIS_SERV)
        .then(documents => {
          //this.ServicesProvider.loading.dismiss();
          this.ServicesProvider.closeLoader();

          this.clearTimeOut();
          if (documents["status"] == 404 || documents["status"] == 500) {
            this.timeOutControl = false;
            this.ServicesProvider.toast(MESSAGES.services.error);
          } else if (documents["status"] == 401) {
            this.ServicesProvider.toast(
              MESSAGES.services.unauthorized
            );
            this.timeOutControl = false;
          } else {
            let respuesta = JSON.parse(documents["_body"]);
            this.typeDocuments = respuesta;
          }
        })
        .catch(err => {
          //this.ServicesProvider.loading.onDidDismiss(() => {
          this.ServicesProvider.closeLoader();
          this.timeOutControl = false;
          this.clearTimeOut();
          this.ServicesProvider.toast(MESSAGES.services.error);
          // });
        });
    }
  }

  onSearchInput(event: any) {
    this.updateProfileService.addMedicalData.get("municipality").setValue(null);
    this.indicativoPhone = "";
    this.citySelected = 0;
    this.searching = true;
    this.hidenList = true;
  }

  setFilteredItems() {
    this.cities = this.updateProfileService.filterItems(
      this.searchTermCity,
      this.updateProfileService.citiesContact
    );
  }

  filterCitySelected(city: any) {
    this.updateProfileService.addMedicalData.get("municipality").setValue(city);
    this.citySelected = city.id_pk;
    this.updateCity(city);
    this.searchTermCity = city.nombre;
    this.hidenList = false;
  }

  newLoad() {
    this.timeOutControl = true;
    this.updateProfileService.addMedicalData.reset();
    this.promisesCountrysAndRelationShip(
      this.ServicesProvider.apiGet("", SERVICES.UBICACION_LIST_PAIS_SERV),
      this.ServicesProvider.apiGet("", SERVICES.TPF_LIST_TIPOPROFESIONAL_SERV)
    );
  }





  saveData() {
    let params = {};
    let rptPatient: any = this.updateProfileService.infoPac;
    let personal_info = this.updateProfileService.updateDataPerson.value;

    params["pac_fk"] = rptPatient.id_pk;
    params["email"] = personal_info.email;
    params["telefonodomicilio"] = personal_info.phone;
    params["celular"] = personal_info.cellphone;
    params["direccion"] = personal_info.address;
    params["pais"] = personal_info.country;
    params["estado"] = personal_info.department;
    params["ciudad"] = personal_info.municipality;

    this.updateProfileService.dataProfesional.length > 0
      ? params["contactoMedico"] = JSON.stringify(this.updateProfileService.dataProfesional) : "";

    console.log("PARAMETROS ADD MEDICAL\n" + JSON.stringify(params));

    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiPost(params, SERVICES.EDITAR_PACIENTE_API_SERV)
      .then(update => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (update["status"] == 404 || update["status"] == 500) {
          console.log("ERROR 404, 500 SAVE DATA");
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (update["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
        } else {
          let respuesta = JSON.parse(update["_body"]);

          if (respuesta.success) {
            this.viewCtrl.dismiss();
            this.ServicesProvider.toast(MESSAGES.updateProfile.update.success,
              "successToast");
          } else {
            this.ServicesProvider.toast(MESSAGES.updateProfile.update.error);
          }
        }
      })
      .catch(err => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.ServicesProvider.toast(MESSAGES.services.error + err);
      });
  }

}
