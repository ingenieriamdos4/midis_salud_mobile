import { Component, ElementRef } from "@angular/core";
import { NavController, App, Events, AlertController, ModalController } from "ionic-angular";
import { Camera, CameraOptions } from "@ionic-native/camera";
import { ImagePicker, ImagePickerOptions } from "@ionic-native/image-picker";
import { UpdateProfileProviderService } from "../../../../providers/update-profile/update-profile";
import { PatientProviderService } from "../../../../providers/patient/patient";
import { ServicesProvider } from "../../../../providers/services/services";
import { DocumentsProviderService } from "../../../../providers/documents/documents";

/* Mensajes de la app */
import { MESSAGES } from "../../../../config/messages";
import { SERVICES } from "../../../../config/url.servicios";

//libreria para fechas entre formatos.
import { DatePipe } from '@angular/common';

import { CropImagePage } from "../../../crop-image/crop-image";

@Component({
  selector: "page-update-other-data",
  templateUrl: "update-other-data.html"
})
export class UpdateOtherDataPage {
  stratums: Array<string> = [];
  levelsEducations: Array<string> = [];
  religiones: Array<string> = [];
  workingStatus: Array<string> = [];
  timeOut: any;
  timeOutControl: boolean = true;
  eps: Array<string> = [];
  arl: Array<string> = [];

  so_flag: boolean = true;

  nameFile: string;
  uploadFile: any;
  imagePreview: any;

  deleteImpediment: any = 0;

  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    public updateProfileService: UpdateProfileProviderService,
    public patientProviderService: PatientProviderService,
    public ServicesProvider: ServicesProvider,
    public appCtrl: App,
    public datepipe: DatePipe,
    private camera: Camera,
    private imagePicker: ImagePicker,
    public events: Events,
    public alertCtrl: AlertController,
    private elem: ElementRef,
    public documentsProviderService: DocumentsProviderService
  ) {
    this.so_flag = this.ServicesProvider.validate_so_v();
    this.updateProfileService.updateOtherData.reset();
    this.nestedPromises(this.updateProfileService.updateDataPerson.value.country);
  }

  ionViewDidLoad() {
    console.log("UpdateOtherDataPage ionViewDidLoad. Después de subscribe...");
  }

  loadDatapatient() {
    let rptPatient: any = this.updateProfileService.infoPac;

    this.updateProfileService.updateOtherData
      .get("stratum")
      .setValue(rptPatient.est_fk);
    this.updateProfileService.updateOtherData
      .get("level_education")
      .setValue(rptPatient.ned_fk);
    this.updateProfileService.updateOtherData
      .get("religion")
      .setValue(rptPatient.rlg_fk);
    this.updateProfileService.updateOtherData
      .get("status_working")
      .setValue(rptPatient.esl_fk);
    this.updateProfileService.updateOtherData
      .get("impediment")
      .setValue(rptPatient.impedimento != null ? rptPatient.impedimento : "");
    this.updateProfileService.updateOtherData
      .get("adjunto_impedimento")
      .setValue(rptPatient.adjunto_impedimento != null ? rptPatient.adjunto_impedimento : "");
  }

  clearTimeOut() {
    clearTimeout(this.timeOut);
  }

  nestedPromises(country: number) {
    this.ServicesProvider.createLoader();
    Promise.all([
      this.ServicesProvider.apiGet(country, SERVICES.ESTRATO_GETDEPPAIS_SERV),
      this.ServicesProvider.apiGet("", SERVICES.NIVEL_EDUCATIVO_GETALL_SERV),
      this.ServicesProvider.apiGet("", SERVICES.RELIGION_GETALL_SERV),
      this.ServicesProvider.apiGet("", SERVICES.ESTADO_LABORAL_GETALL_SERV)
    ])
      .then(promises => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (promises[0]["status"] == 404 || promises[0]["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (promises[0]["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(promises[0]["_body"]);
          this.stratums = respuesta;
        }

        if (promises[1]["status"] == 404 || promises[1]["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (promises[1]["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(promises[1]["_body"]);
          this.levelsEducations = respuesta;
        }

        if (promises[2]["status"] == 404 || promises[2]["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (promises[2]["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(promises[2]["_body"]);
          this.religiones = respuesta;
        }

        if (promises[3]["status"] == 404 || promises[3]["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (promises[3]["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(promises[3]["_body"]);
          this.workingStatus = respuesta;
        }

        this.loadDatapatient();
      })
      .catch(err => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.timeOutControl = false;
        this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  backStep() {
    this.navCtrl.parent.select(3);
  }

  saveData() {
    let params = {};
    let rptPatient: any = this.updateProfileService.infoPac;
    let other_data = this.updateProfileService.updateOtherData.value;

    params["pac_fk"] = rptPatient.id_pk;

    if (other_data != undefined) {
      other_data.stratum != null
        ? params["estrato"] = other_data.stratum
        : "";
      other_data.religion != null
        ? params["religion"] = other_data.religion
        : "";
      other_data.level_education != null
        ? params["niveleducativo"] = other_data.level_education
        : "";
      other_data.status_working != null
        ? params["estadolaboral"] = other_data.status_working
        : "";
      other_data.impediment != null
        ? params["impedimento"] = other_data.impediment == "" ? "MidisAppSaludDeleteRow" : other_data.impediment
        : "";
      other_data.adjunto_impedimento != null
        ? params["cert_impedi"] = other_data.adjunto_impedimento.value
        : "";
    }

    if (this.deleteImpediment) {
      params["elim_cert_impedi"] = this.deleteImpediment;
    }

    if (this.uploadFile) {
      if (this.documentsProviderService.imagesBase64.length > 0) {
        this.documentsProviderService.imagesBase64.forEach(base64 => {
          params["cert_impedi"] = base64;
        });
      }
      else if (this.documentsProviderService.arrayFiles.length > 0) {
        for (let index = 0; index < this.documentsProviderService.arrayFiles.length; index++) {
          params["cert_impedi"] = this.documentsProviderService.arrayFiles[index].file, this.documentsProviderService.arrayFiles[index].nameFile;
        }
      }
    }
    else if (this.imagePreview != "") {
      params["cert_impedi"] = this.imagePreview;
    }
    else if (this.uploadFile != undefined && this.imagePreview != "") {
      this.ServicesProvider.toast(MESSAGES.documents.validate.message1);
      return;
    }
    else {
      this.ServicesProvider.toast(MESSAGES.documents.validate.message2);
      return;
    }

    console.log("OTHER DATA PARAMS\n" + JSON.stringify(params));

    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiPost(params, '/editar_otros_datos_api')
      .then(update => {

        if (update["status"] == 404 || update["status"] == 500) {
          this.ServicesProvider.toast(MESSAGES.services.error);
        }
        else if (update["status"] == 401) {
          this.ServicesProvider.toast(MESSAGES.services.unauthorized);
        }
        else {
          if (!Object.keys) Object.keys = function (o) {
            if (o !== Object(o))
              throw new TypeError('Object.keys called on a non-object');
            var k = [], p;
            for (p in o) if (Object.prototype.hasOwnProperty.call(o, p)) k.push(p);
            console.log("k = ", k)
            return k;
          }

          let respuesta = JSON.parse(update["_body"]);
          console.log(JSON.stringify(respuesta));
          if (respuesta.success) {
            this.ServicesProvider.toast(MESSAGES.updateProfile.update.success, "successToast");
          }
          else if (respuesta.errors != undefined) {
            console.log(Object.keys(respuesta.errors));
            this.ServicesProvider.toast(MESSAGES.updateProfile.update.error);
          }
        }
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
      })
      .catch(err => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.ServicesProvider.toast(MESSAGES.services.error + err);
      });
    this.deleteImpediment = 0;
  }

  getUser(id_paciente: number) {
    let params = { "id_paciente": id_paciente };
    this.ServicesProvider.createLoader("Actualizando.. por favor espere");
    this.ServicesProvider
      .apiPost(params, SERVICES.CONSULTAR_PACIENTE_API_SERV)
      .then(state => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (state["status"] == 404 || state["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (state["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(state["_body"]);
          this.ServicesProvider.toast(
            MESSAGES.updateProfile.update.success,
            "successToast"
          );

          this.updateProfileService.loadProgress = respuesta.porcentaje_avance;
          this.updateProfileService.infoPac = respuesta;
          this.updateProfileService.loadFormProfesional();
          this.updateProfileService.loadFormFamily();

          localStorage.setItem("user", JSON.stringify(respuesta));
          this.events.publish('user:imagen_porcentaje', respuesta.imagen_porcentaje);
        }
      })
      .catch(err => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.timeOutControl = false;
        this.ServicesProvider.toast(MESSAGES.services.error);
      });
  }

  loadNew() {
    this.timeOutControl = true;
    this.nestedPromises(
      this.updateProfileService.updateDataPerson.value.country
    );
  }

  goCropEditor(image: any, aspect: any) {

    let cropAlert = this.alertCtrl.create({
      title: "Midis App Salud",
      message: "¿Desea cortar o editar esta foto?.\nPulse no para utilizar la original.",
      buttons: [
        {
          text: "Sí",
          handler: () => {
            let cropModal = this.modalCtrl.create(CropImagePage, { "image": image, "aspect": aspect });
            cropModal.onDidDismiss(data => {
              if (data.croppedImage != null) {
                this.imagePreview = data.croppedImage;
              }
            });
            cropModal.present();
          }
        },
        {
          text: "No",
          role: "cancel",
          handler: () => {
            this.imagePreview = image;
          }
        }
      ]
    });
    cropAlert.present();

    /*
    let response = this.configPluginsProvider.cropper(image, aspect);
    this.photo = response;
    */
  }

  tomarFoto() {
    let options: CameraOptions = {
      quality: 85,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true
    };

    this.camera
      .getPicture(options)
      .then(
        imageData => {
          this.uploadFile = undefined;
          this.nameFile = "";
          //this.imagePreview = "data:image/jpeg;base64," + imageData;
          this.goCropEditor("data:image/jpeg;base64," + imageData, 0);
        },
        err => {
          this.ServicesProvider.toast(
            MESSAGES.camera.message.selected
          );
        }
      )
      .catch(err => {
        this.ServicesProvider.toast(
          MESSAGES.camera.message.error
        );
      });
  }

  traerFoto() {
    let options: ImagePickerOptions = {
      maximumImagesCount: 1,
      quality: 85,
      width: 510,
      height: 510,
      outputType: 1 // para que sea una imagen en base64, 0 fisico dispositivo
    };

    this.imagePicker
      .getPictures(options)
      .then(
        results => {
          this.uploadFile = true;
          this.imagePreview = this.goCropEditor("data:image/png;base64," + results[0], 0);
          this.nameFile = "Certificado impedimento";
        },
        err => {
          this.ServicesProvider.toast(
            MESSAGES.fileHealtData.upload.error
          );
        }
      )
      .catch(err => {
        this.ServicesProvider.toast(err);
      });
  }

  loadFile() {
    document.getElementById("fileUpload").click();
  }

  deleteFile(type: number) {
    let confirmAlert = this.alertCtrl.create({
      title: "Midis App Salud",
      message: "¿Está seguro que desea eliminar el certificado de su impedimento por religión?<br><br>Tendrás que guardar para que los cambios se reflejen.",
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel'
        },
        {
          text: 'Aceptar',
          role: 'confirm',
          handler: () => {
            type == 1 ?
              (
                this.imagePreview = "",
                this.updateProfileService.updateOtherData.get('adjunto_impedimento').setValue(""),
                this.deleteImpediment = 1
              )
              : (
                this.nameFile = "",
                this.updateProfileService.updateOtherData.get('adjunto_impedimento').setValue(""),
                this.deleteImpediment = 1
              );
            this.uploadFile = undefined;
          }
        }
      ]
    });
    confirmAlert.present();
  }

  deleteFileUp() {
    let confirmAlert = this.alertCtrl.create({
      title: "Midis App Salud",
      message: "¿Está seguro que desea eliminar el certificado de su impedimento por religión?<br><br>Tendrás que guardar para que los cambios se reflejen.",
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel'
        },
        {
          text: 'Aceptar',
          role: 'confirm',
          handler: () => {
            this.updateProfileService.updateOtherData.get('adjunto_impedimento').setValue("");
            this.deleteImpediment = 1;
          }
        }
      ]
    });
    confirmAlert.present();
  }

  helpImpediment() {
    let impedimentAlert = this.alertCtrl.create({
      title: "Midis App Salud",
      message: "En este campo puedes explicar algún impedimento en caso de asistencia médica, sean por religión o cualquier otro motivo.<br><br>De ser necesario puedes adjuntar un certificado o imagen explicativa.",
      buttons: [
        {
          text: 'Aceptar',
          role: 'confirm'
        }
      ]
    });
    impedimentAlert.present();
  }

}
