import { Component, ViewChild } from "@angular/core";
import { NavController, App, Events, Nav } from "ionic-angular";
import { FormControl } from "@angular/forms";
import { UpdateProfileProviderService } from "../../../../providers/update-profile/update-profile";
import { Person } from "../../../../models/person";
import { PatientProviderService } from "../../../../providers/patient/patient";
import { ServicesProvider } from "../../../../providers/services/services";
import { Validators } from "@angular/forms";


import { HomePage } from "../../../home/home";

/* Mensajes de la app */
import { MESSAGES } from "../../../../config/messages";
import { SERVICES } from "../../../../config/url.servicios";
import "rxjs/add/operator/debounceTime";

import { DatePipe } from '@angular/common';


@Component({
  selector: "page-update-personal-information",
  templateUrl: "update-personal-information.html"
})
export class UpdatePersonalInformationPage {
  @ViewChild(Nav) nav: Nav;
  person_validation_phone
  states: Array<string> = [];
  cities: Array<any> = [];
  searchTermCity: string = "";
  searchControl: FormControl;
  searching: any = false;
  citySelected: number;
  countrys: Array<string> = [];
  veredas: Array<string> = [];
  person: Person;
  timeOut: any;
  timeOutControl: boolean = true;
  indicativoCellphone: string = "";
  indicativoPhone: string = "";

  statesP: Array<string> = [];
  citiesP: Array<any> = [];
  veredasP: Array<string> = [];
  searchTermCityP: string = "";
  searchControlP: FormControl;
  searchingP: any = false;
  citySelectedP: number;
  isPersonalData: boolean = false;
  typesDocument: Array<string> = [];
  patientData: any;
  hidenList: boolean = false;

  structurePersonalInformation: any = {};

  constructor(
    public navCtrl: NavController,
    public updateProfileService: UpdateProfileProviderService,
    public patientProviderService: PatientProviderService,
    public ServicesProvider: ServicesProvider,
    public appCtrl: App,
    public events: Events,
    public datepipe: DatePipe

  ) {
    this.updateProfileService.updateDataPerson.reset();
    this.updateProfileService.updateHalthcareData.reset();
    this.updateProfileService.updateOtherData.reset();
    this.updateProfileService.updateDataPersonMain.reset();
    this.updateProfileService.isValidateUpdateHalthcareData = false;

    this.searchControl = new FormControl();
    this.searchControlP = new FormControl();
    this.person = new Person();
    this.getPatient(this.patientProviderService.getCurrentUser().id_pk);
  }

  clearTimeOut() {
    clearTimeout(this.timeOut);
  }

  setRulesPhone(mask_celular, mask_fijo) {
    console.log(mask_celular, mask_fijo)
    let contmaskCell = 0;
    for (let i = 0; i <= mask_celular.length; i++) {
      if (mask_celular[i] == "-") {
        contmaskCell++;
      }
    }
    mask_celular = mask_celular.length - contmaskCell;
    let contmask_fijo = 0;
    for (let i = 0; i <= mask_fijo.length; i++) {
      if (mask_fijo[i] == "-") {
        contmask_fijo++;
      }
    }
    mask_fijo = mask_fijo.length - contmask_fijo;
    console.log(mask_celular, mask_fijo);


    this.person_validation_phone = { "mask_celular": mask_celular, "mask_fijo": mask_fijo }
    this.updateProfileService.updateDataPerson.controls["cellphone"].setValidators([Validators.required, Validators.maxLength(mask_celular), Validators.minLength(mask_celular)]);
    this.updateProfileService.updateDataPerson.controls["cellphone"].updateValueAndValidity();

    this.updateProfileService.updateDataPerson.controls["phone"].setValidators([Validators.maxLength(mask_fijo), Validators.minLength(mask_fijo)]);
    this.updateProfileService.updateDataPerson.controls["phone"].updateValueAndValidity();
  }

  updateTypeDocument() {
    //this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiGet(this.updateProfileService.updateDataPerson.get("country").value,
        SERVICES.TIPODOCUMENTO_GETDEPAIS_SERV)
      .then(state => {
        //this.ServicesProvider.closeLoader();;
        //this.clearTimeOut();
        if (state["status"] == 404 || state["status"] == 500) {
          //this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (state["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          //this.timeOutControl = false;
        } else {
          this.updateProfileService.updateDataPersonMain.controls['type_document'].setValue(null);
          this.updateProfileService.updateDataPersonMain.controls['document'].setValue("");
          /*this.cities = [];
          this.veredas = [];
          this.searchTermCity = "";
          this.updateProfileService.updateDataPerson.get("municipality").setValue(null);
          this.updateProfileService.updateDataPerson.get("vereda").setValue(null);*/

          let rptTypesDocument = JSON.parse(state["_body"]);
          this.typesDocument = rptTypesDocument;
        }
      });
  }

  updateCountry(typeForm: string = "0", selectedValue: any) {
    if (this.countrys.length > 0 && selectedValue) {
      if (typeForm == "0") {
        let country: any = this.countrys.filter(function (item: any) {
          return item.id_pk === Number(selectedValue);
        })[0];
        this.setRulesPhone(country.mask_celular, country.mask_fijo);
        this.updateTypeDocument();


        if (country != undefined) {
          this.states = [];
          this.cities = [];
          this.veredas = [];
          this.searchTermCity = "";
          this.updateProfileService.updateDataPerson
            .get("department")
            .setValue(null);
          this.updateProfileService.updateDataPerson
            .get("municipality")
            .setValue(null);
          this.updateProfileService.updateDataPerson
            .get("vereda")
            .setValue(null);
          this.indicativoCellphone = country.indicativo;
          this.getStatesByCountry("0", selectedValue);
        }
      } else {
        this.statesP = [];
        this.citiesP = [];
        this.veredasP = [];
        this.searchTermCityP = "";
        this.updateProfileService.updateDataPersonMain
          .get("department")
          .setValue(null);
        this.updateProfileService.updateDataPersonMain
          .get("municipality")
          .setValue(null);
        this.updateProfileService.updateDataPersonMain
          .get("vereda")
          .setValue(null);
        this.getStatesByCountry("1", selectedValue);
      }
    }
  }

  getStatesByCountry(typeForm: string = "0", country: number) {
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiGet(country, SERVICES.UBICACION_LIST_ESTADODEPAIS_SERV)
      .then(state => {
        this.ServicesProvider.closeLoader();;
        this.clearTimeOut();
        if (state["status"] == 404 || state["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (state["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(state["_body"]);
          typeForm == "1"
            ? (this.statesP = respuesta)
            : (this.states = respuesta);
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();

        this.clearTimeOut();
        this.timeOutControl = false;
        this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  updateState(typeForm: string, selectedValue: number) {
    if (typeForm == "0") {
      if (this.states.length > 0) {
        this.cities = [];
        this.veredas = [];
        this.searchTermCity = "";
        this.updateProfileService.updateDataPerson
          .get("municipality")
          .setValue(null);
        this.updateProfileService.updateDataPerson.get("vereda").setValue(null);

        this.getCitiesByState("0", selectedValue);
      }
    } else {
      if (this.statesP.length > 0) {
        this.citiesP = [];
        this.veredasP = [];
        this.searchTermCityP = "";
        this.updateProfileService.updateDataPersonMain
          .get("municipality")
          .setValue(null);
        this.updateProfileService.updateDataPersonMain
          .get("vereda")
          .setValue(null);

        this.getCitiesByState("1", selectedValue);
      }
    }
  }

  getCitiesByState(formType: string, state: number) {
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiGet(state, SERVICES.UBICACION_LIST_CIUDADDEESTADO_SERV)
      .then(city => {
        this.ServicesProvider.closeLoader();;
        this.clearTimeOut();
        if (city["status"] == 404 || city["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (state["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(city["_body"]);
          if (formType == "0") {
            this.updateProfileService.citiesPersonalInformation = respuesta;
            this.setFilteredItems("0");
            this.searchControl.valueChanges
              .debounceTime(700)
              .subscribe(search => {
                this.searching = false;
                this.setFilteredItems("0");
              });
          } else {
            this.updateProfileService.citiesPersonalInformationMain = respuesta;
            this.setFilteredItems("1");
            this.searchControlP.valueChanges
              .debounceTime(700)
              .subscribe(search => {
                this.searchingP = false;
                this.setFilteredItems("1");
              });
          }
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.timeOutControl = false;
        this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  updateCity(formType: string, selectedValue: any) {
    if (formType == "0") {
      if (this.cities.length > 0) {
        let city: any = this.cities.filter(function (item: any) {
          return item.id_pk === Number(selectedValue);
        })[0];

        //this.person.vereda = null;
        this.veredas = [];
        this.updateProfileService.updateDataPerson.get("vereda").setValue(null);
        this.indicativoPhone = city.indicativo;
        this.serviceUpdateCity("0", city);
      }
    } else {
      if (this.citiesP.length > 0) {
        let city: any = this.citiesP.filter(function (item: any) {
          return item.id_pk === Number(selectedValue);
        })[0];
        this.veredasP = [];
        this.updateProfileService.updateDataPersonMain
          .get("vereda")
          .setValue(null);
        this.serviceUpdateCity("1", city);
      }
    }
  }

  serviceUpdateCity(formType: string, city: any) {
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiGet(city.id_pk, SERVICES.UBICACION_LIST_VEREDADECIUDAD_SERV)
      .then(city => {
        this.ServicesProvider.closeLoader();;
        this.clearTimeOut();
        if (city["status"] == 404 || city["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (city["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(city["_body"]);
          formType == "0"
            ? (this.veredas = respuesta)
            : (this.veredasP = respuesta);
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.timeOutControl = false;
        this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  // Sumar 48 horas a la fecha de creación de la cuenta
  //dd/mm/aaaa y dd-mm-aaaa. Admite como separador ‘/’ y ‘-‘.
  date_after_created_at(d: any, _fecha: any) {
    let Fecha = new Date();
    let sFecha =
      _fecha ||
      Fecha.getDate() +
      "/" +
      (Fecha.getMonth() + 1) +
      "/" +
      Fecha.getFullYear();
    let sep = sFecha.indexOf("/") != -1 ? "/" : "-";
    let aFecha = sFecha.split(sep);
    let fecha: any = aFecha[2] + "/" + aFecha[1] + "/" + aFecha[0];
    fecha = new Date(fecha);
    fecha.setDate(fecha.getDate() + parseInt(d));
    let anno = fecha.getFullYear();
    let mes = fecha.getMonth() + 1;
    let dia = fecha.getDate();
    mes = mes < 10 ? "0" + mes : mes;
    dia = dia < 10 ? "0" + dia : dia;
    let fechaFinal = anno + "/" + mes + "/" + dia;
    return fechaFinal;
  }

  getPatient(id_paciente: number) {
    let params = { "id_paciente": id_paciente };
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiPost(params, SERVICES.CONSULTAR_PACIENTE_API_SERV)
      .then(state => {
        this.clearTimeOut();
        this.ServicesProvider.closeLoader();;
        if (state["status"] == 404 || state["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (state["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(state["_body"]);
          this.updateProfileService.loadProgress = respuesta.porcentaje_avance;
          this.updateProfileService.infoPac = respuesta;
          this.updateProfileService.avatar_profile =
            respuesta.get_user[0].foto + "?1=" + new Date();
          this.patientProviderService.avatar =
            respuesta.get_user[0].foto + "?1=" + new Date();
          this.patientData = respuesta;

          // Le sumamos dos días a la fecha de creacion de la cuenta,
          // para verificar si puede actualizar los datos personales
          let created_at = respuesta.created_at.split(" ")[0].split("-");
          let created_at_format =
            created_at[2] + "-" + created_at[1] + "-" + created_at[0];
          let fechaAfterCreateAccount = this.date_after_created_at(
            2,
            created_at_format
          );
          let dateToday = new Date()
            .toJSON()
            .slice(0, 10)
            .replace(/-/g, "/");

          if (
            new Date(fechaAfterCreateAccount).getTime() >=
            new Date(dateToday).getTime()
          ) {
            this.isPersonalData = true;
            this.updateProfileService.isVisibleIonToggle = true;
          }
          else {
            this.updateProfileService.updateDataPersonMain.controls['type_document'].disable();
            this.updateProfileService.updateDataPersonMain.controls['document'].disable();
          }

          this.isPersonalData
            ? this.loadDataPatientDataPerson(this.patientData)
            : this.loadDatapatient(this.patientData);
        }
      })
      .catch(err => {
        this.ServicesProvider.closeLoader();
        //this.updateProfileService.loading.onDidDismiss(() => {
        this.clearTimeOut();
        this.timeOutControl = false;
        this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  // Para cargar los datos personales antes de las 6 horas
  loadDataPatientDataPerson(rptPatient: any) {
    //this.isLoadForm1 = true;
    this.ServicesProvider.createLoader();
    Promise.all([
      this.ServicesProvider.apiGet("", SERVICES.UBICACION_LIST_PAIS_SERV),
      this.ServicesProvider.apiGet(
        rptPatient.pais_fk_nac ? rptPatient.pais_fk_nac : rptPatient.pais_fk_res,
        SERVICES.UBICACION_LIST_ESTADODEPAIS_SERV
      ),
      this.ServicesProvider.apiGet(
        rptPatient.est_fk_nac ? rptPatient.est_fk_nac : rptPatient.est_fk_res,
        SERVICES.UBICACION_LIST_CIUDADDEESTADO_SERV
      ),
      this.ServicesProvider.apiGet(
        rptPatient.cda_fk_nac ? rptPatient.cda_fk_nac : rptPatient.cda_fk_res,
        SERVICES.UBICACION_LIST_VEREDADECIUDAD_SERV
      ),

      this.ServicesProvider.apiGet(
        rptPatient.pais_fk_res,  //////////////////////////////////////////////
        SERVICES.TIPODOCUMENTO_GETDEPAIS_SERV
      )
    ])
      .then(values => {
        this.ServicesProvider.closeLoader();;
        this.clearTimeOut();
        if (values[0]["status"] == 404 || values[0]["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (values[0]["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(values[0]["_body"]);
          this.countrys = respuesta;

          let user = (JSON.parse(localStorage.getItem("user")).get_pais_res);
          this.setRulesPhone(user.mask_celular, user.mask_fijo);

        }

        if (values[1]["status"] == 404 || values[1]["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (values[1]["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(values[1]["_body"]);
          this.statesP = respuesta;
        }

        if (values[2]["status"] == 404 || values[2]["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (values[2]["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(values[2]["_body"]);
          //this.cities = respuesta;
          /*
                    this.searchTermCityP = rptPatient.get_ciudad_nac?rptPatient.get_ciudad_nac.nombre:rptPatient.get_ciudad_res.nombre;
          this.citySelectedP = rptPatient.get_ciudad_nac?rptPatient.get_ciudad_nac.id_pk:rptPatient.get_ciudad_res.id_pk;
          */
          if (rptPatient.get_ciudad_nac) {
            this.searchTermCityP = rptPatient.get_ciudad_nac.nombre;
            this.citySelectedP = rptPatient.get_ciudad_nac.id_pk;
            this.updateProfileService.citiesPersonalInformationMain = respuesta;
            this.setFilteredItems("1");
            this.searchControlP.valueChanges
              .debounceTime(700)
              .subscribe(search => {
                this.searchingP = false;
                this.setFilteredItems("1");
              });
          }

        }

        if (values[3]["status"] == 404 || values[3]["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (values[3]["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(values[3]["_body"]);
          this.veredasP = respuesta;
        }

        if (values[4]["status"] == 404 || values[4]["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (values[4]["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let rptTypesDocument = JSON.parse(values[4]["_body"]);
          this.typesDocument = rptTypesDocument;
        }

        this.getDataPatient(rptPatient);
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.timeOutControl = false;
        this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  loadDatapatient(rptPatient: any) {
    //this.isLoadForm2 = true;
    this.ServicesProvider.createLoader();
    Promise.all([
      this.ServicesProvider.apiGet("", SERVICES.UBICACION_LIST_PAIS_SERV),
      this.ServicesProvider.apiGet(
        rptPatient.pais_fk_res,
        SERVICES.UBICACION_LIST_ESTADODEPAIS_SERV
      ),
      this.ServicesProvider.apiGet(
        rptPatient.est_fk_res,
        SERVICES.UBICACION_LIST_CIUDADDEESTADO_SERV
      ),
      this.ServicesProvider.apiGet(
        rptPatient.cda_fk_res,
        SERVICES.UBICACION_LIST_VEREDADECIUDAD_SERV
      )
    ])
      .then(values => {
        this.ServicesProvider.closeLoader();;
        this.clearTimeOut();
        if (values[0]["status"] == 404 || values[0]["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (values[0]["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(values[0]["_body"]);
          this.countrys = respuesta;
          let user = (JSON.parse(localStorage.getItem("user")).get_pais_res);
          this.setRulesPhone(user.mask_celular, user.mask_fijo);

        }

        if (values[1]["status"] == 404 || values[1]["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (values[1]["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(values[1]["_body"]);
          this.states = respuesta;
        }

        if (values[2]["status"] == 404 || values[2]["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (values[2]["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(values[2]["_body"]);
          //this.cities = respuesta;
          this.searchTermCity = rptPatient.get_ciudad_res.nombre;
          this.citySelected = rptPatient.get_ciudad_res.id_pk;
          this.updateProfileService.citiesPersonalInformation = respuesta;
          this.setFilteredItems("0");
          this.searchControl.valueChanges
            .debounceTime(700)
            .subscribe(search => {
              this.searching = false;
              this.setFilteredItems("0");
            });
        }

        if (values[3]["status"] == 404 || values[3]["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (values[3]["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(values[3]["_body"]);
          this.veredas = respuesta;

        }

        // No se cargan estos datos porque ya se cargador los datos personales de contacto
        // Solo se cargan cunado la variable sea igual a false, osea no se cumple
        // la condicion para que se carguen los datos personales
        !this.isPersonalData ? this.getDataPatient(rptPatient) : '';

      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();;

        this.clearTimeOut();
        this.timeOutControl = false;
        this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  getDataPatient(rptPatient: any) {
    // Datos personales
    this.updateProfileService.updateDataPersonMain
      .get("name")
      .setValue(rptPatient.nombres);
    this.updateProfileService.updateDataPersonMain
      .get("surname")
      .setValue(rptPatient.apellido1);
    this.updateProfileService.updateDataPersonMain
      .get("second_surname")
      .setValue(rptPatient.apellido2);

    this.updateProfileService.updateDataPersonMain
      .get("gender")
      .setValue(rptPatient.genero);


    this.updateProfileService.updateDataPersonMain
      .get("country")
      .setValue(rptPatient.pais_fk_nac);
    this.updateProfileService.updateDataPersonMain
      .get("department")
      .setValue(rptPatient.est_fk_nac);
    this.updateProfileService.updateDataPersonMain
      .get("municipality")
      .setValue(rptPatient.cda_fk_nac);
    this.updateProfileService.updateDataPersonMain
      .get("birthdate")
      .setValue(rptPatient.fecha_nac);
    this.updateProfileService.updateDataPersonMain
      .get("type_document")
      .setValue(rptPatient.get_tipo_documento.id_pk);
    this.updateProfileService.updateDataPersonMain
      .get("document")
      .setValue(rptPatient.doc_iden_pk);
    // End Datos personales
    this.updateProfileService.updateDataPerson
      .get("email")
      .setValue(rptPatient.get_user[0].email);
    this.updateProfileService.updateDataPerson
      .get("country")
      .setValue(rptPatient.pais_fk_res);
    this.updateProfileService.updateDataPerson
      .get("department")
      .setValue(rptPatient.est_fk_res);
    this.updateProfileService.updateDataPerson
      .get("municipality")
      .setValue(rptPatient.cda_fk_res);
    if (rptPatient.get_vereda_res != null) {
      this.updateProfileService.updateDataPerson
        .get("vereda")
        .setValue(rptPatient.get_vereda_res.id_pk);
    }
    this.updateProfileService.updateDataPerson
      .get("cellphone")
      .setValue(rptPatient.tlf_contacto != null ? rptPatient.tlf_contacto : "");
    this.updateProfileService.updateDataPerson
      .get("phone")
      .setValue(
        rptPatient.tlf_domicilio != null ? rptPatient.tlf_domicilio : ""
      );
    this.updateProfileService.updateDataPerson
      .get("address")
      .setValue(rptPatient.direccion != null ? rptPatient.direccion : "");

    this.indicativoCellphone = rptPatient.get_pais_res.indicativo;
    this.indicativoPhone = rptPatient.get_ciudad_res.indicativo;

    // Cargar los otros datos de contacto personal
    this.isPersonalData ? this.loadDatapatient(this.patientData) : '';

  }

  cancelEditProfile() {
    //this.navCtrl.popToRoot();
    this.appCtrl.getRootNav().setRoot(HomePage);
  }

  onSearchInput(formType: string, event: any) {
    this.hidenList = true;
    if (formType == "0") {
      this.updateProfileService.updateDataPerson
        .get("municipality")
        .setValue(null);
      this.indicativoPhone = "";
      this.citySelected = 0;
      this.veredas = [];
      this.updateProfileService.updateDataPerson.get("vereda").setValue(null);
      this.searching = true;
    } else {
      this.updateProfileService.updateDataPersonMain
        .get("municipality")
        .setValue(null);
      this.citySelectedP = 0;
      this.veredasP = [];
      this.updateProfileService.updateDataPersonMain
        .get("vereda")
        .setValue(null);
      this.searchingP = true;
    }
  }

  setFilteredItems(formType: string) {
    if (formType == "0") {
      this.cities = this.updateProfileService.filterItems(
        this.searchTermCity,
        this.updateProfileService.citiesPersonalInformation
      );
    } else {
      this.citiesP = this.updateProfileService.filterItems(
        this.searchTermCityP,
        this.updateProfileService.citiesPersonalInformationMain
      );
    }
  }

  filterCitySelected(formType: string, city: any) {
    if (formType == "0") {
      this.updateProfileService.updateDataPerson
        .get("municipality")
        .setValue(city.id_pk);
      this.citySelected = city.id_pk;
      this.updateCity("0", city.id_pk);
      this.searchTermCity = city.nombre;
    } else {
      this.updateProfileService.updateDataPersonMain
        .get("municipality")
        .setValue(city.id_pk);
      this.citySelectedP = city.id_pk;
      //this.updateCity("1", city.id_pk);
      this.searchTermCityP = city.nombre;
    }

    this.hidenList = false;
  }

  loadNew() {
    this.timeOutControl = true;
    this.getPatient(this.patientProviderService.getCurrentUser().id_pk);
  }

  saveData() {
    let params = {};
    let personal_info = this.updateProfileService.updateDataPerson.value;
    let rptPatient: any = this.updateProfileService.infoPac;

    params["pac_fk"] = rptPatient.id_pk;
    params["email"] = personal_info.email;
    params["telefonodomicilio"] = personal_info.phone;
    params["celular"] = personal_info.cellphone;
    params["direccion"] = personal_info.address;
    params["pais"] = personal_info.country;
    params["estado"] = personal_info.department;
    params["ciudad"] = personal_info.municipality;
    personal_info.vereda ? params["vereda"] = personal_info.vereda : "";

    if (this.updateProfileService.isVisibleIonToggle) {
      let personalInformationMain = this.updateProfileService
        .updateDataPersonMain.value;

      if (personalInformationMain) {
        if (personalInformationMain.name) {
          console.log('personalInformationMain.birthdate: ', personalInformationMain.birthdate);
          console.log('personalInformationMain.birthdate: dd/MM/yyyy', this.datepipe.transform(personalInformationMain.birthdate, 'dd/MM/yyyy'));
          params["nombres"] = personalInformationMain.name;
          params["apellido1"] = personalInformationMain.surname;
          params["apellido2"] = personalInformationMain.second_surname;
          params["tipo_documento"] = personalInformationMain.type_document;
          params["doc_ident"] = personalInformationMain.document;
          params["fecha_nacimiento"] = this.datepipe.transform(personalInformationMain.birthdate, 'dd/MM/yyyy');
          params["pais_nac"] = personalInformationMain.country;
          params["estado_nac"] = personalInformationMain.department;
          params["ciudad_nac"] = personalInformationMain.municipality;
          params["genero"] = personalInformationMain.gender;
        }
      }
    }

    //SE CONTIENE EN UN JSON CON LOS PARAMETROS PERTINENTES A ENVIAR PARA LA PESTAÑA
    console.log('PARAMETROS\n' + JSON.stringify(params));
    //

    this.ServicesProvider.createLoader();
    this.ServicesProvider.apiPost(params, '/editar_datos_de_contacto_api')
      .then(success => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (success["status"] == 404 || success["status"] == 500) {
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (success["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
        } else {
          let respuesta = JSON.parse(success["_body"]);
          console.log(respuesta);
          if(respuesta.success){
            this.ServicesProvider.toast(MESSAGES.updateProfile.update.success, "successToast");
          }
          else if (respuesta.errors != undefined){
            console.log(Object.keys(respuesta.errors));
            this.ServicesProvider.toast(MESSAGES.updateProfile.update.error);
          }
        }
      })
      .catch(error => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.ServicesProvider.toast(MESSAGES.services.error);
        console.log("Error, Update personal information: \n" + error);
      });
  }

}
