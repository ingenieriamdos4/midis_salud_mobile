import { Component } from "@angular/core";
import { NavController, NavParams, Platform } from "ionic-angular";
import { FileOpener } from "@ionic-native/file-opener";
import { File } from "@ionic-native/file";
import { PatientProviderService } from "../../../providers/patient/patient";
import { ServicesProvider } from "../../../providers/services/services";
import { SocialSharing } from "@ionic-native/social-sharing";
import { PageHelpPage } from "../../page-help/page-help"
/* Mensajes de la app */
import { MESSAGES } from "../../../config/messages";
import { SERVICES } from "../../../config/url.servicios";
import { ViewController } from "ionic-angular";
declare var cordova: any;
@Component({
  selector: "page-popover-midis",
  templateUrl: "popover-midis.html"
})
export class PopoverMidisPage {
  url: string;
  Message: any;
  timeOut: any;
  timeOutControl: boolean = true;
  storageDirectory: string = "";
  isFuncActive: boolean = true;
  isDownloaded: boolean = false;
  namePdf: string;
  imgHelp: string;
  pageHelp = PageHelpPage;
  isIos: boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public patientProviderService: PatientProviderService,
    public ServicesProvider: ServicesProvider,
    private socialSharing: SocialSharing,
    private fileOpener: FileOpener,
    private file: File,
    public viewCtrl: ViewController,
    public platform: Platform
  ) {
    this.platform.ready().then(() => {
      // make sure this is on a device, not an emulation (e.g. chrome tools device mode)
      if (!this.platform.is("cordova")) {
        return false;
      }
      if (this.platform.is("ios")) {
        this.storageDirectory = cordova.file.documentsDirectory;
        this.isIos = true;
      } else if (this.platform.is("android")) {
        this.storageDirectory = cordova.file.externalRootDirectory;
      } else {
        // exit otherwise, but you could add further types here e.g. Windows
        return false;
      }
    });

    this.imgHelp = this.navParams.get('typePage');
  }

  ionViewDidLoad() {
    let user = JSON.parse(localStorage.getItem("user"));
    this.namePdf = "fichaMedica" + user.nombres + ".pdf";
  }

  clearTimeOut() {
    clearTimeout(this.timeOut);
  }
  close() {
    this.viewCtrl.dismiss();    
  }

  click(action: any){
    this.viewCtrl.dismiss(action);
  }

}
