import { Component } from "@angular/core";

import {
  IndexUpdateProfilePage,
  DatosSaludPage,
  ViewDocumentsPage,
  PageHelpPage,
  SettingsPage

} from "../../index.paginas";
import { IndexAntecedentPage } from "../antecedent/index.paginas";
/* Importing providers */
import { PatientProviderService } from "../../../providers/patient/patient";
import { ServicesProvider } from "../../../providers/services/services";
import { NavController, NavParams, AlertController } from 'ionic-angular';

/* App messages */
import { MESSAGES } from "../../../config/messages";
import { SERVICES } from "../../../config/url.servicios";

import { PrescDetailPage } from '../presc-detail/presc-detail';

@Component({
  selector: "page-notifications",
  templateUrl: "notifications.html"
})
export class NotificationsPage {

  prescPage = PrescDetailPage;

  indexUpdateProfile = IndexUpdateProfilePage;
  indexAntecedent = IndexAntecedentPage;
  datosSalud = DatosSaludPage;
  viewDocuments = ViewDocumentsPage;
  info_institution_vinculacion: any;
  info_institution_almacenamiento: any;
  porcentaje_perfil: string = "assets/imgs/0.png";
  pageHelp = PageHelpPage;
  settings = SettingsPage;

  presc_cargadas: any;
  recomendaciones: any;
  preparaciones: any;
  cuidados: any;

  constructor(
    public patientProviderService: PatientProviderService,
    public ServicesProvider: ServicesProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController
    ) {
    let id_paciente = this.patientProviderService.getCurrentUser().id_pk;
    this.notificationsInstitutions(id_paciente);
    this.getLevelProfile(id_paciente);
    this.getDataNotifications(id_paciente);
  }

  getColorModulo(id: any) {
    switch (id) {
      case 1:
        return '74BF44';
      case 2:
        return '8D2832';
      case 3:
        return 'F7BE32';
      case 4:
        return '8E44AD';
      case 5:
        return '6BB9F0';
      case 6:
        return '0A3043';
      case 7:
        return '808285';
      case 8:
        return 'D91E18';
    }
  }

  alertBasicos(){
    let alert = this.alertCtrl.create({
      title: "Midis App Salud",
      message: "<p>Dentro de la aplicación existen cuatro módulos principales:</p> <ul> <li>Actualizar perfil.</li> <li>Antecedentes.</li> <li>Configuración.</li> <li>Mis documentos.</li> </ul> <p>Completalos y así tu cuenta Midis podrá ayudar a salvar tu vida.</p>",
      buttons: [
          {
              text: 'Aceptar',
              role: 'confirm'
          }
      ]
  });
  alert.present();
  }

  getDetailPrescription(con_fk: any, id_pk: any) {
    let params = { "con_fk": con_fk };
    let prescriptions: any;
    let consult: any;

    this.ServicesProvider.createLoader();
    this.ServicesProvider.apiPost(params, SERVICES.MIDIS_DETAILS)
      .then(midisDataDetails => {
        this.ServicesProvider.closeLoader();
        if (midisDataDetails["status"] == 404 || midisDataDetails["status"] == 500) {
          this.ServicesProvider.toast(MESSAGES.services.error);
        }
        else if (midisDataDetails["status"] == 401) {
          this.ServicesProvider.toast(MESSAGES.services.unauthorized);
        }
        else {
          let response = JSON.parse(midisDataDetails["_body"]);
          prescriptions = response.prescripciones;
          consult = response.consulta;
          prescriptions.forEach(presc => {
            if (presc.id_pk == id_pk) {
              this.pushPrescDetail(presc, consult);
              return;
            }
          });
        }
      })
      .catch(error => {
        this.ServicesProvider.closeLoader();
        this.ServicesProvider.toast(MESSAGES.services.error);
      })
  }

  pushPrescDetail(presc: any, consult: any) {
    let logo = "";
    let nombre = "";
    if (consult.get_sede_institucion) {
      logo = consult.get_sede_institucion.get_institucion.logo;
      nombre = consult.get_sede_institucion.nombre;
    }
    this.navCtrl.push(this.prescPage, {
      elemento: presc,
      profesional: this.getProfesion(consult),
      diagnostico: this.getDiagnosticoPresuntivo(consult),
      tipoEvento: consult.get_tipo_consulta.nombre,
      motivo: consult.motivo,
      profResultados: this.getProfResultados(presc),
      cargaResultados: this.getCargaResultados(presc),
      logoInst: logo,
      nombreInst: nombre
    });
  }

  getProfResultados(element: any) {
    if (element.get_resultados.length > 0) {
      if (element.get_resultados[0].pro_ext && element.get_resultados[0].pro_ext != null) {
        return element.get_resultados[0].pro_ext;
      }

      if (element.get_resultados[0].get_profesional && element.get_resultados[0].get_profesional != null) {
        let response =
          element.get_resultados[0].get_profesional.nombre;
        if (element.get_resultados[0].get_profesional.apellido1) {
          response += " " + element.get_resultados[0].get_profesional.apellido1;
        }
        if (element.get_resultados[0].get_profesional.apellido2) {
          response += " " + element.get_resultados[0].get_profesional.apellido2;
        }
        return response;
      }
      if (element.get_resultados[0].ins_ext && element.get_resultados[0].ins_ext != null) {
        return "Nombre no encontrado";
      }
    }
    return "Error obteniendo la información";
  }

  getCargaResultados(element: any) {
    if (element.get_resultados.length > 0) {
      if (element.get_resultados[0].carga_pac_fk != null) {
        return "Paciente";
      }
      if (element.get_resultados[0].carga_pro_fk != null) {
        return "Profesional";
      }
      return "Usuario no encontrado";
    }
    return "Error obteniendo la información";
  }

  getDiagnosticoPresuntivo(consult: any) {
    let response = "";
    if (consult) {
      let diagnostico = consult.get_diagnostico;
      for (let i = 0; i < diagnostico.length; i++) {
        response += diagnostico[i].nombre;
        if (i + 1 < diagnostico.length) {
          response += "\n";
        }
      }
    }
    return response;
  }

  getProfesion(consult: any) {
    if (consult) {
      let profesional = consult.get_profesional;
      let response =
        profesional.nombre;
      if (profesional.apellido1) {
        response += " " + profesional.apellido1;
      }
      if (profesional.apellido2) {
        response += " " + profesional.apellido2;
      }
      response += " / ";

      if (profesional.get_especialidad.length > 0) {
        for (let i = 0; i < profesional.get_especialidad.length; i++) {
          response += profesional.get_especialidad[i].nombre;
          if (i + 1 < profesional.get_especialidad.length) {
            response += ", ";
          }
        }
      }
      else {
        for (let i = 0; i < profesional.get_subtipo_profesionales.length; i++) {
          response += profesional.get_subtipo_profesionales[i].nombre;
          if (i + 1 < profesional.get_subtipo_profesionales.length) {
            response += ", ";
          }
        }
      }
      return response;
    }
    return "";
  }

  getDataNotifications(pac_fk: any) {
    let params = { "pac_fk": pac_fk };
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiPost(params, SERVICES.NOTIFICATIONS_SERV)
      .then(data => {
        this.ServicesProvider.closeLoader();
        if (data["status"] == 404 || data["status"] == 500) {
          this.ServicesProvider.toast(MESSAGES.services.error);
          console.log("getDataNotifications ERROR");
        }
        else if (data["status"] == 401) {
          this.ServicesProvider.toast(MESSAGES.services.unauthorized);
        }
        else {
          let respuesta = JSON.parse(data["_body"]);
          if (respuesta.prescripciones_cargadas.length > 0) {
            this.presc_cargadas = respuesta.prescripciones_cargadas;
            for (let i = 0; i < this.presc_cargadas.length; i++) {
              if (this.presc_cargadas[i].tpc_fk == 2) { //SE CAPTURAN DOCUMENTOS DE SALUD
                let split = "";
                if (this.presc_cargadas[i].descripcion.includes('Dias: ')) {
                  split = this.presc_cargadas[i].descripcion.split('Dias: ')[1].split(' Fecha de Inicio: ');
                  this.presc_cargadas[i]["dias"] = split[0];
                  let split2 = split[1].split('Fecha de Inicio: ')[0].split(' Fecha de Finalización: ');
                  this.presc_cargadas[i]["fecha_de_inicio"] = split2[0];                  
                  this.presc_cargadas[i]["fecha_de_finalizacion"] = split2[1];        
                  this.presc_cargadas[i]["descripcion"] = "MidisAppSalud_HaveDetails_Inc";          
                }
                else if (this.presc_cargadas[i].descripcion.includes('Fecha: ')) {
                  split = this.presc_cargadas[i].descripcion.split('Fecha: ')[1].split(' Hora Inicio: ');
                  this.presc_cargadas[i]["fecha_cons"] = split[0];
                  let split2 = split[1].split('Hora Inicio: ')[0].split(' Hora Fin: ');
                  this.presc_cargadas[i]["hora_de_inicio"] = split2[0];                  
                  this.presc_cargadas[i]["hora_de_finalizacion"] = split2[1];        
                  this.presc_cargadas[i]["descripcion"] = "MidisAppSalud_HaveDetails_Cons"; 
                }
              }
            }
          }
          if (respuesta.recomendaciones.length > 0) {
            this.recomendaciones = respuesta.recomendaciones;
          }
          if (respuesta.preparaciones.length > 0) {
            this.preparaciones = respuesta.preparaciones;
          }
          if (respuesta.ciudados.length > 0) {
            this.cuidados = respuesta.ciudados;
          }
        }
      })
      .catch(error => {
        this.ServicesProvider.closeLoader();
        //this.ServicesProvider.toast(MESSAGES.services.error + error);
      });
  }

  ionViewDidEnter() {
    /*if (localStorage.getItem("wizzard_notificacion") == undefined) {
      localStorage.setItem("wizzard_notificacion", "true");
      this.navCtrl.push(this.pageHelp, { 'seccion': 'notificacion' });
    }*/
  }
  notificationsInstitutions(id_paciente: any) {
    let data = { "pac_fk": id_paciente };
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiPost(data, SERVICES.VINCULACION_INST_PAC_SERV)
      .then(state => {
        this.ServicesProvider.closeLoader();
        if (state["status"] == 404 || state["status"] == 500) {
          this.ServicesProvider.toast(MESSAGES.services.error);
          console.log("noticationsInstitutions ERROR")
        }
        else if (state["status"] == 401) {
          this.ServicesProvider.toast(MESSAGES.services.unauthorized);
        }
        else {
          let respuesta = JSON.parse(state["_body"]);
          respuesta.forEach(element => {
            if (element.lic_capacidad_inicial == "" && element.lic_fecha_adquirida == "" && element.lic_fecha_caducidad == "") {
              this.info_institution_vinculacion.push(element);
            }
            else {
              this.info_institution_almacenamiento.push(element);
            }
          });

        }
      })
      .catch(err => {
        console.log(err);
      });
    //this.ServicesProvider.loading.dismiss();
    this.ServicesProvider.closeLoader();
  }

  getLevelProfile(id_paciente: any) {
    let data = { "id_paciente": id_paciente };
    this.ServicesProvider
      .apiPost(data, SERVICES.CONSULTAR_PACIENTE_API_SERV)
      .then(state => {
        if (state["status"] == 404 || state["status"] == 500) {
        } else if (state["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
        } else {
          let respuesta = JSON.parse(state["_body"]);
          this.porcentaje_perfil = respuesta.imagen_porcentaje;
        }
      })
      .catch(err => {
        console.log(err);
      });
  }
}
