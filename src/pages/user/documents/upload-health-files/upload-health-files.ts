import { Component, ElementRef } from "@angular/core";
import { NavController, NavParams, ModalController, AlertController } from "ionic-angular";

/*Plugin*/
import { ImagePicker, ImagePickerOptions } from "@ionic-native/image-picker";

import { DocumentsProviderService } from "../../../../providers/documents/documents";

import { Validators, FormBuilder, FormGroup } from "@angular/forms";
import { PatientProviderService } from "../../../../providers/patient/patient";
import { ServicesProvider } from "../../../../providers/services/services";


/*Plugins*/
import { Camera, CameraOptions } from "@ionic-native/camera";
/* Mensajes de la app */
import { MESSAGES } from "../../../../config/messages";
import { SERVICES } from "../../../../config/url.servicios";

import { CropImagePage } from "../../../crop-image/crop-image";

@Component({
  selector: "page-upload-health-files",
  templateUrl: "upload-health-files.html"
})
export class UploadHealthFilesPage {
  categories: Array<string> = [];
  treehealthFile: Array<string> = [];
  formUploadHealth: FormGroup;
  file: string;
  timeOut: any;
  timeOutControl: boolean = true;
  hasFile: boolean = false;
  base64Image: any;

  module: any;
  mensaje: any;

  so_flag: boolean = true;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public documentsProviderService: DocumentsProviderService,
    private formBuilder: FormBuilder,
    public patientProviderService: PatientProviderService,
    public ServicesProvider: ServicesProvider,
    private imagePicker: ImagePicker,
    private camera: Camera,
    private elem: ElementRef
  ) {
    this.so_flag = this.ServicesProvider.validate_so_v();

    this.module = this.navParams.get('module');

    this.formUploadHealth = this.formBuilder.group({
      category: [null, Validators.compose([])],
      date_file: [null, Validators.compose([Validators.required])],
      institution: [null, Validators.compose([Validators.required])],
      profesional_health: [null, Validators.compose([])],
      observation: [null, Validators.compose([Validators.required])],
      privateHealth: ["", Validators.compose([])]
    });

    this.formUploadHealth.get("privateHealth").setValue("0");

    this.getCategoriesByModule(this.module);

  }

  clearTimeOut() {
    clearTimeout(this.timeOut);
  }

  getCategoriesByModule(id_pk: number) {
    let data = id_pk;
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiPost("", SERVICES.CATEGORIAS_MODULO_SERV + data)
      .then(file => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (file["status"] == 404 || file["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (file["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(file["_body"]);
          this.categories = respuesta.modulosHijos;
          this.mensaje = respuesta.mensaje;
        }
      })
      .catch(err => {
        this.ServicesProvider.closeLoader();
        this.timeOutControl = false;
        this.clearTimeOut();
        this.ServicesProvider.toast(MESSAGES.services.error);
      });
  }

  logForm() {
    let id_pk = this.patientProviderService.getCurrentUser().id_pk;
    let formData = new FormData();
    formData.append("pac_fk", id_pk);
    formData.append("mod_fk_padre", this.module);

    if (this.formUploadHealth.value.category == null ||
      this.formUploadHealth.value.category == undefined) {
      this.formUploadHealth.value.category = 0;
    }

    if (this.formUploadHealth.value.category != "") {
      formData.append("mod_fk_hijo", this.formUploadHealth.value.category);
    }

    formData.append("observacion", this.formUploadHealth.value.observation);
    formData.append("institucion", this.formUploadHealth.value.institution);
    formData.append(
      "profesional_salud",
      this.formUploadHealth.value.profesional_health
    );
    formData.append(
      "fecha",
      this.documentsProviderService.formatDate(
        this.formUploadHealth.value.date_file
      )
    );
    formData.append("privado", this.formUploadHealth.value.privateHealth);

    if (this.base64Image) {
      formData.append("array_fotos_archivos_salud[]", this.base64Image);
    }
    else {
      if (this.documentsProviderService.imagesBase64.length > 0) {
        this.documentsProviderService.imagesBase64.forEach(base64 => {
          formData.append("array_fotos_archivos_salud[]", base64);
        });
      }
    }

    for (let index = 0; index < this.documentsProviderService.arrayFiles.length; index++) {
      formData.append("archivo_adjunto[]", this.documentsProviderService.arrayFiles[index].file, this.documentsProviderService.arrayFiles[index].nameFile);
    }
    if (this.hasFile == false) {
      this.ServicesProvider.toast("Debe cargar un archivo");
      return;
    }
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiPost(formData, SERVICES.GUARDAR_ARCHIVOS_SALUD_API_SERV)
      .then(file => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (file["status"] == 404 || file["status"] == 500) {
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (file["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
        } else {
          let respuesta = JSON.parse(file["_body"]);

          if (respuesta.success == true) {
            this.ServicesProvider.toast(MESSAGES.fileHealtData.create.success, "successToast");
            this.documentsProviderService.imagesBase64 = [];
            this.base64Image = undefined;
            this.documentsProviderService.arrayFiles = [];
            this.navCtrl.pop();
          }
          else if (respuesta.success == false) {
            this.ServicesProvider.toast(
              MESSAGES.fileHealtData.create.error
            );
          }
          else if (respuesta.errors != undefined) {
            let i = 0;
            for (i; i < Object.keys(respuesta.errors).length; i++) {
              let campo = Object.keys(respuesta.errors)[i];
              let error = (<any>Object).values(respuesta.errors)[i];
              this.ServicesProvider.toast(campo + ": " + error);
            }
          }
          else if (respuesta.success == "sin_espacio") {
            this.ServicesProvider.toast(respuesta.mensaje);
          }
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  loadImages() {
    let options: ImagePickerOptions = {
      maximumImagesCount: 5,
      quality: 85,
      width: 510,
      height: 510,
      outputType: 1 // para que sea una imagen en base64, 0 fisico dispositivo
    };

    this.imagePicker
      .getPictures(options)
      .then(
        results => {
          for (var i = 0; i < results.length; i++) {
            this.documentsProviderService.imagesBase64.push(
              "data:image/png;base64," + results[i]
            );
          }
          this.hasFile = true;
        },
        err => {
          this.ServicesProvider.toast(
            MESSAGES.fileHealtData.upload.error
          );
        }
      )
      .catch(err => {
        this.ServicesProvider.toast(err);
      });
  }

  cancelLoadImages() {
    this.documentsProviderService.imagesBase64 = [];
    this.base64Image = undefined;
    this.hasFile = false;
    console.log(this.hasFile);
  }

  onUploadChange(ev) {
    let extErrorMessage =
      "Solo se permiten archivos con extensión: .jpg, .jpeg, .gif, .png, .dcm, .doc, docx, o pdf";
    let allowedExtension = [
      "jpg",
      "jpeg",
      "gif",
      "png",
      "dicom",
      "pdf",
      "dcm",
      "doc",
      "docx"
    ];
    let extError = false;
    let myFiles = ev.target.files[0];

    if (myFiles != undefined) {
      let extName = myFiles["name"]
        .split(".")
        .pop()
        .toLowerCase();

      if (allowedExtension.indexOf(extName) == -1) {
        extError = true;
      }

      if (extError) {
        this.ServicesProvider.toast(extErrorMessage);
      } else {
        this.documentsProviderService.arrayFiles.push({
          nameFile: myFiles["name"],
          file: myFiles
        });
        this.elem.nativeElement.querySelector("#fileUpload").value = "";
      }
    }
  }

  deleteFile(index: number) {
    this.documentsProviderService.arrayFiles.splice(index, 1);
    this.hasFile = false;
    console.log(this.hasFile);
  }

  goCropEditor(image: any, aspect: any) {

    let cropAlert = this.alertCtrl.create({
      title: "Midis App Salud",
      message: "¿Desea cortar o editar esta foto?.\nPulse no para utilizar la original.",
      buttons: [
        {
          text: "Sí",
          handler: () => {
            let cropModal = this.modalCtrl.create(CropImagePage, { "image": image, "aspect": aspect });
            cropModal.onDidDismiss(data => {
              if (data.croppedImage != null) {
                this.base64Image = data.croppedImage;
              }
            });
            cropModal.present();
          }
        },
        {
          text: "No",
          role: "cancel",
          handler: () => {
            this.base64Image = image;
          }
        }
      ]
    });
    cropAlert.present();

    /*
    let response = this.configPluginsProvider.cropper(image, aspect);
    this.photo = response;
    */
  }

  tomarPhone() {
    const options: CameraOptions = {
      quality: 85,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    };

    this.camera.getPicture(options).then(
      imageData => {
        let base64Image = "data:image/jpeg;base64," + imageData;
        //let croppedBase64Image = this.goCropEditor(base64Image, 0);
        //this.documentsProviderService.imagesBase64.push(croppedBase64Image);
        this.goCropEditor(base64Image, 0);
        this.hasFile = true;
      },
      err => {
        this.ServicesProvider.toast(
          MESSAGES.camera.message.selected
        );
      }
    );
  }

  loadFile() {
    document.getElementById("fileUpload").click();
    this.hasFile = true;
  }

  loadNew() {
    this.timeOutControl = true;
    this.getCategoriesByModule(this.module);
  }

}
