import { Component } from "@angular/core";
import { NavController } from "ionic-angular";
import { DocumentsProviderService } from "../../../../providers/documents/documents";
import { ListFileHealthPage } from "../list-file-health/list-file-health";
import { SelectModulePage } from "../select-module/select-module";
import { PatientProviderService } from "../../../../providers/patient/patient";
import { ServicesProvider } from "../../../../providers/services/services";
import {  AlmacenamientoPage,PageHelpPage } from "../../../index.paginas";
/* Mensajes de la app */
import { MESSAGES } from "../../../../config/messages";
import { SERVICES } from "../../../../config/url.servicios";

@Component({
  selector: "page-documentos",
  templateUrl: "documentos.html"
})
export class DocumentosPage {
  treehealthFile: Array<string> = [];
  timeOut: any;
  timeOutControl: boolean = true;
  haveStorage: boolean;
  haveHalthFile: boolean = false;
  imagesArchivosSaludPublicity: string;
  almacenamiento = AlmacenamientoPage;
  pageHelp = PageHelpPage;

  constructor(
    public navCtrl: NavController,
    public documentsProviderService: DocumentsProviderService,
    public patientProviderService: PatientProviderService,
    public ServicesProvider: ServicesProvider
  ) {}

  clearTimeOut() {
    clearTimeout(this.timeOut);
  }
  ionViewWillEnter(){
    /*if( localStorage.getItem("wizzard_mis_archivos_salud")==undefined){
      localStorage.setItem("wizzard_mis_archivos_salud","true");
      this.navCtrl.push( this.pageHelp ,{ 'seccion': 'mis_archivos_salud' });
    }  */  
    this.loadPage();
  }
  ionViewDidLoad() {
    this.loadPage();
  }

  loadPage(){
    let params = {
      "id_paciente" : this.patientProviderService.getCurrentUser().id_pk
    };
    this.promisesHealthData(
      this.ServicesProvider.apiPost(params, SERVICES.CONSULTAR_PACIENTE_API_SERV),
      this.ServicesProvider.apiPost({"pac_fk":this.patientProviderService.getCurrentUser().id_pk}, SERVICES.ARBOL_ARCHIVOS_SALUD2_API_SERV)
    );
    this.getPublicity();
  }

  promisesHealthData(promiseUser: any, promiseTreeHealthFiles: any) {
    this.ServicesProvider.createLoader();
    Promise.all([promiseUser, promiseTreeHealthFiles])
      .then(promise => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();

        this.clearTimeOut();

        if (promise[0]["status"] == 404 || promise[0]["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (promise[0]["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(promise[0]["_body"]);
          localStorage.setItem("user", JSON.stringify(respuesta));
          this.haveHalthFile =
            respuesta.get_archivos_salud.length > 0 ? true : false;
          this.haveStorage = this.patientProviderService.verifyAlmacenamiento(
            Number(respuesta.almacenamiento),
            Number(respuesta.almacenamiento_acumulado)
          );
        }

        if (promise[1]["status"] == 404 || promise[1]["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (promise[1]["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(promise[1]["_body"]);
          this.treehealthFile = respuesta;
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
          this.ServicesProvider.closeLoader();

          this.clearTimeOut();
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  uploadHealthFile() {
    this.navCtrl.push(SelectModulePage, {tree: this.treehealthFile});
  }

  listFileHealth(item: any) {
    this.navCtrl.push(ListFileHealthPage, { data: item });
  }

  getPublicity() {
    let params = {
      'mod_padre': 94
    };
    this.ServicesProvider
      .apiPost(params, "/publicidades_modulos")
      .then(state => {
        if (state["status"] == 404 || state["status"] == 500) {
        } else {
          let respuesta = state["_body"];
          this.imagesArchivosSaludPublicity = respuesta;
          console.log(this.imagesArchivosSaludPublicity);
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  loadNew() {
    this.timeOutControl = true;
    this.ionViewDidLoad();
  }

}
