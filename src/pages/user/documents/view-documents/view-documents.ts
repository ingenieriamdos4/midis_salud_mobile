import { Component } from "@angular/core";
import {
  NavController,
  Refresher,
  AlertController,
  Platform
} from "ionic-angular";
import { PageHelpPage } from "../../../index.paginas";
import { DocumentsProviderService } from "../../../../providers/documents/documents";
import { ConfigPluginsProvider } from "../../../../providers/config-plugins/config-plugins";
import { ServicesProvider } from "../../../../providers/services/services";
import { MisDocumentosPage } from "../mis-documentos/mis-documentos";
import { InAppBrowser, InAppBrowserOptions } from "@ionic-native/in-app-browser";
import { SocialSharing } from "@ionic-native/social-sharing";
import { FileTransfer, FileTransferObject } from "@ionic-native/file-transfer";
import { FileEntry } from "@ionic-native/file";
/* Mensajes de la app */
import { MESSAGES } from "../../../../config/messages";
import { SERVICES } from "../../../../config/url.servicios";
declare var cordova: any;

@Component({
  selector: "page-view-documents",
  templateUrl: "view-documents.html"
})
export class ViewDocumentsPage {
  IdentDocumentObject: any;
  segLaboral: Array<string> = [];
  segSocial: Array<string> = [];
  docIdent: any = {};
  isDocumentoIdent: boolean = false;
  documentoIdentFoto: any;
  documentoIdentAdjunto: any;
  documentoIdentArchivo: any;
  documentoIdentId: any;
  arraySeguridad: Array<string>;
  timeOut: any;
  timeOutControl: boolean = true;
  storageDirectory: string = "";
  pageHelp = PageHelpPage;

  isIos: boolean = false;

  constructor(
    public navCtrl: NavController,
    public documentsProviderService: DocumentsProviderService,
    private iab: InAppBrowser,
    private alertCtrl: AlertController,
    private socialSharing: SocialSharing,
    public platform: Platform,
    private transfer: FileTransfer,
    public configPluginsProvider: ConfigPluginsProvider,
    public ServicesProvider: ServicesProvider
  ) {
    this.platform.ready().then(() => {
      // make sure this is on a device, not an emulation (e.g. chrome tools device mode)
      if (!this.platform.is("cordova")) {
        return false;
      }

      if (this.platform.is("ios")) {
        this.storageDirectory = cordova.file.documentsDirectory;
        this.isIos = true;
      } else if (this.platform.is("android")) {
        this.storageDirectory = cordova.file.externalRootDirectory;
      } else {
        // exit otherwise, but you could add further types here e.g. Windows
        return false;
      }
    });
  }

  clearTimeOut() {
    clearTimeout(this.timeOut);
  }

  ionViewDidLoad() {
    this.documentsProviderService.successDocuments = true;
  }


  ionViewWillEnter() {

    /*if (localStorage.getItem("wizzard_mis_documentos") == undefined) {
      localStorage.setItem("wizzard_mis_documentos", "true");
      this.navCtrl.push(this.pageHelp, { 'seccion': 'mis_documentos' });
    }*/

    if (!this.documentsProviderService.successDocuments) {
      return;
    }
    // Método para traer todos los documentos guardados
    let user = JSON.parse(localStorage.getItem("user"));
    let params = "?id_paciente=" + user.id_pk;
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiGet(params, SERVICES.VISUALIZAR_MIS_DOCUMENTOS_SERV)
      .then(data => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (data["status"] == 404 || data["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (data["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let resp = JSON.parse(data["_body"]);
          this.segSocial = [];
          this.segLaboral = [];
          if (resp.documento_identidad) {
            this.docIdent = resp.documento_identidad;
          }
          console.log("CC\n" + JSON.stringify(resp.documento_identidad));
          console.log("SEG_SO\n" + JSON.stringify(resp.seguridad_social));
          if (resp.documento_identidad) {
            this.IdentDocumentObject = resp.documento_identidad;
            this.documentoIdentidad(resp.documento_identidad);
            this.isDocumentoIdent = true;
          }
          if (resp.seguridad_social) {
            resp.seguridad_social.forEach(social => {
              if (social.archivo != null) {
                this.segSocial.push(social);
              }
            });
          }
          if (resp.seguridad_laboral) {
            resp.seguridad_laboral.forEach(laboral => {
              if (laboral.archivo != null) {
                this.segLaboral.push(laboral);
              }
            });
          }
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();
        this.timeOutControl = false;
        this.clearTimeOut();
        this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  documentoIdentidad(documento: any) {
    this.documentoIdentId = documento.id_pk;
    this.documentoIdentArchivo = documento.archivo;
    this.documentoIdentAdjunto = documento.adjunto;
    this.documentoIdentFoto = documento.foto;
  }
  addDocument() {
    this.navCtrl.push(MisDocumentosPage);
  }

  // Eliminar un documento
  deleteDocument(item: any, index: number, type: string) {
    let alert = this.alertCtrl.create({
      title: "Eliminar documento",
      message: "¿Esta seguro que desea eliminar el documento?",
      buttons: [
        {
          text: "Cancelar",
          role: "cancel",
          handler: () => {
            //console.log("Cancel clicked");
            this.documentsProviderService.successDocuments = false;
          }
        },
        {
          text: "Confirmar",
          handler: () => {
            this.documentsProviderService.successDocuments = false;
            this.serviceDeleteDocument(item, index, type);
            this.navCtrl.setRoot(this.navCtrl.getActive().component);
          }
        }
      ],
      cssClass: "alert-my-documents"
    });

    alert.present();
  }

  serviceDeleteDocument(item: any, index: number, type: string) {
    let user = JSON.parse(localStorage.getItem("user"));

    let params =
      "?id_paciente=" +
      user.id_pk +
      "&tabla_update=" +
      item.tabla_update +
      "&filtro_update=" +
      item.filtro_update +
      "&filtro_update2=" +
      (item.filtro_update2 == null ? "" : item.filtro_update2) +
      "&campo_update=" +
      item.campo_update +
      "&id_seguridad=" +
      (item.id_pk == undefined ? "" : item.id_pk);
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiGet(params, SERVICES.ELIMINAR_MIS_DOCUMENTOS_SERV)
      .then(data => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (data["status"] == 404 || data["status"] == 500) {
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (data["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
        } else {
          let respuesta = JSON.parse(data["_body"]);
          if (respuesta.success === true) {
            if (type == "segSocial") {
              this.segSocial.splice(index, 1);
            } else if (type == "segLaboral") {
              this.segLaboral.splice(index, 1);
            } else if (type == "docIdent") {
              this.documentoIdentArchivo = null;
            }
            this.ServicesProvider.toast(
              "Documento eliminado correctamente",
              "successToast"
            );
            //this.ionViewWillEnter();
          } else {
            this.ServicesProvider.toast(
              "Ha ocurrido un error al eliminar el documento"
            );
          }
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  shareFile(file: string) {
    this.ServicesProvider.createLoader();
    this.socialSharing
      .share(MESSAGES.documents.share.message, "Midis App", file, "")
      .then(() => {
        this.ServicesProvider.closeLoader();
      })
      .catch(err => {
        this.ServicesProvider.closeLoader();
        /*this.ServicesProvider.toast(
          MESSAGES.documents.share.error
        );*/
      });
  }

  verifyTypeFile(file: string) {
    if (file != undefined) {
      let ext: any = file
        .split(".")
        .pop()
        .toLowerCase();

      if (ext == "jpg" || ext == "jpeg" || ext == "png") {
        return true;
      }
    }

    return false;
  }

  splitFileName(file: string) {
    if(file != undefined){
      return file.split("/").pop();
    }
  }

  reviewFile(file: string) {
    let options: InAppBrowserOptions = {
      hardwareback: 'yes'
    };
    this.iab.create(file, "_blank", options);
  }

  download(_url: string) {

    if (this.isIos) {
      this.shareFile(_url);
    }

    let promise = this.configPluginsProvider.statusExternalStorageForApplication()

    this.configPluginsProvider.downloader(
      promise,
      this.transfer,
      _url,
      this.storageDirectory
    );
  }

  reloadDocumentsList(refresher: Refresher) {
    setTimeout(() => {
      this.ionViewWillEnter();
      refresher.complete();
    }, 1500);
  }
}
