import { Component, AfterViewInit } from "@angular/core";
import { NavController, NavParams, Refresher } from "ionic-angular";
import { DocumentsProviderService } from "../../../../providers/documents/documents";
import { PatientProviderService } from "../../../../providers/patient/patient";
import { ServicesProvider } from "../../../../providers/services/services";
import { HealthFilesDetailPage } from "../health-files-detail/health-files-detail";
/* Mensajes de la app */
import { MESSAGES } from "../../../../config/messages";
import { SERVICES } from "../../../../config/url.servicios";

@Component({
  selector: "page-list-file-health",
  templateUrl: "list-file-health.html"
})
export class ListFileHealthPage implements AfterViewInit {
  healthData: any;
  listData: Array<string> = [];
  timeOut: any;
  timeOutControl: boolean = true;
  imagesArchivosSaludPublicity: string;

  imageNoData: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public documentsProviderService: DocumentsProviderService,
    public patientProviderService: PatientProviderService,
    public ServicesProvider: ServicesProvider
  ) {
    this.healthData = this.navParams.get("data");
  }

  getImageNoData(title: any) {
    switch (title) {
      case 'Eventos de Salud':
        return "assets/imgs/eventos.png";
      case 'Documentos de Salud':
        return "assets/imgs/documentos.png";
      case 'Estudios Diagnósticos':
        return "assets/imgs/estudios.png";
      case 'Tratamientos de Salud':
        return "assets/imgs/tratamientos.png";
      case 'Intervenciones':
        return "assets/imgs/intervenciones.png";
      case 'Internaciones':
        return "assets/imgs/internaciones.png";
      case 'Urgencias':
        return "assets/imgs/urgencias.png";
      case 'Bancos de Salud':
        return "assets/imgs/bancos.png";
      case 'Consentimientos':
        return "assets/imgs/consentimientos.png";
      case 'Otros archivos':
        return "assets/imgs/otros.png";
    }
  }

  ngAfterViewInit() {
    this.listFileHealth();
  }

  ionViewWillEnter() {
    /*this.documentsProviderService.updateListWhenChangeStatus
      ? this.listFileHealth()
      : "";*/
    this.listFileHealth();
  }

  clearTimeOut() {
    clearTimeout(this.timeOut);
  }

  listFileHealth() {
    let id_pk = this.patientProviderService.getCurrentUser().id_pk;
    let params = { "pac_fk": id_pk, "mod_fk_padre": this.healthData["id"] };
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiPost(params, SERVICES.LISTAR_ARCHIVOS_SALUD_API_SERV)
      .then(file => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (file["status"] == 404 || file["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (file["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(file["_body"]);
          this.documentsProviderService.listHealthFile = respuesta;
          this.documentsProviderService.updateListWhenChangeStatus = false;
          if (this.documentsProviderService.listHealthFile.length == 0) {
            //this.getStoragePublicity();
            this.imageNoData = this.getImageNoData(this.healthData.text);
          }
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();
        this.timeOutControl = false;
        this.clearTimeOut();
        //this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  detailHealthData(index: number, item: any) {
    console.log("--- item: ", item);
    this.navCtrl.push(HealthFilesDetailPage, { index: index, data: item });
  }

  getStoragePublicity() {
    let params = {
      'mod_padre': 94,
      'mod_hijo': this.healthData["id"]
    };
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiPost(params, "/publicidades_modulos")
      .then(state => {
        this.ServicesProvider.loading.dismiss();
        this.clearTimeOut();
        if (state["status"] == 404 || state["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (state["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = state["_body"];
          this.imagesArchivosSaludPublicity = respuesta;
        }
      })
      .catch(err => {
        this.ServicesProvider.loading.onDidDismiss(() => {
          this.timeOutControl = false;
          this.clearTimeOut();
          this.ServicesProvider.toast(MESSAGES.services.error);
        });
      });
  }

  reloadListHealthFile(refresher: Refresher) {
    setTimeout(() => {
      this.listFileHealth();
      refresher.complete();
    }, 1500);
  }
}
