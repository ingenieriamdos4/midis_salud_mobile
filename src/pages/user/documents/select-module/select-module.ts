import { Component } from "@angular/core";
import { NavController } from "ionic-angular";
import { ServicesProvider } from "../../../../providers/services/services";

import { UploadHealthFilesPage } from "../upload-health-files/upload-health-files";
import { PatientProviderService } from "../../../../providers/patient/patient";


/* Mensajes de la app */
import { MESSAGES } from "../../../../config/messages";
import { SERVICES } from "../../../../config/url.servicios";
import { NavParams } from "ionic-angular/navigation/nav-params";

@Component({
  selector: "page-select-module",
  templateUrl: "select-module.html"
})
export class SelectModulePage {
  timeOut: any;
  timeOutControl: boolean = true;
  so_flag: boolean = true;

  tree: Array<string> = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public ServicesProvider: ServicesProvider,
    public patientProviderService: PatientProviderService
  ) {
    this.so_flag = this.ServicesProvider.validate_so_v();
    this.tree = navParams.get('tree');
    //this.getTree();
  }

  selectedModule(module: any){
    this.navCtrl.push(UploadHealthFilesPage, {'module':module.id});
  }

  getTree() {
    this.ServicesProvider.createLoader();
    this.ServicesProvider.apiPost({"pac_fk":this.patientProviderService.getCurrentUser().id_pk}, SERVICES.ARBOL_ARCHIVOS_SALUD_API_SERV)
      .then(tree => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (tree["status"] == 404 || tree["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (tree["status"] == 401) {
          this.ServicesProvider.toast(MESSAGES.services.unauthorized);
          this.timeOutControl = false;
        }
        else {
          this.tree = JSON.parse(tree["_body"]);
        }
      })
      .catch(error => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.timeOutControl = false;
        this.ServicesProvider.toast(MESSAGES.services.error + error);
      });
  }

  clearTimeOut() {
    clearTimeout(this.timeOut);
  }

  loadNew() {
    this.timeOutControl = true;

  }

}
