import { Component } from "@angular/core";
import { NavController, NavParams, Events, ModalController, AlertController } from "ionic-angular";
import { Validators, FormBuilder, FormGroup } from "@angular/forms";
import { Camera, CameraOptions } from "@ionic-native/camera";
import { ImagePicker, ImagePickerOptions } from "@ionic-native/image-picker";
/* Mensajes de la app */
import { MESSAGES } from "../../../../config/messages";

/* Providers */
import { DocumentsProviderService } from "../../../../providers/documents/documents";
import { ServicesProvider } from "../../../../providers/services/services";
import { SERVICES } from "../../../../config/url.servicios";

import { CropImagePage } from "../../../crop-image/crop-image";

@Component({
  selector: "page-mis-documentos",
  templateUrl: "mis-documentos.html"
})
export class MisDocumentosPage {
  SecurityOption: any;
  optionsArray: Array<string> = [];
  objectDatos: any;
  filtro_update2: string;
  arrayDatos: Array<string> = [];
  selectOption: any;
  formUploadDocuments: FormGroup;
  lista: any;
  imagePreview: string;
  promisesOptions: Array<any> = [];
  timeOut: any;
  timeOutControl: boolean = true;
  nameFile: string;
  uploadFile: any;
  // secConf almacena 0 ó 1. 1 cuando si está conf
  //  0 cuando no está configurada la seguridad social ó laboral
  //  1 cuando sí está configurada la seguridad social ó laboral ó se carga un documento de identidad
  // el cual no requiere el array datos para ser guardado mediante la api
  secConf: number = 0;
  //siempre es true, excepto que sea un android en una versión menor a la 4.4, en ese caso será false de acuerdo a la ejecución de la función validate_so_v
  so_flag: boolean = true;

  enableSave: boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private formBuilder: FormBuilder,
    public alertCtrl: AlertController,
    public documentsProviderService: DocumentsProviderService,
    public ServicesProvider: ServicesProvider,
    private camera: Camera,
    private imagePicker: ImagePicker,
    private events: Events,
    public modalCtrl: ModalController
  ) {
    this.so_flag = this.ServicesProvider.validate_so_v();
    this.formUploadDocuments = this.formBuilder.group({
      type: [null, Validators.compose([Validators.required])]
    });

    // if(this.arrayDatos.length > 0)
    // {
    //   this.formUploadDocuments = this.formBuilder.group({
    //     type: [null, Validators.compose([Validators.required])],
    //     options: [null, Validators.compose([Validators.required])]
    //   });
    // }
    // else
    // {
    //   this.formUploadDocuments = this.formBuilder.group({
    //     type: [null, Validators.compose([Validators.required])]        
    //   });
    // }

  }

  ionViewDidLoad() {
    this.ServicesProvider.createLoader();
    let currentUser = JSON.parse(localStorage.getItem("user"));
    let data = { "id_paciente": currentUser.id_pk };
    this.ServicesProvider
      .apiPost(data, SERVICES.OPCIONES_MIS_DOCUMENTOS_SERV)
      .then(data => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (data["status"] == 404 || data["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (data["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(data["_body"]);
          this.lista = respuesta;
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();
        this.timeOutControl = false;
        this.clearTimeOut();
        this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  clearTimeOut() {
    clearTimeout(this.timeOut);
  }

  traerFoto() {
    let options: ImagePickerOptions = {
      maximumImagesCount: 1,
      quality: 85,
      width: 510,
      height: 510,
      outputType: 1 // 1 para que sea una imagen en base64, 0 para URL fisico dispositivo
    };

    this.imagePicker.getPictures(options).then(
      imageData => {
        this.uploadFile = undefined;
        this.nameFile = "";
        this.imagePreview = "data:image/jpeg;base64," + imageData;
      },
      err => {
        this.ServicesProvider.toast(
          MESSAGES.imagePicker.message.error
        );
      }
    );
  }

  goCropEditor(image: any, aspect: any) {

    let cropAlert = this.alertCtrl.create({
      title: "Midis App Salud",
      message: "¿Desea cortar o editar esta foto?.\nPulse no para utilizar la original.",
      buttons: [
        {
          text: "Sí",
          handler: () => {
            let cropModal = this.modalCtrl.create(CropImagePage, { "image": image, "aspect": aspect });
            cropModal.onDidDismiss(data => {
              if (data.croppedImage != null) {
                this.imagePreview = data.croppedImage;
              }
            });
            cropModal.present();
            return;
          }
        },
        {
          text: "No",
          role: "cancel",
          handler: () => {
            this.imagePreview = image;
          }
        }
      ]
    });
    cropAlert.present();

    /*
    let response = this.configPluginsProvider.cropper(image, aspect);
    this.photo = response;
    */
  }

  tomarFoto() {
    let options: CameraOptions = {
      quality: 85,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true
    };

    this.camera
      .getPicture(options)
      .then(
        imageData => {
          this.uploadFile = undefined;
          this.nameFile = "";
          //this.imagePreview = "data:image/jpeg;base64," + imageData;
          this.goCropEditor("data:image/jpeg;base64," + imageData, 0);
        },
        err => {
          this.ServicesProvider.toast(
            MESSAGES.camera.message.selected
          );
        }
      )
      .catch(err => {
        this.ServicesProvider.toast(
          MESSAGES.camera.message.error
        );
      });
  }

  selectedOption(selected: any) {
    this.secConf = 0;
    console.log(selected);
    this.selectOption = selected;
    //console.log(this.selectOption);
    if (this.selectOption.id_pk != 1) {
      this.arrayDatos = this.selectOption.datos;
      this.enableSave = false;
    }
    else {
      this.enableSave = true;
    }
    if (this.selectOption.id_pk != 1) {
      if (this.selectOption.otra_tabla == "eps") {
        this.optionsArray = [];
        this.promisesOptions = [];
        if (this.arrayDatos.length > 0) {
          this.secConf = 1;
          for (let consultEPS of this.arrayDatos) {
            console.log("EPS: ", consultEPS);

            this.promisesOptions.push(
              this.ServicesProvider.apiGet(
                consultEPS["eps_fk"],
                SERVICES.CONSULTAR_EPS_SERV + "?id="
              )
            );
          }
        }
        else {
          this.ServicesProvider.toast("Primero debe configurar su seguridad laboral en la opción actualizar perfil");
        }
        this.promisesOptionsDocuments(this.promisesOptions);
      } else {
        this.optionsArray = [];
        this.promisesOptions = [];
        if (this.arrayDatos.length > 0) {
          this.secConf = 1;
          for (let consultARL of this.arrayDatos) {

            this.promisesOptions.push(
              this.ServicesProvider.apiGet(consultARL["arl_fk"], SERVICES.CONSULTAR_ARL_SERV + "?id=")
            );
          }
        }
        else {
          this.ServicesProvider.toast("Primero debe configurar su seguridad laboral en la opción actualizar perfil");
        }
        this.promisesOptionsDocuments(this.promisesOptions);
      }
    } else {
      this.secConf = 1;
      this.arrayDatos = [];
      this.optionsArray = [];
    }
  }

  promisesOptionsDocuments(arrayPromisesStates: any) {
    this.ServicesProvider.createLoader();
    Promise.all(arrayPromisesStates)
      .then(promises => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        for (let index = 0; index < promises.length; index++) {
          let respuesta = JSON.parse(promises[index]["_body"]);
          this.optionsArray.push(respuesta);
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();
        this.timeOutControl = false;
        this.clearTimeOut();
        this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  selectedSecurityOption(selectedSecurityOption: any) {
    //console.log(selectedSecurityOption);
    if (this.selectOption.otra_tabla == "eps") {
      this.SecurityOption = selectedSecurityOption.id_pk;
      // console.log(
      //   "usted ha seleccionado la tabla eps y el id es el " +
      //     this.SecurityOption
      // );
    } else {
      this.SecurityOption = selectedSecurityOption.id_pk;
      // console.log(
      //   "usted ha seleccionado la tabla arl y el id es el " +
      //     this.SecurityOption
      // );
    }
    if (this.SecurityOption != null) {
      this.enableSave = true;
    }
    else {
      this.enableSave = false;
    }
  }

  saveData() {
    // Subir documentos
    let formData = new FormData();
    if (this.arrayDatos.length != 0) {
      if (this.SecurityOption == null) {
        this.ServicesProvider.toast("Todos los campos son obligatorios.");
        return;
      }
    }
    let user = JSON.parse(localStorage.getItem("user"));
    //let params = {"id_paciente": user.id_pk};
    formData.append("id_paciente", user.id_pk);
    if (this.uploadFile != undefined) {
      formData.append("adjunto", this.uploadFile);
      formData.append("nombre", this.uploadFile.name);
      //params["adjunto"] = { "adjunto" : this.uploadFile, "nombre":this.uploadFile.name};
      formData.append("foto", "");
      //params["foto"] = "";
    }
    else if (this.imagePreview != "") {
      //params["adjunto"] = "";
      formData.append("adjunto", "");
      //params["foto"] = this.imagePreview;
      formData.append("foto", this.imagePreview);
    }
    else if (this.uploadFile != undefined && this.imagePreview != "") {
      this.ServicesProvider.toast(MESSAGES.documents.validate.message1);
      return;
    }
    else {
      this.ServicesProvider.toast(MESSAGES.documents.validate.message2);
      return;
    }
    //params["tabla_update"] = this.selectOption.tabla_update;
    formData.append("tabla_update", this.selectOption.tabla_update);
    //params["filtro_update"] = this.selectOption.filtro_update;
    formData.append("filtro_update", this.selectOption.filtro_update);
    if (this.selectOption.filtro_update2 != null) {
      //params["filtro_update2"] = this.selectOption.filtro_update2;
      formData.append("filtro_update2", this.selectOption.filtro_update2);
    } else {
      //params["filtro_update2"] = "";
      formData.append("filtro_update2", "");
    }
    //params["campo_update"] = this.selectOption.campo_update;
    formData.append("campo_update", this.selectOption.campo_update);
    if (this.SecurityOption != null) {
      //params["id_seguridad"] = this.SecurityOption;
      formData.append("id_seguridad", this.SecurityOption);
    } else {
      //params["id_seguridad"] = "";
      formData.append("id_seguridad", "");
    }
    //console.log("foto------------", formData.get('foto'));
    if (this.imagePreview == null || this.imagePreview == 'undefined') {
      this.ServicesProvider.toast("Debe cargar un archivo para poder continuar.");
      return;
    }
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiPost(formData, SERVICES.CARGAR_MIS_DOCUMENTOS_SERV)
      .then(data => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        //console.log("data", data);
        if (data["status"] == 404 || data["status"] == 500) {
          this.ServicesProvider.toast(MESSAGES.services.error);
        }
        else if (data["status"] == 401) {
          this.ServicesProvider.toast(MESSAGES.services.unauthorized);
        }
        else if (data["status"] == 403) {
          //parece que el servidor no procesa archivos muy grandes
          this.ServicesProvider.toast(MESSAGES.services.error);
        }
        else {
          let respuesta = JSON.parse(data["_body"]);
          if (respuesta.success === true) {
            this.ServicesProvider.toast(MESSAGES.documents.create.success, "successToast");
            this.uploadFile = undefined;
            this.nameFile = "";
            this.imagePreview = "";
            this.documentsProviderService.successDocuments = true;
            this.getUser(user.id_pk);
            this.navCtrl.pop();
          }
          else if (respuesta.success === false) {
            this.ServicesProvider.toast(MESSAGES.documents.create.error);
          }
          else if (respuesta.errors != undefined) {
            let i = 0;
            console.log("ERRORS ON CARGAR_MIS_DOCUMENTOS")
            for (i; i < Object.keys(respuesta.errors).length; i++) {
              let campo = Object.keys(respuesta.errors)[i];
              let error = (<any>Object).values(respuesta.errors)[i];
              console.log(campo + ": " + error);
            }
          }
        }
      })
      .catch(err => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.ServicesProvider.toast("Error: " + err);
      });
  }

  getUser(id_paciente: number) {
    let params = { "id_paciente": id_paciente };
    this.ServicesProvider
      .apiPost(params, SERVICES.CONSULTAR_PACIENTE_API_SERV)
      .then(state => {
        this.clearTimeOut();
        if (state["status"] == 404 || state["status"] == 500) {
          this.timeOutControl = false;
        }
        else if (state["status"] == 401) {
          this.timeOutControl = false;
        }
        else {
          let respuesta = JSON.parse(state["_body"]);
          localStorage.setItem("user", JSON.stringify(respuesta));
          this.events.publish('user:imagen_porcentaje', respuesta.imagen_porcentaje);
        }
      })
      .catch(err => { });
  }

  onUploadChange(ev) {

    let extErrorMessage =
      "Solo se permiten archivos con extensión: .jpg, .jpeg, .png o pdf";
    let allowedExtension = [
      "jpg",
      "jpeg",
      "png",
      "pdf"
    ];
    let extError = false;
    let myFiles = ev.target.files[0];
    if (myFiles != undefined) {

      let extName = myFiles["name"]
        .split(".")
        .pop()
        .toLowerCase();

      if (allowedExtension.indexOf(extName) == -1) {
        extError = true;
      }

      if (extError) {
        this.ServicesProvider.toast(extErrorMessage);
      } else {
        this.uploadFile = myFiles;
        this.nameFile = myFiles["name"];
        this.imagePreview = "";
      }
    } else {
      this.nameFile = "";
    }
  }

  loadFile() {
    document.getElementById("adjunto").click();
  }

  deleteFile(type: number) {
    type == 1 ? (this.imagePreview = "") : (this.nameFile = "");
    this.uploadFile = undefined;
  }
}
