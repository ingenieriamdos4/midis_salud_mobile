import { Component } from "@angular/core";
import {
  NavController,
  NavParams,
  AlertController,
  Platform
} from "ionic-angular";
import { DomSanitizer } from "@angular/platform-browser";
import { DocumentsProviderService } from "../../../../providers/documents/documents";
import { ConfigPluginsProvider } from "../../../../providers/config-plugins/config-plugins";
import { InAppBrowser, InAppBrowserOptions } from "@ionic-native/in-app-browser";
import { FileTransfer } from "@ionic-native/file-transfer";
import { SocialSharing } from "@ionic-native/social-sharing";

import {
  Validators,
  FormBuilder,
  FormGroup
} from "@angular/forms";

/* Mensajes de la app */
import { MESSAGES } from "../../../../config/messages";
/* Providers */
import { ServicesProvider } from "../../../../providers/services/services";
import { SERVICES } from "../../../../config/url.servicios";
declare var cordova: any;
declare var functionDicoms: any;

@Component({
  selector: "page-health-files-detail",
  templateUrl: "health-files-detail.html"
})
export class HealthFilesDetailPage {
  listData: Array<string> = [];
  healthData: Array<string> = [];
  index: number;
  timeOut: any;
  timeOutControl: boolean = true;
  storageDirectory: string = "";
  user = JSON.parse(localStorage.getItem("user"));
  formPrivacity: FormGroup;  

  filesDicom: Array<string> = [];
  filesImg: Array<string> = [];
  filesPdf: Array<string> = [];
  filesWord: Array<string> = [];
  isPrivate: boolean = false;
  isIos: boolean = false;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public documentsProviderService: DocumentsProviderService,
    private iab: InAppBrowser,
    private alertCtrl: AlertController,
    private transfer: FileTransfer,
    private socialSharing: SocialSharing,
    public platform: Platform,
    public configPluginsProvider: ConfigPluginsProvider,
    public sanitizer: DomSanitizer,
    public ServicesProvider: ServicesProvider,
    private formBuilder: FormBuilder,
  ) {
    this.platform.ready().then(() => {
      // make sure this is on a device, not an emulation (e.g. chrome tools device mode)
      if (!this.platform.is("cordova")) {
        return false;
      }

      if (this.platform.is("ios")) {
        this.storageDirectory = cordova.file.documentsDirectory;
        this.isIos = true;
      } else if (this.platform.is("android")) {
        this.storageDirectory = cordova.file.externalRootDirectory;
      } else {
        // exit otherwise, but you could add further types here e.g. Windows
        return false;
      }
    });

    this.formPrivacity = new FormGroup({});

    this.formPrivacity = this.formBuilder.group({
      description: ["", Validators.compose([Validators.required])]
    });

    this.healthData = this.navParams.get("data");    
    this.healthData["privado"] == '1' ? this.isPrivate = true : this.isPrivate = false;
    this.loadFilesHealth(this.healthData);
    this.index = this.navParams.get("index");
    console.log('-- The data: '+ this.navParams.get("data"));
    console.log("-- archivo con privacidad desde nav = ", this.healthData["privado"]);
    console.log("-- this.isPrivate = ", this.isPrivate);
  }

  clearTimeOut() {
    clearTimeout(this.timeOut);
  }

  loadFilesHealth(healthData: any) {
    if (healthData.get_adjuntos_archivos_salud) {
      healthData.get_adjuntos_archivos_salud.forEach(fileHealth => {
        if (fileHealth.archivo_dicom) {
          this.filesDicom.push(fileHealth.archivo_dicom);
        }
        if (fileHealth.archivo_foto) {
          this.filesImg.push(fileHealth.archivo_foto);
        }

        if (fileHealth.archivo_adjunto) {
          let ext: any = fileHealth.archivo_adjunto
            .split(".")
            .pop()
            .toLowerCase();
          if (ext == "pdf") {
            this.filesPdf.push(fileHealth.archivo_adjunto);
          }
          if (ext == "doc" || ext == "docx") {
            this.filesWord.push(
              "http://docs.google.com/viewer?url=" +
                fileHealth.archivo_adjunto +
                "&embedded=true&widget=false&headers=false"
            );
          }
          if (ext == "jpg" || ext == "png" || ext == "jpeg" || ext == "gif") {
            this.filesImg.push(fileHealth.archivo_adjunto);
          }
        }
      });

      // Cargar los archivos dicom
      if (this.filesDicom.length > 0) {
        functionDicoms(this.filesDicom);
      }
    }
  }

  deleteHealthData(id_pk: string) {
    let alert = this.alertCtrl.create({
      title: "Eliminar archivo de salud",
      message: "¿Esta seguro que desea eliminar el archivo de salud?",
      buttons: [
        {
          text: "Cancelar",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        },
        {
          text: "Confirmar",
          handler: () => {
            this.serviceDeleteFileHealth(id_pk);
          }
        }
      ]
    });

    alert.present();
  }

  serviceDeleteFileHealth(id_pk: string) {    
    let data = {"id" : id_pk, "pac_fk": this.user.id_pk};
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiPost(data, SERVICES.ELIMINAR_ARCHIVO_SALUD_API_SERV)
      .then(file => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.ServicesProvider.clearTimeOut();
        if (file["status"] == 404) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (file["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(file["_body"]);

          if (respuesta.success) {
            this.ServicesProvider.toast(
              MESSAGES.fileHealtData.delete.success,
              "successToast"
            );

            this.documentsProviderService.listHealthFile.splice(this.index, 1);
            this.navCtrl.pop();
          } else {
            this.ServicesProvider.toast(
              MESSAGES.fileHealtData.delete.error
            );
          }
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
          this.ServicesProvider.closeLoader();
          this.clearTimeOut();
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  splitFileName(file: string) {
    return file.split("/").pop();
  }

  reviewFile(fileUpload: string) {
    let options: InAppBrowserOptions = {
      hardwareback: 'yes'
    };
    this.iab.create(fileUpload, "_blank", options);
  }

  download(_url: string) {

    if (this.isIos) {
      this.shareQr(_url);
      return;
    }

    let promise = this.configPluginsProvider.statusExternalStorageForApplication()

    this.configPluginsProvider.downloader(
      promise,
      this.transfer,
      _url,
      this.storageDirectory
    );

  }

  shareQr(url: string) {
    this.ServicesProvider.createLoader();
    this.socialSharing
      .share(MESSAGES.fileHealtData.share.message, "Midis App", url, "")
      .then(success => {
        this.ServicesProvider.closeLoader();
      })
      .catch(err => {
        this.ServicesProvider.closeLoader();
        //this.ServicesProvider.toast(MESSAGES.codQr.share.error);
      });
  }

  privacityHealthData() {
    let alert = this.alertCtrl.create({
      title: "Cambiar privacidad archivo de salud",
      message:
        "¿Está seguro que desea cambiar la privacidad de " +
        (this.isPrivate ? "privado a público " : "público a privado ") +
        "del archivo de salud?",
      buttons: [
        {
          text: "Cancelar",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        },
        {
          text: "Confirmar",
          handler: () => {
            this.servicePrivacityHealthData();
          }
        }
      ]
    });

    alert.present();
  }

  // $request->privado (bit) 1 -> privado, 0 -> público
  servicePrivacityHealthData() {
    console.log("this.isPrivate ? '0' : '1' = ", this.isPrivate ? '0' : '1')
    console.log("cambiando estado privacidad... privacidad = " + this.isPrivate);
    let formData = new FormData();
    // this.healthData["privado"] == "1"
    //   ? (this.isPrivate = true)
    //   : (this.isPrivate = false)
    formData.append("id", this.healthData["id_pk"]);
    formData.append("privado_arc", this.isPrivate ? '0' : '1');
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiPost(formData, SERVICES.EDITAR_PRIVACIDAD_ARCHIVOS_SALUD_API_SERV)
      .then(antec => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (antec["status"] == 404 || antec["status"] == 500) {
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (antec["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
        } else {
          let respuesta = JSON.parse(antec["_body"]);
          if (respuesta.success) {
            this.isPrivate ? (this.isPrivate = false) : (this.isPrivate = true);
            this.documentsProviderService.updateListWhenChangeStatus = true;
            this.ServicesProvider.toast(
              MESSAGES.antecedent.privacity.success,
              "successToast"
            );
          } else {
            this.ServicesProvider.toast(
              MESSAGES.antecedent.privacity.error
            );
          }
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
          this.ServicesProvider.closeLoader();
          this.clearTimeOut();
          this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }
}
