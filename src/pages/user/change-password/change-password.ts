import { Component } from "@angular/core";
import { NavController, NavParams, App } from "ionic-angular";
import { Validators, FormBuilder, FormGroup } from "@angular/forms";
import { PatientProviderService } from "../../../providers/patient/patient";
import { ServicesProvider } from "../../../providers/services/services";
import { HomePage } from "../../index.paginas";
/* Mensajes de la app */
import { MESSAGES } from "../../../config/messages";
import { SERVICES } from "../../../config/url.servicios";
import { Events } from 'ionic-angular';

@Component({
  selector: "page-change-password",
  templateUrl: "change-password.html"
})
export class ChangePasswordPage {
  formChangePass: FormGroup;
  timeOut: any;
  timeOutControl: boolean = true;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private formBuilder: FormBuilder,
    public ServicesProvider: ServicesProvider,
    public patientProviderService: PatientProviderService,
    public appCtrl: App,
    public events: Events
  ) {
    this.formChangePass = this.formBuilder.group(
      {
        // email: [
        //   null,
        //   Validators.compose([
        //     Validators.required,
        //     Validators.maxLength(30),
        //     Validators.pattern("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$")
        //   ])
        // ],
        password: [
          null,
          Validators.compose([
            Validators.required,
            Validators.minLength(7),
            Validators.maxLength(15)
          ])
        ],
        confirmPassword: [
          null,
          Validators.compose([
            Validators.required,
            Validators.minLength(7),
            Validators.maxLength(15)
          ])
        ],
        new_password: [
          null,
          Validators.compose([
            Validators.required,
            Validators.minLength(7),
            Validators.maxLength(15)
          ])
        ]
      },
      { validator: this.matchingPasswords("new_password", "confirmPassword") }
    );
  }

   clearTimeOut() {
    clearTimeout(this.timeOut);
  }

  matchingPasswords(passwordKey: string, confirmPasswordKey: string) {
    // TODO maybe use this https://github.com/yuyang041060120/ng2-validation#notequalto-1
    return (group: FormGroup): { [key: string]: any } => {
      let password = group.controls[passwordKey];
      let confirmPassword = group.controls[confirmPasswordKey];

      if (password.value !== confirmPassword.value) {
        return {
          mismatchedPasswords: true
        };
      }
    };
  }

  logOut()
  {
    this.events.publish('user:logout');
  }

  saveData() {
    let params = {
      "password" : this.formChangePass.value.password,
      "password_nueva" : this.formChangePass.value.new_password,
      "email" : this.patientProviderService.getCurrentUser().correo,
      "pac_fk" : this.patientProviderService.getCurrentUser().id_pk
    };
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiPost(params, SERVICES.ACTUALIZAR_CLAVE_SERV)
      .then(login => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (login["status"] == 404 || login["status"] == 500) {
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (login["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
        } else {
          let respuesta = JSON.parse(login["_body"]);
          if (respuesta.success === true)
          {
            this.ServicesProvider.toast(MESSAGES.changePassword.message.success, "successToast");
            this.formChangePass.reset();            
            this.appCtrl.getRootNav().setRoot(HomePage);
            this.logOut();
          }
          else if(respuesta.success != true && respuesta.success != undefined)
          { 
            console.log("validar mensajes de la API. respuesta.success =" + respuesta.success)
            switch (respuesta.success) {
              case 1:
                this.ServicesProvider.toast("error de credenciales inválidas");
                break;

              case 2:
                this.ServicesProvider.toast("error de conexión");
                break;

              case 3:
                this.ServicesProvider.toast("error, el email no tiene formato de correo");
                break;
              
              case 4:
                this.ServicesProvider.toast("error de parámetros registrados incorrectamente");
                break;

              case 5:
                this.ServicesProvider.toast("error de credenciales: la credencial actual es igual a la nueva");
                break;

              case 6:
                this.ServicesProvider.toast("error, los datos no corresponde al paciente que está en sesión");
                break;            
                          
            }
          }
          else if(respuesta.errors != undefined)
          {
            console.log("respuesta.success = " + respuesta.success);
            for (var name in respuesta.errors) 
            {                            
              //console.log("xxxxxxxxxxxxxxxxxxxx  campo: " + name)
              this.ServicesProvider.toast(respuesta.errors[name].toString());          
            }
            // let i = 0;            
            // for(i; i < Object.keys(respuesta.errors).length; i ++)
            // {              
            //   let error = (<any>Object).values(respuesta.errors)[i];              
            //   this.ServicesProvider.toast(error);
            // }            
          }
          //else
          //{
            // for (var name in respuesta.errors) 
            // {
            //   let value = respuesta.errors[name];
            //   this.ServicesProvider.toast(value.toString());              
            // }
            //esta función valida errores devueltos por la api con mensajes quemados en el código fuente. Finalmente se decide mostrar errores del servicio pues permite mensajes dinámicos configurados en la api sin afectar la lógica central
            //this.getStatus(respuesta.success);
          //}
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
          this.ServicesProvider.closeLoader();
          this.clearTimeOut();
          this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  getStatus(status: number) {
    switch (status) {
      case 1:
        this.ServicesProvider.toast(
          "Error de credenciales inválidas"
        );
        break;
      case 2:
        this.ServicesProvider.toast("Error de conexion");
        break;
      case 3:
        this.ServicesProvider.toast(
          "Error de que el email no es identificado como un correo"
        );
        break;
      case 4:
        this.ServicesProvider.toast(
          "Error de parámetros registrados incorrectamente"
        );
        break;
      default:
        this.ServicesProvider.toast("Error de actualizacion");
    }
  }
}
