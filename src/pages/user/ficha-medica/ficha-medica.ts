import { Component } from "@angular/core";
import { PopoverController } from "ionic-angular";
import { AntecedentProviderService } from "../../../providers/antecedent/antecedent";
import { ListAntecedentPage } from "../antecedent/list-antecedent/list-antecedent";
import { PatientProviderService } from "../../../providers/patient/patient";
import { ServicesProvider } from "../../../providers/services/services";
/* Mensajes de la app */
import { MESSAGES } from "../../../config/messages";
import {
  DatosSaludPage,
  PopoverShareMedicalTabPage,
  PageHelpPage
} from "../../index.paginas";
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: "page-ficha-medica",
  templateUrl: "ficha-medica.html"
})
export class FichaMedicaPage {
  listAntecedent: any = ListAntecedentPage;
  listAntecedentAlert: Array<string> = [];
  datos_spa: Array<string> = [];
  currentUser: Array<string> = [];
  names: string;
  avatar: string;
  edad: any;
  typeRH: string[] = [];
  datosGO: string[] = [];
  timeOut: any;
  timeOutControl: boolean = true;
  antecedent_alert: Array<string> = [];
  othersAntecedent: Array<string> = [];
  healthyHabits: Array<string> = [];
  indiceMC: string;
  lastEvent: Array<string> = [];
  diagnostic: Array<string> = [];
  pageHelp = PageHelpPage;
  logoInstitution: string;
  nameInstitution: string;
  typeEvent: string;
  datosSalud = DatosSaludPage;

  constructor(
    public antecedentProviderService: AntecedentProviderService,
    public patientProviderService: PatientProviderService,
    public ServicesProvider: ServicesProvider,
    public popoverCtrl: PopoverController,
    public navCtrl: NavController,
    public navParams: NavParams
  ) {
    this.currentUser = JSON.parse(localStorage.getItem("user"));
    let currentPatient: any = this.currentUser;
    if (this.currentUser != undefined) {
      this.names =
        currentPatient.nombres +
        " " +
        currentPatient.apellido1 +
        " " +
        (currentPatient.apellido2 != null ? currentPatient.apellido2 : "");
      this.avatar = currentPatient.get_user[0].foto + "?1=" + new Date();
      this.edad = ServicesProvider.getDateZero(currentPatient.fecha_nac);

    }
  }

  ionViewWillEnter() {
    /*if (localStorage.getItem("wizzard_ficha_unica") == undefined) {
      localStorage.setItem("wizzard_ficha_unica", "true");
      this.navCtrl.push(this.pageHelp, { 'seccion': 'ficha_unica' });
    }*/
  }

  ionViewDidLoad() {
    let params = { "pac_fk": this.patientProviderService.getCurrentUser().id_pk };
    let params2 = { "pac_fk": this.patientProviderService.getCurrentUser().id_pk, "solo_alerta": 0, "mostrar_ant_completo": 0, "tan_fk": "" };
    let params3 = { "pac_fk": this.patientProviderService.getCurrentUser().id_pk };
    this.promisesInitialize(
      this.ServicesProvider.apiPost(
        params,
        "/datos_salud_paciente_api"
      ),
      this.ServicesProvider.apiPost(
        params2,
        "/listar_antecedentes_paciente"
      ),
      this.ServicesProvider.apiPost(
        params3,
        "/ultimoEventoSalud"
      )
    );
  }
  clearTimeOut() {
    clearTimeout(this.timeOut);
  }

  getAge(date_birthday: string) {
    let birthday = +new Date(date_birthday);
    let today = +new Date();
    let c_age = (today - birthday) / 31557600000;
    let age = Math.floor(c_age);
    return age;
  }

  promisesInitialize(
    promiseDateHealth: any,
    listAntecedent: any,
    lastEventHealth: any
  ) {
    this.ServicesProvider.createLoader();
    Promise.all([promiseDateHealth, listAntecedent, lastEventHealth])
      .then(promises => {
        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (promises[0]["status"] == 404 || promises[0]["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (promises[0]["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(promises[0]["_body"])[0];
          this.datos_spa = respuesta;
          if (respuesta.imc != "") {
            this.indiceMC = this.patientProviderService.clasificationImc(
              respuesta.imc
            );
          } else {
            this.indiceMC = "";
          }
          if (respuesta.get_tipo_sangre != null) {
            this.typeRH = respuesta.get_tipo_sangre;
          }
          this.datosGO = respuesta.get_datos_gineco_obstetricos;
          //console.log(this.datos_spa);
        }

        if (promises[1]["status"] == 404 || promises[1]["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (promises[1]["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(promises[1]["_body"]);
          //this.listAntecedentAlert = respuesta;
          this.antecedent_alert = [];
          this.othersAntecedent = [];
          this.healthyHabits = [];

          respuesta.forEach(antecedent => {
            if (antecedent["antPac"]["get_tipo_antecedente"]["alerta"] == "1") {
              this.antecedent_alert.push(antecedent);
            }

            if (
              antecedent["antPac"]["get_tipo_antecedente"]["alerta"] == "0" &&
              antecedent["antPac"]["get_tipo_antecedente"][
              "habito_saludable"
              ] == null
            ) {
              this.othersAntecedent.push(antecedent);
            }

            if (
              antecedent["antPac"]["get_tipo_antecedente"]["alerta"] == "0" &&
              antecedent["antPac"]["get_tipo_antecedente"][
              "habito_saludable"
              ] == "1"
            ) {
              this.healthyHabits.push(antecedent);
            }
          });
        }

        if (promises[2]["status"] == 404 || promises[2]["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (promises[2]["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(promises[2]["_body"]);
          if (respuesta.id_pk != undefined) {
            this.lastEvent = respuesta;
            if (respuesta.get_sede_institucion) {
              this.logoInstitution = respuesta.get_sede_institucion.get_institucion.logo;
              this.nameInstitution = respuesta.get_sede_institucion.nombre;
            }
            this.typeEvent = respuesta.get_tipo_consulta.nombre;
            this.diagnostic = respuesta.get_diagnostico;
          }
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.timeOutControl = false;
        this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }

  popoverShare(myEvent) {
    let popover = this.popoverCtrl.create(
      PopoverShareMedicalTabPage,
      { typePage: "107" },
      { cssClass: "popover-ficha-medica" }
    );
    popover.present({
      ev: myEvent
    });
    //popover.dismiss();
  }

  formatDate(date: string) {
    if (date) {
      let split_data = date.split(" ")[0].split("-");
      return split_data[2] + "-" + split_data[1] + "-" + split_data[0];
    }
  }

  loadNew() {
    this.timeOutControl = true;
    this.ionViewDidLoad();
  }
}
