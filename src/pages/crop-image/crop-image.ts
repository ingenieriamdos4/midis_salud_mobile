import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { AngularCropperjsComponent } from 'angular-cropperjs';

@Component({
  selector: 'page-crop-image',
  templateUrl: 'crop-image.html',
})
export class CropImagePage {
  @ViewChild('angularCropper') public angularCropper: AngularCropperjsComponent;
  cropperOptions: any;
  croppedImage = null;
  image: any;

  scaleValX = 1;
  scaleValY = 1;

  ratio: any;


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController
  ) {
    this.image = this.navParams.get('image');
    this.ratio = this.navParams.get('aspect'); //1: Cuadrado, 0: Rectangulo.
    this.cropperOptions = {
      dragMode: 'crop',
      aspectRatio: this.ratio,
      autoCrop: true,
      movable: true,
      zoomable: true,
      sacalable: true,
      autoCropArea: 0.8,
      keepAspect: false
    };
  }

  close(){
    this.viewCtrl.dismiss({ croppedImage: null});
  }

  save(){
    let croppedImageB64String: string = this.angularCropper.cropper.getCroppedCanvas().toDataURL('image/jpeg', (100/100));
    this.croppedImage = croppedImageB64String;
    this.viewCtrl.dismiss({ croppedImage : croppedImageB64String });
  }

  reset(){
    this.angularCropper.cropper.reset();
  }

  rotate(){
    this.angularCropper.cropper.rotate(90);
  }

  zoom(zoomIn: boolean){
    let factor = zoomIn ? 0.1 : -0.1;
    this.angularCropper.cropper.zoom(factor);
  }

  scaleX(){
    this.scaleValX = this.scaleValX * -1;
    this.angularCropper.cropper.scaleX(this.scaleValX);
  }

  scaleY(){
    this.scaleValY = this.scaleValY * -1;
    this.angularCropper.cropper.scaleY(this.scaleValY);
  }

  move(x, y){
    this.angularCropper.cropper.move(x, y);
  }

}
