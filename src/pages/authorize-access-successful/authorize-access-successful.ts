import { Component } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";

@Component({
  selector: "page-authorize-access-successful",
  templateUrl: "authorize-access-successful.html"
})
export class AuthorizeAccessSuccessfulPage {

  nameProfesional: string;

  constructor(public navCtrl: NavController, public navParams: NavParams) {

    this.nameProfesional = this.navParams.get('profesional');

  }
}
