import { Component } from '@angular/core';
import { NavController, ViewController, MenuController, App } from 'ionic-angular';

import { ViewDocumentsPage, LoginPage } from '../index.paginas';

@Component({
  selector: 'page-popover-menu',
  templateUrl: 'popover-menu.html',
})
export class PopoverMenuPage {

  constructor(public viewCtrl: ViewController,
              public navCtrl: NavController,
              private menuCtrl: MenuController,
              public appCtrl:App) {}

  irMisDocumentos() {
    this.navCtrl.push(ViewDocumentsPage);
  }

  close() {
    localStorage.clear();
    this.viewCtrl.dismiss();
    this.menuCtrl.enable(false);
    this.appCtrl.getRootNav().setRoot(LoginPage);
  }

}
