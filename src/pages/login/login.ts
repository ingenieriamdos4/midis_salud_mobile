import { Component } from "@angular/core";
import { NavController, NavParams, Platform, App, Events, AlertController } from "ionic-angular";

/* Plugins */
import { Validators, FormBuilder, FormGroup } from "@angular/forms";
import { SQLiteObject, SQLite } from "@ionic-native/sqlite";
import { InAppBrowser, InAppBrowserOptions } from "@ionic-native/in-app-browser";

/* Servicio */
import { CreateAccountServiceProvider } from "../../providers/create-account/create-account";
import { PatientProviderService } from "../../providers/patient/patient";
import { ServicesProvider } from "../../providers/services/services";
import { ConfigPluginsProvider } from "../../providers/config-plugins/config-plugins";
import { PushnotificationProvider } from "../../providers/pushnotification/pushnotification";

import { MESSAGES } from '../../config/messages';
import { SERVICES, URL } from "../../config/url.servicios";

import {
  HomePage,
  PageHelpPage,
  ValidateTokenPage,
  PasswordRecoveryPage,
  EmergenciasPage,
  BasicPatientInformationPage
} from "../index.paginas";
//import { BarcodeProvider } from "../../providers/barcode/barcode";


@Component({
  selector: "page-login",
  templateUrl: "login.html"
})
export class LoginPage {
  database: SQLiteObject;
  authForm: FormGroup;
  userRpt: any[] = [];
  idPacientoByEncryption: number;
  timeOut: any;
  timeOutControl: boolean = true;
  idDispositivo: string;
  so: any;

  pageHelp = PageHelpPage;
  emergencia = EmergenciasPage;
  recoverPass = PasswordRecoveryPage;

  boolInicio: boolean = true;
  showLogin: boolean = false;
  showCreate: boolean = false;
  have_token: boolean = false;
  valid: boolean = false;
  invalid: boolean = false;

  tokenText: string = "";

  constructor(
    private navCtrl: NavController,
    private appCtrl: App,
    private navParams: NavParams,
    private platform: Platform,
    private events: Events,
    private formBuilder: FormBuilder,
    private sqlite: SQLite,
    private oneSignalProvider: PushnotificationProvider,
    //private barcodeScanner: BarcodeProvider,
    private patientProviderService: PatientProviderService,
    private ServicesProvider: ServicesProvider,
    private createAccountService: CreateAccountServiceProvider,
    private configPluginsProvider: ConfigPluginsProvider,
    private alertCtrl: AlertController,
    private iab: InAppBrowser

  ) {

    this.authForm = this.formBuilder.group({
      email: [
        null,
        Validators.compose([Validators.required, Validators.maxLength(30)])
      ],
      password: [
        null,
        Validators.compose([
          Validators.required,
          Validators.minLength(7),
          Validators.maxLength(15)
        ])
      ]
    });
  }

  ionViewDidLoad() {
    this.initializerUsers();
    console.log("LOGIN:::" + (this.patientProviderService.users))
  }

  validate_so_v() {
    this.ServicesProvider.validate_so_v();
  }

  ionViewWillEnter() {

    if (this.navCtrl.getPrevious()) {
      if (this.navParams.get('accion') == "qr") {
        this.navCtrl.getPrevious().data = {};
        this.scan();
      }
    }

    this.configPluginsProvider.set(
      "register",
      this.createAccountService.register.value
    );
    this.configPluginsProvider.set(
      "register2",
      this.createAccountService.register2.value
    );
    this.configPluginsProvider.set(
      "register3",
      this.createAccountService.register3.value
    );
    this.configPluginsProvider.set(
      "register4",
      this.createAccountService.register4.value
    );
  }

  helpLogin() {
    this.navCtrl.push(this.pageHelp, { 'seccion': 'login' });
  }

  async initializerUsers() {
    await this.platform.ready();
    if (this.platform.is("cordova")) {
      this.sqlite
        .create({
          name: "midis.db",
          location: "default"
        })
        .then((db: SQLiteObject) => {
          this.database = db;
          db
            .executeSql("SELECT * FROM users", [])
            .then(data => {
              this.patientProviderService.users = [];
              for (let index = 0; index < data.rows.length; index++) {
                this.patientProviderService.users.push(data.rows.item(index));
                alert("ADD USER");
              }
            })
            .catch(e => {
              console.log(JSON.stringify(e));
            });
        });
    }
  }

  indexCreateAccountPage() {
    this.navCtrl.push(ValidateTokenPage);
  }

  submitForm(value: any): void {
    let data = { "email": value.email, "password": value.password };
    if (this.idDispositivo) {
      data["id_dispositivo"] = this.idDispositivo;
    }
    else {
      data["id_dispositivo"] = "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx";
    }
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiPost(data, SERVICES.LOGIN_API_SERV)
      .then(login => {
        this.ServicesProvider.clearTimeOut();
        if (login["status"] == 404 || login["status"] == 500) {
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (login["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
        } else {
          let respuesta = JSON.parse(login["_body"]);
          if (respuesta.success != undefined) {
            this.getStatus(respuesta.success);
          }
          else {
            this.ServicesProvider.getToken(respuesta.token);
            localStorage.setItem("token", respuesta.token);
            this.getUser(respuesta.id_paciente);
            this.authForm.reset();
          }
        }
        this.ServicesProvider.closeLoader();
      })
      .catch(err => {
        this.ServicesProvider.closeLoader();
        this.ServicesProvider.clearTimeOut();
        this.ServicesProvider.toast(MESSAGES.services.error);
      });
  }

  getLevelProfile(id_paciente: any) {
    let data = { "id_paciente": id_paciente };
    this.ServicesProvider
      .apiPost(data, SERVICES.CONSULTAR_PACIENTE_API_SERV)
      .then(state => {
        if (state["status"] == 404 || state["status"] == 500) {
        } else if (state["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
        } else {
          let respuesta = JSON.parse(state["_body"]);
          if (respuesta.imagen_porcentaje) {
            localStorage.setItem("imagen_porcentaje", respuesta.imagen_porcentaje);
          }
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  getUser(id_paciente: number) {
    let params = { "id_paciente": id_paciente };
    this.ServicesProvider.createLoader("Verificando datos...");
    this.ServicesProvider
      .apiPost(params, SERVICES.CONSULTAR_PACIENTE_API_SERV)
      .then(state => {
        this.ServicesProvider.closeLoader();;
        this.ServicesProvider.clearTimeOut();
        if (state["status"] == 404 || state["status"] == 500) {
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (state["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
        } else {
          let respuesta = JSON.parse(state["_body"]);
          localStorage.setItem("user", JSON.stringify(respuesta));
          localStorage.setItem("imagen_porcentaje", respuesta.imagen_porcentaje);
          this.events.publish('user:imagen_porcentaje', respuesta.imagen_porcentaje);
          console.log("imagen_porcentaje :", respuesta.imagen_porcentaje);
          let dataHealth: any = JSON.stringify({
            weight: respuesta.peso,
            height: respuesta.estatura,
            imc: respuesta.imc
          });
          localStorage.setItem("dataHealth", dataHealth);
          this.patientProviderService.avatar =
            respuesta.get_user[0].foto + "?1=" + new Date();
          this.patientProviderService.height = respuesta.estatura;
          this.patientProviderService.weight = respuesta.peso;
          this.patientProviderService.imc = respuesta.imc;

          let user = {
            name:
              respuesta.nombres +
              " " +
              respuesta.apellido1 +
              " " +
              (respuesta.apellido2 != null ? respuesta.apellido2 : ""),
            avatar: respuesta.get_user[0].foto + "?1=" + new Date(),
            status: 1,
            pac_fk: id_paciente,
            username: respuesta.get_user[0].login_pk,
            type_user: 1
          };

          this.appCtrl.getRootNav().setRoot(HomePage);

          if (this.platform.is("cordova")) {
            this.createUserSqlite(id_paciente, user);
          }
        }
      })
      .catch(err => {
        this.ServicesProvider.closeLoader();
        this.ServicesProvider.clearTimeOut();
        this.ServicesProvider.toast(MESSAGES.services.error);
      });
  }

  prueba(){
    alert("de one");
  }

  createUserSqlite(id_paciente: number, user: any) {
    this.patientProviderService
      .getFindByUser(id_paciente)
      .then(userFind => {
        if (userFind == undefined) {
          this.createUser(user);
        } else {
          this.patientProviderService
            .updateUser(user)
            .then(response => {
              console.log(
                "Se actualizo correctamente" + JSON.stringify(response)
              );
            })
            .catch(error => {
              this.ServicesProvider.toast(
                "Error al actualzar" + JSON.stringify(user) + error
              );
            });
        }
      })
      .catch(err => {
        this.ServicesProvider.toast(
          "Error al encontrar el usuario" + JSON.stringify(user) + err
        );
      });
  }

  createUser(data: any) {
    this.patientProviderService
      .createUser(data)
      .then(response => {
        this.patientProviderService.users.unshift(data);
      })
      .catch(error => {
        console.log("Error al crear el usuario", error);
        this.ServicesProvider.toast(
          MESSAGES.sqlite.error2 + JSON.stringify(error)
        );
      });
  }

  getStatus(status: number) {
    switch (status) {
      case 1:
        this.ServicesProvider.toast(MESSAGES.login.status.state_1);
        break;
      case 2:
        this.ServicesProvider.toast(MESSAGES.login.status.state_2);
        break;
      case 3:
        this.ServicesProvider.toast(MESSAGES.login.status.state_3);
        break;
      case 4:
        this.ServicesProvider.toast(MESSAGES.login.status.state_4);
        break;
      default:
        this.ServicesProvider.toast(MESSAGES.login.status.state_5, "successToast");
    }
  }

  scan() {
    if (localStorage.getItem("wizzard_codigo_qr") == undefined) {
      localStorage.setItem("wizzard_codigo_qr", "true");
      this.navCtrl.push(this.pageHelp, { 'seccion': 'codigo_qr', 'accion': 'qr' });
    }

  }

  getPatientInformationByEncryption(id_paciente: string) {
    console.log("-------------si getPatientInformationByEncryption");
    let data = { "id_paciente": id_paciente };
    this.ServicesProvider.createLoader();
    this.ServicesProvider
      .apiPost(data, SERVICES.INF_BASICA_OTROS_PACIENTES)
      .then(state => {
        console.log("-------------si apiPost. state=", state["status"]);
        this.ServicesProvider.closeLoader();;
        this.ServicesProvider.clearTimeOut();
        if (state["status"] == 404 || state["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (state["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
        } else {
          let respuesta = JSON.parse(state["_body"]);
          if (respuesta.errors != undefined) {
            this.ServicesProvider.toast("si respuesta.errors != undefined");
            let i = 0;
            for (i; i < Object.keys(respuesta.errors).length; i++) {
              let campo = Object.keys(respuesta.errors)[i];
              let error = (<any>Object).values(respuesta.errors)[i];
              console.log(campo + " : " + error);
              this.ServicesProvider.toast(campo + ": " + error);
            }
          }
          else {
            console.log("getPatientInformationByEncryption-----------------------R =", respuesta);
            this.navCtrl.push(BasicPatientInformationPage, {
              paciente: respuesta,
              personal: true,
            });
            console.log("despues de this.navCtrl.push(BasicPatientInformationPage-----------------------R =", respuesta);
          }
        }
      })
      .catch(err => {
        console.log("getPatientInformationByEncryption-----------------------R =", err);
        this.ServicesProvider.closeLoader();
        this.ServicesProvider.clearTimeOut();
        this.timeOutControl = false;
        this.ServicesProvider.toast(MESSAGES.services.error);
      });
  }

  emergency() {
    let users: any = this.patientProviderService.users;
    this.navCtrl.push(EmergenciasPage, { count: users.length });
  }

  //MANIPULACION DE ELEMENTOS EN LA VISTA
  clickLogin() {
    this.showLogin = !this.showLogin;
    this.boolInicio = false;
  }

  clickCreate() {
    this.showCreate = !this.showCreate;
    this.boolInicio = false;
    this.navCtrl.push(this.pageHelp, { 'seccion': 'token' });
  }

  showInicio() {
    this.boolInicio = true;
    this.showLogin = false;
    this.showCreate = false;
  }

  enableValidateToken() {
    this.have_token = !this.have_token;
  }
  //


  //SECCION DE CREACIÓN DE CUENTAS
  helpCode() {
    let alert = this.alertCtrl.create({
      title: "Midis App Salud",
      message:
        "Para registrarte necesitas un código de registro.<br><br>Si ya realizaste el pago correspondiente, enviamos un código único a tu correo y a tu celular. Ingresalo aquí para registrarte.<br><br>Si no has pagado, no tardes, en la opción <b>'Obtener un código de registro'</b> te direccionaremos correctamente para que hagas tu compra.",
      buttons: [
        {
          text: "Aceptar",
          role: "confirm"
        }
      ]
    });
    alert.present();
  }

  onInputTime(token: any) {
    if (token.length == 10) {
      this.tokenText = token;
      let params = { "token": token };
      this.ServicesProvider.createLoader();
      this.ServicesProvider.apiPost(params, SERVICES.VALIDATE_TOKEN)
        .then(response => {
          this.ServicesProvider.closeLoader();
          if (response["status"] == 404 || response["status"] == 500) {
            this.ServicesProvider.toast(MESSAGES.services.error);
          }
          else if (response["status"] == 401) {
            this.ServicesProvider.toast(MESSAGES.services.unauthorized);
          }
          else {
            let validation = JSON.parse(response["_body"]);
            if (validation == 1) {
              this.valid = true;
              this.invalid = false;
            }
            else if (validation == 3) {
              this.valid = false;
              this.invalid = false;
              this.ServicesProvider.toast("El token ingresado ya fue utilizado para realizar un registro");
            }
            else if (validation == 2) {
              this.invalid = true;
            }
          }
        });
    }
    else {
      this.valid = false;
      this.invalid = false;
    }
  }

  register() {
    //this.navCtrl.push(this.registerPage, { 'token': this.tokenText });
  }

  pay() {
    let options: InAppBrowserOptions = {
      hardwareback: 'yes'
    };
    this.iab.create(URL + "/pagosusuario", "_blank", options);
  }
  //
}
