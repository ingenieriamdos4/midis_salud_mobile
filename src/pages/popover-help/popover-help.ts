import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { PageHelpPage} from '../index.paginas'

@Component({
  selector: 'page-popover-help',
  templateUrl: 'popover-help.html',
})
export class PopoverHelpPage {
  pageHelp = PageHelpPage;
  typePage: string;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.typePage = this.navParams.get('typePage');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PopoverHelpPage');
    
  }

}
