export class Person {

  name: string;
  surname: string; // primer apellido
  second_surname: string;
  gender: string;
  address:string;
  country:number;
  department:number;
  municipality:number;
  vereda: number;
  phone:number;
  cellphone:number;
  email:string;
  birthdate:string;
  document:string;
  password:string;
  type_document:number;
  relationShip:number;
  typeProfessional: string;
  professional: string;
  speciality: string;
}
