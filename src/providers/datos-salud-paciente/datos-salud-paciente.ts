import { Http, Headers, RequestOptions } from "@angular/http";
import { Injectable } from "@angular/core";

@Injectable()
export class DatosSaludPacienteProviderService {
  headers: Headers;
  headersPost: Headers;
  options: RequestOptions;

  constructor(
    public http: Http
  ) {}
}
