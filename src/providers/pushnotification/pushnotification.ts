import { Injectable } from "@angular/core";
import { OneSignal } from "@ionic-native/onesignal";
import { Platform } from "ionic-angular";
import { Http, Headers } from "@angular/http";
import { URL_SERVICIOS } from "../../config/url.servicios";

@Injectable()
export class PushnotificationProvider {
  headers: Headers;
  headersPost: Headers;
  urlApi: string = URL_SERVICIOS;
  constructor(
    private oneSignal: OneSignal,
    public platform: Platform,
    public http: Http
  ) {}

  init_notifications() {
    if (this.platform.is("cordova")) {
      this.oneSignal.startInit(
        "046440a6-eb44-43fd-ab13-3903ddf34756",
        "405604547056"
      );
      this.oneSignal.inFocusDisplaying(
        this.oneSignal.OSInFocusDisplayOption.Notification
      );
      this.oneSignal.handleNotificationReceived().subscribe(() => {
        console.log("Notification recibida");
      });

      this.oneSignal.handleNotificationOpened().subscribe(() => {
        // console.log('Notification abierta');
        //alert(this.oneSignal.OSNotificationPayload());
      });
      this.oneSignal.endInit();
    } else {
      console.log("OneSignal No funciona en el navegador de Pcs");
    }
  }

  getDeviceId() {
    if (this.platform.is("cordova")) {
      this.oneSignal
        .getIds()
        .then(success => {
          let userId = success.userId;
          localStorage.setItem("playerId", userId);
        })
        .catch(error => {
          alert("No se recibió un ID de dispositivo");
        });
    }
  }

}
