import { FormGroup } from "@angular/forms";
import { Http, Headers, RequestOptions } from "@angular/http";
import { Injectable } from "@angular/core";
import { ToastController, LoadingController } from "ionic-angular";

@Injectable()
export class DocumentsProviderService {
  headers: Headers;
  headersPost: Headers;
  options: RequestOptions;
  loading: any;
  uploadDocuments: FormGroup;
  listHealthFile: Array<string> = [];
  imagesBase64: Array<any> = [];
  arrayFiles: Array<any> = [];
  successDocuments: boolean = true;
  updateListWhenChangeStatus: boolean = false;

  constructor(
    public http: Http,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController
  ) {}

  // Params yyy-mm-dd ejem. 2017-11-30
  // Transform dd/mm/yyyy
  formatDate(date: string) {
    if (date != null || date != undefined) {
      let dateTransform = date.split("-"); // asi se debe enviar YYY-MM-DD Ej: 2017-12-29
      let format_Date =
        dateTransform[2] +
        "/" +
        dateTransform[1] +
        "/" +
        dateTransform[0].split(" ")[0];
      return format_Date;
    }

    return null;
  }
}
