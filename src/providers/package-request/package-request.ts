import { Http, Headers, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';


@Injectable()
export class PackageRequestProviderService {

  headers: Headers;
  headersPost: Headers;
  options: RequestOptions;
  loading: any;

  constructor(public http: Http) {
  }


  getCurrentUser() {
    let currentUser = JSON.parse(localStorage.getItem('user'));
    return { usuario: currentUser};
  }

}
