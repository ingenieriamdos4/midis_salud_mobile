import { Http, Headers, RequestOptions } from "@angular/http";
import { Injectable } from "@angular/core";
import { ToastController, LoadingController } from "ionic-angular";

@Injectable()
export class AntecedentProviderService {
  headers: Headers;
  headersPost: Headers;
  options: RequestOptions;
  listAntecedentAlert: Array<string> = [];
  loading: any;
  antecedent_alert: Array<string> = [];
  othersAntecedent: Array<string> = [];
  healthyHabits: Array<string> = [];
  antecedentsSuccess: boolean = false;

  constructor(
    public http: Http,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController
  ) {}

  /*  LISTAR ANTECEDENTES
      solo_alerta, 1 muestran solo los de alerta e incluye intervenciones, 0 se muestran todos
      mostrar antecedente completo, 1 se muestran la información completa de cada antecedentes,
      0 se muestran unicamente los datos principales de cada antecedente para una visualización más corta
  */

}
