import { Http, Headers, RequestOptions } from "@angular/http";
import { Injectable } from "@angular/core";


@Injectable()
export class ReportsProviderService {
  headers: Headers;
  headersPost: Headers;
  options: RequestOptions;
  loading: any;

  constructor(
    public http: Http,
  ) {}

}
