import { Http, Headers, RequestOptions } from "@angular/http";
import { URL_SERVICIOS } from "../../config/url.servicios";
import { ToastController, LoadingController, Platform } from "ionic-angular";
import { MESSAGES } from "../../config/messages";
import { Injectable } from "@angular/core";

import { Events } from 'ionic-angular';

@Injectable()
export class ServicesProvider {
  timeOut: any;
  timeOutControl: boolean = true;
  loading: any;
  headers: Headers;
  headersExternal: Headers;
  urlApi: string = URL_SERVICIOS;
  options: RequestOptions;
  optionsExternal: RequestOptions;
  token: any;
  oToast: any;
  //token =  JSON.parse(localStorage.getItem("user"));
  so: any;

  constructor(
    public http: Http,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public events: Events,
    public platform: Platform,
  ) { }

  createHeaders() {
    if (localStorage.getItem("token") != null) {
      this.token = localStorage.getItem("token");
    }
    this.headers = new Headers(
      {
        //'Content-Type' : 'application/json',
        //'Content-Type' : 'multipart/form-data',
        //'Content-Type': undefined,
        'x-api-key': this.token
      });
    this.headersExternal = new Headers(
      {
        //'Content-Type' : 'application/json',
        //'Accept' : 'application/json',
        //'Content-Type' : 'multipart/form-data'
        //'Access-Control-Allow-Origin':'*',
        //'Access-Control-Allow-Headers': 'Origin, Content-Type, Accept, Access-Control-Allow-Origin'
      });
    this.options = new RequestOptions({ headers: this.headers });
    this.optionsExternal = new RequestOptions({ headers: this.headersExternal });
  }
  /**
     * Función para Crear el toast de información
     * @param  {}
     * @return  {}
     */
  toast(mensaje: string, toastClass: string = "errorToast") {

    if (mensaje == MESSAGES.services.unauthorized) {
      toastClass = "warningToast"
    }

    try {
      this.oToast.dismiss();
    } catch (e) { }



    this.oToast = this.toastCtrl.create({
      message: mensaje,
      duration: MESSAGES.toast.time,
      position: "bottom",
      cssClass: toastClass,
      showCloseButton: true,
      closeButtonText: "X"
    });
    this.oToast.present();
  }

  /**
   * Función para Crear el loader de espera
   * @param  {}
   * @return  {}
   */
  createLoader(message: string = MESSAGES.loading.message) {
    if (!this.loading) {
      this.loading = this.loadingCtrl.create({
        content: message
      });
      this.loading.present();
    }
  }
  closeLoader() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }


  getDateZero(fecha_nacimiento) {

    var today: any = new Date();
    var dd: any = today.getDate();
    var mm: any = today.getMonth() + 1; //January is 0!

    var yyyy = today.getFullYear();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    today = yyyy + "/" + mm + "/" + dd;
    console.log(today, " ", fecha_nacimiento)
    var splitDate = fecha_nacimiento.split("-");
    console.log(splitDate)
    console.log(yyyy, " ", mm, " ", dd);
    console.log((splitDate[2], " ", splitDate[1], " ", splitDate[0]))
    var d1: any = new Date(yyyy, mm, dd);                // April 5, 2014
    var d2: any = new Date(splitDate[0], splitDate[1], splitDate[2]);               // February 22, 2013

    var dy = d1.getYear() - d2.getYear();
    var dm = d1.getMonth() - d2.getMonth();
    dd = d1.getDate() - d2.getDate();

    if (dd < 0) { dm -= 1; dd += 30; }
    if (dm < 0) { dy -= 1; dm += 12; }

    if (dy != 0) {
      if (dy == 1) {
        return dy + " Año";
      }
      else {
        return dy + " Años";
      }

    }
    else {
      if (dm == 0) {
        if (dd == 1) {
          return (dd + " Dia");
        }
        else {
          return (dd + " Dias");
        }

      }
      else {
        console.log(dd)
        if (dm == 1) {
          return (dm + " Mes " + (dd == 1 ? (' y 1 dia') : (" y " + dd + " dias")));
        }
        else {
          return (dm + " Meses " + (dd == 1 ? (' y 1 dia') : (" y " + dd + " dias")));
        }

      }
    }
  }

  /**
   * Función para crear el controlador de tiempo de espera
   * @param  {}
   * @return  {}
   */
  clearTimeOut() {
    clearTimeout(this.timeOut);
  }

  /**
   * Función para traer el token de sesión
   * @param  {}
   * @return  {}
   */
  getToken(token) {
    this.token = token;
  }

  logOut() {
    this.events.publish('user:logout');
  }

  /**
   * Función para peticiones a la API por el método GET
   * @param  {}
   * @return  {}
   */
  apiGet(params: any, url: string) {
    this.createHeaders();
    return new Promise((resolve, reject) => {
      this.http.get(this.urlApi + url + params, this.options).subscribe(
        res => {
          resolve(res);
        },
        err => {
          if (err["status"] == 401) {
            this.logOut();
          }
          resolve(err);
        }
      );
    });
  }

  /**
   * Función para peticiones a la API por el método POST
   * @param  {}
   * @return  {}
   */
  apiPost(formData: any, url: string) {
    this.createHeaders();
    return new Promise((resolve, reject) => {
      this.http.post(this.urlApi + url, formData, this.options).subscribe(
        res => {
          resolve(res);
        },
        err => {
          if (err["status"] == 401) {
            this.logOut();
          }
          resolve(err);
        }
      );
    });
  }

  apiPostExternalUrl(formData: any, url: string) {
    this.createHeaders();
    return new Promise((resolve, reject) => {
      this.http.post(this.urlApi + url, formData, this.optionsExternal).subscribe(
        res => {
          resolve(res);
        },
        err => {
          if (err["status"] == 401) {
            this.logOut();
          }
          resolve(err);
        }
      );
    });
  }
  apiGetExternalUrl(formData: any, url: string) {
    //this.createHeaders();
    return new Promise((resolve, reject) => {
      this.http.get(this.urlApi + url, formData).subscribe(
        res => {
          resolve(res);
        },
        err => {
          if (err["status"] == 401) {
            this.logOut();
          }
          resolve(err);
        }
      );
    });
  }

  validate_so_v() {
    if (this.platform.is('android')) {
      this.so = this.platform.versions();
      console.log("platform.versions() = ", this.platform.versions());
      console.log("platform.versions.android.str() = ", this.so.android.str);
      if (this.so.android.str > 4.4) {
        console.log("-- Android versión = " + this.so.android.str + " es mayor a 4.4")
        return true;
      }
      else {
        console.log("-- Android igual o menor a 4.4 = " + this.so.android.str);
        return false;
      }
    }
    else {
      return true;
    }
  }

}

