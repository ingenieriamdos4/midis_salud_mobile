import { Injectable } from "@angular/core";

import { LoadingController, ToastController, Platform } from "ionic-angular";
import { OneSignal } from "@ionic-native/onesignal";
import { Validators, FormBuilder, FormGroup } from "@angular/forms";

import { Http, Headers, RequestOptions } from "@angular/http";

import { URL_SERVICIOS } from "../../config/url.servicios";
import { ServicesProvider } from "../../providers/services/services";
import { MESSAGES } from "../../config/messages";
import { SERVICES } from "../../config/url.servicios";

import "rxjs/add/operator/map";

@Injectable()
export class CreateAccountServiceProvider {
  headers: Headers;
  headersPost: Headers;
  options: RequestOptions;
  //habilitar_tab_contacto:boolean=false;
  person_validation_phone: any;
  urlApi: string = URL_SERVICIOS;

  _verifyAge: boolean = false;

  register: FormGroup;
  register2: FormGroup;
  register3: FormGroup;
  register4: FormGroup;
  discapacidad: Array<object> = [];

  countrys: string[];
  indicativoCompleto: any;
  loading: any;
  isOlder: number;
  timeOut: any;
  timeOutControl: boolean = true;
  idDispositivo: string;
  citiesContact: any;
  citiesPersonalInformation: any;
  citiesResposibleData: any;
  citiesContactoDetails: any;
  countrySelected: any;

  constructor(
    private formBuilder: FormBuilder,
    public http: Http,
    public loadingCtrl: LoadingController,
    public ServicesProvider: ServicesProvider,
    public toastCtrl: ToastController,
    private oneSignal: OneSignal,
    public platform: Platform
  ) {
    //this.getCountrysService(); // Carga, lista de paises

    this.register = this.formBuilder.group({

      indicativo_pais: [
        false,
        Validators.compose([Validators.compose([])])
      ], indicativo_ciudad: [
        false,
        Validators.compose([Validators.compose([])])
      ],

      name: [
        null,
        Validators.compose(
          [
            Validators.required,
            Validators.maxLength(20),
            Validators.pattern(
              /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]*$/
            )
          ])
      ],
      surname: [
        null,
        Validators.compose([Validators.required,
        Validators.maxLength(20),
        Validators.pattern(
          /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]*$/
        )
        ])
      ],
      second_surname: [null, Validators.compose(
        [
          Validators.maxLength(20),
          Validators.pattern(
            /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]*$/
          )
        ])],
      gender: [null, Validators.compose([Validators.required])],
      discapacidad: [null, Validators.compose([])],
      discapacidad_desc: [null, Validators.compose([])],
      check_discapacidad: [null, Validators.compose([])],
      reside_actualmente: [null, Validators.compose([])],
      token: [null, Validators.compose([])],
      type_document: [null, Validators.compose([Validators.required])],
      document: [
        null,
        Validators.compose([Validators.required, Validators.maxLength(15)])
      ],
      birthdate: [null, Validators.compose([Validators.required])],
      country: [null, Validators.compose([Validators.required])],
      department: [null, Validators.compose([Validators.required])],
      municipality: [null, Validators.compose([Validators.required])],
      vereda: [null, Validators.compose([])]
    });

    this.register2 = this.formBuilder.group({
      name: [
        null,
        Validators.compose([
          Validators.required,
          Validators.maxLength(20),
          Validators.pattern(
            /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]*$/
          )]
        )],
      surname: [
        null,
        Validators.compose([
          Validators.required,
          Validators.maxLength(20),
          Validators.pattern(
            /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]*$/
          )]),
      ],
      second_surname: [
        null,
        Validators.compose([
          Validators.maxLength(20),
          Validators.pattern(
            /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]*$/
          )]),
      ],
      country: [null, Validators.compose([Validators.required])],
      department: [null, Validators.compose([Validators.required])],
      municipality: [null, Validators.compose([Validators.required])],
      email: [
        null,
        Validators.compose([
          Validators.required,
          Validators.maxLength(30),
          Validators.pattern(
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
          )
        ])
      ],
      cellphone: [
        null,
        Validators.compose([Validators.required, Validators.maxLength(50)])
      ],
      phone: [null, Validators.compose([Validators.maxLength(50)])],
      address: [
        null,
        Validators.compose([])
      ],
      parentesco: [null, Validators.compose([Validators.required])],
      document: [
        null,
        Validators.compose([])
      ],
      familiar_encontrado: [
        null,
        Validators.compose([])
      ],
      pack_fk_responsable: [
        null,
        Validators.compose([])
      ]

    });

    this.register3 = this.formBuilder.group({
      /*country: [null, Validators.compose([Validators.required])],
      department: [null, Validators.compose([Validators.required])],
      municipality: [null, Validators.compose([Validators.required])],
      vereda: [null, Validators.compose([])],*/
      cellphone: [
        null,
        Validators.compose([Validators.required, Validators.maxLength(50)])
      ],
      phone: [null, Validators.compose([Validators.maxLength(50)])]
      //'address': [null, Validators.compose( [Validators.required, Validators.maxLength(50)] )]
    });



    this.register4 = formBuilder.group(
      {
        email: [
          null,
          Validators.compose([
            Validators.required,
            Validators.maxLength(60),
            Validators.pattern(
              "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
            )
          ])
        ],
        user: [
          null,
          Validators.compose([
            Validators.required,
            Validators.maxLength(25),
            Validators.minLength(5)
          ])
        ],
        password: [
          null,
          Validators.compose([
            Validators.required,
            Validators.minLength(7),
            Validators.maxLength(15),
            Validators.pattern(
              /^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/
            )
          ])
        ],
        confirmPassword: [
          null,
          Validators.compose([
            Validators.required,
            Validators.minLength(7),
            Validators.maxLength(15)
          ])
        ],
        accept_Terms: [null, Validators.compose([Validators.required])]
      },
      { validator: this.matchingPasswords("password", "confirmPassword") }
    );
    //,Validators.pattern('/^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/')
    this.getDeviceId();
  }



  matchingPasswords(passwordKey: string, confirmPasswordKey: string) {
    // TODO maybe use this https://github.com/yuyang041060120/ng2-validation#notequalto-1
    return (group: FormGroup): { [key: string]: any } => {
      let password = group.controls[passwordKey];
      let confirmPassword = group.controls[confirmPasswordKey];

      if (password.value !== confirmPassword.value) {
        return {
          mismatchedPasswords: true
        };
      }
    };
  }

  verifyAge() {
    let birthdate = this.register.value.birthdate;
    let age: number = this.getAge(birthdate);
    console.log("si verifyAge. fecha nac: " + birthdate);
    console.log(age, " ", this.isOlder)
    if (age < this.isOlder) {
      console.log("age < this.isOlder -------------- return false. Age = " + age)
      return false;
    }
    console.log("age >= this.isOlder-------------- return true. Age = " + age)
    return true;
  }

  getAge(birth_day: string) {
    let birthday = +new Date(birth_day);
    let today = +new Date();
    let c_age = (today - birthday) / 31557600000;
    let age = Math.floor(c_age);
    return age;
  }


  fn_getDiscapacidades() {

    this.ServicesProvider.createLoader();
    /*this.timeOut = setTimeout(() => {
      this.timeOutControl = false;
      this.ServicesProvider.loading.dismiss();
      this.ServicesProvider.toast(MESSAGES.services.timeOut);
    }, MESSAGES.loading.time);*/
    this.ServicesProvider.apiGet("", SERVICES.GET_DISCAPACIDADES)
      .then(disc => {
        this.ServicesProvider.closeLoader();

        /*this.ServicesProvider.loading.dismiss();
        clearTimeout(this.timeOut);*/
        if (disc["status"] == 404 || disc["status"] == 500) {
          /*this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);*/
        } else if (disc["status"] == 401) {
          /*this.ServicesProvider.toast(MESSAGES.services.unauthorized);
          this.timeOutControl = false;*/
        } else {
          let respuesta = JSON.parse(disc["_body"]);
          this.discapacidad = respuesta;
          console.log(this.discapacidad)
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();

        this.timeOutControl = false;
        clearTimeout(this.timeOut);
        this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }


  getCountrysService() {
    this.ServicesProvider.createLoader();
    this.ServicesProvider.loading.present();
    this.timeOut = setTimeout(() => {
      this.timeOutControl = false;
      //this.ServicesProvider.loading.dismiss();
      this.ServicesProvider.closeLoader();

      this.ServicesProvider.toast(MESSAGES.services.timeOut);
    }, MESSAGES.loading.time);
    this.ServicesProvider.apiGet("", SERVICES.UBICACION_LIST_PAIS_SERV)
      .then(country => {
        this.ServicesProvider.closeLoader();

        //this.ServicesProvider.loading.dismiss();
        clearTimeout(this.timeOut);
        if (country["status"] == 404 || country["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (country["status"] == 401) {
          this.ServicesProvider.toast(MESSAGES.services.unauthorized);
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(country["_body"]);
          this.countrys = respuesta;
        }
      })
      .catch(err => {
        this.ServicesProvider.closeLoader();

        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.timeOutControl = false;
        clearTimeout(this.timeOut);
        this.ServicesProvider.toast(MESSAGES.services.error);
        // });
      });
  }

  getDeviceId() {
    this.oneSignal
      .getIds()
      .then(success => {
        this.idDispositivo = success.userId;
      })
      .catch(error => {
        console.log("No se recibio ningun ID");
      });
  }

  filterItems(searchTerm, cities) {
    if (searchTerm) {
      return cities.filter(item => {
        return item.nombre.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
      });
    }
  }

  newLoad() {
    this.timeOutControl = true;
    this.getCountrysService();
  }
}
