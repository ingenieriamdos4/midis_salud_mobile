import { Injectable } from "@angular/core";

@Injectable()
export class LoginProviderService {
  constructor() {}


  getCurrentUser() {
    let currentUser = JSON.parse(localStorage.getItem("user"));

    if (currentUser != undefined) {
      let name =
        currentUser.nombres +
        " " +
        currentUser.apellido1 +
        " " +
        (currentUser.apellido2 != null ? currentUser.apellido2 : "");
      let avatar = currentUser.get_user[0].foto;

      return { usuario: currentUser, name: name, avatar: avatar };
    }

    return { usuario: currentUser };
  }

}
