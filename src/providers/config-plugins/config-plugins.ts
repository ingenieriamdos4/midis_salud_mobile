import { Injectable } from "@angular/core";
import { Diagnostic } from "@ionic-native/diagnostic";
import { Storage } from "@ionic/storage";

import { ServicesProvider } from "../../providers/services/services";
import { MESSAGES } from "../../config/messages";
import { FileTransferObject } from "@ionic-native/file-transfer";
import { DocumentViewer } from '@ionic-native/document-viewer';

import { CropImagePage } from "../../pages/crop-image/crop-image";
import { ModalController } from "ionic-angular";
import { SocialSharing } from "@ionic-native/social-sharing";



@Injectable()
export class ConfigPluginsProvider {

  currentPage: any;

  constructor(
    private diagnostic: Diagnostic,
    public storage: Storage,
    private ServicesProvider: ServicesProvider,
    private document: DocumentViewer,
    public modalCtrl: ModalController,
    private socialSharing: SocialSharing
  ) {

  }

  checkPermissions() {
    this.diagnostic
      .requestExternalStorageAuthorization()
      .then(isAvailable => { })
      .catch(e => {
        console.log("error almacen", e);
      });
  }

  statusExternalStorageForApplication() {
    return new Promise((resolve, reject) => {
      this.diagnostic
        .getExternalStorageAuthorizationStatus()
        .then(isAvailable => {
          console.log("Provider", isAvailable);
          resolve(isAvailable);
        })
        .catch(e => {
          resolve(e);
        });
    });
  }

  shareFile(file: string) {
    this.ServicesProvider.createLoader();
    this.socialSharing
      .share(MESSAGES.documents.share.message, "Midis App", file, "")
      .then(() => {
        this.ServicesProvider.closeLoader();
      })
      .catch(err => {
        this.ServicesProvider.closeLoader();
      });
  }

  cropper(image: any, aspect: any) {
    let cropModal = this.modalCtrl.create(CropImagePage, { "image": image, "aspect": aspect });
    cropModal.onDidDismiss(data => {
      if (data.croppedImage) {
        return data.croppedImage;
      }
      else {
        return null;
      }
    });
    cropModal.present();
  }

  downloader(promise: any, transfer: any, _url: any, storageDirectory: any) {
    let temp: any = String(Math.floor(Math.random() * (100) + 1));

    promise.then(success => {
      if (success != "DENIED") {
        this.ServicesProvider.createLoader();
        const fileTransfer: FileTransferObject = transfer.create();
        const url = _url;

        let route =
          storageDirectory + temp + url.substring(url.lastIndexOf('/') + 1);
        fileTransfer.download(url, route)
          .then(fe => {
            this.ServicesProvider.closeLoader();
            this.ServicesProvider.toast(MESSAGES.downloader.success, "successToast");
            let url = fe.toURL();
            this.document.viewDocument(url, 'application/pdf', {});
          },
            error => {
              this.ServicesProvider.closeLoader();
              this.ServicesProvider.toast(MESSAGES.downloader.error + error);
            });
      } else {
        this.checkPermissions();
      }
    })
      .catch(e => {
        console.log("Error plugin almacenamiento. ", e);
      });
  }

  downloader2(promise: any, storageDirectory: any, _url: any, file: any, transfer: any) {

    let temp: any = String(Math.floor(Math.random() * (100) + 1));

    promise.then(success => {
      if (success != "DENIED") {
        this.ServicesProvider.createLoader();
        const fileTransfer: FileTransferObject = transfer.create();
        const url = _url;
        let result = file.createDir(storageDirectory, 'Download', true);

        result.then(resp => {
          this.ServicesProvider.closeLoader();
          let path = resp.toURL();
          fileTransfer.download(url, path + url.substring(url.lastIndexOf("/") + 1) + temp)
            .then(entry => {
              this.ServicesProvider.toast(
                MESSAGES.downloader.success,
                "successToast"
              );
            },
              error => {
                this.ServicesProvider.toast(
                  MESSAGES.downloader.error + error
                );
              });
        }, error => {
          this.ServicesProvider.closeLoader();
          this.ServicesProvider.toast("Error en módulo de descargas. " + error);
        });
      } else {
        this.checkPermissions();
      }
    })
      .catch(e => {
        console.log("Error plugin almacenamiento. ", e);
      });

  }

  public set(settingName, value) {
    return this.storage.set(`setting:${settingName}`, value);
  }
  public async get(settingName) {
    return await this.storage.get(`setting:${settingName}`);
  }
  public async remove(settingName) {
    return await this.storage.remove(`setting:${settingName}`);
  }
  public clear() {
    this.storage.clear().then(() => {
      console.log("all keys cleared");
    });
  }
}
