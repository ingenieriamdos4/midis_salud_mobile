import { Http, Headers, RequestOptions } from "@angular/http";
import { Injectable } from "@angular/core";
import { ToastController } from "ionic-angular";

@Injectable()
export class PackageProviderService {
  headers: Headers;
  headersPost: Headers;
  options: RequestOptions;

  constructor(public http: Http, public toastCtrl: ToastController) {}
}
