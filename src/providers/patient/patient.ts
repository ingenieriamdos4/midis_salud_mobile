import { Http, Headers, RequestOptions } from "@angular/http";
import { Injectable } from "@angular/core";
import { SQLiteObject } from "@ionic-native/sqlite";
import { ToastController, LoadingController } from "ionic-angular";
//import { Observable } from "rxjs/Rx";


@Injectable()
export class PatientProviderService {
  headers: Headers;
  headersPost: Headers;
  options: RequestOptions;
  loading: any;
  db: SQLiteObject = null;
  patient: string = JSON.parse(localStorage.getItem("user")) != null
    ? JSON.parse(localStorage.getItem("user"))
    : ""; //Paciente
  avatar: string = JSON.parse(localStorage.getItem("user")) != null
    ? JSON.parse(localStorage.getItem("user")).get_user[0].foto
    : "";
  weight: string = ""; //Peso
  height: string = ""; // Estatura
  imc: string = ""; //Indice de masa corporal

  users: Array<string> = [];
  // users = [
  //   {
  //     pac_fk: 20,
  //     avatar: 'http://2.bp.blogspot.com/_Tt5yOvw4o_A/S-DBz6Zkk6I/AAAAAAAAJ0w/ET2h1BeqJJU/s1600/asainaang.jpg'
  //   },
  //   { pac_fk: 1,
  //     avatar: 'http://2.bp.blogspot.com/_Tt5yOvw4o_A/S-DBz6Zkk6I/AAAAAAAAJ0w/ET2h1BeqJJU/s1600/asainaang.jpg'
  //   },
  //   { pac_fk: 34,
  //     avatar: 'http://2.bp.blogspot.com/_Tt5yOvw4o_A/S-DBz6Zkk6I/AAAAAAAAJ0w/ET2h1BeqJJU/s1600/asainaang.jpg'
  //   }
  // ];

  spinnerFichaMedica: boolean = false;

  constructor(
    public http: Http,
    public toastCtrl: ToastController,

    public loadingCtrl: LoadingController
  ) {
    let localstorageHealth = localStorage.getItem("dataHealth");
    if (localstorageHealth != null) {
      let objectHealth = JSON.parse(localstorageHealth);
      this.weight = objectHealth.weight;
      this.height = objectHealth.height;
      this.imc = objectHealth.imc;
    }
  }

  setDatabase(db: SQLiteObject) {
    if (this.db === null) {
      this.db = db;
    }
  }


  createTableUser() {
    let sql =
      "CREATE TABLE IF NOT EXISTS users(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, avatar TEXT, status INTEGER, pac_fk INTEGER, type_user INTEGER, username TEXT )";
    return this.db.executeSql(sql, []);
  }

  verifyIfExistColumn(table: string, fieldName: string) {
    let sql = "PRAGMA table_info(" + table + ")";
    return this.db
      .executeSql(sql, null)
      .then(response => {
        let ifExist: boolean = false;
        for (let index = 0; index < response.rows.length; index++) {
          if (response.rows.item(index).name == fieldName) {
            ifExist = true;
          }
        }
        return Promise.resolve(ifExist);
      })
      .catch(error => {
        alert("Exist" + JSON.stringify(error));
      });
  }

  addColumTable(table: string, fieldName: string) {
    let sql =
      "ALTER TABLE " +
      table +
      " ADD COLUMN " +
      fieldName +
      " TEXT default null";
    return this.db.executeSql(sql, []);
  }

  getFindByUser(pac_fk: number) {
    let sql = "SELECT * FROM users where pac_fk=?";
    return this.db
      .executeSql(sql, [pac_fk])
      .then(response => {
        let user = [];
        user.push(response.rows.item(0));
        return Promise.resolve(response.rows.item(0));
      })
      .catch(error => Promise.reject(error));
  }

  getAllUser() {
    let sql = "SELECT * FROM users";
    return this.db
      .executeSql(sql, [])
      .then(response => {
        let users = [];
        for (let index = 0; index < response.rows.length; index++) {
          users.push(response.rows.item(index));
        }
        return Promise.resolve(users);
      })
      .catch(error => Promise.reject(error));
  }

  deleteUser(user: any) {
    let sql = "DELETE FROM users WHERE pac_fk=?";
    return this.db.executeSql(sql, [user.pac_fk]);
  }

  createUser(user: any) {
    let sql =
      "INSERT INTO users(name, avatar, status, pac_fk, type_user, username) VALUES(?,?,?,?,?,?)";
    return this.db.executeSql(sql, [
      user.name,
      user.avatar,
      user.status,
      user.pac_fk,
      user.type_user,
      user.username
    ]);
  }

  updateUser(user: any) {
    let sql = "UPDATE users SET avatar=?, username=? WHERE pac_fk=?";
    return this.db.executeSql(sql, [user.avatar, user.username, user.pac_fk]);
  }

  // Params yyy-mm-dd ejem. 2017-11-30
  // Transform dd/mm/yyyy
  formatDate(date: string) {
    if (date != null || date != undefined) {
      let dateTransform = date.split("-"); // asi se debe enviar YYY-MM-DD Ej: 2017-12-29
      let format_Date =
        dateTransform[2] +
        "/" +
        dateTransform[1] +
        "/" +
        dateTransform[0].split(" ")[0];
      return format_Date;
    }

    return null;
  }

  getCurrentUser() {
    return JSON.parse(localStorage.getItem("user"));
  }



  clasificationImc(imc: number) {
    let indiceMc = "";
    if (imc < 18.5) {
      indiceMc = "Insuficiencia ponderal";
    } else if (imc >= 18.5 && imc <= 24.899999999999999) {
      indiceMc = "Normal";
    } else if (imc >= 25 && imc <= 26.899999999999999) {
      indiceMc = "Sobrepeso";
    } else if (imc >= 27 && imc <= 29.899999999999999) {
      indiceMc = "Obesidad Grado I";
    } else if (imc >= 30 && imc <= 34.899999999999999) {
      indiceMc = "Obesidad Grado II";
    } else if (imc >= 35 && imc <= 39.899999999999999) {
      indiceMc = "Obesidad Grado III";
    } else if (imc >= 40 && imc <= 99.900000000000006) {
      indiceMc = "Obesidad Grado IV";
    }

    return indiceMc;
  }

  verifyAlmacenamiento(almacenamiento: number = Number(this.getCurrentUser().almacenamiento),
                      almacenamiento_acumulado: number = Number(this.getCurrentUser().almacenamiento_acumulado))
    {      
      if(this.getCurrentUser() != null)
      {
        if (almacenamiento_acumulado == 0)
        {          
          return false;
        }
        else
        {
          if(almacenamiento < almacenamiento_acumulado)
          {            
            return true;
          }
          else
          {            
            return false;
          }
        }
      }
  }
}
