import { Injectable } from "@angular/core";

import { Validators, FormBuilder, FormGroup, FormControl } from "@angular/forms";
import { Http, Headers, RequestOptions } from "@angular/http";
import { LoadingController, ToastController } from "ionic-angular";
import { ServicesProvider } from "../../providers/services/services";
import { MESSAGES } from "../../config/messages";
import { SERVICES } from "../../config/url.servicios";

import { URL_SERVICIOS } from "../../config/url.servicios";

@Injectable()
export class UpdateProfileProviderService {
  urlApi: string = URL_SERVICIOS;
  timeOut: any;
  headers: Headers;
  headersPost: Headers;
  options: RequestOptions;
  timeOutControl: boolean = true;
  updateDataPerson: FormGroup;
  updateHalthcareData: FormGroup;
  updateOtherData: FormGroup;
  updateMedicalData: FormGroup;
  existFamily: FormGroup;
  addMedicalData: FormGroup;
  addFamilyData: FormGroup;
  updateDataPersonMain: FormGroup;
  loading: any;
  indicativoCellphone: string;
  indicativoPhone: string;
  infoPac: Array<string> = [];
  arrayModel: Array<any> = [];
  arrayModelFamily: Array<any> = [];
  // Datos del profesional y familiar
  dataProfesional: Array<any> = [];
  dataFamily: Array<any> = [];
  avatar_profile: any = "";
  citiesPersonalInformation: any;
  citiesContact: any;
  citiesPersonalInformationMain: any;
  isVisibleIonToggle: boolean = false;
  loadProgress: any = 0;
  isValidateUpdateHalthcareData: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public http: Http,
    public ServicesProvider: ServicesProvider

  ) {
    this.updateDataPerson = this.formBuilder.group({
      email: [
        null,
        Validators.compose([
          Validators.required,
          Validators.maxLength(30),
          Validators.pattern(
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
          )
        ])
      ],
      country: [null, Validators.compose([Validators.required])],
      department: [null, Validators.compose([Validators.required])],
      municipality: [null, Validators.compose([Validators.required])],
      vereda: [null, Validators.compose([])],
      cellphone: [
        null,
        Validators.compose([Validators.required, Validators.maxLength(50)])
      ],
      phone: [null, Validators.compose([Validators.maxLength(50)])],
      address: [
        null,
        Validators.compose([Validators.required, Validators.maxLength(50)])
      ]
    });

    this.updateHalthcareData = this.formBuilder.group({
      eps: [null, Validators.compose([])],
      arl: [null, Validators.compose([])],
      prepaid_security: [null, Validators.compose([Validators.maxLength(40)])],
      institution: [null, Validators.compose([Validators.maxLength(40)])],
      service_ambulance: [null, Validators.compose([Validators.maxLength(40)])],
      service_special_transfer: [null, Validators.compose([Validators.maxLength(40)])],
      soat: [null, Validators.compose([])],
      seg_ve: [null, Validators.compose([])]
    });

    this.updateOtherData = this.formBuilder.group({
      stratum: [null, Validators.compose([])],
      level_education: [null, Validators.compose([])],
      religion: [null, Validators.compose([])],
      status_working: [null, Validators.compose([])],
      impediment: [null, Validators.compose([Validators.maxLength(50)])],
      adjunto_impedimento: [null, Validators.compose([])]
    });

    this.updateMedicalData = new FormGroup({});

    this.addMedicalData = this.formBuilder.group({
      medico_encontrado: [
        null,
        Validators.compose([])
      ],
      name: [
        null,
        Validators.compose([
          Validators.required, 
          Validators.maxLength(20),
          Validators.pattern(
            /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]*$/
          )
        ])
      ],
      second_surname: [
        null,
        Validators.compose([
          Validators.required, 
          Validators.maxLength(50),
          Validators.pattern(
            /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]*$/
          )
        ])
      ],
      email: [
        null,
        Validators.compose([
          Validators.maxLength(30),
          Validators.pattern(
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
          )
        ])
      ],
      address: [null, Validators.compose([Validators.maxLength(50)])],
      country: [null, Validators.compose([Validators.required])],
      department: [null, Validators.compose([Validators.required])],
      municipality: [null, Validators.compose([Validators.required])],
      cellphone: [
        null,
        Validators.compose([Validators.required, Validators.maxLength(50)])
      ],
      phone: [null, Validators.compose([Validators.maxLength(50)])],

      profesion: [null, Validators.compose([])],
      tipo_prof: [null, Validators.compose([])],
      especialidad: [null, Validators.compose([])],
      id_dispositivo: [null, Validators.compose([])]
    });

    this.addFamilyData = this.formBuilder.group({
      familiar_encontrado: [
        null,
        Validators.compose([])
      ],
      name: [
        null,
        Validators.compose([
          Validators.required, 
          Validators.maxLength(20),
          Validators.pattern(
            /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]*$/
          )
        ])
      ],
      parentesco: [null, Validators.compose([Validators.required])],
      surname: [
        null,
        Validators.compose([
          Validators.required, 
          Validators.maxLength(20),
          Validators.pattern(
            /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]*$/
          )
        ])
      ],
      second_surname: [null, Validators.compose([
        Validators.maxLength(20),
        Validators.pattern(
          /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]*$/
        )
      ])
      ],
      email: [
        null,
        Validators.compose([
          Validators.maxLength(30),
          Validators.pattern(
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
          )
        ])
      ],
      address: [null, Validators.compose([Validators.maxLength(50)])],
      country: [null, Validators.compose([Validators.required])],
      department: [null, Validators.compose([Validators.required])],
      municipality: [null, Validators.compose([Validators.required])],
      cellphone: [
        null,
        Validators.compose([Validators.required, Validators.maxLength(50)])
      ],
      phone: [null, Validators.compose([Validators.maxLength(50)])],
      id_dispositivo: [null, Validators.compose([])]
    });

    this.existFamily = this.formBuilder.group({
      country: [null, Validators.compose([Validators.required])],
      //typeDocument: [null, Validators.compose([Validators.required])],
      document: [
        null,
        Validators.compose([Validators.required, Validators.maxLength(20)])
      ]
    });

    this.updateDataPersonMain = this.formBuilder.group({
      name: [
        null,
        Validators.compose([
          Validators.required,
          Validators.maxLength(20),
          Validators.pattern(
            /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]*$/)
        ])
      ],
      surname: [
        null,
        Validators.compose([
          Validators.required,
          Validators.maxLength(20),
          Validators.pattern(
            /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]*$/
          )
        ])
      ],
      second_surname: [null, Validators.compose([
        Validators.maxLength(20),
        Validators.pattern(
          /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]*$/
        )
      ])
      ],
      gender: [null, Validators.compose([])],



      //gender: [null, Validators.compose([Validators.required])],
      type_document: [null, Validators.compose([Validators.required])],
      document: [
        null,
        Validators.compose([Validators.required, Validators.maxLength(15)])
      ],
      birthdate: [null, Validators.compose([Validators.required])],
      country: [null, Validators.compose([Validators.required])],
      department: [null, Validators.compose([Validators.required])],
      municipality: [null, Validators.compose([Validators.required])],
      vereda: [null, Validators.compose([])]
    });
  }

  static onlyNumber(control: FormControl): any {

    if (control.value % 1 !== 0) {
      return {
        "not a whole number": true
      };
    }

    return null;
  }


  loadFormProfesional() {
    let profesionales = this.infoPac["get_datos_asist_profesional"];
    this.dataProfesional = [];

    /*

    $request->nombreprofesional (varchar 50)
    $request->apellidosprofesional (varchar 50)
    $request->tipo_prof (int) opcional
    $request->profesion (int) opcional
    $request->especialidad (int) opcional
    $request->direccionprofesional (varchar 20) opcional
    $request->paisProfesional (int) opcional
    $request->estadoProfesional (int) opcional
    $request->ciudadProfesional (int) opcional
    $request->telefonofijoprofesional (varchar 20) opcional
    $request->celularprofesional (varchar 20)
    $request->emailprofesional (varchar 30) opcional

    */
    console.log(profesionales)
    for (let i = 0; i < profesionales.length; i++) {
      this.arrayModel[i] = {
        country: profesionales[i]["pais_fk_res"],
        department: profesionales[i]["est_fk_res"],
        municipality: profesionales[i]["cda_fk_res"]
      };

      let params = {
        pac_fk_midis: profesionales[i]["pac_fk_midis"],
        id_pk: profesionales[i]["id_pk"],
        mask_celular: profesionales[i].get_pais.mask_celular,
        mask_fijo: profesionales[i].get_pais.mask_fijo,
        //id_pk: profesionales[i]["id_pk"],
        nombreprofesional: profesionales[i]["nombres"],
        apellidosprofesional: profesionales[i]["apellidos"],
        emailprofesional: profesionales[i]["email1"],
        direccionprofesional: profesionales[i]["direccion"],
        paisProfesional: profesionales[i]["get_pais"]["id_pk"],
        paisProfesionalNombre: profesionales[i]["get_pais"]["nombre"],
        estadoProfesional: profesionales[i]["get_estado"]["id_pk"],
        estadoProfesionalNombre: profesionales[i]["get_estado"]["nombre"],
        ciudadProfesional: profesionales[i]["get_ciudad"]["id_pk"],
        ciudadProfesionalNombre: profesionales[i]["get_ciudad"]["nombre"],
        celularprofesional:
          profesionales[i]["tlf_celular"] == null
            ? ""
            : profesionales[i]["tlf_celular"],
        celularprofesionalIndicativo:
          profesionales[i]["get_pais"]["indicativo"],
        telefonofijoprofesional:
          profesionales[i].tlf_domicilio != null
            ? profesionales[i].tlf_domicilio
            : "",
        telefonofijoprofesionalIndicativo:
          profesionales[i]["get_ciudad"]["indicativo"],
        profesion_nombre:
          profesionales[i].get_sub_tipo_profesional != null
            ? profesionales[i].get_sub_tipo_profesional.nombre
            : "",
        profesion:
          profesionales[i].get_sub_tipo_profesional != null
            ? profesionales[i].get_sub_tipo_profesional.id_pk
            : "",
        tipo_prof_nombre:
          profesionales[i].get_tipo_profesional != null
            ? profesionales[i].get_tipo_profesional.nombre
            : "",
        tipo_prof:
          profesionales[i].get_tipo_profesional != null
            ? profesionales[i].get_tipo_profesional.id_pk
            : "",
        especialidad_nombre:
          profesionales[i].get_especialidad != null
            ? profesionales[i].get_especialidad.nombre
            : "",
        especialidad:
          profesionales[i].get_especialidad != null
            ? profesionales[i].get_especialidad.id_pk
            : "",
        id_dispositivo:
          profesionales[i]["id_dispositivo"] != null
            ? profesionales[i]["id_dispositivo"]
            : ""
      };

      this.dataProfesional.push(params);
    }
  }

  loadFormFamily() {
    let family = this.infoPac["get_datos_familiares"];
    this.dataFamily = [];
    /*$request->parentesco (int)
      $request->nombrefamiliar (varchar 50)
      $request->apellido1familiar (varchar 20)
      $request->apellido2familiar (varchar 20) opcional
      $request->telefonofijofamiliar (varchar 20) opcional
      $request->celular (varchar 20)
      $request->emailfamiliar (varchar 30) opcional
      $request->paisFamiliarcre (int)
      $request->estadoFamiliarcre (int)
      $request->ciudadFamiliarcre (int)
      $request->direccionfamiliar (varchar 20) opcional*/

    for (let i = 0; i < family.length; i++) {
      let params = {
        pac_fk_midis: family[i]["pac_fk_midis"],
        id_pk: family[i]["id_pk"],
        mask_celular: family[i].get_pais.mask_celular,
        mask_fijo: family[i].get_pais.mask_fijo,
        //id_pk: family[i]["id_pk"],
        parentesco: family[i]["get_parentesco"]["id_pk"],
        parentescoNombre: family[i]["get_parentesco"]["nombre"],
        nombrefamiliar: family[i]["nombres"],
        apellido1familiar: family[i]["apellido1"],
        apellido2familiar:
          family[i]["apellido2"] != null ? family[i]["apellido2"] : "",
        emailfamiliar: family[i]["email"],
        direccionfamiliar: family[i]["direccion"],
        paisFamiliarcre: family[i]["get_pais"]["id_pk"],
        paisFamiliarcreNombre: family[i]["get_pais"]["nombre"],
        estadoFamiliarcre: family[i]["get_estado"]["id_pk"],
        estadoFamiliarcreNombre: family[i]["get_estado"]["nombre"],
        ciudadFamiliarcre: family[i]["get_ciudad"]["id_pk"],
        ciudadFamiliarcreNombre: family[i]["get_ciudad"]["nombre"],
        celular: family[i]["celular"] == null ? "" : family[i]["celular"],
        celularIndicativo: family[i]["get_pais"]["indicativo"],
        telefonofijofamiliar:
          family[i]["telefono"] == null ? "" : family[i]["telefono"],
        telefonofijofamiliarIndicativo: family[i]["get_ciudad"]["indicativo"],
        id_dispositivo:
          family[i]["id_dispositivo"] != null ? family[i]["id_dispositivo"] : ""
      };

      this.dataFamily.push(params);
    }
  }

  filterItems(searchTerm, cities) {
    if (searchTerm) {
      return cities.filter(item => {
        return item.nombre.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
      });
    }
  }

  clearTimeOut() {
    clearTimeout(this.timeOut);
  }


  getUser(id_paciente: number) {
    let params = { "id_paciente": id_paciente };
    this.ServicesProvider.createLoader("Actualizando.. por favor espere");
    this.ServicesProvider
      .apiPost(params, SERVICES.CONSULTAR_PACIENTE_API_SERV)
      .then(state => {

        //this.ServicesProvider.loading.dismiss();
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        if (state["status"] == 404 || state["status"] == 500) {
          this.timeOutControl = false;
          this.ServicesProvider.toast(MESSAGES.services.error);
        } else if (state["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
          this.timeOutControl = false;
        } else {
          let respuesta = JSON.parse(state["_body"]);
          /* this.ServicesProvider.toast(
             MESSAGES.updateProfile.update.success,
             "successToast"
           );*/

          this.loadProgress = respuesta.porcentaje_avance;
          this.infoPac = respuesta;
          console.log('INFO PAC, PROVIDER PROFILE\n' +this.infoPac)
          this.loadFormProfesional();
          this.loadFormFamily();

          localStorage.setItem("user", JSON.stringify(respuesta));
        }
      })
      .catch(err => {
        //this.ServicesProvider.loading.onDidDismiss(() => {
        this.ServicesProvider.closeLoader();
        this.clearTimeOut();
        this.timeOutControl = false;
        this.ServicesProvider.toast(MESSAGES.services.error);
        //});
      });
  }


}
