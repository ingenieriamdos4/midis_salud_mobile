
export const WIZZARD_SPA = {
  "registro":
      [
        {
          "class_contenedor":"contenedor_primer_slide",
          "icon":null,
          "class_icon":null,
          "icon_medio":"icon-qr-margin",
          "icon_medio_class":"icon_medio raya_qr",
          "titulo_wizzard":"¡Bienvenido a Midis app Salud!",
          "class_titulo_wizzard":"titulo_wizzard w_73 no_border mb_20p mt_0p",
          "subtitulo_wizzard":null,
          "class_subtitulo_wizzard":null,
          "texto_wizzard":"Ya hemos generado tu código QR que te permitirá llevar a todos lados tu información básica de salud y acceder a ella cuando sea necesario.",
          "class_texto_wizzard":"texto_wizzard_1 texto_wizzard mt_13p",
          "subtexto_wizzard":null,
          "class_subtexto_wizzard":null,
          "background_image":"./assets/imgs/Bienvenida1.jpg"
        },
        {
          "class_contenedor":"contenedor_primer_slide",
          "icon":"icon-almacenamiento-dos",
          "class_icon":"icon_wizzard  font-177",
          "titulo_wizzard":  ("Podrás guardar copia de: "),
          "class_titulo_wizzard":"titulo_wizzard  border_bottom w_66 m_0",
          "subtitulo_wizzard":null,
          "class_subtitulo_wizzard":null,
          "texto_wizzard":("<div class='margin-chulo-rigth h3v'><ion-icon class='ion-md-checkmark chulos_wizzard_acPerfil'></ion-icon></div>Documento de identidad.<br><div class='margin-chulo-rigth h3v'><ion-icon class='ion-md-checkmark chulos_wizzard_acPerfil'></ion-icon></div>Seguridad social.<br><div class='margin-chulo-rigth h3v'><ion-icon class='ion-md-checkmark chulos_wizzard_acPerfil'></ion-icon></div>Seguridad laboral y otros."),
          "class_texto_wizzard":"texto_wizzard_1 texto_wizzard mt_13p justify",
          "subtexto_wizzard":null,
          "class_subtexto_wizzard":null,          
          "background_image":"./assets/imgs/Bienvenida2.jpg"
        }

      ],
      "actualizar_perfil":[
        {
          "class_contenedor":"contenedor_primer_slide",
          "icon":"icon-actualizar-perfil",
          "class_icon":"icon_wizzard",
          "titulo_wizzard":"Actualizar perfil",
          "class_titulo_wizzard":"titulo_wizzard w_60p",
          "subtitulo_wizzard":null,
          "class_subtitulo_wizzard":null,
          "texto_wizzard":"Mantén tus datos actualizados, tómalo como una herramienta preventiva.",
          "class_texto_wizzard":"texto_wizzard_1",
          "subtexto_wizzard":null,
          "class_subtexto_wizzard":null,
          "background_image":"./assets/imgs/actualizar_perfil.jpg"
        },
        {
          "class_contenedor":"contenedor_primer_slide",
          "icon":null,
          "class_icon":null,
          "titulo_wizzard":  ("Podrás  <b>registrar</b>, <b>editar</b> y <b>visualizar</b> datos como:"),
          "class_titulo_wizzard":"titulo_wizzard",
          "subtitulo_wizzard":("<ion-icon class='ion-md-checkmark chulos_wizzard_acPerfil'></ion-icon> Asistencia médica.<br><ion-icon class='ion-md-checkmark chulos_wizzard_acPerfil'></ion-icon> Contactos familiares.<br><ion-icon class='ion-md-checkmark chulos_wizzard_acPerfil'></ion-icon> Contactos médicos.<br><ion-icon class='ion-md-checkmark chulos_wizzard_acPerfil'></ion-icon> Entre otros."),
          "class_subtitulo_wizzard":"subtitulo_wizzard",
          "texto_wizzard":("<br>Estos datos harán parte de tu <b>información básica</b>, útil a la hora que escaneen tu código QR."),
          "class_texto_wizzard":"texto_wizzard_1",
          "subtexto_wizzard":null,
          "class_subtexto_wizzard":null,          
          "background_image":"./assets/imgs/actualizar_perfil2.jpg"
        },
        {
          "class_contenedor":"contenedor_primer_slide",
          "icon":null,
          "class_icon":"icon_wizzard",
          "titulo_wizzard":"¡Recuerda!",
          "class_titulo_wizzard":"titulo_wizzard  w_40p",
          "subtitulo_wizzard":null,
          "class_subtitulo_wizzard":null,
          "texto_wizzard":"Estar actualizando tu perfil, te permitirá recibir una atención oportuna.",
          "class_texto_wizzard":"texto_wizzard_1",
          "subtexto_wizzard":null,
          "class_subtexto_wizzard":null,          
          "background_image":"./assets/imgs/actualizar_perfil3.jpg"
        }

      ],
      "autorizar_acceso":[
        {
          "class_contenedor":"contenedor_primer_slide",
          "icon":"icon-autor-prof",
          "class_icon":"icon_wizzard ",
          "titulo_wizzard":"Autorización",
          "class_titulo_wizzard":"titulo_wizzard w_53p mt_2p",
          "subtitulo_wizzard":null,
          "class_subtitulo_wizzard":null,
          "texto_wizzard":"Recuerda tu eres  el dueño de tu información y a través del código QR  autorizas el acceso.",
          "class_texto_wizzard":"texto_wizzard_1",
          "subtexto_wizzard":null,
          "class_subtexto_wizzard":null,
          "background_image":"./assets/imgs/autorizacion.jpg"
        }
      ],
      "codigo_qr":[
        {
          "class_contenedor":"contenedor_primer_slide",
          "icon":"icon-qr-margin",
          "class_icon":"icon_wizzard font-89",
          "titulo_wizzard":"Código QR",
          "class_titulo_wizzard":"titulo_wizzard w_73p mt_2p",
          "subtitulo_wizzard":null,
          "class_subtitulo_wizzard":null,
          "texto_wizzard":"Puedes escanear el código QR  de otro usuario de Midis app Salud.  En caso de urgencia o emergencia podrás auxiliarlo al llamar a sus contactos familiares y médicos registrados, así como conocer sus datos de salud básicos.",
          "class_texto_wizzard":"texto_wizzard_1 w_75p",
          "subtexto_wizzard":null,
          "class_subtexto_wizzard":null,
          "background_image":"./assets/imgs/Bienvenida1.jpg"
        }
      ],
      "codigo_qr_midis":[
        {
          "class_contenedor":"contenedor_primer_slide",
          "icon":"icon-qr-margin",
          "class_icon":"icon_wizzard font-89",
          "titulo_wizzard":"Código QR",
          "class_titulo_wizzard":"titulo_wizzard w_73p mt_2p",
          "subtitulo_wizzard":null,
          "class_subtitulo_wizzard":null,
          "texto_wizzard":" Otras personas o entidades de socorro podrán escanear tu código QR para visualizar tus datos básicos de alerta y contactos.",
          "class_texto_wizzard":"texto_wizzard_1 w_76p",
          "subtexto_wizzard":null,
          "class_subtexto_wizzard":null,
          "texto_options": ["Es importante que descargues tu código QR y llevarlo siempre en un lugar visible."],
          "background_image":"./assets/imgs/ambulancia.jpg"
        }
      ], 

      "datos_salud":[
        {
          "class_contenedor":"contenedor_primer_slide",
          "icon":"icon-datos-salud",
          "class_icon":"icon_wizzard",
          "titulo_wizzard":"Datos de Salud",
          "class_titulo_wizzard":"titulo_wizzard w_60p mt_2p",
          "subtitulo_wizzard":null,
          "class_subtitulo_wizzard":null,
          "texto_wizzard":"Es importante completar tus datos de salud. Puedes hacer control diario o periódico y obtener reportes.",
          "class_texto_wizzard":"texto_wizzard_1 w_73p",
          "subtexto_wizzard":null,
          "class_subtexto_wizzard":null,
          "background_image":"./assets/imgs/datos_salud.jpg"
        }
      ],    
      "emergencia":[
        {
          "class_contenedor":"contenedor_primer_slide",
          "icon":"icon-emergencia",
          "class_icon":"icon_wizzard",
          "titulo_wizzard":"¿Tienes una emergencia?",
          "class_titulo_wizzard":"titulo_wizzard w_60p mt_2p",
          "subtitulo_wizzard":null,
          "class_subtitulo_wizzard":null,
          "texto_wizzard":"Recuerda mantener activado el GPS de tu móvil para que identifiquen rápidamente el lugar donde te encuentras.",
          "class_texto_wizzard":"texto_wizzard_1 w_73p",
          "subtexto_wizzard":null,
          "class_subtexto_wizzard":null,
          "background_image":"./assets/imgs/emergencia_2.jpg"
        },
        {
          "class_contenedor":"contenedor_primer_slide",
          "icon":null,
          "class_icon":null,
          "titulo_wizzard":"¡Recuerda!",
          "class_titulo_wizzard":"titulo_wizzard w_60p mt_2p",
          "subtitulo_wizzard":null,
          "class_subtitulo_wizzard":null,
          "texto_wizzard":"Mantén actualizados tus contactos familiares y profesionales que puedan brindar información en caso de emergencias.",
          "class_texto_wizzard":"texto_wizzard_1 w_73p",
          "subtexto_wizzard":null,
          "class_subtexto_wizzard":null,
          "background_image":"./assets/imgs/emergencia1.jpg"
        }

      ],  
      "notificacion":[
        {
          "class_contenedor":"contenedor_primer_slide",
          "icon":"icon-notificaciones",
          "class_icon":"icon_wizzard",
          "titulo_wizzard":"Notificaciones",
          "class_titulo_wizzard":"titulo_wizzard w_60p  mt_2p",
          "subtitulo_wizzard":null,
          "class_subtitulo_wizzard":null,
          "texto_wizzard":"Aquí podrás recibir diferentes notificaciones y recordatorios, tales como:",
          "class_texto_wizzard":"texto_wizzard_1 w_75p",
          "subtexto_wizzard":null,
          "class_subtexto_wizzard":null,
          "texto_options": ["Resultados cargados.","Recomendaciones.","Cuidados.","Entre otros."],
          "background_image":"./assets/imgs/reportes.jpg"
        },
        {
          "class_contenedor":"contenedor_primer_slide",
          "icon":null,
          "class_icon":null,
          "titulo_wizzard":"No olvides!",
          "class_titulo_wizzard":"titulo_wizzard w_60p  mt_2p",
          "subtitulo_wizzard":null,
          "class_subtitulo_wizzard":null,
          "texto_wizzard":"Es importante que diligencies la información de las cuatro secciones principales, así tu información básica estará completa.",
          "class_texto_wizzard":"texto_wizzard_1 w_75p",
          "subtexto_wizzard":null,
          "class_subtexto_wizzard":null,
          "icons_aditional": ["icon-actualizar-perfil","icon-antecedentes_a","icon-datos-salud","icon-doc-personales"],
          "background_image":"./assets/imgs/notificacion.jpg"
        }
      ],  
      "mis_documentos":[
        {
          "class_contenedor":"contenedor_primer_slide",
          "icon":"icon-doc-personales",
          "class_icon":"icon_wizzard",
          "titulo_wizzard":"Mis documentos",
          "class_titulo_wizzard":"titulo_wizzard w_60p  mt_2p",
          "subtitulo_wizzard":null,
          "class_subtitulo_wizzard":null,
          "texto_wizzard":"<div class='justify'>Podrás adjuntar y guardar los documentos que respalden  tu información de salud como:</div>",
          "class_texto_wizzard":"texto_wizzard_1 w_75p",
          "subtexto_wizzard":null,
          "class_subtexto_wizzard":null,
          "texto_options": ["Documento de identidad.","Seguridad social.","Seguridad laboral.","Entre otros."],          
          "background_image":"./assets/imgs/mis_documentos.jpg"
        }
      ],
      "antecedente":[
        {
          "class_contenedor":"contenedor_primer_slide",
          "icon":"icon-antec",
          "class_icon":"icon_wizzard",
          "titulo_wizzard":"Antecedentes",
          "class_titulo_wizzard":"titulo_wizzard w_60p",
          "subtitulo_wizzard":null,
          "class_subtitulo_wizzard":null,
          "texto_wizzard":"Carga tus antecedentes de salud: Alergias, enfermedades, medicación  actual y otros.",
          "class_texto_wizzard":"texto_wizzard_1",
          "subtexto_wizzard":null,
          "class_subtexto_wizzard":null,
          "background_image":"./assets/imgs/antecedentes1.jpg"
        },
        {
          "class_contenedor":"contenedor_primer_slide",
          "icon":null,
          "class_icon":null,
          "titulo_wizzard":  ("Mantener tus antecedentes actualizados permitirá ahorrar tiempo ante cualquier atención o situación de urgencia."),
          "class_titulo_wizzard":"titulo_wizzard_border_top",
          "subtitulo_wizzard":null,
          "class_subtitulo_wizzard":null,
          "texto_wizzard":null,
          "class_texto_wizzard":null,
          "subtexto_wizzard":null,
          "class_subtexto_wizzard":null,          
          "background_image":"./assets/imgs/antecedentes2.jpg"
        },
        {
          "class_contenedor":"contenedor_primer_slide",
          "icon":null,
          "class_icon":null,
          "titulo_wizzard":"¡Recuerda!",
          "class_titulo_wizzard":"titulo_wizzard  w_53p",
          "subtitulo_wizzard":null,
          "class_subtitulo_wizzard":null,
          "texto_wizzard":"Podrás clasificar tus antecedentes como <b>privados o públicos</b>. <br><br> Al clasificarlos como públicos, las personas que escaneen tu código QR podrán visualizarlos.",
          "class_texto_wizzard":"texto_wizzard_1 w_75p",
          "subtexto_wizzard":null,
          "class_subtexto_wizzard":null,    
          "icons_aditional": ["icon-alergicos","icon-discapacidad","icon-medicacion_actual"],
          "background_image":"./assets/imgs/antecedentes3.jpg"
        }
      ],
      "ficha_unica":[
        {
          "class_contenedor":"contenedor_primer_slide",
          "icon":"icon-Ficha-medica",
          "class_icon":"icon_wizzard",
          "titulo_wizzard":"Ficha médica única digital ",
          "class_titulo_wizzard":"titulo_wizzard w_66p",
          "subtitulo_wizzard":null,
          "class_subtitulo_wizzard":null,
          "texto_wizzard":"<div class='margin-chulo-rigth h12v'><ion-icon class='ion-md-checkmark chulos_wizzard_acPerfil'></ion-icon></div> Aquí se resumen todos tus datos de salud prioritarios, podrás visualizarlos, descargarlos y compartirlos.<br><br><div class='margin-chulo-rigth'><ion-icon class='ion-md-checkmark chulos_wizzard_acPerfil'></ion-icon></div>Además muestra tu estado de salud actual.",
          "class_texto_wizzard":"texto_wizzard_1 w_90p justify fs15" ,
          "subtexto_wizzard":null,
          "class_subtexto_wizzard":null,
          "background_image":"./assets/imgs/ficha_medica.jpg"
        }
      ],
      "mis_archivos_salud":[
        {
          "class_contenedor":"contenedor_primer_slide",
          "icon":"icon-doc_previos",
          "class_icon":"icon_wizzard",
          "titulo_wizzard":"Mis archivos de salud",
          "class_titulo_wizzard":"titulo_wizzard w_72p",
          "subtitulo_wizzard":null,
          "class_subtitulo_wizzard":null,
          "texto_wizzard":"Podrás guardar documentos como: resultados de laboratorio, radiografías, imágenes e informes médicos que haz obtenido a lo largo de tu vida.",
          "class_texto_wizzard":"texto_wizzard_1 w_72p",
          "subtexto_wizzard":null,
          "class_subtexto_wizzard":null,
          "background_image":"./assets/imgs/mis_archivos_salud1.jpg"
        },
        {
          "class_contenedor":"contenedor_primer_slide",
          "icon":null,
          "class_icon":null,
          "titulo_wizzard":  ("Clasifícalos teniendo en cuenta los módulos sugeridos por Midis app Salud."),
          "class_titulo_wizzard":"titulo_wizzard_border_top w_75p mt_4p",
          "subtitulo_wizzard":null,
          "class_subtitulo_wizzard":null,
          "texto_wizzard":null,
          "class_texto_wizzard":null,
          "subtexto_wizzard":null,
          "class_subtexto_wizzard":null,          
          "background_image":"./assets/imgs/mis_archivos_salud2.jpg"
        },
        {
          "class_contenedor":"contenedor_primer_slide",
          "icon":null,
          "class_icon":null,
          "titulo_wizzard":"¡Recuerda!",
          "class_titulo_wizzard":"titulo_wizzard  w_53p",
          "subtitulo_wizzard":null,
          "class_subtitulo_wizzard":null,
          "texto_wizzard":"Las instituciones o profesionales que aún no utilizan <b>Midis app Salud</b> también podrán cargarte resultados. Somos conscientes de la importancia de tener toda tu información alojada en un solo lugar.",
          "class_texto_wizzard":"texto_wizzard_1 w_75p",
          "subtexto_wizzard":null,
          "class_subtexto_wizzard":null,          
          "background_image":"./assets/imgs/mis_archivos_salud3.jpg"
        }
      ],
      "reportes":[
        {
          "class_contenedor":"contenedor_primer_slide",
          "icon":"icon-reportes",
          "class_icon":"icon_wizzard",
          "titulo_wizzard":"Reportes ",
          "class_titulo_wizzard":"titulo_wizzard w_60p",
          "subtitulo_wizzard":null,
          "class_subtitulo_wizzard":null,
          "texto_wizzard":"A través de esta sección podrás filtrar, visualizar y hacer seguimiento a la variación de cualquiera de tus datos de salud.",
          "class_texto_wizzard":"texto_wizzard_1 w_80p" ,
          "subtexto_wizzard":null,
          "class_subtexto_wizzard":null,
          "background_image":"./assets/imgs/reportes.jpg"
        }
      ],
      "midis_salud":[
        {
          "class_contenedor":"contenedor_primer_slide",
          "icon":"icon-midis_contorno",
          "class_icon":"icon_wizzard",
          "titulo_wizzard":"Midis Salud",
          "class_titulo_wizzard":"titulo_wizzard w_60p",
          "subtitulo_wizzard":null,
          "class_subtitulo_wizzard":null,
          "texto_wizzard":null,
          "class_texto_wizzard":"texto_wizzard_1 w_81p 15_1_px" ,
          "subtexto_wizzard":null,
          "class_subtexto_wizzard":null,
          "texto_options":["Permite al profesional médico de tu confianza que <b>valide e ingrese</b> tu información.", "Podrás visualizar de manera general y detallada todas tus consultas y resultados."],
          "background_image":"./assets/imgs/midis_salud.jpg"
        }
      ],
      "app_salud":[
        {
          "class_contenedor":"contenedor_primer_slide",
          "icon":"icon-app_salud",
          "class_icon":"icon_wizzard",
          "titulo_wizzard":"App Salud",
          "class_titulo_wizzard":"titulo_wizzard w_60p",
          "subtitulo_wizzard":null,
          "class_subtitulo_wizzard":null,
          "texto_wizzard":"<div class='justify'>Aquí podrás visualizar y cargar los resultados de tus estudios médicos realizados como: </div> <br> <div class='margin-chulo-rigth h3v'><ion-icon class='ion-md-checkmark chulos_wizzard_acPerfil'></ion-icon></div><div class='justify'>Imágenes diagnósticas.</div> <br> <div class='margin-chulo-rigth h3v'><ion-icon class='ion-md-checkmark chulos_wizzard_acPerfil'></ion-icon></div><div class='justify'>Informe de laboratorios.</div> <br> <div class='margin-chulo-rigth h3v'><ion-icon class='ion-md-checkmark chulos_wizzard_acPerfil'></ion-icon></div><div class='justify'>Otros estudios.</div>",
          "class_texto_wizzard":"texto_wizzard_1 w_82p",
          "subtexto_wizzard":null,
          "class_subtexto_wizzard":null,
          "background_image":"./assets/imgs/app_salud.jpg"
        },
        {
          "class_contenedor":"contenedor_primer_slide",
          "icon":"icon-recomendaciones",
          "class_icon":"icon_wizzard",
          "titulo_wizzard": null,
          "class_titulo_wizzard":null,
          "subtitulo_wizzard":null,
          "class_subtitulo_wizzard":null,
          "texto_wizzard": "También encontrarás las recomendaciones de preparación y cuidado de los diferentes procedimientos que te van a realizar." ,
          "class_texto_wizzard":"texto_wizzard_1 w_82p",
          "subtexto_wizzard":null,
          "class_subtexto_wizzard":null,          
          "background_image":"./assets/imgs/help_app_salud.jpg"
        }
      ],
     "almacenamiento":[
        {
          "class_contenedor":"contenedor_primer_slide",
          "icon":"icon-almacenamiento",
          "class_icon":"icon_wizzard font-117",
          "titulo_wizzard":"Almacenamiento",
          "class_titulo_wizzard":"titulo_wizzard w_65p mt_0",
          "subtitulo_wizzard":null,
          "class_subtitulo_wizzard":null,
          "texto_wizzard":null,
          "class_texto_wizzard":"texto_wizzard_1 w_82p",
          "subtexto_wizzard":null,
          "class_subtexto_wizzard":null,
          "texto_options": ["Guarda tu información en la nube para que dispongas de ella cuando la necesites en cualquier momento y lugar.","Otras entidades responsables de tu salud, también podrán adjudicarte espacio para guardar tu información."],
          "icons_aditional_down": ["icon-pacientes","icon-profesionales","icon-instituciones"],
          "background_image":"./assets/imgs/almacenamiento.jpg"
        }
      ],
      "configuracion":[
        {
          "class_contenedor":"contenedor_primer_slide",
          "icon":"icon-contrasena",
          "class_icon":"icon_wizzard",
          "titulo_wizzard":"Configuraciones ",
          "class_titulo_wizzard":"titulo_wizzard w_65p mt_0",
          "subtitulo_wizzard":null,
          "class_subtitulo_wizzard":null,
          "texto_wizzard":"<div class='margin-chulo-rigth h12v'><ion-icon class='ion-md-checkmark chulos_wizzard_acPerfil'></ion-icon></div><div class='justify'>Desde aquí podrás configurar la seguridad de los datos que serán visibles al momento de escanear tu código QR.</div><br><div class='margin-chulo-rigth h8v'><ion-icon class='ion-md-checkmark chulos_wizzard_acPerfil'></ion-icon></div><div class='justify'>Podrás cambiar tu contraseña o eliminar tu cuenta.</div>",
          "class_texto_wizzard":"texto_wizzard_1 w_83p" ,
          "subtexto_wizzard":null,
          "class_subtexto_wizzard":null,
          "background_image":"./assets/imgs/configuracion.jpg"
        }
      ],
      "crear_cuenta":[
        {
          "class_contenedor":"contenedor_primer_slide",
          "icon":"icon-crear-cuenta",
          "class_icon":"icon_wizzard",
          "titulo_wizzard":"Crear cuenta",
          "class_titulo_wizzard":"titulo_wizzard w_60p mt_0",
          "subtitulo_wizzard":null,
          "class_subtitulo_wizzard":null,
          "texto_wizzard":"Elige tener tus datos seguros y disponibles desde cualquier lugar y momento que lo necesites. ¡Previene y salva tu vida!",
          "class_texto_wizzard":"texto_wizzard_1 w_72p" ,
          "subtexto_wizzard":null,
          "class_subtexto_wizzard":null,
          "background_image":"./assets/imgs/crear_cuenta.jpg"
        }
      ],
      "home":[
        {
          "class_contenedor":"contenedor_primer_slide",
          "icon":"icon-midis_hoy",
          "class_icon":"icon_wizzard",
          "titulo_wizzard":"Midis hoy",
          "class_titulo_wizzard":"titulo_wizzard w_60p mt_0",
          "subtitulo_wizzard":null,
          "class_subtitulo_wizzard":null,
          "texto_wizzard":"En tu <b>Midis hoy</b> encontrarás las principales funciones que te permitirán llevar el control de tu salud.<br><br><br>En tu primera notificación te recomendamos completar tu <b>información básica</b>, útil para facilitar tu asistencia en caso de urgencia y agilizar atenciones futuras.",
          "class_texto_wizzard":"texto_wizzard_1", 
          "subtexto_wizzard":null,
          "class_subtexto_wizzard":null,
          "background_image":"./assets/imgs/help_hoy_1.jpg"
        },
        {
          "class_contenedor":"contenedor_primer_slide",
          "icon":null,
          "class_icon":null,
          "titulo_wizzard":"Primer paso",
          "class_titulo_wizzard":"titulo_wizzard midis_hoy w_60p mt_0",
          "subtitulo_wizzard":null,
          "class_subtitulo_wizzard":null,
          "texto_wizzard":null,
          "class_texto_wizzard":null , 
          "subtexto_wizzard":null,
          "class_subtexto_wizzard":null,
          "texto_options":["Gestiona tus datos de asistencia médica, contactos familiares y médicos, útiles al momento de asistirte.","Configura la privacidad de tus datos y decide qué información muestras u ocultas en caso de requerir asistencia de emergencia."],
          "class_texto_options":"options_hoy",
          "background_image":"./assets/imgs/help_hoy_2.jpg"
        },
        {
          "class_contenedor":"contenedor_primer_slide",
          "icon":null,
          "class_icon":null,
          "titulo_wizzard":"Segundo paso",
          "class_titulo_wizzard":"titulo_wizzard midis_hoy w_60p mt_0",
          "subtitulo_wizzard":null,
          "class_subtitulo_wizzard":null,
          "texto_wizzard":"Registra tus antecedentes de salud como:<br> Alergias, medicación que tomas actualmente, posibles enfermedades, entre otros. <br><br>Decide si mostrar u ocultar esta información en caso de emergencia.",
          "class_texto_wizzard":"texto_wizzard_1 options_hoy" , 
          "subtexto_wizzard":null,
          "class_subtexto_wizzard":null,
          "background_image":"./assets/imgs/help_hoy_3.jpg"
        },
        {
          "class_contenedor":"contenedor_primer_slide",
          "icon":null,
          "class_icon":null,
          "titulo_wizzard":"Tercer paso",
          "class_titulo_wizzard":"titulo_wizzard midis_hoy w_60p mt_0",
          "subtitulo_wizzard":null,
          "class_subtitulo_wizzard":null,
          "texto_wizzard":null,
          "class_texto_wizzard":null , 
          "subtexto_wizzard":null,
          "class_subtexto_wizzard":null,
          "texto_options":["Actualiza y lleva un registro diario o periódico de tus datos de salud.","Podrás adjuntar copia de los documentos que respalden tu información de salud como: documento de identidad, seguridad social, seguridad laboral, entre otros."],
          "class_texto_options":"options_hoy",
          "background_image":"./assets/imgs/help_hoy_4.jpg"
        }
      ],
      "token":[
        {
          "class_contenedor":"contenedor_primer_slide",
          "icon":"icon-crear-cuenta",
          "class_icon":"icon_wizzard",
          "titulo_wizzard":"Crear cuenta",
          "class_titulo_wizzard":"titulo_wizzard w_60p mt_0",
          "subtitulo_wizzard":null,
          "class_subtitulo_wizzard":null,
          "texto_wizzard":"Para tener acceso a las funcionalidades de <b>Midis app Salud</b> debes registrarte y tener una cuenta propia.",
          "class_texto_wizzard":"texto_wizzard_1 w_72p" ,
          "subtexto_wizzard":null,
          "class_subtexto_wizzard":null,
          "background_image":"./assets/imgs/crear_cuenta.jpg"
        },
        {
          "class_contenedor":"contenedor_primer_slide",
          "icon":"icon-qr",
          "class_icon":"icon_wizzard",
          "titulo_wizzard":"Código QR",
          "class_titulo_wizzard":"titulo_wizzard w_60p mt_0",
          "subtitulo_wizzard":null,
          "class_subtitulo_wizzard":null,
          "texto_wizzard":"<div><div div class='margin-chulo-rigth h4v'><ion-icon class='ion-md-checkmark chulos_wizzard_acPerfil'></ion-icon></div><div class='justify'>Con <b>Midis app Salud</b> generarás un código QR que te permitirá llevar tu información básica de salud y acceder a ella cuando la necesites.</div></div><br><br><div><div class='margin-chulo-rigth'><ion-icon class='ion-md-checkmark chulos_wizzard_acPerfil'></ion-icon></div><div class='justify'>Guarda tu información de salud de forma ordenada y facilita tus atenciones médicas futuras.</div></div>",
          "class_texto_wizzard":"texto_wizzard_1 w_72p" ,
          "subtexto_wizzard":null,
          "class_subtexto_wizzard":null,
          "background_image":"./assets/imgs/autorizacion.jpg"
        },
        {
          "class_contenedor":"contenedor_primer_slide",
          "icon":null,
          "class_icon":null,
          "titulo_wizzard":null,
          "class_titulo_wizzard":null,
          "subtitulo_wizzard":null,
          "class_subtitulo_wizzard":null,
          "texto_wizzard":"<div> <p style='border: #EF6D34 1px solid; color: #EF6D34' text-wrap>SI <b>NO</b> TIENES UN CÓDIGO DE REGISTRO</p> <div div class='margin-chulo-rigth h4v'><ion-icon class='ion-md-checkmark chulos_wizzard_acPerfil'></ion-icon></div><div class='justify'>Pulsa el botón <b>Obtener un código de registro</b></div><div class='margin-chulo-rigth h4v'><ion-icon class='ion-md-checkmark chulos_wizzard_acPerfil'></ion-icon></div><div class='justify'>Accede a <b>PayU</b> y selecciona la opción de pago de tu preferencia.</div><div class='margin-chulo-rigth h4v'><ion-icon class='ion-md-checkmark chulos_wizzard_acPerfil'></ion-icon></div><div class='justify'>Tu código de registro será enviado al email y teléfono celular que proporcionaste</div></div> <br><br> <div> <p style='border: #1E8AC2 1px solid; color: #1E8AC2' text-wrap>SI <b>YA TIENES</b> UN CÓDIGO DE REGISTRO</p> <div div class='margin-chulo-rigth h4v'><ion-icon class='ion-md-checkmark chulos_wizzard_acPerfil'></ion-icon></div><div class='justify'>Pulsa el botón <b>Tengo un código de registro</b>.</div></div> <div div class='margin-chulo-rigth h4v'><ion-icon class='ion-md-checkmark chulos_wizzard_acPerfil'></ion-icon></div><div class='justify'>Ingresa tu código y una vez validado podrás continuar con la creación de tu cuenta.</div>",
          "class_texto_wizzard":"texto_wizzard_1 w_82p" , 
          "subtexto_wizzard":null,
          "class_subtexto_wizzard":null,
          "background_image":"./assets/imgs/help_crear.jpg"
        }
      ],
      "login": [
        {
          "class_contenedor":"contenedor_primer_slide",
          "icon":null,
          "class_icon":null,
          "titulo_wizzard":null,
          "class_titulo_wizzard":null,
          "subtitulo_wizzard":null,
          "class_subtitulo_wizzard":null,
          "texto_wizzard":"Ayuda 1",
          "class_texto_wizzard":"texto_wizzard_1 w_82p" , 
          "subtexto_wizzard":null,
          "class_subtexto_wizzard":null,
          "background_image":"./assets/imgs/crear_cuenta.jpg"
        },
        {
          "class_contenedor":"contenedor_primer_slide",
          "icon":null,
          "class_icon":null,
          "titulo_wizzard":null,
          "class_titulo_wizzard":null,
          "subtitulo_wizzard":null,
          "class_subtitulo_wizzard":null,
          "texto_wizzard":"Ayuda 2",
          "class_texto_wizzard":"texto_wizzard_1 w_82p" , 
          "subtexto_wizzard":null,
          "class_subtexto_wizzard":null,
          "background_image":"./assets/imgs/crear_cuenta.jpg"
        }
      ]
};
