export const MESSAGES = {
  services: {
    error: "Ha ocurrido un error al momento de hacer la petición",
    timeOut: "Error con el servidor",
    unauthorized: "Su sesión ha expirado, por favor ingrese nuevamente"
  },
  // loading: { message: "Espera por favor...", time: 30000 },
  loading: { message: "", time: 30000 },
  toast: { time: 10000 },
  sqlite: {
    error:
      "Ha ocurrido un error al momento de almacenar los datos en la bd interna",
      error2: 'Error bd interna'
  },
  // createAccount: { success: "Felicitaciones te has registrado exitosamente!" },
  createAccount: { success: "" },
  login: {
    status: {
      state_1: "datos inválidos",
      state_2: "No se ha establecido conexión",
      state_3: "si el nombre de usuario es email y no cumple con la regla de validación",
      state_4: "los campos no cumplen con la regla de validación",
      state_5: "Ha iniciado sesión correctamente"
    }
  },
  downloader: {
    success: "Descarga correcta, verifica en la carpeta 'Descargas' de tu dispositivo.",
    error: "Error en la descarga"
  },
  codQr: {
    message: {
      close: 'Acaba de cancelar el scanner',
      error: 'No se pudo utilizar la cámara de su dispositivo!',
      invalid: 'El código QR que has escaneado es inválido'
    },
    downloader: {
      success: "Descarga correcta, verifica en la carpeta descargas del almacenamiento interno",
      error: "Ha fallado la descarga"
    },
    share: {
      message: "Comparto el codigoQr que tengo en Midis App",
      success: "Compartido con exito",
      error: "Error al momento de compartir"
    },
    prompt: {
      message: 'Escanee el código QR de otra persona en caso de urgencia/emergencia.',
      auth: 'Escanee el código QR del profesional de salud al cual dará acceso'
    }
  },
  updateProfile: {
    update: {
      error: "Ha ocurrido al momento de actualizar tu perfil",
      success: "El perfil ha sido actualizado correctamente!"
    },
    medical:{
      delete: {
        error: "No se puede eliminar el contacto médico",
        success: "El contacto médico se ha eliminado correctamente!"
      }
    },
    family:{
      delete: {
        error: "No se puede eliminar,debes de tener al menos un contacto familiar",
        success: "El contacto médico se ha eliminado correctamente!"
      }      
    },
    family_founded:{
      error: "No se encuentra el contacto familiar registrado",
      success: "Contacto familiar encontrado satisfactoriamente",
      duplicated: "Ya has agregado este contacto familiar"
    },
    medical_founded:{
      error: "No se encuentra el contacto médico registrado",
      success: "Contacto médico encontrado satisfactoriamente",
      duplicated: "Ya has agregado este contacto médico"
    }   

  },
  antecedent: {
    delete: {
      error: "Ha ocurrido un error al momento de eliminar un antecendete",
      success: "El antecedente se ha eliminado correctamente"
    },
    create: {
      error: "Ha ocurrido al momento de agregar el antecedente",
      success: "El antecedente se ha agregado correctamente"
    },
    share: {
      message: "Comparto el archivo antecedente que tengo en Midis App",
      success: "Compartido con exito",
      error: "Error al momento de compartir"
    },
    downloader: {
      success: "Descarga correcta, verifique en la carpeta descargas de su dispositivo.",
      error: "Ha fallado la descarga"
    },
    privacity: {
      success: 'Ha cambiado la privacidad correctamente',
      error: 'Error al cambiar la privacidad'
    }
  },
  fileHealtData: {
    upload:{
      success: 'Las imagenes se han cargado correctamente!',
      error: 'Error al momento de cargar las imagenes'
    },
    delete: {
      error: "Ha ocurrido un error al momento de eliminar el archivo de salud",
      success: "El archivo de salud se ha eliminado correctamente"
    },
    create: {
      error: "Ha ocurrido un error al momento de agregar un archivo de salud",
      success: "El archivo de salud se ha cargado correctamente!"
    },
    share: {
      message: "Comparto el archivo de salud que tengo en Midis App",
      success: "Compartido con exito",
      error: "Error al momento de compartir"
    },
    downloader: {
      success: "Descarga correcta, verifica en la carpeta descargas del almacenamiento interno",
      error: "Ha fallado la descarga"
    }
  },
  documents: {
    upload:{
      success: 'Las imagenes se han cargado correctamente!',
      error: 'Error al momento de cargar las imagenes'
    },
    delete: {
      error: "Ha ocurrido un error al momento de eliminar el archivo de salud",
      success: "El archivo de salud se ha eliminado correctamente"
    },
    create: {
      error: "Ha ocurrido al momento agregar un documento",
      success: "El documento se ha cargado correctamente",
      upload: 'El adjunto o foto sobrepasa los 3000 Kb (3 MB)'
    },
    validate:{
      message1: 'Debe agregar un sólo tipo de archivo; ya sea un documento adjunto o una imagen',
      message2: 'Debe agregar un archivo o una imagen'
    },
    share:{
      success: 'El documento se ha compartido correctamente!',
      error: 'Error al momento de compartir el documento',
      message: "Comparto el documento que tengo en Midis App",
    }
  },
  dataHealth: {
    basic: {
      update:{
        success: 'Sus datos basicos se han actualizado correctamente',
        error: 'Problema al actualizar los datos de salud'
      }
    },
    ginecoobstetricos:{
      update:{
        success: 'Los datos gineco-obstetricos se han actualizado correctamente'
      }
    },
    modal_sangre:{
      cargar_sangre:{
        error:"Ha ocurrido un problema subiendo el archivo"
      }
    }
  },
  configuration:{
    update: {
      success: 'Se ha configurado correctamente',
      error: 'Error al momento de configurar'
    }
  },
  imagePicker:{
    message: {
      error: 'Ha ocurrido un error intentando utilizar la galería del dispositivo'
    }
  },
  camera:{
    message: {
      selected: 'No selecciono ninguna imagen',
      error: 'Ha ocurrido un error intentando utilizar la cámara del dispositivo'
    }

  },
  geolocation:{
    message:{
      error: "Verifique si tiene activada la geolocalización del dispositivo"
    },
    position: {
      error: 'Su ubicación no pudo ser confirmada'
    }
  },
  notification:{
    message:{
      success: 'La emergencia se ha enviado correctamente!',
      error: 'Error al enviar la emergencia'
    }
  },
  callNumber:{
    message:{
      success: 'Exito en la llamada',
      error: 'Ha ocurrido un error al momento de realizar la llamada'
    }
  },
  changePassword:{
    message:{
      success: 'Ha cambiado la contraseña con exito'
    }
  },
  almacenaminto:{
    message: {
      success: 'Se ha enviado la solicitud para la compra correctamente.'
    },
    customRequest:{
      message:{
        success: 'La solicitud de almacemiento se ha enviado correctamente',
        error: 'No se pudo enviar la solicitud'
      }
    }
  },
  autorizationPatien:{
    message:{
      success: 'Ha autorizado permisos al médico con exito!',
      error: 'Error al solicitar permisos'
    }
  },
  inactiveAccount:{
    message:{
      success: 'Ha eliminado la cuenta con exito!',
      error: 'No se pudo eliminar la cuenta'
    }
  },
  medical_record:{
    downloader: {
      success: 'Verifique la descarga, almacenamiento interno ',
    }
  },
  convertBase64ToPdf:{
    message: {
      error: 'Problema con la descarga intentalo de nuevo'
    }
  },
  fileOpener:{
    message:{
      error: 'Hubo un problema al abrir el archivo',
    }
  },
  passwordRecovery:{
    message:{
      success: 'Le hemos enviado al correo un código de verificación',
      error: 'Hubo un problema al verificar el correo'
    },
    verifyCod:{
      success: 'Perfecto, por favor ingrese su nueva contraseña',
    }
  },
  filters:{
    dateMidis:{
      error: 'No es posible filtrar por rango de fechas si no establece un inicio y un fin de dicho rango'
    }
  }
};
