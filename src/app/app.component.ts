import { Component, ViewChild } from "@angular/core";
import {
  Platform,
  MenuController,
  PopoverController,
  App,
  Nav,
  ModalController,
  AlertController
} from "ionic-angular";
import { Events } from 'ionic-angular';
/* Plugins */
import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";

import { SQLite } from "@ionic-native/sqlite";
import { OneSignal } from "@ionic-native/onesignal";

import {
  HomePage,
  IndexCreateAccountPage,
  ValidateTokenPage,
  LoginPage,
  DatosSaludPage,
  DocumentosPage,
  ReportsPage,
  WelcomePage,
  QrMidisPage,
  IndexUpdateProfilePage,
  UpdatePhotoProfilePage,
  PopoverMenuPage,
  EmergenciasPage,
  FichaMedicaPage,
  MidisSaludPage,
  AppSaludPage,
  AlmacenamientoPage,
  SettingsPage,
  MisDocumentosPage,
  ViewDocumentsPage,
  PopoverShareMedicalTabPage,
  PopoverMidisPage,
  EmergencyNotificationPage,

} from "../pages/index.paginas";

import { PushnotificationProvider } from "../providers/pushnotification/pushnotification";
import {
  ListAntecedentPage,
  IndexAntecedentPage
} from "../pages/user/antecedent/index.paginas";
/* Importing services*/
import { PatientProviderService } from "../providers/patient/patient";
import { ServicesProvider } from "../providers/services/services";
import { LoginProviderService } from "../providers/login/login";
import { ConfigPluginsProvider } from "../providers/config-plugins/config-plugins";
import { UpdateProfileProviderService } from "../providers/update-profile/update-profile";

import { TranslateService } from '@ngx-translate/core';

/* App messages */
//import { URL_NIVELES_PERFIL_USUARIO } from "../config/url.servicios";
import { MESSAGES } from "../config/messages";
import { SERVICES } from "../config/url.servicios";

@Component({
  templateUrl: "app.html"
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  createAccount = IndexCreateAccountPage;
  login = LoginPage;
  home = HomePage;
  docs = MisDocumentosPage;
  verDocs = ViewDocumentsPage;
  popoverShare = PopoverShareMedicalTabPage;
  popoverMidis = PopoverMidisPage;  
  listAntecedent = ListAntecedentPage;
  indexAntecedent = IndexAntecedentPage;
  datosSalud = DatosSaludPage;
  documentosPrevios = DocumentosPage;
  reportes = ReportsPage;
  tutorial = WelcomePage;
  qrMidis = QrMidisPage;
  reportarEmergecia = EmergenciasPage;
  indexUpdateProfile = IndexUpdateProfilePage;
  updatePhotoProfile = UpdatePhotoProfilePage;
  fichaMedica = FichaMedicaPage;
  midisSalud = MidisSaludPage;
  appSalud = AppSaludPage;
  almacenamiento = AlmacenamientoPage;
  settings = SettingsPage;
  rootPage: any;
  porcentaje_perfil_temp: string = "assets/imgs/0.png";
  //porcentaje_perfil: string = localStorage.getItem("imagen_porcentaje");
  porcentaje_perfil: string = (localStorage.getItem("imagen_porcentaje") != undefined || localStorage.getItem("imagen_porcentaje") != null)
                             ? (localStorage.getItem("imagen_porcentaje")) : (this.porcentaje_perfil_temp);
  showedAlert: boolean;
  confirmAlert;
  user = JSON.parse(localStorage.getItem("user"));
  
  //19/07/2019
  constructor
  (
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public sqlite: SQLite,
    private menuCtrl: MenuController,
    public loginProviderService: LoginProviderService,
    public patientProviderService: PatientProviderService,
    public ServicesProvider: ServicesProvider,
    public popoverCtrl: PopoverController,
    public pushnotification: PushnotificationProvider,
    private oneSignal: OneSignal,
    public app: App,
    public configPluginsProvider: ConfigPluginsProvider,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public events: Events,
    public updateProfileService: UpdateProfileProviderService,
    private translateService: TranslateService
  ) 
  { 
    this.initializeApp();                
    if (this.user) 
    {            
      this.getLevelProfile(this.user.id_pk);      
      this.rootPage = HomePage;
      //this.rootPage = AlmacenamientoPage;
      
      this.updateProfileService.avatar_profile = this.user.get_user[0].foto;
    }
    else
    {      
      this.rootPage = LoginPage;
      //*******
    }
    events.subscribe('user:imagen_porcentaje', (imagen_porcentaje) => {
      this.porcentaje_perfil = imagen_porcentaje;
      console.log("si se ejecuto events.subscribe('user:imagen_porcentaje')");
    });
    events.subscribe('user:logout', () => {
      this.logOut();
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {

      //LENGUAJE DE LA APLICACIÓN
      this.translateService.setDefaultLang('en');
      this.translateService.use('es');
      //
      
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.showedAlert = false;            

      if (this.platform.is("cordova")) 
      {
        this.configPluginsProvider.checkPermissions();
        this.createDatabase();
        //this.pushnotification.init_notifications();
        // this.notificationReceived();
        this.oneSignal.startInit(
          "046440a6-eb44-43fd-ab13-3903ddf34756",
          "405604547056"
        );

        this.pushnotification.getDeviceId();        

        this.oneSignal.inFocusDisplaying(
          this.oneSignal.OSInFocusDisplayOption.Notification
        );

        this.oneSignal.handleNotificationOpened().subscribe(jsonData => {
          localStorage.setItem("coords", JSON.stringify(jsonData));
          this.nav.setRoot(EmergencyNotificationPage);          
        });

        this.oneSignal.endInit();

        // Confirm exit
        this.platform.registerBackButtonAction(() => {
          console.log("--- this.nav.length() :",this.nav.length());
          if (this.nav.length() == 1) {
              if (!this.showedAlert) {
                  this.confirmExitApp();
              } else {
                  this.showedAlert = false;
                  this.confirmAlert.dismiss();
              }
          }
          else{
            this.nav.pop();
          }
        });

      }
    });
  }

  confirmExitApp() {
    this.showedAlert = true;
    this.confirmAlert = this.alertCtrl.create({
        title: "Salir",
        message: "¿Está seguro que desea salir de la aplicación?",
        buttons: [
            {
                text: 'Cancelar',
                handler: () => {
                    this.showedAlert = false;
                    return;
                }
            },
            {
                text: 'Aceptar',
                handler: () => {
                    this.platform.exitApp();
                }
            }
        ]
    });
    this.confirmAlert.present();
  }

  openPage(pagina: any) {
    if (!this.configPluginsProvider.currentPage) {
      this.configPluginsProvider.currentPage = pagina;
      this.nav.setRoot(pagina);
    } else {
      if (this.configPluginsProvider.currentPage != pagina) {
        this.nav.setRoot(pagina);
        this.configPluginsProvider.currentPage = pagina;
      }
    }
    
    this.menuCtrl.close();
  }

  updatePhoto_profile(avatar: string)
  {   
    //this.nav.setRoot(IndexUpdateProfilePage, {tab: 4});                           
    // this.openPage(this.indexUpdateProfile);
    // setTimeout(() => {      
    //   this.nav.setRoot(IndexUpdateProfilePage, {tab: 4});
    // }, 1500);     
    // setTimeout(() => {      
    //   console.log("generando tiempo entre ejecuciones...");
    // }, 500); 
    
    //this.events.publish('actualizarPerfil:save');   
    //this.nav.setRoot(IndexUpdateProfilePage, {tab: 0});
    //this.nav.setRoot(IndexUpdateProfilePage, {tab: 1});
    //this.nav.setRoot(IndexUpdateProfilePage, {tab: 2});
    //this.nav.setRoot(IndexUpdateProfilePage, {tab: 3});       
    

    let profileModal = this.modalCtrl.create(UpdatePhotoProfilePage, {photo: avatar});
    profileModal.onDidDismiss(data => 
    {
      if(data != undefined)
      {            
        if (data.avatar != undefined) 
        {
          // this.updateProfileService.avatar_profile = data.avatar;
          // this.events.publish('actualizarPerfil:save');
          // console.log("se publico actualizarPerfil:save ");
          console.log("si onDidDismiss en app.component");
        }
      }
    });
    profileModal.present();
    
    
    // let profileModal = this.modalCtrl.create(UpdatePhotoProfilePage, { photo: avatar });
    // profileModal.onDidDismiss(data => {
    //   console.log("-----data: ", data);
    //   if(data != undefined)
    //   {
    //     if (data.avatar != undefined) 
    //     {          
    //       this.patientProviderService.avatar = data.avatar;
    //       this.updateProfileService.avatar_profile = data.avatar;
    //       this.nav.insert(1,IndexUpdateProfilePage, {tab:4});
    //       this.nav.popToRoot();
    //       setTimeout(() => {
    //         this.events.publish('actualizarPerfil:save');
    //       }, 500);          
    //     }
    //   }
    //   // if (data != undefined && data != null) {
    //   //   if (data.avatar != undefined) {
    //   //     console.log("Avatar nuevo", data.avatar);
    //   //     this.patientProviderService.avatar = data.avatar;
    //   //     //this.nav.setRoot(IndexUpdateProfilePage, {tab: 4});
    //   //     this.nav.insert(1,IndexUpdateProfilePage, {tab:4});
    //   //     this.nav.popToRoot();
    //   //     this.events.publish('actualizarPerfil:save');
    //   //   }
    //   // }
    //   // else
    //   // {
    //   //   console.log("data undefined!");
    //   // }
    // });
    // profileModal.present();
  }

  private createDatabase() {
    this.sqlite
      .create({
        name: "midis.db",
        location: "default" // the location field is required
      })
      .then(db => {
        console.log(db);
        this.patientProviderService.setDatabase(db);
        let createTable = this.patientProviderService.createTableUser();
        this.verifyExistColum("users", "username");
        return createTable;
      })
      .catch(error => {
        console.error(error);
      });
  }

  private verifyExistColum(table: string, fieldName: string) {
    this.patientProviderService
      .verifyIfExistColumn(table, fieldName)
      .then(response => {        
        if (!response) {
          this.addColum(table, fieldName);
        }
      })
      .catch(error => {
        console.log("Error", JSON.stringify(error));
      });
  }

  private addColum(table: string, fieldName: string) {
    this.patientProviderService
      .addColumTable(table, fieldName)
      .then(success => {
        console.log("Se agrego correctamente" + JSON.stringify(success));
      })
      .catch(error => {
        console.log("Error al agregar" + JSON.stringify(error));
      });
  }

  getLevelProfile(id_paciente:any) {
    let data = {"id_paciente": id_paciente};
    this.ServicesProvider
      .apiPost(data, SERVICES.CONSULTAR_PACIENTE_API_SERV)
      .then(state => {
        if (state["status"] == 404 || state["status"] == 500) {
        } else if (state["status"] == 401) {
          this.ServicesProvider.toast(
            MESSAGES.services.unauthorized
          );
        } else {
          let respuesta = JSON.parse(state["_body"]);
          localStorage.setItem("user", JSON.stringify(respuesta));

          if(respuesta.imagen_porcentaje)
          {
            this.porcentaje_perfil = respuesta.imagen_porcentaje;
          }          
          console.log("this.porcentaje_perfil :",this.porcentaje_perfil);
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create(
      PopoverMenuPage,
      {},
      { cssClass: "popover-menu" }
    );
    popover.present({
      ev: myEvent
    });
  }

  /* Version previa a integración adaris 7 diciembre 2018
  logOut() {
    /*localStorage.removeItem("user");
    localStorage.removeItem("dataHealth");
    localStorage.clear();
    Object.keys(localStorage).forEach((key) => {
      if (!key.includes('wizzard')) delete localStorage[key];
    });
    
    this.menuCtrl.enable(false);
    this.app.getRootNav().setRoot(LoginPage);
  }*/

  logOut() {
    //this.app.getRootNav().setRoot(HomePage);    
    this.openPage(HomePage);
    setTimeout(() => 
    {
      console.log("si setTimeout en función logOut");
      localStorage.removeItem("user");
      localStorage.removeItem("dataHealth");
      localStorage.clear();
      this.menuCtrl.enable(false);
      this.app.getRootNav().setRoot(LoginPage);
      
    }, 300); 
    
  }
}
