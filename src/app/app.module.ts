import { BrowserModule } from "@angular/platform-browser";
import { ErrorHandler, NgModule } from "@angular/core";
import { IonicApp, IonicErrorHandler, IonicModule } from "ionic-angular";
import { SplashScreen } from "@ionic-native/splash-screen";
import { StatusBar } from "@ionic-native/status-bar";

/* Plugins */
import { BarcodeScanner } from "@ionic-native/barcode-scanner";
import { SQLite } from "@ionic-native/sqlite";
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';
import { Geolocation } from "@ionic-native/geolocation";
import { OneSignal } from "@ionic-native/onesignal";
import { SocialSharing } from '@ionic-native/social-sharing';
import { Camera } from '@ionic-native/camera';
import { Crop } from '@ionic-native/crop';
import { FileChooser } from "@ionic-native/file-chooser";
import { ImagePicker} from '@ionic-native/image-picker';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { DocumentViewer } from '@ionic-native/document-viewer';
import { CallNumber } from '@ionic-native/call-number';
import { FileTransfer } from '@ionic-native/file-transfer';
import { AgmCoreModule } from '@agm/core';
import { Diagnostic } from '@ionic-native/diagnostic';
import { IonicImageViewerModule } from 'ionic-img-viewer';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { HttpModule } from "@angular/http";
import { IonicStorageModule } from '@ionic/storage';
import { DatePipe } from '@angular/common'
import { Base64 } from '@ionic-native/base64';

/* Components */
import { MyApp } from "./app.component";

import { ProgressBarComponent } from "../components/progress-bar/progress-bar";

import { SelectSearchableModule } from 'ionic-select-searchable';

import { AngularCropperjsModule } from 'angular-cropperjs';

import {
  HomePage,
  ContactDetailsPage,
  PersonalInformationPage,
  ResponsibleDataPage,
  TermsConditionsPage,
  IndexCreateAccountPage,
  ValidateTokenPage,
  LoginPage,
  ModalTermsConditionsPage,
  WelcomePage,
  DatosSaludPage,
  ModalSangre,
  BasicPatientInformationPage,
  CropImagePage,
  GeolocationPage,
  EmergenciasRecibidasNotificacionPage,
  ContactInformationScanPage,
  DataHealthScanPage,
  DocumentosPage,
  ReportsPage,
  QrMidisPage,
  IndexUpdateProfilePage,
  UpdatePersonalInformationPage,
  PopoverMenuPage,
  MisDocumentosPage,
  FichaMedicaPage,
  MidisSaludPage,
  AppSaludPage,
  SettingsPage,
  NotificationsPage,
  UpdateHealthcareDataPage,
  UpdateOtherDataPage,
  PasswordRecoveryPage,
  PasswordRecoveryFormPage,
  ClassificationImcPage,
  ChangePasswordPage,
  ViewDocumentsPage,
  UpdateMedicalContactDataPage,
  UpdateFamilyContactDataPage,
  ModalAddMedicalPage,
  ModalAddFamilyPage,
  ModalEditMedicalPage,
  PopoverShareMedicalTabPage,
  PopoverMidisPage,
  ListFileHealthPage,
  HealthFilesDetailPage,
  UploadHealthFilesPage,
  SelectModulePage,
  ModalEditFamilyPage,
  AuthorizeAccessPage,
  AuthorizeAccessSuccessfulPage,
  UpdatePhotoProfilePage,
  AlmacenamientoPage,
  PopoverHelpPage,
  PageHelpPage
} from "../pages/index.paginas";

import {
  ListAntecedentPage,
  ShowAntecedentPage,
  IndexAntecedentPage,
  CreateAntecedentPage,
  SubcategoryAntecedentPage
} from "../pages/user/antecedent/index.paginas";

import { EmergenciasPage, EmergencyNotificationPage, EmergenciasRecibidasPage } from "../pages/emergency/index.paginas";

/* Servicio */
import { CreateAccountServiceProvider } from "../providers/create-account/create-account";
import { LoginProviderService } from '../providers/login/login';
import { AntecedentProviderService } from '../providers/antecedent/antecedent';
import { UpdateProfileProviderService } from '../providers/update-profile/update-profile';
import { PushnotificationProvider } from '../providers/pushnotification/pushnotification';
import { PatientProviderService } from '../providers/patient/patient';

//servicios
import { DatosSaludPacienteProviderService, ReportsProviderService, PackageProviderService, PackageRequestProviderService } from '../providers/index.service';

import { DocumentsProviderService } from '../providers/documents/documents';
import { ConfigPluginsProvider } from '../providers/config-plugins/config-plugins';
import { ServicesProvider } from '../providers/services/services';
import { MidisDetailPage } from "../pages/user/midis-salud/midis-detail/midis-detail";
import { MidisFilterPage } from "../pages/user/midis-salud/midis-filter/midis-filter";
import { PrescDetailPage } from "../pages/user/presc-detail/presc-detail";
import { UploadResultsPage } from "../pages/user/presc-detail/upload-results/upload-results";
import { CategoryAppSaludPage } from "../pages/user/app-salud/category-app-salud/category-app-salud";
import { AppFilterPage } from "../pages/user/app-salud/app-filter/app-filter";
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from "@angular/common/http";

//FUNCION PARA OBTENER EL ARCHIVO DE TRADUCCIÓN
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ContactDetailsPage,
    PersonalInformationPage,
    ResponsibleDataPage,
    TermsConditionsPage,
    IndexCreateAccountPage,
    ValidateTokenPage,
    LoginPage,
    ModalTermsConditionsPage,
    WelcomePage,
    ListAntecedentPage,
    ShowAntecedentPage,
    IndexAntecedentPage,
    CreateAntecedentPage,
    SubcategoryAntecedentPage,
    DatosSaludPage,
    ModalSangre,
    BasicPatientInformationPage,
    CropImagePage,
    GeolocationPage,
    ContactInformationScanPage,
    DataHealthScanPage,
    DocumentosPage,
    ReportsPage,
    EmergenciasPage,
    EmergenciasRecibidasPage,
    EmergenciasRecibidasNotificacionPage,
    QrMidisPage,
    IndexUpdateProfilePage,
    UpdatePersonalInformationPage,
    PopoverMenuPage,
    MisDocumentosPage,
    FichaMedicaPage,
    MidisSaludPage,
    MidisDetailPage,
    MidisFilterPage,
    CategoryAppSaludPage,
    AppFilterPage,
    PrescDetailPage,
    UploadResultsPage,
    AppSaludPage,
    SettingsPage,
    NotificationsPage,
    UpdateHealthcareDataPage,
    UpdateOtherDataPage,
    PasswordRecoveryPage,
    PasswordRecoveryFormPage,
    ClassificationImcPage,
    ChangePasswordPage,
    ViewDocumentsPage,
    UpdateMedicalContactDataPage,
    UpdateFamilyContactDataPage,
    ModalAddMedicalPage,
    ModalAddFamilyPage,
    ModalEditMedicalPage,
    PopoverShareMedicalTabPage,
    PopoverMidisPage,
    ModalAddFamilyPage,
    ListFileHealthPage,
    HealthFilesDetailPage,
    EmergencyNotificationPage,
    UploadHealthFilesPage,
    SelectModulePage,
    ModalEditFamilyPage,
    AuthorizeAccessPage,
    AuthorizeAccessSuccessfulPage,
    UpdatePhotoProfilePage,
    AlmacenamientoPage,
    PopoverHelpPage,
    PageHelpPage,
    ProgressBarComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    PdfViewerModule,
    IonicImageViewerModule,
    AngularCropperjsModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp,{
      backButtonText: '',
      pageTransition: 'ios-transition',
      monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre' ],
      monthShortNames: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sept', 'Oct', 'Nov', 'Dec'],
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCiGjqfJsBSFljj6HA2IQetROs3HDYAO0U'
    }),
    IonicStorageModule.forRoot({
      name: '__midisApp',
      driverOrder: ['indexeddb', 'sqlite', 'websql']
    }),
    SelectSearchableModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ContactDetailsPage,
    PersonalInformationPage,
    ResponsibleDataPage,
    TermsConditionsPage,
    IndexCreateAccountPage,
    ValidateTokenPage,
    LoginPage,
    ModalTermsConditionsPage,
    WelcomePage,
    ListAntecedentPage,
    ShowAntecedentPage,
    IndexAntecedentPage,
    CreateAntecedentPage,
    SubcategoryAntecedentPage,
    DatosSaludPage,
    ModalSangre,
    GeolocationPage,
    BasicPatientInformationPage,
    CropImagePage,
    ContactInformationScanPage,
    DataHealthScanPage,
    DocumentosPage,
    ReportsPage,
    EmergenciasPage,
    EmergenciasRecibidasPage,
    EmergenciasRecibidasNotificacionPage,
    QrMidisPage,
    IndexUpdateProfilePage,
    UpdatePersonalInformationPage,
    PopoverMenuPage,
    MisDocumentosPage,
    FichaMedicaPage,
    MidisSaludPage,
    MidisDetailPage,
    MidisFilterPage,
    CategoryAppSaludPage,
    AppFilterPage,
    AppSaludPage,
    SettingsPage,
    NotificationsPage,
    UpdateHealthcareDataPage,
    UpdateOtherDataPage,
    PasswordRecoveryPage,
    PasswordRecoveryFormPage,
    ClassificationImcPage,
    ChangePasswordPage,
    ViewDocumentsPage,
    UpdateMedicalContactDataPage,
    UpdateFamilyContactDataPage,
    ModalAddMedicalPage,
    ModalAddFamilyPage,
    ModalEditMedicalPage,
    PopoverShareMedicalTabPage,
    PopoverMidisPage,
    ModalAddFamilyPage,
    ListFileHealthPage,
    HealthFilesDetailPage,
    EmergencyNotificationPage,
    UploadHealthFilesPage,
    SelectModulePage,
    ModalEditFamilyPage,
    AuthorizeAccessPage,
    AuthorizeAccessSuccessfulPage,
    UpdatePhotoProfilePage,
    AlmacenamientoPage,
    PopoverHelpPage,
    PageHelpPage,
    PrescDetailPage,
    UploadResultsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    CreateAccountServiceProvider,
    LoginProviderService,
    BarcodeScanner,
    SQLite,
    File,
    FileOpener,
    DatosSaludPacienteProviderService,
    Geolocation,
    OneSignal,
    Camera,
    Crop,
    SocialSharing,
    FileChooser,
    InAppBrowser,
    DocumentViewer,
    ImagePicker,
    CallNumber,
    FileTransfer,
    Diagnostic,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    AntecedentProviderService,
    UpdateProfileProviderService,
    PushnotificationProvider,
    PatientProviderService,
    ReportsProviderService,
    DocumentsProviderService,
    PackageProviderService,
    PackageRequestProviderService,
    ConfigPluginsProvider,
    ServicesProvider,
    DatePipe,
    Base64
  ]
})
export class AppModule {}
